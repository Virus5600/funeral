@extends('template.register')

@section('title', 'Recover Password')

@section('body')
<div class="container mb-5 d-flex justify-content-center">
	<h1>Recover <span class="highlighted">Password</span></h1>
</div>

<!-- Form -->
<form action="{{ route('recoverPass.submit') }}" method="POST" class="container-fluid" enctype="multipart/form-data">
	{{ csrf_field() }}
	<div class="row">
		<div class="col-12 mx-auto">
			<div class="row">
				<div class="col-lg-9 col-12 p-md-5 p-4 rounded mx-auto w-50" id="register_form">
					<div class="form-group">
						<p>That's okay, it happens!</p>
						<label for="email" class="form-label">Enter your email so we can send you a recovery link.</label>
						<input type="email" class="form-control" id="email" name="email" value="{{ old('email') }}">
						<span class="badge badge-danger w-100 validation-message">{{$errors->first('email')}}</span>
					</div>
				</div>
			</div>

			<div class="row mt-2">
				<div class="col-lg-9 col-12 mx-auto">
					<div class="form-group text-center">
						<input type="submit" class="btn-lg btn-light mb-2" data-action="submit" value="Send Recovery Link"/>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>
@endsection