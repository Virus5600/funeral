<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subcategory extends Model
{
	protected $fillable = [
		'subcategory_name',
		'category_id'
	];

	public $timestamps = false;

	protected function category() {
		return $this->belongsTo('App\Category');
	}

	protected function product() {
		return $this->hasMany('App\Product');
	}
}