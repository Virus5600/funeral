<?php

use Illuminate\Database\Seeder;

use App\Service;

class ServicesTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Service::create([
			'service_name' => 'Funeral',
			'description' => 'Personalizing an end-of-life celebration or funeral gives you a chance to pour your love (and your grief) into something important for you and for other people who loved the person who\'s died. It\'s also a chance to create a service, a gathering (or both) to reflect a beautiful life.'
		]);

		Service::create([
			'service_name' => 'Burial Services',
			'description' => 'Usually a short ceremony held after the main funeral service as the coffin is lowered into the ground. Mourners are often invited to attend the burial, which may include short readings and prayers, depending on religious beliefs.'
		]);

		Service::create([
			'service_name' => 'Memorial Services',
			'description' => 'A memorial service online. This could be online. With this web app, it could be set up with live streaming. Family could mourn together with their loved ones’ portrait.'
		]);
	}
}