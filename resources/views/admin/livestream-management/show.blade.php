@extends('template.admin')

@section('title', 'Livestream Management')
@section('sidebar', 'admin')

@section('css')
<link rel="stylesheet" href="https://unpkg.com/@mux/videojs-kit@latest/dist/index.css">
<link href="https://unpkg.com/@videojs/themes@1/dist/forest/index.css" rel="stylesheet">
@endsection

@section('body')
<h1 class="font-weight-bold"><a class="text-dark" href="{{route('admin.livestream-management.index')}}"><i class="fas fa-chevron-left mr-2"></i>Livestream #{{ $livestream->id }} for {{ $livestream->ls_user->getName() }} ({{ $livestream->ls_user->email }})</a></h1>

<hr class="hr-thick" style="border-color: #707070;">

<div class="container-fluid">
	<div class="row">
		<div class="col">
			<video id="stream" class="video-js vjs-theme-forest vjs-fluid text-center mx-auto" controls data-setup="{}">
				<source src="{{$livestream->stream_link}}" type="application/x-mpegURL"/>

				<p class="vjs-no-js">
					To view this video please enable JavaScript, and consider upgrading to a
					web browser that
					<a href="https://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
				</p>
			</video>
		</div>

		<div class="col d-flex flex-d-col">
			<h3>{{$livestream->title or '[Not Specified]'}}</h3>
			<p>
				<b>Description:</b><br>
				{{$livestream->description or '[Not Specified]'}}
			</p>

			<p>
				<b>Scheduled Date:</b><br>
				{{$livestream->schedule_date == null ? '[Not Specified]' : \Carbon\Carbon::parse($livestream->schedule_date)->format('M d, Y')}}
			</p>

			<p>
				<b>Scheduled Time:</b><br>
				{{$livestream->schedule_time == null ? '[Not Specified]' : \Carbon\Carbon::parse($livestream->schedule_time)->format('H:i A')}}
			</p>

			<p>
				<b>Status:</b><br>
				@if ($livestream->status == 'cancelled')
				<i class="fas fa-circle mr-2 text-danger"></i>
				@elseif ($livestream->status == 'waiting for proof' || $livestream->status == 'awaiting schedule')
				<i class="fas fa-circle mr-2 text-info"></i>
				@elseif ($livestream->status == 'pending' || $livestream->status == 'streaming')
				<i class="fas fa-circle mr-2 text-warning"></i>
				@elseif ($livestream->status == 'finished')
				<i class="fas fa-circle mr-2 text-success"></i>
				@endif
				{{ $livestream->displayStatus() }}
			</p>

			<div class="d-flex flex-d-row">
				@if ($livestream->status == 'pending')
				<a class="btn btn-success mx-1" href="{{route('livestream.approve', [$livestream->id])}}">Approve</a>
				<a class="btn btn-danger mx-1" href="{{route('livestream.cancel', [$livestream->id])}}">Cancel</a>
				@elseif (((int) $livestream->displayStatus(true)) == 3)
				<a class="btn btn-info mx-1" href="{{route('livestream.streaming', [$livestream->id])}}">Mark as Streaming</a>
				@elseif (((int) $livestream->displayStatus(true)) == 4)
				<a class="btn btn-success mx-1" href="{{route('livestream.done', [$livestream->id])}}">Mark as Done</a>
				@endif
			</div>
		</div>
	</div>
</div>

<div class="row mb-5 pb-5">
	<div class="col-12 col-md-6">
		<hr class="hr-thick" style="border-color: #707070;">
		<h3>Stream Details</h3>

		<div class="d-flex flex-d-col mt-3">
			<p>
				<b>Stream ID:</b><br>
				{{$livestream->stream_id}}
			</p>

			<p>
				<b>Stream Key:</b><br>
				{{$livestream->stream_key}}
			</p>

			<p>
				<b>Stream Link:</b><br>
				<a class="text-decoration-none" href="{{route('livestream.show', [$livestream->stream_id])}}" target="_balnk" title="{{$livestream->title or 'Livestream'}}">{{route('livestream.show', [$livestream->stream_id])}}</a>
			</p>
		</div>
	</div>

	<div class="col-12 col-md-6">
		<hr class="hr-thick" style="border-color: #707070;">
		<h3>Add-ons</h3>

		<div class="d-flex flex-d-col mt-3">
			<p>
				<b>Standing Photo:</b><br>
				<span id="standing_photo">
					@if ($livestream->getOptionStatus('standing_photo') == 1)
					<i class="fas fa-circle mr-2 text-success"></i>Yes
					@else
					<i class="fas fa-circle mr-2 text-danger"></i>No
					@endif
				</span>
			</p>

			<p>
				<b>Video Slideshow:</b><br>
				<span id="video_slideshow">
					@if ($livestream->getOptionStatus('video_slideshow') == 1)
					<i class="fas fa-circle mr-2 text-success"></i>Yes
					@else
					<i class="fas fa-circle mr-2 text-danger"></i>No
					@endif
				</span>
			</p>

			<p>
				<b>Flower Stands:</b><br>
				<span id="flower_stands">
					@if ($livestream->getOptionStatus('flower_stands') == 1)
					<i class="fas fa-circle mr-2 text-success"></i>Yes
					@else
					<i class="fas fa-circle mr-2 text-danger"></i>No
					@endif
				</span>
			</p>
		</div>
	</div>
</div>
@endsection

@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/video.js/7.0.0/video.min.js" integrity="sha512-LiILFcpZ9QKSH41UkK59Zag/7enHzqjr5lO2M0wGqGn8W19A/x2rV3iufAHkEtrln+Bt+Zv1U6NlLIp+lEqeWQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/videojs-contrib-hls/5.15.0/videojs-contrib-hls.min.js" integrity="sha512-R1+Pgd+uyqnjx07LGUmp85iW8MSL1iLR2ICPysFAt8Y4gub8C42B+aNG2ddOfCWcDDn1JPWZO4eby4+291xP9g==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

<script>
	var player = videojs('stream');
	var intervalUpdate = null;
	// player.play();

	function updateAddOns() {
		$.get('{{route('livestream.options.get', [$livestream->id])}}', (data) => {
			$.each(data, (k, v) => {
				if (v == 1)
					$('#' + k).html('<i class="fas fa-circle mr-2 text-success"></i>Yes');
				else
					$('#' + k).html('<i class="fas fa-circle mr-2 text-danger"></i>No');
			});
		});
	}

	$(document).ready(() => {
		intervalUpdate = setInterval(updateAddOns, 1250);
	});
</script>
@endsection