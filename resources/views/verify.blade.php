@extends('template.register')

@section('title', 'Verify Email')

@section('body')
<div class="container mb-5 d-flex justify-content-center">
	<h1>Verify <span class="highlighted">Email</span></h1>
</div>

<!-- Form -->
<form action="{{ route('email-verification.verify') }}" method="GET" class="container-fluid" enctype="multipart/form-data">
	{{ csrf_field() }}
	<div class="row">
		<div class="col-12 mx-auto">
			<div class="row">
				<div class="col-lg-9 col-12 p-md-5 p-4 rounded mx-auto w-50" id="register_form">
					<div class="form-group">
						<p class="text-center text-lg-left">We need to verify your email beforehand!</p>
						<label for="code" class="form-label w-100 text-center text-lg-left">Enter the verification code sent to your email.</label>
						<input type="text" class="form-control" id="code" value="{{$code}}" name="code">
						<span class="badge badge-danger w-100 validation-message">{{ $errors->first('code') }}</span>
						<span class="badge badge-info w-100 validation-message">{{ Session::get('flash_message') }}</span>
					</div>

					<div class="form-group text-center mb-0">
						<button type="submit" data-action="submit" class="btn-lg btn-light mb-2">Submit Code</button>
					</div>

					<p class="text-center p-0 m-0">
						<small>
							<small>
								Email not received or code expired? <a class="text-info" href="javascript:void(0);" id="resend_code">Resend verification code</a>
							</small>
						</small>
					</p>
				</div>
			</div>
		</div>
	</div>
</form>
@endsection

@section('script')
<script type="text/javascript">
	$(document).ready(() => {
		$('#resend_code').on('click', () => {
			$.post('{{ route("email-verification.resend") }}', {
				_token: '{{ csrf_token() }}'
			}).done((response) => {
				if (response.success) {
					Swal.fire({
						icon: `info`,
						title: `Verification code sent!`,
						html: `<p>A new verification code was sent to your email, it will only be valid for five (5) minutes.</p>`,
						position: `center`,
						showConfirmButton: true,
						background: `#17a2b8`,
						customClass: {
							title: `text-white`,
							content: `text-white`,
							popup: `px-3`
						},
					});
				}
				else {
					console.log(response);

					Swal.fire({
						title: `Something went wrong! Please try again later.`,
						position: `top`,
						showConfirmButton: false,
						toast: true,
						timer: 10000,
						background: `#dc3545`,
						customClass: {
							title: `text-white`,
							content: `text-white`,
							popup: `px-3`
						},
					});
				}
			});
		});
	});
</script>
@endsection