@extends('template.client.account')

@section('subtitle', 'My Account')

@section('page-css')
<link rel="stylesheet" href="/css/order_tracking.index.css">
@endsection

@section('sub-body')
<!-- Order Info -->
@forelse (Auth::user()->livestreams->sortByDesc('id') as $l)
@php
if ($l->status == 'cart')
	continue;
@endphp

<div class="itemrow row mt-5 p-3 bg-light">
	<a href="{{ route('account.livestreams.show', [$l->id]) }}" class="container text-decoration-none">
		<div class="row mt-2">
			<div class="col-12">
				<h3 class="text-dark text-truncate">{{$l->title}}</h3>
			</div>

			<div class="col-12 mt-3 mt-md-0">
				<h5 class="text-dark">ORDER ID: {{ $l->stream_id }}</h5>
			</div>
		</div>

		<hr class="dotted">
		<div class="row">
			<div class="col-6">
				<small>{{$l->duration > 1 ? $l->duration . ' hours' : $l->duration . ' hour'}}</small>
			</div>
			<div class="col-6 right-align-items" >
				<h5>Total: <b>&#8369;{{number_format($l->price, 2)}}</b></h5> 
			</div>
		</div>

		<hr class="solid">
		<div class="row">
			<div class="col-10 col-9">
				<p class="text-left"><span class="badge {{$l->displayStatus(true) == 0 ? 'badge-danger' : (($l->displayStatus(true) == 2 || $l->displayStatus(true) == 4) ? 'badge-warning' : ($l->displayStatus(true) == 5 ? 'badge-success' : 'badge-info'))}} text-white">{{$l->displayStatus()}}</span></p>
				<p><i class="fas fa-info-circle text-dark mr-2"></i>{{$l->getLatestlog() == null ? 'Waiting for proof of payment.' : $l->getLatestLog()->getDescription()}}</p>
			</div>
			<div class="col-2 right-align-items">
			   <i class="bi bi-chevron-right text-dark"></i>
			</div>
		</div>

		<hr class="solid">
		@if ($l->status == strtolower(App\Livestream::getStatusList()[1]))
		<div class="row" >
			<div class="col-md-3 col-12 ml-auto text-right">
				<a href="{{ route('account.livestreams.upload_proof', [$l->id]) }}" class="btn btn-dark">Upload Proof</a>
				<a href="{{ route('account.livestreams.cancel', [$l->id]) }}" class="btn btn-dark">Cancel</a>
			</div>
		</div>
		@elseif ($l->status == strtolower(App\Livestream::getStatusList()[2]))
		<div class="row">
			<div class="col-md-3 col-12 ml-auto text-right">
				<button class="btn btn-dark" disabled>Processing your proof of payment</button>
			</div>
		</div> 
		@elseif ($l->status == strtolower(App\Livestream::getStatusList()[3]))
		<div class="row">
			<div class="col-md-9 col-sm-12">
				<p>Payment accepted. Awaiting scheduled date of livestream</p>
			</div>
			<div class="col-md-3 col-12 d-grid text-right">
				<a href="{{ route('account.livestreams.show', [$l->id]) }}" class="btn btn-secondary">See Details</a>
			</div>
		</div>
		@endif
	</a>
</div>
@empty
<div class="itemrow row mt-5 p-3">
	<div class="container-fluid text-center border rounded p-3">
		No orders yet, nothing to show...
	</div>
</div>
@endforelse
<!-- End of Order Info -->
@endsection

@section('subscript')
<script type="text/javascript">
	var nav = document.querySelector('nav');

	window.addEventListener('scroll', function () {
		if (window.pageYOffset > 100) {
			nav.classList.add('bg-dark', 'shadow');
		} else {
			nav.classList.remove('bg-dark', 'shadow');
		}
	});
</script>

<script>
	document.getElementById("togglemenu").addEventListener("click", function() {
		document.getElementById("navbarmain").classList.add('bg-dark', 'shadow');
	});
</script>
@endsection