@extends('template.client.account')

@section('subtitle', 'My Account')

@section('page-css')
<link rel="stylesheet" href="/css/account.edit.css" >
@endsection

@section('sub-body')
<form action="{{ route('account.update') }}" method="POST" class="row mt-5 px-md-5 mx-1">
	{{ csrf_field() }}
	<div class="container bg-light">
		<div class="row bg-gray py-3">
			<h5>Personal Details</h5>
		</div>

		<div class="row mt-3 mb-5 forminfo">
			<p class="text-center my-2"><span class="badge badge-success" style="font-size: 1rem;">{{ Session::get('flash_success') }}</span></p>

			<div class="col col-lg-8 mx-5">
				<div class="row mb-2">
					<div class="form-group mt-1 mb-1">
						<label class="form-label">Email:</label>
						<input class="form-control" type="text" value="{{Auth::user()->email}}" aria-label="Disabled input example" disabled readonly>
						<span class="badge badge-danger w-100 validation-message">{{ $errors->first('email') }}</span>
					</div>
				</div>

				<div class="row">
					<div class="form-group mt-1 col-lg-6">
						<label class="form-label">First Name</label>
						<input type="text" class="form-control" name="first_name" value="{{Auth::user()->first_name}}">
						<span class="badge badge-danger w-100 validation-message">{{ $errors->first('first_name') }}</span>
					</div>

					<div class="form-group mt-1 col-lg-6">
						<label class="form-label">Last Name</label>
						<input type="text" class="form-control" name="last_name" value="{{Auth::user()->last_name}}">
						<span class="badge badge-danger w-100 validation-message">{{ $errors->first('last_name') }}</span>
					</div>
				</div>

				<div class="row">
					<div class="col-12 col-lg-6">
						<label class="form-label mt-3">Contact Number</label>
						<input type="text" class="form-control" name="contact_number" value="{{Auth::user()->contact_number}}" data-mask>
						<span class="badge badge-danger w-100 validation-message">{{ $errors->first('contact_number') }}</span>
					</div>

					<div class="col-12 col-lg-6">
						<label class="form-label mt-3">Gender</label>
						<select class="form-select" aria-label="Default select example" name="gender">
							<option value="Male" {{ Auth::user()->gender == "Male" ? 'selected' : '' }}>Male</option>
							<option value="Female" {{ Auth::user()->gender == "Female" ? 'selected' : '' }}>Female</option>
							<option value="Others" {{ Auth::user()->gender == "Others" ? 'selected' : '' }}>Others</option>
						</select>
						<span class="badge badge-danger w-100 validation-message">{{ $errors->first('gender') }}</span>
					</div>
				</div>

				<div class="row">
					<div class="col-lg-6">
						<label for="exampleFormControlInput1" class="form-label mt-3">Birthday</label>
						
						<div class="row">
							<div class="col mx-0 px-1">
								<input type="date" class="form-control" value="{{ Auth::user()->birthday }}" name="birthday">
							</div>
						</div>
						<span class="badge badge-danger w-100 validation-message">{{ $errors->first('birthday') }}</span>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class=" flex-row-reverse mt-3">
		<div class="col-lg-3 d-grid" id="buttonrow" >
			<button class="btn-lg btn-light">Save Changes</button>
		</div>
	</div>
</form>
@endsection

@section('subscript')
<script type="text/javascript">
	$(document).ready(() => {
		// Activate masking
		$("[data-mask]").inputmask('mask', {
			'mask' : '+63 999 999 9999',
			'removeMaskOnSubmit' : true,
			'autoUnmask':true
		});
	});
</script>
@endsection