<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Requests;

use App\Category;
use App\Product;
use App\Subcategory;

use DB;
use Log;
use Exception;

class OwnerProductsubcategoryController extends Controller
{
	protected function index($category_name) {
		$category = Category::where('category_name', '=', preg_replace('/_/', ' ', $category_name))->first();
		$subcategory = subcategory::where('category_id', '=', $category->id);
		$search = null;

		if (request()->has('s')) {
			$search = request()->get('s');
			$subcategory = $subcategory->leftJoin('products', 'products.subcategory_id', '=', 'subcategories.id');

			$subcategory = $subcategory->where('subcategories.subcategory_name', 'LIKE', '%'.$search.'%')
				->orWhere('products.product_name', 'LIKE', '%'.$search.'%')
				->select('subcategories.*')
				->distinct();
		}

		return view('owner.category.subcategory.index', [
			'search' => $search,
			'category' => $category,
			'subcategories' => $subcategory->paginate(15)
		]);
	}

	protected function create($category) {
		return view('owner.category.subcategory.product.create', [
			'isFrom' => 'subcategory',
			'categories' => Category::where('category_name', '=', preg_replace('/_/', ' ', $category))->first(),
			'subcategories' => Category::where('category_name', '=', preg_replace('/_/', ' ', $category))->first()->subcategory
		]);
	}

	protected function delete($id) {
		try {
			DB::beginTransaction();

			$subcategory = Subcategory::find($id);
			$subcategoryName = $subcategory->subcategory_name;

			$subcategory->delete();

			DB::commit();
		} catch (Exception $e) {
			Log::error($e);
			DB::rollback();

			return redirect()
				->back()
				->with('flash_error', 'Something went wrong, please try again later.');
		}

		return redirect()
			->route('owner.category.index')
			->with('flash_success', 'Successfully removed "' . $subcategoryName . '".');
	}
}