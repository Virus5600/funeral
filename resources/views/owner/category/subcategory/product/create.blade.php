@extends('template.admin')

@section('title', 'Products')
@section('sidebar', 'owner')

@section('body')
<!-- <h1 class="font-weight-bold"><a class="text-dark" href="javascript:void(0);" onclick="confirmLeave('{{route('owner.obituary.index')}}');"><i class="fas fa-chevron-left mr-2"></i>Obituary</a></h1> -->
<h1 class="font-weight-bold">@include('template.breadcrumbs', ['startIndex' => 2, 'confirmLeave' => true])</h1>

<hr class="hr-thick" style="border-color: #707070;">

<form action="{{ route('owner.product.store') }}" method="POST" class="form" enctype="multipart/form-data">
	{{ csrf_field() }}

	<div class="row">
		<div class="col-12 col-lg-8">
			<div class="row">
				<div class="form-group col-12">
					<label class="form-label font-weight-bold">Product Name</label>
					<input type="text" class="form-control" name="product_name" value="{{old('product_name')}}">
					<span class="badge badge-danger w-100 validation-message">{{ $errors->first('product_name') }}</span>
				</div>
			</div>

			<div class="row">
				<div class="form-group col-12 col-lg-4">
					<label class="form-label font-weight-bold">Inventory</label>
					<input class="form-control" type="number" name="inventory" min="0" value="{{old('inventory')}}">
				</div>

				<div class="form-group col-12 col-lg-4">
					<label class="form-label font-weight-bold">Selling Price</label>
					<div class="input-group">
						<div class="input-group-prepend">
							<span class="input-group-text">&#8369;</span>
						</div>
						<input type="number" class="form-control" name="selling_price" value="{{old('selling_price')}}" min="0.00" step=".01">
						<span class="badge badge-danger w-100 validation-message">{{ $errors->first('selling_price') }}</span>
					</div>
				</div>

				<div class="form-group col-12 col-lg-4">
					<label class="form-label font-weight-bold">Status</label>
					<select class="custom-select" name="status">
						<option value='active' {{old('status') == 'active' ? 'selected' : ''}}>Active</option>
						<option value="inactive" {{old('status') == 'inactive' ? 'selected' : ''}}>Inactive</option>
					</select>
					<span class="badge badge-danger w-100 validation-message">{{ $errors->first('status') }}</span>
				</div>
			</div>
		</div>

		<div class="form-group col-12 col-lg-4">
			<div class="d-flex flex-d-row">
				<div class="mx-auto text-center" style="overflow: hidden; max-width: 37.5%;">
					<img src="/uploads/products/product.png" class="cursor-pointer img-fluid" id="image" alt="Image">
				</div>

				<div class="mx-auto">
					<h6 class="form-label font-weight-bold" style="word-break: break-word" id="image_label">UPLOAD IMAGE</h6>
					<input type="file" name='image' class="hidden" accept=".jpg,.jpeg,.png" id="imgInput">
					<small class="text-muted pb-0 mb-0"><b>FORMATS ALLOWED:</b> JPEG, JPG, PNG</small><br>
					<small class="text-muted pt-0 mt-0"><b>MAX SIZE:</b> 5MB</small><br>
				</div>
			</div>
			<span class="badge badge-danger w-100 validation-message">{{ $errors->first('image') }}</span>
		</div>
	</div>

	<div class="row">
		<div class="form-group col-12 col-lg-6">
			<label class="form-label font-weight-bold">Category</label>
			@if ($isFrom == 'subcategory' || $isFrom == 'product')
			<input type="text" class="form-control" value="{{$categories->category_name}}" disabled/>
			<input type="hidden" class="form-control" name="category" value="{{$categories->category_name}}" />
			@else
			<input type="text" class="form-control" name="category" value="{{old('category')}}"/>
			@endif
			<span class="badge badge-danger w-100 validation-message">{{ $errors->first('category') }}</span>
		</div>

		<div class="form-group col-12 col-lg-6">
			<label class="form-label font-weight-bold">Subcategory</label>
			@if ($isFrom == 'product')
			<input type="text" class="form-control" value="{{$subcategories->subcategory_name}}" disabled/>
			<input type="hidden" class="form-control" name="subcategory" value="{{$subcategories->subcategory_name}}" />
			@else
			<input type="text" class="form-control" name="subcategory" value="{{old('subcategory')}}"/>
			@endif
			<span class="badge badge-danger w-100 validation-message">{{ $errors->first('subcategory') }}</span>
		</div>
	</div>

	<div class="row">
		<div class="form-group col-12">
			<label class="form-label font-weight-bold">Description</label>
			<textarea class="form-control not-resizable" name="description" rows="5">{{old('description')}}</textarea>
			<span class="badge badge-danger w-100 validation-message">{{ $errors->first('description') }}</span>
		</div>
	</div>

	<div class="row">
		<div class="form-group col-12">
			<label class="form-label font-weight-bold">Size</label>
			<input type="text" class="form-control" name="size" value="{{old('size')}}">
			<span class="badge badge-danger w-100 validation-message">{{ $errors->first('size') }}</span>
		</div>
	</div>

	<div class="row">
		<div class="col-12">
			<button type="submit" class="btn btn-dark" data-action="submit">Save Changes</button>
		</div>
	</div>
</form>
@endsection

@section('script')
<script type="text/javascript">
	function openInput(obj) {
		$("[name=" + obj.attr("id") + "]").trigger("click");
	}

	function swapImgFile(obj) {
		if (obj.files && obj.files[0]) {
			let reader = new FileReader();

			reader.onload = function(e) {
				$("#image").attr("src", e.target.result);
				$("#image_label").html(obj.files[0].name);
			}

			reader.readAsDataURL(obj.files[0])
		}
		else {
			$("#image").attr("src", "/uploads/products/product.png");
			$("#image_label").html("UPLOAD IMAGE");
		}
	}

	$(document).ready(() => {
		// AUTOCOMPLETE
		@if ($isFrom == 'category')
		let availableCategories = [
			@foreach ($categories as $c)
			'{{$c->category_name}}',
			@endforeach
		];

		$('[name=category]').autocomplete({
			source: availableCategories,
			minLength: 0,
			delay: 0
		}).on('click focus', (e) => {
			$(e.currentTarget).autocomplete('search', $(e.currentTarget).val());
		});
		@endif

		@if ($isFrom == 'category' || $isFrom == 'subcategory')
		let availableSubcategories = [
			@foreach ($subcategories as $s)
			'{{$s->subcategory_name}}',
			@endforeach
		];

		$('[name=subcategory]').autocomplete({
			source: availableSubcategories,
			minLength: 0,
			delay: 0
		}).on('click focus', (e) => {
			$(e.currentTarget).autocomplete('search', $(e.currentTarget).val());
		});
		@endif

		// Triggering click on the image box
		$("#image").on("click", (e) => {
			openInput($(e.currentTarget));
		});
		// Applying changes when file input changes
		$("#imgInput").on('change', (e) => {
			swapImgFile(e.currentTarget);
		});

		// Change subcategory if category changed
		$('[name=category]').on('input autocompleteselect', (e, ui) => {
			let obj = $(e.currentTarget);

			$.post('{{ route('utility.get_subcategories') }}', {
				_token: '{{csrf_token()}}',
				category: e.type == 'autocompleteselect' ? ui.item['value'] : obj.val()
			}).done((data) => {
				availableSubcategories = [];
				let i = 0;

				$.each(data, (k, v) => {
					availableSubcategories[i++] = v['subcategory_name'];
				});

				$('[name=subcategory]').autocomplete({
					source: availableSubcategories,
					delay: 0
				});
			});
		});
	});
</script>
@endsection