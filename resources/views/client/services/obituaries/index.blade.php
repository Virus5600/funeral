@extends('template.client')

@section('title', 'Services - Obituary')

@section('page-css')
<link rel="stylesheet" href="/css/services.obituaries.index.css" >
@endsection

@section('body')
{{-- OBITUARIES BANNER --}}
<section class="p-md-5" id="banner">
	<div class="container pt-md-5" >
		<div class="d-sm-flex justify-content-between">
			<div class ="col-xl-11 col-md-8 col-sm-12 p-5" id="bannertext">
				<h1>Obituaries celebrate and honor unique lives.</h1>
			</div>
		</div>
	</div>
</section>

<!-- Obituary -->
<section class="py-5" id="obituary">
	<div class="container">
		<div class="row">
			<p class="lead">Today’s obituaries are more than simple funeral announcements and a list of the names of family members. Modern obituaries include treasured memories, funny stories and important details that showcase the joys and moments of a life lived. A great obituary tells a unique story and reflects the personality of the person it commemorates.</p>
		</div>

		@if (Auth::check())
		<div class="row">
			<div class="col text-center">
				<a href="{{ route('services.obituary.request') }}" class="btn btn-outline-light">Request an Obituary</a>
			</div>
		</div>
		@endif

		<div class="container py-5">
            @forelse($obituaries as $o)
			<a href="{{ route('services.obituary.show', [$o->id]) }}" class="obituarycard card mb-3 text-decoration-none hover-enlarge">
				<div class="row">
					<div class="fillimg col-md-3">
						<img src="/uploads/obituary/{{ $o->image_name or 'default.png' }}" class="card-img" alt="...">
					</div>

					<div class="col-md-7 mt-1">
						<div class="card-body">
							<h1 class="obname card-title">{{$o->name}}</h1>
							<h2 class="card-text"><small class="text-muted">{{\Carbon\Carbon::parse($o->birth_date)->format('F d, Y')}} – {{\Carbon\Carbon::parse($o->death_date)->format('F d, Y')}}</small></h2>
						</div>
					</div>

					<div class="col-md-2">
						<img src="/images/UI/bookmark.svg" class="mark card-img" alt="...">
					</div>
				</div>
			</a>
			@empty
			<h1>No Obituaries</h1>
            @endforelse
		</div>

		<div class="row">
			<div class="col-lg py-5">
				<div class="float-right d-flex flex-d-row bg-dark">
					<a href="{{$obituaries->previousPageUrl()}}" class="text-danger border border-end-0 mr-0 p-2" style="border-top-left-radius: .25rem; border-bottom-left-radius: .25rem; border-right: solid #fff;"><i class="fas fa-chevron-left mx-2 cursor-pointer"></i></a>
					<span class="border border-start-0 border-end-0 mx-0 p-2">Total Results {{($obituaries->currentPage()-1) * $obituaries->perPage() + 1}}-{{(($obituaries->currentPage()-1) * $obituaries->perPage()) + $obituaries->count()}} of {{$obituaries->total()}}</span>
					<a href="{{$obituaries->nextPageUrl()}}" class="text-dark border border-start-0 ml-0 p-2" style="border-top-right-radius: .25rem; border-bottom-right-radius: .25rem;"><i class="fas fa-chevron-right mx-2 cursor-pointer"></i></a>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection