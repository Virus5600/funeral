@extends('template.admin')

@section('title', 'Products')
@section('sidebar', 'owner')

@section('body')
<h1 class="font-weight-bold">@include('template.breadcrumbs', ['startIndex' => 2])</h1>

<hr class="hr-thick" style="border-color: #707070;">

<div class="row mx-0 px-0">
	<form action="{{ route(Request::route()->getName(), [preg_replace('/ /', ' ', strtolower($category_name)), preg_replace('/ /', '_', strtolower($subcategory_name))]) }}" method="GET" class="col-12 col-lg-5 mx-0 px-0">
		<div class="input-group">
			<input type="text" class="form-control" placeholder="Search for..." name="s" value="{{$search}}">
			<div class="input-group-append">
		    	<button type="submit" data-action="none" class="btn btn-dark"><i class="fas fa-search"></i></button>
			</div>
		</div>
	</form>

	<div class="col-12 col-lg-2 mt-3 mt-lg-0">
		<a href="{{ route('owner.product.create', [preg_replace('/ /', '_', strtolower($category_name)), preg_replace('/ /', '_', strtolower($subcategory_name))]) }}" class="btn btn-success w-100"><i class="fas fa-plus-circle mr-2"></i>Add Product</a>
	</div>
</div>

<div class="row mx-0 px-0">
	<div class="col-12 px-0">
		<div class="card border rounded dark-shadow h-100 overflow-x-auto">
			<h3 class="card-header font-weight-bold text-custom w-100">Product Category List</h3>
			<div class="card-body mx-0 pt-0 px-0">
				{{-- TABLE --}}
				<table class="table table-striped">
					<thead>
						<tr class="font-weight-bold">
							<td>Product Name</td>
							<td>Inventory</td>
							<td>Price</td>
							<td>Satus</td>
							<td></td>
						</tr>
					</thead>

					<tbody id='table-content'>
						@forelse ($products as $p)
						<tr>
							<td>{{$p->product_name}}</td>
							<td>{{$p->inventory}}</td>
							<td>&#8369; {{$p->selling_price}}</td>
							<td>{!! ($p->inventory) > 10 ? '<i class="fas fa-circle text-success mr-2"></i>Active' : '<i class="fas fa-circle text-danger mr-2"></i>Inactive' !!}</td>
							<td>
								<button type="button" class="btn btn-primary dropdown-toggle float-right py-0" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action</button>

								<div class="dropdown-menu dropdown-menu-right">
									<a class="dropdown-item" href="{{ route('owner.product.show', [preg_replace('/ /', '_', strtolower($category_name)), preg_replace('/ /', '_', strtolower($subcategory_name)), preg_replace('/ /', '_', strtolower($p->product_name))]) }}"><i class="far fa-eye mr-2"></i>View Details</a>
									<a class="dropdown-item" href="{{ route('owner.product.edit', [preg_replace('/ /', '_', strtolower($category_name)), preg_replace('/ /', '_', strtolower($subcategory_name)), preg_replace('/ /', '_', strtolower($p->product_name))]) }}"><i class="fas fa-pencil-alt mr-2"></i>Edit Details</a>
									<a class="dropdown-item" href="{{ route('owner.product.delete', [$p->id]) }}"><i class="fas fa-trash mr-2"></i>Delete</a>
								</div>
							</td>
						</tr>
						@empty
						<tr>
							<td colspan="4" class="text-center">No Avaiable Data in Table</td>
						</tr>
						@endforelse
					</tbody>
				</table>
				{{-- TABLE END --}}
			</div>
		</div>
	</div>
</div>

{{-- PAGINATION --}}
<div class="row mx-0 px-0">
	<div class="col-12 px-0 text-lg-left text-center">
		<span>Total Results {{($products->currentPage()-1) * $products->perPage() + 1}}-{{(($products->currentPage()-1) * $products->perPage()) + $products->count()}} of {{$products->total()}} <a href="{{$products->previousPageUrl()}}" class="text-dark"><i class="fas fa-chevron-left mx-2 cursor-pointer"></i></a><a href="{{$products->nextPageUrl()}}" class="text-dark"><i class="fas fa-chevron-right ml-2 cursor-pointer"></i></a></span>
	</div>
</div>
@endsection