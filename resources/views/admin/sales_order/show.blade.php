@extends('template.admin')

@section('title', 'Sales Order')
@section('sidebar', 'admin')

@section('css')
<style type="text/css">
	@media (min-width: 768px) {
		.so-img {
			max-width: 25%;
		}
	}
</style>
@endsection

@section('body')
<h1 class="font-weight-bold"><a class="text-dark" href="{{route('admin.sales-order.index')}}"><i class="fas fa-chevron-left mr-2"></i>Livestream ID #{{$livestream->stream_id}}</a></h1>

<hr class="hr-thick" style="border-color: #707070;">

<div class="row mx-0 px-0">
	<div class="col-12 mx-0 px-0">
		<div class="card border rounded dark-shadow px-0 overflow-auto">
			<div class="card-body px-0">
				{{-- STEP BAR START --}}
				<div class="step-progress w-75 mx-auto mb-3" data-progress='{{((int) $livestream->displayStatus(true)) - 1}}'>
					<div class="component">
						{{-- BAR --}}
						<div class="bar">
							<hr class="bar-progress">
							<hr class="bar-remaining">
						</div>

						{{-- STEPS --}}
						<div class="steps">
							@php ($i = 0)
							@foreach ($steps as $s)
							@if ($i == 5)
							@php
							break;
							@endphp
							@endif

							<div class="step-icon">
								<i class="fas fa-circle fa-2x h-100"></i>
							</div>
							@php ($i++)
							@endforeach
						</div>
					</div>

					<div class="labels">
						@foreach ($steps as $s)
						@if ($s == 'Cancelled' || $s == 'Cart')
						@php
						continue;
						@endphp
						@endif
						
						<div class="step-label">
							{{$s}}
						</div>
						@endforeach
					</div>
				</div>
				{{-- STEP BAR END --}}

				{{-- TABLE START --}}
				<table class="table mt-5">
					<thead class="text-white text-center" style="background-color: rgba(112, 112, 112, 0.75);">
						<tr>
							<td>Title</td>
							<td>ID</td>
							<td>Price</td>
							<td>Duration</td>
							<td>Total Amount</td>
						</tr>
					</thead>

					<tbody class="text-center">
						<tr>
							<td>{{$livestream->title}}</td>
							<td>{{$livestream->stream_id}}</td>
							<td>₱{{number_format(100, 2)}}/hour</td>
							<td>{{$livestream->duration}} hour{{$livestream->duration > 1 ? 's' : ''}}</td>
							<td>₱{{number_format($livestream->price, 2)}}</td>
						</tr>

						<tr>
							<td colspan="3" rowspan="4"></td>
							<td class="text-right">Subtotal:</td>
							<td class="text-left">₱{{number_format($livestream->price, 2)}}</td>
						</tr>

						<tr>
							<td class="text-right">Tax:</td>
							<td class="text-left">₱0.00</td>
						</tr>

						<tr>
							<td colspan="2" class="font-weight-bold">₱{{number_format($livestream->price, 2)}}</td>
						</tr>
					</tbody>
				</table>
				{{-- TABLE END --}}
			</div>
		</div>
	</div>
</div>

<div class="row-spacing-3">
	<div class="row mx-0 px-0">
		<div class="col-6">
			<div class="card h-100">
				<div class="card-body d-flex flex-d-col">
					<span class="font-weight-bold">Customer Details</span>
					<span>{{$livestream->ls_user->getName()}}</span>
					<span>(+63){{$livestream->ls_user->contact_number}}</span>
					<span>{{$livestream->ls_user->email}}</span>
				</div>
			</div>
		</div>

		<div class="col-6">
			<div class="card h-100">
				<div class="card-body d-flex flex-d-col">
					<span class="font-weight-bold">Billing Details</span>
					<span>{{$livestream->ls_user->getName()}}</span>
					<span>{{$livestream->ls_user->getAddress(true, true)}}</span>
					<span>{{$livestream->ls_user->city}}, {{$livestream->ls_user->region}}</span>
				</div>
			</div>
		</div>
	</div>

	<div class="row mx-0 px-0">
		<div class="col-6">
			<div class="card h-100">
				<div class="card-body d-flex flex-d-col">
					<span class="font-weight-bold">Payment Method</span>
					<span>Bank Transfer{{--ucwords($livestream->payment_method)--}}</span>
				</div>
			</div>
		</div>
	</div>
</div>

<hr class="hr-thick">
<h5>Proof of Payment</h5>
<div class="row">
	@foreach ($livestream->logs->reverse() as $l)
		@if ($l->type == 1 || $l->type == 2)
		<div class="col-12">
			<div class="card">
				<div class="card-body">
					<div class="row">
						<div class="col-9">
							<p class="mb-2"><i class="fas fa-info-circle mr-2"></i><b>{{\Carbon\Carbon::parse($l->create_at)->format('M d, Y')}}:</b> {{$l->getDescription()}}</p>
						</div>
						<div class="col-3 text-right">
							@if ($livestream->displayStatus(true) == 2 && $l->description == 'Submitted Proof of Payment' && $l->id == \App\LivestreamLog::where('livestream_id', '=', $livestream->id)->orderBy('id', 'DESC')->first()->id)
								<a href="{{ route('admin.sales-order.proof.accept', [$livestream->id]) }}" class="btn btn-success" title="Accept"><i class="fas fa-check-circle fa-lg"></i></a>
								<a href="{{ route('admin.sales-order.proof.reject', [$livestream->id]) }}" class="btn btn-danger" title="Reject"><i class="fas fa-times-circle fa-lg"></i></a>
							@else
							<span class="badge {{ $l->description == 'Submitted Proof of Payment' ? 'badge-info' : ($l->isRejected() ? 'badge-danger' : ($l->isAccepted() ? 'badge-success' : 'badge-info'))}}">
								@if ($l->description == 'Submitted Proof of Payment')
								Submitted
								@elseif ($l->isRejected())
								Rejected
								@else
								Accepted
								@endif
							</span>
							@endif
						</div>
					</div>

					<div class="row">
						<div class="col-12 col-md-3">
							@if ($l->isRejected() || $l->isAccepted())
							<button class="btn btn-dark" data-toggle="modal" data-target="#proof_modal{{$l->id}}">Show Proof</button>

							<div class="modal fade" id="proof_modal{{$l->id}}" aria-hidden="true">
								<div class="modal-dialog modal-dialog-centered modal-lg">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>

										<div class="modal-body d-flex flex-d-row">
											<img src="/proof/livestreams/{{$l->getProofImage()}}" alt="{{ $l->getProofImage() }}" class="img-fluid w-100 mx-auto">
										</div>
									</div>
								</div>
							</div>
							@elseif ($livestream->logs[count($livestream->logs)-1]->id == $l->id)
							<img src="/proof/livestreams/{{$livestream->proof_of_payment}}" alt="{{ $livestream->proof_of_payment }} thumbnail" class="img-fluid img-thumbnail cursor-pointer" data-toggle="modal" data-target="#proof_modal">

							<div class="modal fade" id="proof_modal" aria-hidden="true">
								<div class="modal-dialog modal-dialog-centered modal-lg">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>

										<div class="modal-body d-flex flex-d-row">
											<img src="/proof/livestreams/{{$livestream->proof_of_payment}}" alt="{{ $livestream->proof_of_payment }}" class="img-fluid w-100 mx-auto">
										</div>
									</div>
								</div>
							</div>
							@endif
							{{-- FIX IMAGE WHEN REJECTED(/ACCEPTED?) --}}
						</div>
					</div>
				</div>
			</div>
		</div>
		@endif
	@endforeach
</div>
@endsection