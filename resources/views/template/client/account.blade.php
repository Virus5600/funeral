@extends('template.client')

@section('title')
@yield('subtitle')
@endsection

@section('page-css')
@yield('subpage-css')
@endsection

@section('body')
<section class="my-5 pt-5 p-md-5">
	<div class="container-fluid mt-5 justify-content-center">
		<div class="row">
			<div class="{{(!Request::is('account/orders') && !Request::is('account/orders/*')) && (!Request::is('account/livestreams') && !Request::is('account/livestreams/*')) && (Request::is('account') || Request::is('account/*')) ? 'active' : ''}} col col-lg-2">
				<h5>
					@if (Request::is('account'))
					<span>My Account</span>
					@elseif (!Request::is('account/orders') && !Request::is('account/orders/*') && !Request::is('account/livestreams') && !Request::is('account/livestreams/*') && Request::is('account/*'))
					<a href="{{ route('account') }}" class="text-white text-decoration-none"><span>My Account</span></a>
					@else
					<a href="{{ route('account') }}" class="text-white text-decoration-none"><span>My Account</span></a>
					@endif
				</h5>
			</div>

			<div class="{{Request::is('account/orders') || Request::is('account/orders/*') ? 'active' : ''}} col col-lg-2">
				<h5>
					@if (Request::is('orders'))
					<span>Orders and Tracking</span>
					@elseif (Request::is('orders/*'))
					<a href="{{ route('account.orders') }}" class="text-white text-decoration-none"><span>Orders and Tracking</span></a>
					@else
					<a href="{{ route('account.orders') }}" class="text-white text-decoration-none"><span>Orders and Tracking</span></a>
					@endif
				</h5>
			</div>

			<div class="{{Request::is('account/livestreams') || Request::is('account/livestreams/*') ? 'active' : ''}} col col-lg-2">
				<h5>
					@if (Request::is('livestreams'))
					<span>Livestreams</span>
					@elseif (Request::is('livestreams/*'))
					<a href="{{ route('account.livestreams') }}" class="text-white text-decoration-none"><span>Livestreams</span></a>
					@else
					<a href="{{ route('account.livestreams') }}" class="text-white text-decoration-none"><span>Livestreams</span></a>
					@endif
				</h5>
			</div>

			@yield('sub-body')
		</div>
	</div>
</section>
@endsection

@section('script')
@yield('subscript')
@endsection