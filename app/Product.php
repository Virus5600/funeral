<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
	protected $fillable = [
		'subcategory_id',
		'store_id',
		'product_name',
		'selling_price',
		'inventory',
		'status',
		'description',
		'size',
		'image_name',
		'discount',
		'discount_start',
		'discount_end'
	];

	protected function subcategory() {
		return $this->belongsTo('App\Subcategory');
	}

	protected function category() {
		return $this->subcategory->belongsTo('App\Category');
	}

	protected function store() {
		return $this->belongsTo('App\Store');
	}

	protected function productImage() {
		return $this->hasMany('App\ProductImage');
	}

	protected function cartItem() {
		return $this->hasMany('App\CartItem');
	}
}