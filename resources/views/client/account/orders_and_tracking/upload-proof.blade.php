@extends('template.client.account')

@section('subtitle', 'My Account')

@section('page-css')
<link rel="stylesheet" href="/css/order_tracking.show.css">
<style type="text/css">
	.cursor-pointer-all, .cursor-pointer-all * {
		cursor: pointer!important;
	}
</style>
@endsection

@section('sub-body')
<div class="container mt-3 pt-2 bg-light">
	<div class="itemrow row">
		<div class="col-3"> 
			<h5><a href="{{ route('account.orders') }}" class="font-weight-bold text-decoration-none text-dark"><i class="bi bi-chevron-left text-dark"></i>Back</a></h5>
		</div>
		<div class="col-9 right-align-items px-4">
			<h5><b>ORDER ID. {{$sales_order->id}}</b></h5>
		</div>
	</div>
</div>
<div class="row mt-5">
	@if ($sales_order->status > 0)
	<div class="step-progress w-75 mx-auto mb-3" data-progress='{{$sales_order->status-1}}'>
		<div class="component">
			<div class="bar">
				<hr class="bar-progress">
				<hr class="bar-remaining">
			</div>

			<div class="steps">
				<div class="step-icon">
					<img src="/images/UI/order_payment.png" alt="Payment">
				</div>
				<div class="step-icon">
					<img src="/images/UI/order_processing.png" alt="Processing">
				</div>
				<div class="step-icon">
					<img src="/images/UI/order_shipped.png" alt="Shipped">
				</div>
				<div class="step-icon">
					<img src="/images/UI/order_received.png" alt="Received">
				</div>
				<div class="step-icon">
					<img src="/images/UI/order_complete.png" alt="Complete">
				</div>
			</div>
		</div>

		<div class="labels">
			<div class="step-label">
				To Pay
			</div>
			<div class="step-label">
				Processing
			</div>
			<div class="step-label">
				To Ship
			</div>
			<div class="step-label">
				To Receive
			</div>
			<div class="step-label">
				Completed
			</div>
		</div>
	</div>
	@else
	<div class="w-75 mx-auto mb-3 d-flex flex-d-col align-items-center">
		<div class="step-icon">
			<img src="/images/UI/order_cancelled.png" alt="">
		</div>
		<div class="step-label">
			Cancelled
		</div>
	</div>
	@endif
</div>

<div class="container itemrow bg-light p-3">
	<form action="{{ route('upload-proof.orders', [$sales_order->id]) }}", method="POST" enctype="multipart/form-data">
		{{csrf_field()}}

		<label for="proof" class="form-label text-dark">Upload File:</label>
		<div class="custom-file cursor-pointer-all">
			<label class="custom-file-label" for="proof" id="proof_label">Upload Proof of Payment</label>
			<input id="proof" type="file" name="proof" accept=".jpg,.jpeg,.png" class="custom-file-input">
			<span class="badge badge-danger w-100 validation-message">{{$errors->first('proof')}}</span>
		</div>

		<div class="d-flex mt-3">
			<input type="submit" data-action="submit" class="btn btn-dark mx-auto">
		</div>
	</form>
</div>
@endsection

@section('subscript')
<script>
$(document).ready(() => {
	stepProgressInit();

	$('[name=proof]').on('change', (e) => {
		let $obj = $(e.currentTarget);
		let obj = e.currentTarget;

		if (obj.files && obj.files[0]) {
			let reader = new FileReader();

			reader.onload = function(ee) {
				$("#proof_label").html(obj.files[0].name);
			}

			reader.readAsDataURL(obj.files[0])
		}
		else {
			$("#proof_label").html("Upload Proof of Payment");
		}
	});
});

// COPYING
function copyToClipboard(element) {
	let temp = $("<input>");
	$("body").append(temp);

	let textToCopy;
	if (typeof $(element).attr('data-copy-target') != 'undefined')
		textToCopy = $($(element).attr('data-copy-target')).val();
	else if (typeof $(element).attr('data-copy-text') != 'undefined')
		textToCopy = $(element).attr('data-copy-text');
	else
		textToCopy = $(element).val();

	temp.val(textToCopy).select();
	document.execCommand("copy");
	temp.remove();
	
	Swal.fire({
		title: `Text copied`,
		position: `bottom`,
		showConfirmButton: false,
		toast: true,
		timer: 3750,
		background: `#28a745`,
		customClass: {
			title: `text-white`,
			popup: `px-0`
		},
		width: 150
	});
}

// STEP PROGRESS
function stepProgressInit() {
	let obj = $('.step-progress');
	let bars = obj.find('.bar').children();
	let steps = obj.find('.steps').children();
	let labels = obj.find('.labels').children();
	let stepCount = steps.length-1;
	// In percentage
	let progress = (obj.attr('data-progress')*100)/stepCount;
	let remaining = 100-progress;

	// Set length and height of steps
	obj.find('.steps').css('width', obj.find('.bar').width());
	obj.find('.steps').css('height', obj.find('.bar').height());

	// Attaching a resize event listener to .steps
	$(window).resize((e) => {
		obj.find('.steps').css('width', obj.find('.bar').width());
		obj.find('.steps').css('height', obj.find('.bar').height());
	});

	// Placing the step milestone
	steps.css('width', (100/stepCount) + "%");
	$(steps[stepCount]).css('width', '');

	// Setting the color for step milestone
	for (let i = 0; i <= obj.attr('data-progress'); i++)
		$(steps[i]).addClass('active');

	// Setting the length of the bar
	$(bars[0]).css('width', progress + "%");
	$(bars[1]).css('width', remaining + "%");

	// Setting the labels
	labels.css('width', (100/stepCount) + "%");
};
</script>

<script type="text/javascript">
	var nav = document.querySelector('nav');

	window.addEventListener('scroll', function () {
		if (window.pageYOffset > 100) {
			nav.classList.add('bg-dark', 'shadow');
		} else {
			nav.classList.remove('bg-dark', 'shadow');
		}
	});
</script>

<script>
	document.getElementById("togglemenu").addEventListener("click", function() {
		document.getElementById("navbarmain").classList.add('bg-dark', 'shadow');
	});
</script>
@endsection