-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Dec 09, 2021 at 10:54 AM
-- Server version: 5.7.31
-- PHP Version: 7.4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `soulace`
--

-- --------------------------------------------------------

--
-- Table structure for table `cart_items`
--

DROP TABLE IF EXISTS `cart_items`;
CREATE TABLE IF NOT EXISTS `cart_items` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `quantity` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cart_items_user_id_foreign` (`user_id`),
  KEY `cart_items_product_id_foreign` (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cart_items`
--

INSERT INTO `cart_items` (`id`, `user_id`, `product_id`, `quantity`, `created_at`, `updated_at`) VALUES
(1, 3, 11, 1, '2021-12-09 10:53:19', '2021-12-09 10:53:19'),
(2, 3, 22, 1, '2021-12-09 10:53:19', '2021-12-09 10:53:19');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `category_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `category_name`) VALUES
(1, 'Casket'),
(2, 'Coffin'),
(3, 'Urn'),
(4, 'Flowers');

-- --------------------------------------------------------

--
-- Table structure for table `email_verifications`
--

DROP TABLE IF EXISTS `email_verifications`;
CREATE TABLE IF NOT EXISTS `email_verifications` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `verification_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `expiry_time` datetime NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `email_verifications_user_id_foreign` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `livestream`
--

DROP TABLE IF EXISTS `livestream`;
CREATE TABLE IF NOT EXISTS `livestream` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Funeral Wake Livestream',
  `stream_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `stream_key` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `stream_link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `schedule_date` date DEFAULT NULL,
  `schedule_time` time DEFAULT NULL,
  `duration` int(10) UNSIGNED NOT NULL,
  `price` decimal(18,2) NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `status` enum('cancelled','waiting for proof','pending','awaiting schedule','streaming','finished','cart') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'cart',
  `proof_of_payment` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '0',
  `options` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'standing_photo=0;video_slideshow=0;flower_stands=0',
  `category` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `livestream_user_id_foreign` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `livestream_logs`
--

DROP TABLE IF EXISTS `livestream_logs`;
CREATE TABLE IF NOT EXISTS `livestream_logs` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `livestream_id` int(10) UNSIGNED NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `time` time NOT NULL,
  `type` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `livestream_logs_livestream_id_foreign` (`livestream_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `locations`
--

DROP TABLE IF EXISTS `locations`;
CREATE TABLE IF NOT EXISTS `locations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `store_id` int(10) UNSIGNED NOT NULL,
  `address` mediumtext COLLATE utf8_unicode_ci,
  `latitude` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `longitude` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `locations_store_id_foreign` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_11_000000_create_stores_table', 1),
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2021_08_10_094500_create_subcribers_table', 1),
('2021_08_11_130346_create_shipping_address_table', 1),
('2021_08_13_120208_create_categories_table', 1),
('2021_08_13_122409_create_subcategories_table', 1),
('2021_08_13_124519_create_products_table', 1),
('2021_08_20_114548_create_obituaries_table', 1),
('2021_08_21_155400_create_services_table', 1),
('2021_08_23_142638_create_product_image_table', 1),
('2021_08_23_190033_create_cart_items_table', 1),
('2021_08_26_185932_create_sales_order_table', 1),
('2021_08_26_191244_create_sales_order_items_table', 1),
('2021_08_27_091239_create_var_table', 1),
('2021_08_30_181119_create_livestream_table', 1),
('2021_08_30_182401_create_sales_order_logs_tables', 1),
('2021_09_17_004443_create_locations_table', 1),
('2021_10_29_224607_create_email_verifications_table', 1),
('2021_11_27_024944_create_livestream_logs_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `obituaries`
--

DROP TABLE IF EXISTS `obituaries`;
CREATE TABLE IF NOT EXISTS `obituaries` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `customer_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `birth_date` date NOT NULL,
  `death_date` date NOT NULL,
  `store_id` int(10) UNSIGNED NOT NULL,
  `description` mediumtext COLLATE utf8_unicode_ci,
  `available_from` date NOT NULL,
  `available_to` date DEFAULT NULL,
  `begin_time` time NOT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` mediumtext COLLATE utf8_unicode_ci,
  `image_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `obituaries_customer_id_foreign` (`customer_id`),
  KEY `obituaries_store_id_foreign` (`store_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `obituaries`
--

INSERT INTO `obituaries` (`id`, `customer_id`, `name`, `birth_date`, `death_date`, `store_id`, `description`, `available_from`, `available_to`, `begin_time`, `location`, `address`, `image_name`, `status`) VALUES
(1, 2, 'Shannan Quinn', '1963-11-14', '2021-06-28', 1, 'Shannan, passed away peacefully due to diabetes and congestive heart failure. She was born March 2, 2000 in Ooltewah, Tennessee to Carl and Annie Lynn. Donna loved to love on people. She sewed dolls and little quilted sayings constantly. Her hands were rarely still. Raggedy Annes and Andies were her favorites. She also shared her love with food such as fresh cut fries for her grandsons, lasagna and real fried chicken. Her life was a living example of her favorite Bible verse, Ephesians 4:32 — “And be kind and compassionate to one another, forgiving one another, just as God also forgave you in Christ.', '2021-12-09', NULL, '18:53:19', 'ST. AGUSTIN\'S CHAPEL', '1631 Denman Ave. Coshocton, Ohio 43812', 'shannan_quinn.png', 2),
(2, 2, 'Tatiana Watson', '1963-11-14', '2021-06-28', 1, 'Tatiana, passed away peacefully due to diabetes and congestive heart failure. She was born March 2, 2000 in Ooltewah, Tennessee to Carl and Annie Lynn. Donna loved to love on people. She sewed dolls and little quilted sayings constantly. Her hands were rarely still. Raggedy Annes and Andies were her favorites. She also shared her love with food such as fresh cut fries for her grandsons, lasagna and real fried chicken. Her life was a living example of her favorite Bible verse, Ephesians 4:32 — “And be kind and compassionate to one another, forgiving one another, just as God also forgave you in Christ.', '2021-12-09', NULL, '18:53:19', 'ST. AGUSTIN\'S CHAPEL', '1631 Denman Ave. Coshocton, Ohio 43812', 'tatiana_watson.png', 2);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL,
  UNIQUE KEY `password_resets_email_unique` (`email`),
  UNIQUE KEY `password_resets_token_unique` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
CREATE TABLE IF NOT EXISTS `products` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `subcategory_id` int(10) UNSIGNED NOT NULL,
  `store_id` int(10) UNSIGNED NOT NULL,
  `product_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `selling_price` decimal(18,2) NOT NULL,
  `inventory` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL,
  `description` mediumtext COLLATE utf8_unicode_ci,
  `size` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `discount` decimal(18,2) DEFAULT NULL,
  `discount_start` date DEFAULT NULL,
  `discount_end` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `products_subcategory_id_foreign` (`subcategory_id`),
  KEY `products_store_id_foreign` (`store_id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `subcategory_id`, `store_id`, `product_name`, `selling_price`, `inventory`, `status`, `description`, `size`, `image_name`, `discount`, `discount_start`, `discount_end`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'Baby Bear Infant Casket - Pink', '49550.00', 42, 1, NULL, NULL, 'casket_1.jpg', NULL, NULL, NULL, '2021-12-09 10:53:17', '2021-12-09 10:53:17'),
(2, 1, 1, 'Baby Bear Infant Casket - Blue', '49550.00', 667, 1, NULL, NULL, 'casket_2.jpg', NULL, NULL, NULL, '2021-12-09 10:53:18', '2021-12-09 10:53:18'),
(3, 1, 1, 'Arisius Infant Casket', '149160.00', 701, 1, NULL, NULL, 'casket_3.png', NULL, NULL, NULL, '2021-12-09 10:53:18', '2021-12-09 10:53:18'),
(4, 1, 1, 'Auber Child Casket', '149160.00', 740, 1, NULL, NULL, 'casket_4.jpg', NULL, NULL, NULL, '2021-12-09 10:53:18', '2021-12-09 10:53:18'),
(5, 1, 1, 'Grecian Urn - Half Lid MDF Casket - White', '190070.00', 420, 1, NULL, NULL, 'casket_5.jpg', NULL, NULL, NULL, '2021-12-09 10:53:18', '2021-12-09 10:53:18'),
(6, 1, 1, 'Grecian Urn - Half Lid MDF Casket - Rosewood', '190070.00', 551, 1, NULL, NULL, 'casket_6.jpg', NULL, NULL, NULL, '2021-12-09 10:53:18', '2021-12-09 10:53:18'),
(7, 7, 1, 'Serenity Flat Lid Coffin - Brown', '49550.00', 767, 1, NULL, NULL, 'coffin_1.jpg', NULL, NULL, NULL, '2021-12-09 10:53:18', '2021-12-09 10:53:18'),
(8, 7, 1, 'Serenity Flat Lid Coffin - White', '49550.00', 218, 1, NULL, NULL, 'coffin_2.jpg', NULL, NULL, NULL, '2021-12-09 10:53:18', '2021-12-09 10:53:18'),
(9, 7, 1, 'The Light Wicker', '74450.00', 687, 1, NULL, NULL, 'coffin_3.png', NULL, NULL, NULL, '2021-12-09 10:53:18', '2021-12-09 10:53:18'),
(10, 7, 1, 'The Standard Wicker', '74450.00', 89, 1, NULL, NULL, 'coffin_4.jpg', NULL, NULL, NULL, '2021-12-09 10:53:18', '2021-12-09 10:53:18'),
(11, 7, 1, 'Freedom Raised Lid Coffin - White', '99400.00', 829, 1, NULL, NULL, 'coffin_5.jpg', NULL, NULL, NULL, '2021-12-09 10:53:18', '2021-12-09 10:53:18'),
(12, 7, 1, 'Freedom Raised Lid Coffin - Brown', '99400.00', 420, 1, NULL, NULL, 'coffin_6.jpg', NULL, NULL, NULL, '2021-12-09 10:53:18', '2021-12-09 10:53:18'),
(13, 10, 1, 'Blooming Fields Ceramic Urn', '24600.00', 160, 1, NULL, NULL, 'urn_1.jpg', NULL, NULL, NULL, '2021-12-09 10:53:18', '2021-12-09 10:53:18'),
(14, 10, 1, 'Majestic White Ceramic Urn', '24600.00', 29, 1, NULL, NULL, 'urn_2.jpg', NULL, NULL, NULL, '2021-12-09 10:53:18', '2021-12-09 10:53:18'),
(15, 10, 1, 'Lavander Love Ceramic Urn', '74450.00', 450, 1, NULL, NULL, 'urn_3.jpg', NULL, NULL, NULL, '2021-12-09 10:53:18', '2021-12-09 10:53:18'),
(16, 10, 1, 'Maybelle MDF Urns', '149160.00', 732, 1, NULL, NULL, 'urn_4.jpg', NULL, NULL, NULL, '2021-12-09 10:53:18', '2021-12-09 10:53:18'),
(17, 10, 1, 'Glouchester MDF Urn', '190070.00', 952, 1, NULL, NULL, 'urn_5.jpg', NULL, NULL, NULL, '2021-12-09 10:53:18', '2021-12-09 10:53:18'),
(18, 10, 1, 'Adalberto Wood Urn', '190070.00', 12, 1, NULL, NULL, 'urn_6.jpg', NULL, NULL, NULL, '2021-12-09 10:53:18', '2021-12-09 10:53:18'),
(19, 14, 1, 'Lavander And White Funeral Standing Spray', '24600.00', 910, 1, NULL, NULL, 'flower_1.jpg', NULL, NULL, NULL, '2021-12-09 10:53:18', '2021-12-09 10:53:18'),
(20, 14, 1, 'White Floor Basket Arrangement', '24600.00', 610, 1, NULL, NULL, 'flower_2.jpg', NULL, NULL, NULL, '2021-12-09 10:53:19', '2021-12-09 10:53:19'),
(21, 14, 1, 'Blue And White Funeral Standing Basket', '74450.00', 274, 1, NULL, NULL, 'flower_3.jpg', NULL, NULL, NULL, '2021-12-09 10:53:19', '2021-12-09 10:53:19'),
(22, 14, 1, 'White Funeral Standing Basket', '149160.00', 295, 1, NULL, NULL, 'flower_4.jpg', NULL, NULL, NULL, '2021-12-09 10:53:19', '2021-12-09 10:53:19'),
(23, 14, 1, 'Classic All White Arrangement', '190070.00', 86, 1, NULL, NULL, 'flower_5.jpg', NULL, NULL, NULL, '2021-12-09 10:53:19', '2021-12-09 10:53:19'),
(24, 14, 1, 'Floral Embrace', '190070.00', 16, 1, NULL, NULL, 'flower_6.jpg', NULL, NULL, NULL, '2021-12-09 10:53:19', '2021-12-09 10:53:19');

-- --------------------------------------------------------

--
-- Table structure for table `product_images`
--

DROP TABLE IF EXISTS `product_images`;
CREATE TABLE IF NOT EXISTS `product_images` (
  `product_id` int(10) UNSIGNED NOT NULL,
  `image_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product_images`
--

INSERT INTO `product_images` (`product_id`, `image_name`) VALUES
(1, 'item-2.png'),
(1, 'item-3.png'),
(1, 'item-4.png'),
(1, 'item-5.png');

-- --------------------------------------------------------

--
-- Table structure for table `sales_order`
--

DROP TABLE IF EXISTS `sales_order`;
CREATE TABLE IF NOT EXISTS `sales_order` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `customer_id` int(10) UNSIGNED NOT NULL,
  `courier` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `payment_method` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pay_later` tinyint(4) NOT NULL DEFAULT '0',
  `shipping_addresses_id` int(10) UNSIGNED NOT NULL,
  `instructions` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `total` decimal(18,2) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `proof_of_payment` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `store_id` int(10) UNSIGNED NOT NULL,
  `tracking_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sales_order_customer_id_foreign` (`customer_id`),
  KEY `sales_order_shipping_addresses_id_foreign` (`shipping_addresses_id`),
  KEY `sales_order_store_id_foreign` (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sales_order_items`
--

DROP TABLE IF EXISTS `sales_order_items`;
CREATE TABLE IF NOT EXISTS `sales_order_items` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `customer_id` int(10) UNSIGNED NOT NULL,
  `order_id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `quantity` int(11) NOT NULL,
  `price` decimal(18,4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sales_order_items_customer_id_foreign` (`customer_id`),
  KEY `sales_order_items_order_id_foreign` (`order_id`),
  KEY `sales_order_items_product_id_foreign` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sales_order_logs`
--

DROP TABLE IF EXISTS `sales_order_logs`;
CREATE TABLE IF NOT EXISTS `sales_order_logs` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `order_id` int(10) UNSIGNED NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `time` time NOT NULL,
  `type` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sales_order_logs_order_id_foreign` (`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

DROP TABLE IF EXISTS `services`;
CREATE TABLE IF NOT EXISTS `services` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `service_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` mediumtext COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `service_name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Funeral', 'Personalizing an end-of-life celebration or funeral gives you a chance to pour your love (and your grief) into something important for you and for other people who loved the person who\'s died. It\'s also a chance to create a service, a gathering (or both) to reflect a beautiful life.', '2021-12-09 10:53:19', '2021-12-09 10:53:19'),
(2, 'Burial Services', 'Usually a short ceremony held after the main funeral service as the coffin is lowered into the ground. Mourners are often invited to attend the burial, which may include short readings and prayers, depending on religious beliefs.', '2021-12-09 10:53:19', '2021-12-09 10:53:19'),
(3, 'Memorial Services', 'A memorial service online. This could be online. With this web app, it could be set up with live streaming. Family could mourn together with their loved ones’ portrait.', '2021-12-09 10:53:19', '2021-12-09 10:53:19');

-- --------------------------------------------------------

--
-- Table structure for table `shipping_addresses`
--

DROP TABLE IF EXISTS `shipping_addresses`;
CREATE TABLE IF NOT EXISTS `shipping_addresses` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contact_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `barangay` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `zip_code` int(11) DEFAULT NULL,
  `region` enum('NCR','CAR','Region I','Region II','Region III','Region IV-A','Region IV-B','Region V','Region VI','Region VII','Region VIII','Region IX','Region X','Region XI','Region XII','Region XIII','BARMM') COLLATE utf8_unicode_ci NOT NULL,
  `address_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_default` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `shipping_addresses_user_id_foreign` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `shipping_addresses`
--

INSERT INTO `shipping_addresses` (`id`, `user_id`, `first_name`, `last_name`, `contact_number`, `address`, `barangay`, `city`, `zip_code`, `region`, `address_name`, `is_default`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 3, 'Client', 'クライエント', '1234567890', 'Paseo Montereal, El Monteverde Subdivision', 'San Juan', 'Rizal', NULL, 'Region IV-A', 'Address 1', 0, '2021-12-09 10:53:16', '2021-12-09 10:53:16', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `stores`
--

DROP TABLE IF EXISTS `stores`;
CREATE TABLE IF NOT EXISTS `stores` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contact_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitter` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `stores`
--

INSERT INTO `stores` (`id`, `name`, `email`, `contact_number`, `facebook`, `twitter`, `created_at`, `updated_at`) VALUES
(1, 'E & C GOMEZ FUNERAL HOMES', NULL, NULL, NULL, NULL, '2021-12-09 10:53:16', '2021-12-09 10:53:16');

-- --------------------------------------------------------

--
-- Table structure for table `subcategories`
--

DROP TABLE IF EXISTS `subcategories`;
CREATE TABLE IF NOT EXISTS `subcategories` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `category_id` int(10) UNSIGNED NOT NULL,
  `subcategory_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `subcategories_category_id_foreign` (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `subcategories`
--

INSERT INTO `subcategories` (`id`, `category_id`, `subcategory_name`) VALUES
(1, 1, 'Infant Casket'),
(2, 1, 'Cardboard Casket'),
(3, 1, 'Cremation Casket'),
(4, 1, 'MDF Casket'),
(5, 1, 'Metal Caskets'),
(6, 1, 'Wood Casket'),
(7, 2, 'MDF Coffins'),
(8, 2, 'Wood Coffins'),
(9, 2, 'Wicker Coffins'),
(10, 3, 'Bamboo Urn'),
(11, 3, 'MDF Urns'),
(12, 3, 'Ceramic Urn'),
(13, 3, 'Wood Urns'),
(14, 4, 'Funeral Standing Sprays'),
(15, 4, 'Funeral Flowers'),
(16, 4, 'Funeral Floor Baskets'),
(17, 4, 'Funeral Flower Baskets'),
(18, 4, 'Funeral Plants'),
(19, 4, 'Funeral Wreaths'),
(20, 4, 'Creamation Wreaths'),
(21, 4, 'Casket Sprays');

-- --------------------------------------------------------

--
-- Table structure for table `subscribers`
--

DROP TABLE IF EXISTS `subscribers`;
CREATE TABLE IF NOT EXISTS `subscribers` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  UNIQUE KEY `subscribers_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `subscribers`
--

INSERT INTO `subscribers` (`email`) VALUES
('admin@admin.com');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `middle_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `suffix` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contact_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` mediumtext COLLATE utf8_unicode_ci,
  `region` enum('NCR','CAR','Region I','Region II','Region III','Region IV-A','Region IV-B','Region V','Region VI','Region VII','Region VIII','Region IX','Region X','Region XI','Region XII','Region XIII','BARMM') COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zip_code` int(11) DEFAULT NULL,
  `birthday` date NOT NULL,
  `gender` enum('Male','Female','Others') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Others',
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` enum('client','owner','admin') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'client',
  `store_id` int(10) UNSIGNED DEFAULT NULL,
  `is_privacy_read` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `is_validated` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `users_store_id_foreign` (`store_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `middle_name`, `last_name`, `suffix`, `contact_number`, `address`, `region`, `city`, `zip_code`, `birthday`, `gender`, `email`, `password`, `type`, `store_id`, `is_privacy_read`, `is_validated`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', NULL, '', NULL, '', NULL, NULL, NULL, NULL, '2000-05-06', 'Others', 'admin@admin.com', '$2y$10$LdJJzofCpReIma1U1o2FcekOG7dqOY84c/oPDPeJvdw4BGZDWCnhi', 'admin', NULL, 0, 1, NULL, '2021-12-09 10:53:16', '2021-12-09 10:53:16'),
(2, 'Store', NULL, 'Owner', NULL, '', NULL, NULL, NULL, NULL, '2000-05-06', 'Others', 'store_owner@ec.com', '$2y$10$GKNNofp05WpyM.LorCBPZOng1Yjs7/6Y/Zle/FAN.PNPGUopeYbR.', 'owner', 1, 0, 1, NULL, '2021-12-09 10:53:16', '2021-12-09 10:53:16'),
(3, 'Sample', NULL, 'User', NULL, '', NULL, NULL, NULL, NULL, '2000-05-06', 'Others', 'sample@client.com', '$2y$10$kcrFmbY6OKWMVBw7Yh5syuA/sf.wmLWFbT5QyapvHGhtLwrrRSBmq', 'client', NULL, 0, 1, NULL, '2021-12-09 10:53:16', '2021-12-09 10:53:16');

-- --------------------------------------------------------

--
-- Table structure for table `var`
--

DROP TABLE IF EXISTS `var`;
CREATE TABLE IF NOT EXISTS `var` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `var`
--

INSERT INTO `var` (`id`, `name`, `value`, `description`, `created_at`, `updated_at`) VALUES
(1, 'product_id', 'product_id', 'LIVESTREAM KEY', NULL, NULL);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `cart_items`
--
ALTER TABLE `cart_items`
  ADD CONSTRAINT `cart_items_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `cart_items_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `email_verifications`
--
ALTER TABLE `email_verifications`
  ADD CONSTRAINT `email_verifications_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `livestream`
--
ALTER TABLE `livestream`
  ADD CONSTRAINT `livestream_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `livestream_logs`
--
ALTER TABLE `livestream_logs`
  ADD CONSTRAINT `livestream_logs_livestream_id_foreign` FOREIGN KEY (`livestream_id`) REFERENCES `livestream` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `locations`
--
ALTER TABLE `locations`
  ADD CONSTRAINT `locations_store_id_foreign` FOREIGN KEY (`store_id`) REFERENCES `stores` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `obituaries`
--
ALTER TABLE `obituaries`
  ADD CONSTRAINT `obituaries_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `obituaries_store_id_foreign` FOREIGN KEY (`store_id`) REFERENCES `stores` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_store_id_foreign` FOREIGN KEY (`store_id`) REFERENCES `stores` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `products_subcategory_id_foreign` FOREIGN KEY (`subcategory_id`) REFERENCES `subcategories` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `sales_order`
--
ALTER TABLE `sales_order`
  ADD CONSTRAINT `sales_order_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `sales_order_shipping_addresses_id_foreign` FOREIGN KEY (`shipping_addresses_id`) REFERENCES `shipping_addresses` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `sales_order_store_id_foreign` FOREIGN KEY (`store_id`) REFERENCES `stores` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `sales_order_items`
--
ALTER TABLE `sales_order_items`
  ADD CONSTRAINT `sales_order_items_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `sales_order_items_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `sales_order` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `sales_order_items_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `sales_order_logs`
--
ALTER TABLE `sales_order_logs`
  ADD CONSTRAINT `sales_order_logs_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `sales_order` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `shipping_addresses`
--
ALTER TABLE `shipping_addresses`
  ADD CONSTRAINT `shipping_addresses_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `subcategories`
--
ALTER TABLE `subcategories`
  ADD CONSTRAINT `subcategories_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_store_id_foreign` FOREIGN KEY (`store_id`) REFERENCES `stores` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
