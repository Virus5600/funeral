<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;

use App\Subscriber;

use Validator;
use Log;
use DB;

class SubscriberController extends Controller
{
	protected function store(Request $req) {
		$validator = Validator::make($req->all(), [
			'email' => 'required|email|unique:subscribers,email',
		], [
			'email.required' => 'E-mail is required.',
			'email.email' => 'E-mail provided is not a valid e-mail.',
			'email.unique' => 'E-mail is already subscribed to the news letter.'
		]);

		if ($validator->fails())
			return redirect()
				->back()
				->withErrors($validator)
				->withInput();

		try {
			DB::beginTransaction();

			Subscriber::insert([
				'email' => $req->email
			]);

			DB::commit();
		} catch (\Exception $e) {
			DB::rollback();
		}

		return redirect()
			->back()
			->with('subscribe_success', 'Successfully subscribed to our news letter.');
	}
}