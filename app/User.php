<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;

use DB;

class User extends Authenticatable
{
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'first_name',
		'middle_name',
		'last_name',
		'suffix',
		'contact_number',
		'address',
		'region',
		'city',
		'zip_code',
		'birthday',
		'gender',
		'email',
		'password',
		'type',
		'store_id',
		'is_privacy_read',
		'is_validated'
	];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
		'password', 'remember_token',
	];

	// Relationships
	protected function shippingAddress() { return $this->hasMany('App\ShippingAddress'); }
	protected function store() { return $this->belongsTo('App\Store'); }
	protected function cartItem() { return $this->hasMany('App\CartItem'); }
	protected function salesOrder() { return $this->hasMany('App\SalesOrder', 'customer_id'); }
	protected function obituaries() { return $this->hasMany('App\Obituary', 'customer_id'); }
	protected function livestreams() { return $this->hasMany('App\Livestream'); }

	// Custom Functions
	public function getName() {
		return $this->first_name . ' ' . $this->last_name;
	}

	public static function getRegions() {
		$values = array(
			'NCR',
			'CAR',
			'Region I',
			'Region II',
			'Region III',
			'Region IV-A',
			'Region IV-B',
			'Region V',
			'Region VI',
			'Region VII',
			'Region VIII',
			'Region IX',
			'Region X',
			'Region XI',
			'Region XII',
			'Region XIII',
			'BARMM',
		);
		return $values;
	}

	public static function getCities($region='NCR') {
		if ($region == null)
			$region = 'NCR';

		$cities = array(
			'NCR' => array('Caloocan', 'Las Piñas', 'Makati', 'Malabon', 'Mandaluyong', 'Manila', 'Marikina', 'Muntinlupa', 'Navotas', 'Parañaque', 'Pasay', 'Pasig', 'Pateros', 'Quezon City', 'San Juan', 'Taguig', 'Valenzuela'),
			'CAR' => array('Abra', 'Apayao', 'Baguio', 'Benguet', 'Ifugao', 'Kalinga', 'Mountain Province'),
			'Region I' => array('Dagupan', 'Ilocos Norte', 'Ilocos Sur', 'La Union', 'Pangasinan'),
			'Region II' => array('Batanes', 'Cagayan', 'Isabela', 'Nueva Vizcaya', 'Quirino', 'Santiago'),
			'Region III' => array('Angeles City', 'Aurora', 'Bataan', 'Bulacan', 'Nueva Ecija', 'Olongapo', 'Pampanga', 'Tarlac', 'Zambales'),
			'Region IV-A' => array('Batangas', 'Cavite', 'Laguna', 'Lucena', 'Quezon', 'Rizal'),
			'Region IV-B' => array('Marinduque', 'Occidental Mindoro', 'Oriental Mindoro', 'Palawan', 'Puerto Princesa', 'Romblon'),
			'Region V' => array('Albay', 'Camarines Norte', 'Camarines Sur', 'Catanduanes', 'Masbate', 'Naga', 'Sorsogon'),
			'Region VI' => array('Albay', 'Camarines Norte', 'Camarines Sur', 'Catanduanes', 'Masbate', 'Naga', 'Sorsogon'),
			'Region VII' => array('Bohol', 'Cebu', 'Cebu City', 'Lapu-Lapu', 'Mandaue', 'Negros Oriental', 'Siquijor'),
			'Region VIII' => array('Biliran', 'Eastern Samar', 'Leyte', 'Northern Samar', 'Ormoc', 'Samar', 'Southern Leyte', 'Tacloban'),
			'Region IX' => array('Isabela City', 'Zamboanga City', 'Zamboanga del Norte', 'Zamboanga del Sur', 'Zamboanga Sibugay'),
			'Region X' => array('Bukidnon', 'Cagayan de Oro', 'Camiguin', 'Iligan', 'Lanao del Norte', 'Misamis Occidental', 'Misamis Oriental'),
			'Region XI' => array('Davao City', 'Davao de Oro', 'Davao del Norte', 'Davao del Sur', 'Davao Occidental', 'Davao Oriental'),
			'Region XII' => array('Cotabato', 'General Santos', 'Sarangani', 'South Cotabato', 'Sultan Kudarat'),
			'Region XIII' => array('Agusan del Norte', 'Agusan del Sur', 'Butuan', 'Dinagat Islands', 'Surigao del Norte', 'Surigao del Sur'),
			'BARMM' => array('Basilan', 'Cotabato City', 'Lanao del Sur', 'Maguindanao', 'Sulu', 'Tawi-Tawi', '63 barangays in Cotabato')
		);

		return $cities[$region];
	}

	public function getAddress($omitCity=false, $omitRegion=false) {
		$address = $this->address;
		if ($this->city != null && !$omitCity)
			$address .= ', ' . $this->city;
		if ($this->region != null && !$omitRegion)
			$address .= ', ' . $this->region; 
		if ($this->zip_code != null)
			$address .= ', ' . $this->zip_code;

		return $address;
	}

	public function livestreamCart() {
		return Livestream::where('status', '=', 'cart')->get();
	}

	public function livestreamTotal() {
		$total = 0.00;
		foreach (Auth::user()->livestreams as $l)
			if ($l->status == 'cart')
				$total += $l->price;

		return $total;
	}

	public function cartTotal() {
		$total = 0.00;
		foreach (Auth::user()->cartItem as $c)
			$total += $c->total();

		return $total;
	}

	public function getStoreOfCartItems() {
		$storeIds = array();
		foreach($this->cartItem as $i) {
			$store_name = strtolower(preg_replace('/ /', '_', $i->product->store->name));

			if (array_key_exists($store_name, $storeIds))
				$storeIds[$store_name] += 1;
			else
				$storeIds[$store_name] = 1;
		}

		$storeId = null;
		$test = null;
		foreach ($storeIds as $k => $v) {
			if ($test == null) {
				$test = $v;
				$storeId = Store::where('name', '=', preg_replace('/_/', ' ', $k))->first()->id;
			}

			if ($test < $v)
				$storeId = Store::where('name', '=', preg_replace('/_/', ' ', $k))->first()->id;
		}

		return $storeId;
	}
}