<?php

use Illuminate\Database\Seeder;

use App\CartItem;

class CartItemsTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{

		DB::table('var')->insert([
			'name' => 'product_id',
			'value' => 'product_id',
			'description' => 'LIVESTREAM KEY',
			]);

		CartItem::create([
			'user_id' => 3,
			'product_id' => 11
		]);

		CartItem::create([
			'user_id' => 3,
			'product_id' => 22
		]);
	}
}