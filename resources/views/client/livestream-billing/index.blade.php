<!DOCTYPE html>
<html lang="en">
	<head>
		{{-- META DATA --}}
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		{{-- SITE META --}}
		<meta name="author" content="Code Senpai, Project on Rush">
		<meta name="type" content="website">
		<meta name="title" content="{{ env('APP_NAME') }}">
		<meta name="description" content="{{ env('APP_DESC') }}">
		<meta name="image" content="/images/branding/soulace_black.jpg">
		<meta name="keywords" content="Soulace, Funeral, Parlor, Funeral Parlor">
		<meta name="application-name" content="{{ env('APP_NAME') }}">

		{{-- TWITTER META --}}
		<meta name="twitter:card" content="summary_large_image">
		<meta name="twitter:title" content="{{ env('APP_NAME') }}">
		<meta name="twitter:description" content="{{ env('APP_DESC') }}">
		<meta name="twitter:image" content="{{Request::url()}}/images/branding/soulace_black.jpg">

		{{-- OG META --}}
		<meta name="og:url" content="{{Request::url()}}">
		<meta name="og:type" content="website">
		<meta name="og:title" content="{{ env('APP_NAME') }}">
		<meta name="og:description" content="{{ env('APP_DESC') }}">
		<meta name="og:image" content="/images/branding/soulace_black.jpg">

		{{-- Bootrstrap 5.0.2 --}}
		<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
		<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">

		{{-- jQuery 3.6.0 --}}
		<!-- <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script> -->
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

		{{-- popper.js 1.16.0 --}}
		<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>

		{{-- Bootstrap 4.4 --}}
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

		{{-- Removes the code that shows up when script is disabled/not allowed/blocked --}}
		<script type="text/javascript" id="for-js-disabled-js">$('head').append('<style id="for-js-disabled">#js-disabled { display: none; }</style>');$(document).ready(function() {$('#js-disabled').remove();$('#for-js-disabled').remove();$('#for-js-disabled-js').remove();});</script>

		{{-- Sweet Alert 2 --}}
		<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>

		{{-- Input Mask 5.0.5 --}}
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/5.0.5/jquery.inputmask.min.js"></script>

		{{-- Fontawesome --}}
		<script src="https://kit.fontawesome.com/d4492f0e4d.js" crossorigin="anonymous"></script>
		
		{{-- Custom CSS --}}
		<link rel="stylesheet" href="/css/style.css">
		<link rel="stylesheet" href="/css/billing.css">
		
		<style type="text/css" id="extend-circle">
			.extend-circle:after {
				height: 100%;
				transform: translateY(100%);
			}
		</style>

		{{-- Favicon --}}
		<link rel='icon' type='image/png' href='/images/branding/logo_gradient.png'>

		{{-- Title --}}
		<title>{{ env('APP_NAME') }} | Checkout</title>
	</head>
	<body>
		{{-- SHOWS THIS INSTEAD WHEN JAVASCRIPT IS DISABLED --}}
		<div style="position: absolute; height: 100vh; width: 100vw; background-color: #ccc;" id="js-disabled">
			<style type="text/css">
				/* Make the element disappear if JavaScript isn't allowed */
				.js-only {
					display: none!important;
				}
			</style>
			<div class="row h-100">
				<div class="col-12 col-md-4 offset-md-4 py-5 my-auto">
					<div class="card shadow my-auto">
						<h4 class="card-header card-title text-center">Javascript is Disabled</h4>

						<div class="card-body">
							<p class="card-text">This website required <b>JavaScript</b> to run. Please allow/enable JavaScript and refresh the page.</p>
							<p class="card-text">If the JavaScript is enabled or allowed, please check your firewall as they might be the one disabling JavaScript.</p>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- Navbar -->
		<nav class="navbar navbar-expand-md navbar-dark p-md-3" id="navbarmain">
			<div class="container">
				<div class="row mt-2">
					<div class="brand_logo">
						<a href="{{route('home')}}" class="navbar-brand">
							<img src="/images/branding/soulace_white.png" id="soulace" class="img-fluid w-25">
						</a>
					</div>
				</div>
			</div>
		</nav>

		<!-- Content -->
		<div class="container">
			<div class="row mt-5">
				<div class="col-lg-8">
					<form action="{{ route('billing.livestream.finished') }}" method="POST">
						{{csrf_field()}}
						
						{{-- STEP 1 --}}
						<hr class="hr-thick">
						<h3 class="mb-5">Payment Method</h3>

						<div class="btn-group-toggle btn-group mb-5" data-toggle="buttons">
							<label class="btn btn-light active">
								<input type="radio" name="payment_option" value="bank_transfer" checked>BANK TRANSFER
							</label>

							<label class="btn btn-light disabled">
								<input type="radio" name="payment_option" value="paypal" disabled>PAYPAL
							</label>

							<label class="btn btn-light disabled">
								<input type="radio" name="payment_option" value="gcash" disabled>GCASH
							</label>
						</div>

						<h4 class="text-decoration-underline">Our Bank Details</h4>
						<div class="row">
							<div class="col">
								<h5>Account Name:</h5>
							</div>

							<div class="col">
								<h5 class="font-weight-bold">Sample Name</h5>
							</div>
						</div>

						<div class="row mb-5">
							<div class="col">
								<h5>Account Number:</h5>
							</div>

							<div class="col">
								<h5 class="font-weight-bold">0123456789</h5>
							</div>
						</div>

						{{-- STEP 2 --}}
						<hr class="hr-thick mt-5">
						<h3 class="mb-5">Review Order</h3>

						<h5>Livestream Details</h5>
						@php ($total = 0)
						@foreach (Auth::user()->livestreamCart() as $l)
						<div class="row">
							<div class="col-12">
								<div class="bg-dark px-3 py-2">
									<div class="row">
										<div class="col">
											<div class="d-flex flex-row">
												<span class="w-50"><b>Title:</b></span>
												<span class="w-50">{{$l->title}}</span>
											</div>
											
											<div class="d-flex flex-row">
												<span class="w-50"><b>Description:</b></span>
												<span class="w-50">{{$l->description}}</span>
											</div>

											<div class="d-flex flex-row">
												<span class="w-50"><b>Date:</b></span>
												<span class="w-50">{{Carbon\Carbon::parse($l->schedule_date)->timezone('Asia/Manila')->format('M d, Y (l)')}}</span>
											</div>

											<div class="d-flex flex-row">
												<span class="w-50"><b>Time:</b></span>
												<span class="w-50">{{Carbon\Carbon::parse($l->schedule_date.' '.$l->schedule_time)->timezone('Asia/Manila')->format('H:m A')}}</span>
											</div>

											<div class="d-flex flex-row">
												<span class="w-50">Duration:</span>
												<span class="w-50">{{ $l->duration }} hour{{ $l->duration > 1 ? 's' : '' }}</span>
											</div>

											<div class="d-flex flex-row">
												<span class="w-50">Price:</span>
												<span class="w-50">₱{{ number_format($l->price, 2) }}</span>
												@php ($total += $l->price)
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						@endforeach
						<hr>
						<div class="d-flex flex-row mt-5">
							<h5 class="w-50">Total:</h5>
							<h5 class="w-50">₱ {{ number_format($total, 2) }}</h5>
						</div>
						<hr class="hr-thick">
						<h3 class="mt-5">Billing Details</h3>
						<p class="my-0">{{ Auth::user()->getName() }}</p>
						<p class="my-0">(+63){{ Auth::user()->contact_number }}</p>
						<p class="my-0">{{ Auth::user()->getAddress(true, true) }}</p>
						<p class="my-0">{{ Auth::user()->city }}, {{ Auth::user()->region }}</p>

						<button type="submit" class="btn btn-light py-1 mt-5">Check Out</button>
					</form>
				</div>
			</div>
		</div>

		<!-- Footer -->
		<footer class="pt-1">
			<div class="container">
				<div class="row mt-5">
					<div class="reserved col-lg-6 py-4" id="policy"> 
						<small >Privacy Policy | Terms and Conditions</small>
					</div>

					<div class="reserved col-lg-6 py-4" id="soulrights">
						<small>©2021 Soulace All Rights Reserved.</small>
					</div>
				</div>
			</div>
		</footer>
		
		<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
		<script type="text/javascript">
			@if (Session::has('flash_error'))
			Swal.fire({
				{!!Session::has('has_icon') ? "icon: `error`," : ""!!}
				title: `{{Session::get('flash_error')}}`,
				{!!Session::has('message') ? 'html: `' . Session::get('message') . '`,' : ''!!}
				position: {!!Session::has('position') ? '`' . Session::get('position') . '`' : '`top`'!!},
				showConfirmButton: false,
				toast: {!!Session::has('is_toast') ? Session::get('is_toast') : true!!},
				{!!Session::has('has_timer') ? (Session::get('has_timer') ? (Session::has('duration') ? ('timer: ' . Session::get('duration')) . ',' : `timer: 10000,`) : '') : `timer: 10000,`!!}
				background: `#dc3545`,
				customClass: {
					title: `text-white`,
					content: `text-white`,
					popup: `px-3`
				},
			});
			@elseif (Session::has('flash_message'))
			Swal.fire({
				{!!Session::has('has_icon') ? "icon: `info`," : ""!!}
				title: `{{Session::get('flash_message')}}`,
				{!!Session::has('message') ? 'html: `' . Session::get('message') . '`,' : ''!!}
				position: {!!Session::has('position') ? '`' . Session::get('position') . '`' : '`top`'!!},
				showConfirmButton: false,
				toast: {!!Session::has('is_toast') ? Session::get('is_toast') : true!!},
				{!!Session::has('has_timer') ? (Session::get('has_timer') ? (Session::has('duration') ? ('timer: ' . Session::get('duration')) . ',' : `timer: 10000,`) : '') : `timer: 10000,`!!}
				background: `#17a2b8`,
				customClass: {
					title: `text-white`,
					content: `text-white`,
					popup: `px-3`
				},
			});
			@elseif (Session::has('flash_success'))
			Swal.fire({
				{!!Session::has('has_icon') ? "icon: `success`," : ""!!}
				title: `{{Session::get('flash_success')}}`,
				{!!Session::has('message') ? 'html: `' . Session::get('message') . '`,' : ''!!}
				position: {!!Session::has('position') ? '`' . Session::get('position') . '`' : '`top`'!!},
				showConfirmButton: false,
				toast: {!!Session::has('is_toast') ? Session::get('is_toast') : true!!},
				{!!Session::has('has_timer') ? (Session::get('has_timer') ? (Session::has('duration') ? ('timer: ' . Session::get('duration')) . ',' : `timer: 10000,`) : '') : `timer: 10000,`!!}
				background: `#28a745`,
				customClass: {
					title: `text-white`,
					content: `text-white`,
					popup: `px-3`
				},
			});
			@endif
		</script>
		<script>
			// document.getElementById("togglemenu").addEventListener("click", function() {
			// 	document.getElementById("navbarmain").classList.add('bg-dark', 'shadow');
			// });
			var extendCircleClass = $('#extend-circle');
			var step = {
				// Step 1
				first: {
					// Step 1 fields
					fields: {
						'pre-made_address': 'select',
						'first_name': 'text',
						'last_name': 'text',
						'contact_number': 'text',
						'address': 'text',
						'region': 'select',
						'city': 'select',
						'barangay': 'text',
						'zip_code': 'text',
						'instructions': 'textarea',
						'make_default': 'checkbox'
					}
				}
			};

			$(document).ready(() => {
				// ???
				$("#togglemenu").on('click', (e) => {
					$("#navbarmain").addClass('bg-dark shadow');
				});

				// Sets the initial length of the line at the side...
				extendCircleClass.html('.extend-circle:after {height: ' + $('.step-active').outerHeight(true) + 'px; transform: translateY(100%);}');
				$('.step-active').find('.circle').addClass('extend-circle');

				// Resize the line on window resize.
				$(window).on('resize', (e) => {
					let obj = $('.step-active');

					$('.step').removeClass('extend-circle');
					extendCircleClass.html('.extend-circle:after {height: ' + obj.outerHeight(true) + 'px; transform: translateY(100%);}');
					obj.find('.circle').addClass('extend-circle');
				});

				// Set values of pre-made address to the form below.
				$('[name=pre-made_address]').on('change', (e) => {
					let obj = $(e.currentTarget);

					if (obj.val() != 'other_address') {
						$.post('{{ route('utility.get_shipping_address') }}', {
							_token: '{{ csrf_token() }}',
							id: obj.val()
						}).done((data) => {
							$.each(data, (k, v) => {
								if (k == 'region' || k == 'city') {
									let target = $('[name='+k+']');
									var options = "";
									target.html('');

									if (k == 'region') {
										$.post('{{ route('utility.get_regions') }}', {
											_token: '{{ csrf_token() }}'
										}).done((subdata) => {
											$.each(subdata, (sk, sv) => {
												if (v == sv)
													options += "<option value='" + sv + "' selected>" + sv + "</option>";
												else
													options += "<option value='" + sv + "'>" + sv + "</option>";
											});

											target.append(options);
										});
									}
									else if (k == 'city') {
										$.post('{{ route('utility.get_cities') }}', {
											_token: '{{ csrf_token() }}',
											region: data.region
										}).done((subdata) => {
											$.each(subdata, (sk, sv) => {
												if (v == sv)
													options += "<option value='" + sv + "' selected>" + sv + "</option>";
												else
													options += "<option value='" + sv + "'>" + sv + "</option>";
											});

											target.append(options);
										});
									}
								}
								else {
									$('[name='+k+']').val(v);
								}
							});
						});
					}
					else {
						$('#step-1-done').html('Save and Continue');
						$('#step-1-content').find('input').val("");
					}
				});

				// Activate masking
				$(document).on('ready load click focus', "[data-mask]", (e) => {
					let obj = $(e.currentTarget);
					if (!obj.attr('data-masked')) {
						obj.inputmask('mask', {
							'mask' : obj.attr('data-mask-format'),
							'removeMaskOnSubmit' : true,
							'autoUnmask':true
						});

						obj.attr('data-masked', 'true');
					}
				});
				$("[data-mask]").inputmask('mask', {
					'mask' : '+63 999 999 9999',
					'removeMaskOnSubmit' : true,
					'autoUnmask':true
				}).attr('data-masked', 'true');

				// Changing the city field of that respected form group
				$(document).on('change', '[name=region]', (e) => {
					let obj = $(e.currentTarget).attr('data-target');
					console.log($(obj));

					$.ajax({
						url: '{{route('utility.get_cities')}}',
						method: 'POST',
						data: {
							_token: '{{ csrf_token() }}',
							region: $(e.currentTarget).val()
						},
						success: function(data, status, xhr) {
							$(obj).html('');
							for (let i = 0; i < data.length; i++)
								$(obj).append(`<option value="` + data[i] + `">` + data[i] + `</option>`);
						}
					});
				});

				// Moving to step 2
				$('#step-1-done').on('click', (e) => {
					let values = {
						_token: '{{csrf_token()}}'
					};

					$.each(step.first.fields, (k, v) => {
						values[k] = $('[name=' + k + ']').val();
					});

					$.post('{{ route('utility.validate_step_1') }}', values);
				});
			});
		</script>
	</body>
</html>