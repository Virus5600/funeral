@extends('template.admin')

@section('title', 'Sales Order')
@section('sidebar', 'admin')

@section('body')
<h1 class="font-weight-bold"><a class="text-dark" href="javascript:void(0);" onclick="confirmLeave('{{route('admin.user-management.index')}}');"><i class="fas fa-chevron-left mr-2"></i>Manage User</a></h1>

<hr class="hr-thick" style="border-color: #707070;">

<form action="{{ route('admin.user-management.store') }}" method="POST" class="form w-lg-50" enctype="multipart/form-data">
	{{csrf_field()}}

	<div class="row">
		<div class="form-group col-12 col-lg-3">
			<label class="form-label font-weight-bold">First Name</label>
			<input type="text" class="form-control" name="first_name" value="{{old('first_name')}}">
			<span class="badge badge-danger w-100 validation-message">{{$errors->first('first_name')}}</span>
		</div>

		<div class="form-group col-12 col-lg-3">
			<label class="form-label font-weight-bold">Middle Name</label>
			<input type="text" class="form-control" name="middle_name" value="{{old('middle_name')}}">
			<span class="badge badge-danger w-100 validation-message">{{$errors->first('middle_name')}}</span>
		</div>

		<div class="form-group col-12 col-lg-3">
			<label class="form-label font-weight-bold">Last Name</label>
			<input type="text" class="form-control" name="last_name" value="{{old('last_name')}}">
			<span class="badge badge-danger w-100 validation-message">{{$errors->first('last_name')}}</span>
		</div>

		<div class="form-group col-12 col-lg-3">
			<label class="form-label font-weight-bold">Suffix</label>
			<input type="text" class="form-control" name="suffix" value="{{old('suffix')}}">
			<span class="badge badge-danger w-100 validation-message">{{$errors->first('suffix')}}</span>
		</div>
	</div>

	<div class="row">
		<div class="form-group col-12 col-lg-3">
			<label class="form-label font-weight-bold">Email</label>
			<input type="text" class="form-control" name="email" value="{{old('email')}}">
			<span class="badge badge-danger w-100 validation-message">{{$errors->first('email')}}</span>
		</div>

		<div class="form-group col-12 col-lg-3">
			<label class="form-label font-weight-bold">Contact Number</label>
			<input type="text" class="form-control" name="contact_number" value="{{old('contact_number')}}" data-mask data-mask-format="+63 999 999 9999">
			<span class="badge badge-danger w-100 validation-message">{{$errors->first('contact_number')}}</span>
		</div>
	</div>

	<hr class="hr-thick-50 border-color-custom">

	<div class="row">
		<div class="form-group col-12">
			<label class="form-label font-weight-bold">Address</label>
			<input type="text" class="form-control" name="address" value="{{old('address')}}">
			<span class="badge badge-danger w-100 validation-message">{{$errors->first('address')}}</span>
		</div>
	</div>

	<div class="row">
		<div class="form-group col-6 col-lg-4">
			<label class="form-label font-weight-bold">Region</label>
			<select class="custom-select" name="region">
				@foreach(App\User::getRegions() as $r)
				<option value="{{$r}}" {{old('region') == $r ? 'selected' : ''}}>{{$r}}</option>
				@endforeach
			</select>
			<span class="badge badge-danger w-100 validation-message">{{$errors->first('region')}}</span>
		</div>

		<div class="form-group col-6 col-lg-4">
			<label class="form-label font-weight-bold">City</label>
			<select class="custom-select" name="city">
				@foreach(App\User::getCities() as $c)
				<option value="{{$c}}" {{old('city') == $c ? 'selected' : ''}}>{{$c}}</option>
				@endforeach
			</select>
			<span class="badge badge-danger w-100 validation-message">{{$errors->first('city')}}</span>
		</div>
	</div>

	<div class="row">
		<div class="form-group col-6 col-lg-3 mx-auto mx-lg-0">
			<label class="form-label font-weight-bold">Zip Code</label>
			<input type="text" class="form-control" name="zip_code" value="{{old('zip_code')}}">
			<span class="badge badge-danger w-100 validation-message">{{$errors->first('zip_code')}}</span>
		</div>
	</div>

	<hr class="hr-thick-50 border-color-custom">

	<div class="row">
		<div class="form-group col-6">
			<label class="form-label font-weight-bold">User Type</label>
			<select class="custom-select" name="user_type">
				<option value="admin" {{old('user_type') == 'admin' ? 'selected' : ''}}>Admin</option>
				<option value="owner" {{old('user_type') == 'owner' ? 'selected' : ''}}>Owner</option>
				<option value="client" {{old('user_type') == 'client' ? 'selected' : ''}}>Client</option>
			</select>
			<span class="badge badge-danger w-100 validation-message">{{$errors->first('user_type')}}</span>
		</div>

		<div class="form-group col-6">
			<label class="form-label font-weight-bold">Owner of</label>
			<select class="custom-select" name="store_owner" disabled>
				@foreach ($stores as $s)
				<option value="{{$s->id}}" {{old('store_owner') == $s->id ? 'selected' : ''}}>{{$s->name}}</option>
				@endforeach
			</select>
			<span class="badge badge-danger w-100 validation-message">{{$errors->first('store_owner')}}</span>
		</div>
	</div>

	<br>
	<div class="row">
		<div class="col-12">
			<button type="submit" class="btn btn-dark" data-action="submit">Save Changes</button>
		</div>
	</div>
</form>
@endsection

@section('script')
<script type="text/javascript">
	$(document).ready(() => {
		$('[name=region]').on('change', (e) => {
			$.ajax({
				url: '{{route('utility.get_cities')}}',
				method: 'POST',
				data: {
					_token: '{{ csrf_token() }}',
					region: $(e.currentTarget).val()
				},
				success: function(data, status, xhr) {
					$('[name=city]').html('');
					for (let i = 0; i < data.length; i++)
						$('[name=city]').append(`<option value="` + data[i] + `">` + data[i] + `</option>`);
				}
			});
		});

		$('[name=user_type]').on('change', (e) => {
			let obj = $(e.currentTarget);

			if (obj.val() == 'owner')
				$('[name=store_owner]').prop('disabled', false);
			else
				$('[name=store_owner]').prop('disabled', true);
		});
	});
</script>
@endsection