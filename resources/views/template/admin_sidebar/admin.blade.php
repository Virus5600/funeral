@if (\Request::is('admin/dashboard'))
<div class="sidebar-item active text-truncate">
	<i class="fas fa-border-all mr-2"></i>Dashboard
</div>
@else
<a class="sidebar-item text-truncate" href="{{ route('admin.dashboard') }}">
	<i class="fas fa-border-all mr-2"></i>Dashboard
</a>
@endif

@if (\Request::is('admin/sales-order'))
<div class="sidebar-item active text-truncate">
	<i class="fas fa-percent mr-2"></i>Sales Order
</div>
@elseif (\Request::is('admin/sales-order/*'))
<a class="sidebar-item active text-truncate" href="{{ route('admin.sales-order.index') }}">
	<i class="fas fa-percent mr-2"></i>Sales Order
</a>
@else
<a class="sidebar-item d-block text-truncate" href="{{ route('admin.sales-order.index') }}">
	<i class="fas fa-percent mr-2"></i>Sales Order
</a>
@endif

@if (\Request::is('admin/store'))
<div class="sidebar-item active text-truncate">
	<i class="fas fa-store mr-2"></i>Store
</div>
@elseif (\Request::is('admin/store/*'))
<a class="sidebar-item active text-truncate" href="{{ route('admin.store.index') }}">
	<i class="fas fa-store mr-2"></i>Store
</a>
@else
<a class="sidebar-item d-block text-truncate" href="{{ route('admin.store.index') }}">
	<i class="fas fa-store mr-2"></i>Store
</a>
@endif

@if (\Request::is('admin/user-management'))
<div class="sidebar-item active text-truncate">
	<i class="fas fa-users mr-2"></i>User Management
</div>
@elseif (\Request::is('admin/user-management/*'))
<a class="sidebar-item active text-truncate" href="{{ route('admin.user-management.index') }}">
	<i class="fas fa-users mr-2"></i>User Management
</a>
@else
<a class="sidebar-item d-block text-truncate" href="{{ route('admin.user-management.index') }}">
	<i class="fas fa-users mr-2"></i>User Management
</a>
@endif

@if (\Request::is('admin/services'))
<div class="sidebar-item active text-truncate">
	<i class="fas fa-hands-helping mr-2" style="transform: rotate(30deg);"></i>Services
</div>
@elseif (\Request::is('admin/services/*'))
<a class="sidebar-item active text-truncate" href="{{ route('admin.services.index') }}">
	<i class="fas fa-hands-helping mr-2" style="transform: rotate(30deg);"></i>Services
</a>
@else
<a class="sidebar-item text-truncate" href="{{ route('admin.services.index') }}">
	<i class="fas fa-hands-helping mr-2" style="transform: rotate(30deg);"></i>Services
</a>
@endif

@if (\Request::is('admin/livestream-management'))
<div class="sidebar-item active text-truncate">
	<i class="fas fa-play-circle mr-2"></i>Livestream Management
</div>
@elseif (\Request::is('admin/livestream-management/*'))
<a class="sidebar-item active text-truncate" href="{{ route('admin.livestream-management.index') }}">
	<i class="fas fa-play-circle mr-2"></i>Livestream Management
</a>
@else
<a class="sidebar-item d-block text-truncate" href="{{ route('admin.livestream-management.index') }}">
	<i class="fas fa-play-circle mr-2"></i>Livestream Management
</a>
@endif