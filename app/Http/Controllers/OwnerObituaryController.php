<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;
use App\Http\Requests;

use App\Obituary;
use App\User;

use Validator;
use Log;
use DB;

class OwnerObituaryController extends Controller
{
	protected function index() {
		$obituaries = Obituary::where('store_id', '=', Auth::user()->store_id);
		$search = null;

		if (request()->has('s')) {
			$search = request()->get('s');

			$obituaries = $obituaries->where('name', '=', $search)
				->orWhere('description', 'LIKE', '%'.$search.'%')
				->orWhere('address', 'LIKE', '%'.$search.'%')
				->orWhere('location', 'LIKE', '%'.$search.'%');
		}

		return view('owner.obituary.index', [
			'search' => $search,
			'obituary' => $obituaries->paginate(15)
		]);
	}

	protected function create() {
		return view('owner.obituary.create');
	}

	protected function store(Request $req) {
		$validator = Validator::make($req->all(), [
			'name' => 'required|min:3',
			'birth_date' => 'required|date|before:death_date',
			'death_date' => 'required|date|after:birth_date',
			'image' => 'file|mimes:jpeg,jpg,png|max:5120',
			'description' => 'min:5',
			'obituary_duration_start' => 'required|date|before:obituary_duration_end',
			'obituary_duration_end' => 'required|date|after:obituary_duration_start',
			'begin_time' => 'required',
			'customer_email' => 'required|email|exists:users,email'
		], [
			'name.required' => 'Full name is required.',
			'name.min' => 'Please provide the full name of the deceased.',
			'birth_date.required' => 'Date of birth is required.',
			'birth_date.date' => 'Please provide a proper date value.',
			'birth_date.before' => 'Date of birth must be before the date of death.',
			'death_date.required' => 'Date of death is required.',
			'death_date.date' => 'Please provide a proper date value.',
			'death_date.after' => 'Date of death should be after the date of birth.',
			'image.file' => 'Image must be a valid file.',
			'image.mimes' => 'Image formats allowed are only JPEG, JPG, and PNG.',
			'image.max' => 'Maximum image file size should be 5MB.',
			'description.min' => 'Please provide a proper description or leave it blank entirely.',
			'obituary_duration_start.required' => 'The start date of when this obituary will be displayed is required.',
			'obituary_duration_start.date' => 'Please provide a proper date value.',
			'obituary_duration_start.before' => 'Date of birth must be before the date of death.',
			'obituary_duration_end.required' => 'The end date of when this obituary will be displayed is required.',
			'obituary_duration_end.date' => 'Please provide a proper date value.',
			'obituary_duration_end.after' => 'Date of death should be after the date of birth.',
			'begin_time.required' => 'This field is required.',
			'customer_email.required' => 'Customer\'s email is required.',
			'customer_email.email' => 'Please provide the "email" of the requestor\'s account.',
			'customer_email.exists' => 'A user with an email of "' . $req->customer_email . '" does not exists.'
		]);

		if ($validator->fails()) {
			return redirect()
				->back()
				->withErrors($validator)
				->withInput();
		}

		try {
			DB::beginTransaction();

			// Generate file name
			$fileName = null;
			if ($req->hasFile('image')) {
				$file = $req->file('image');
				$fileName = 'obituaries-'.uniqid().'.'.$file->getClientOriginalExtension();
				// Move file
				$filepath = 'uploads/obituary/';
				$file->move($filepath, $fileName);
			}

			Obituary::create([
				'customer_id' => User::where('email', '=', $req->customer_email)->first()->id,
				'name' => $req->name,
				'birth_date' => $req->birth_date,
				'death_date' => $req->death_date,
				'store_id' => Auth::user()->store->id,
				'description' => $req->description,
				'available_from' => $req->obituary_duration_start,
				'available_to' => $req->obituary_duration_end,
				'begin_time' => $req->begin_time,
				'location' => $req->location,
				'address' => $req->address,
				'image_name' => $fileName,
				'status' => 1
			]);

			DB::commit();
		} catch (\Exception $e) {
			Log::error($e);
			DB::rollback();

			return redirect()
				->back()
				->with('flash_error', 'Something went wrong, please try again later.')
				->withInput();
		}

		return redirect()
			->route('owner.obituary.index')
			->with('flash_success', 'Successfully added new obituary entry.');
	}

	protected function show($name) {
		return view('owner.obituary.edit', [
			'isShow' => true,
			'obituary' => Obituary::where('name', '=', preg_replace('/_/', ' ', $name))->first()
		]);
	}

	protected function edit($name) {
		return view('owner.obituary.edit', [
			'isShow' => false,
			'obituary' => Obituary::where('name', '=', preg_replace('/_/', ' ', $name))->first()
		]);
	}

	protected function update(Request $req, $id) {
		$validator = Validator::make($req->all(), [
			'name' => 'required|min:3',
			'birth_date' => 'required|date|before:death_date',
			'death_date' => 'required|date|after:birth_date',
			'image' => 'file|mimes:jpeg,jpg,png|max:5120',
			'description' => 'min:5',
			'obituary_duration_start' => 'required|date|before:obituary_duration_end',
			'obituary_duration_end' => 'required|date|after:obituary_duration_start',
			'begin_time' => 'required',
		], [
			'name.required' => 'Full name is required.',
			'name.min' => 'Please provide the full name of the deceased.',
			'birth_date.required' => 'Date of birth is required.',
			'birth_date.date' => 'Please provide a proper date value.',
			'birth_date.before' => 'Date of birth must be before the date of death.',
			'death_date.required' => 'Date of death is required.',
			'death_date.date' => 'Please provide a proper date value.',
			'death_date.after' => 'Date of death should be after the date of birth.',
			'image.file' => 'Image must be a valid file.',
			'image.mimes' => 'Image formats allowed are only JPEG, JPG, and PNG.',
			'image.max' => 'Maximum image file size should be 5MB.',
			'description.min' => 'Please provide a proper description or leave it blank entirely.',
			'obituary_duration_start.required' => 'The start date of when this obituary will be displayed is required.',
			'obituary_duration_start.date' => 'Please provide a proper date value.',
			'obituary_duration_start.before' => 'Date of birth must be before the date of death.',
			'obituary_duration_end.required' => 'The end date of when this obituary will be displayed is required.',
			'obituary_duration_end.date' => 'Please provide a proper date value.',
			'obituary_duration_end.after' => 'Date of death should be after the date of birth.',
			'begin_time.required' => 'This field is required.',
		]);

		if ($validator->fails()) {
			return redirect()
				->back()
				->withErrors($validator)
				->withInput();
		}

		try {
			DB::beginTransaction();

			$obituary = Obituary::find($id);
			
			if ($req->isChanged == 1) {
				// Generate file name
				$file = $req->file('image');
				$fileName = 'obituaries-'.uniqid().'.'.$file->getClientOriginalExtension();
				// Move file
				$filepath = 'uploads/obituary/';
				$file->move($filepath, $fileName);
				// Delete old file
				File::delete(public_path() . '/uploads/obituary/' . $obituary->image_name);
			}

			$obituary->name = $req->name;
			$obituary->birth_date = $req->birth_date;
			$obituary->death_date = $req->death_date;
			$obituary->description = $req->description;
			$obituary->available_from = $req->obituary_duration_start;
			$obituary->available_to = $req->obituary_duration_end;
			$obituary->begin_time = $req->begin_time;
			$obituary->location = $req->location;
			$obituary->address = $req->address;

			if ($req->isChanged == 1)
				$obituary->image_name = $fileName;

			$obituary->save();

			DB::commit();
		} catch (\Exception $e) {
			Log::error($e);
			DB::rollback();

			return redirect()
				->back()
				->with('flash_error', 'Something went wrong, please try again later.')
				->withInput();
		}

		return redirect()
			->route('owner.obituary.index')
			->with('flash_success', 'Successfully updated obituary entry.');
	}

	protected function delete($id) {
		Obituary::find($id)->delete();

		return redirect()
			->back()
			->with('flash_success', 'Successfully removed obituary entry.');
	}

	protected function updateStatus($id, $status) {
		try {
			DB::beginTransaction();

			$o = Obituary::find($id);

			if ($status > 3)
				$status == 3;
			if ($status < 0)
				$status = 0;

			$o->status = $status;
			$o->save();

			DB::commit();
		} catch (Exception $e) {
			DB::rollback();
			Log::error($e);

			return redirect()
				->back()
				->with('flash_error', 'Something went wrong, please try again later');
		}
		return redirect()
			->back()
			->with('flash_success', 'Successfully updated status of Obituary');
	}
}