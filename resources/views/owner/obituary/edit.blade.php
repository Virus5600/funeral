@extends('template.admin')

@section('title', 'Obituary')
@section('sidebar', 'owner')

@section('body')
<h1 class="font-weight-bold">@include('template.breadcrumbs', ['startIndex' => 2, 'confirmLeave' => !$isShow])</h1>

<hr class="hr-thick" style="border-color: #707070;">

<form class="form" action="{{ route('owner.obituary.update', [$obituary->id]) }}" method="POST" enctype="multipart/form-data">
	{{ csrf_field() }}
	<div class="row">
		<div class="col-12 col-lg-8">
			<div class="row">
				<div class="form-group col-12">
					<label class="form-label font-weight-bold">Name{{ $isShow ? ':' : '' }}</label>
					@if ($isShow)
					<p>{{$obituary->name}}</p>
					@else
					<input type="text" class="form-control" name="name" value="{{$obituary->name}}">
					<span class="badge badge-danger w-100 validation-message">{{ $errors->first('name') }}</span>
					@endif
				</div>
			</div>

			<div class="row">
				<div class="form-group col-12 col-lg--6">
					<label class="form-label font-weight-bold">Date of Birth</label>
					@if ($isShow)
					<p>{{\Carbon\Carbon::parse($obituary->death_date)->format('F d, Y')}}</p>
					@else
					<input type="date" class="form-control" name="birth_date" value="{{$obituary->birth_date}}">
					<span class="badge badge-danger w-100 validation-message">{{ $errors->first('birth_date') }}</span>
					@endif
				</div>

				<div class="form-group col-12 col-lg-6">
					<label class="form-label font-weight-bold">Date of Death{{ $isShow ? ':' : '' }}</label>
					@if ($isShow)
					<p>{{\Carbon\Carbon::parse($obituary->death_date)->format('F d, Y')}}</p>
					@else
					<input type="date" class="form-control" name="death_date" value="{{$obituary->death_date}}">
					<span class="badge badge-danger w-100 validation-message">{{ $errors->first('death_date') }}</span>
					@endif
				</div>
			</div>
		</div>

		<div class="form-group col-12 col-lg-4 d-flex flex-d-row">
			<div class="mx-auto text-center" style="overflow: hidden; max-width: 37.5%;">
				<img src="/uploads/obituary/{{$obituary->image_name}}" class="{{$isShow ? '' : 'cursor-pointer'}} img-fluid" id="image" alt="Image" onerror="imgError(this, '{{asset('/uploads/obituary/default.png')}}');">
			</div>

			<div class="mx-auto">
				@if (!$isShow)
				<h6 class="form-label font-weight-bold" style="word-break: break-word" id="image_label">UPLOAD IMAGE</h6>
				<input type="file" name='image' class="hidden" accept=".jpg,.jpeg,.png" id="imgInput">
				<input type="hidden" name="isChanged" value="0">
				<small class="text-muted pb-0 mb-0"><b>FORMATS ALLOWED:</b> JPEG, JPG, PNG</small><br>
				<small class="text-muted pt-0 mt-0"><b>MAX SIZE:</b> 5MB</small>
				@endif
			</div>
			<span class="badge badge-danger w-100 validation-message">{{ $errors->first('image') }}</span>
		</div>
	</div>

	<div class="row">
		<div class="form-group col-12">
			<label class="form-label font-weight-bold">In care of{{ $isShow ? ':' : '' }}</label>
			@if ($isShow)
			<p>{{$obituary->store->name}}</p>
			@else
			<input type="text" class="form-control" name="store" value="{{$obituary->store->name}}" disabled>
			<span class="badge badge-danger w-100 validation-message">{{ $errors->first('store') }}</span>
			@endif
		</div>
	</div>

	<div class="row">
		<div class="form-group col-12">
			<label class="form-label font-weight-bold">Description{{ $isShow ? ':' : '' }}</label>
			@if ($isShow)
			<p>{{$obituary->description}}</p>
			@else
			<textarea class="form-control not-resizable" name="description" rows="5">{{$obituary->description}}</textarea>
			<span class="badge badge-danger w-100 validation-message">{{ $errors->first('description') }}</span>
			@endif
		</div>
	</div>

	<label class="form-label font-weight-bold">Date of Posted Obituary</label>
	<div class="row">
		<div class="form-group col-12 col-lg-6 {{ $isShow ? 'px-5' : 'd-flex flex-d-row'}}" style="align-items: center;">
			@if ($isShow)
			<label class="form-label font-weight-bold mr-3">From{{ $isShow ? ':' : '' }}</label>{!! $isShow ? '<br>' : '' !!}
			<p>{{\Carbon\Carbon::parse($obituary->available_from)->format('F d, Y')}}</p>
			@else
			<input type="date" class="form-control" name="obituary_duration_start" value="{{$obituary->available_from}}">
			<span class="badge badge-danger w-100 validation-message">{{ $errors->first('obituary_duration_start') }}</span>
			@endif
		</div>

		<div class="form-group col-12 col-lg-6 {{ $isShow ? 'px-5' : 'd-flex flex-d-row'}}" style="align-items: center;">
			<label class="form-label font-weight-bold mr-3">To{{ $isShow ? ':' : '' }}</label>{!! $isShow ? '<br>' : '' !!}
			@if ($isShow)
			<p>{{\Carbon\Carbon::parse($obituary->available_to)->format('F d, Y')}}</p>
			@else
			<input type="date" class="form-control" name="obituary_duration_end" value="{{$obituary->available_to}}">
			<span class="badge badge-danger w-100 validation-message">{{ $errors->first('obituary_duration_end') }}</span>
			@endif
		</div>
	</div>

	<hr class="hr-thick" style="border-color: #707070;">

	<h3>Memorial Service Details</h3>

	<div class="row">
		<div class="form-group col-12 col-lg-6">
			<label class="form-label font-weight-bold">Start Time</label>
			@if ($isShow)
			<p>{{\Carbon\Carbon::parse($obituary->begin_time)->format('h:i A')}}</p>
			@else
			<input type="time" class="form-control" name="begin_time" value="{{$obituary->begin_time}}">
			<span class="badge badge-danger w-100 validation-message">{{ $errors->first('begin_time') }}</span>
			@endif
		</div>
	</div>

	<div class="row">
		<div class="form-group col-12 col-lg-6">
			<label class="form-label font-weight-bold">Location</label>
			@if ($isShow)
			<p>{{$obituary->location}}</p>
			@else
			<input type="text" class="form-control" name="location" value="{{$obituary->location}}">
			<span class="badge badge-danger w-100 validation-message">{{ $errors->first('location') }}</span>
			@endif
		</div>

		<div class="form-group col-12 col-lg-6">
			<label class="form-label font-weight-bold">Address</label>
			@if ($isShow)
			<p>{{$obituary->address}}</p>
			@else
			<input type="text" class="form-control" name="address" value="{{$obituary->address}}">
			<span class="badge badge-danger w-100 validation-message">{{ $errors->first('address') }}</span>
			@endif
		</div>
	</div>

	@if (!$isShow)
	<div class="row">
		<div class="col-12">
			<button type="submit" class="btn btn-dark" data-action="update">Save Changes</button>
		</div>
	</div>
	@endif
</form>
@endsection

@section('script')
<script type="text/javascript">
	function openInput(obj) {
		$("[name=" + obj.attr("id") + "]").trigger("click");
	}

	function swapImgFile(obj) {
		if (obj.files && obj.files[0]) {
			let reader = new FileReader();

			reader.onload = function(e) {
				$("#image").attr("src", e.target.result);
				$("#image_label").html(obj.files[0].name);
				$("[name=isChanged]").val('1');
			}

			reader.readAsDataURL(obj.files[0])
		}
		else {
			$("#image").attr("src", "/uploads/obituary/{{$obituary->image_name}}");
			$("#image_label").html("UPLOAD IMAGE");
			$("[name=isChanged]").val('0');
		}
	}

	$(document).ready(() => {
		// Triggering click on the image box
		$("#image").on("click", (e) => {
			openInput($(e.currentTarget));
		});
		// Applying changes when file input changes
		$("#imgInput").on('change', (e) => {
			swapImgFile(e.currentTarget);
		});
	});
</script>
@endsection