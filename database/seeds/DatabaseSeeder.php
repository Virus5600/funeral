<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$this->call(StoresTableSeeder::class);

		$this->call(UsersTableSeeder::class);
		$this->call(SubscribersTableSeeder::class);
		$this->call(ShippingAddressTableSeeder::class);

		$this->call(CategoriesTableSeeder::class);
		$this->call(SubcategoriesTableSeeder::class);
		$this->call(ProductsTableSeeder::class);
		$this->call(ProductImagesTableSeeder::class);

		$this->call(ObituariesTableSeeder::class);

		$this->call(ServicesTableSeeder::class);

		$this->call(CartItemsTableSeeder::class);

		$this->call(LocationsTableSeeder::class);
	}
}