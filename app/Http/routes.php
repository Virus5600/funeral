<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
// export PATH=$PATH:"D:\wamp\bin\mysql\mysql5.7.31\bin"
// mysql -h j21q532mu148i8ms.cbetxkdyhwsb.us-east-1.rds.amazonaws.com -u teguambiojrxew3u -pd78v17f89meavlqr brxxddjv8ddn7in7 < drop.sql
// mysql -h j21q532mu148i8ms.cbetxkdyhwsb.us-east-1.rds.amazonaws.com -u teguambiojrxew3u -pd78v17f89meavlqr brxxddjv8ddn7in7 < soulace.sql

// Utility Functions
Route::group(['prefix' => 'utility'], function() {
	Route::post('/get-regions', 'UtilityFunctionController@getRegions')->name('utility.get_regions');
	Route::post('/get-cities', 'UtilityFunctionController@getCities')->name('utility.get_cities');
	Route::post('/get-subcategories', 'UtilityFunctionController@getSubcategories')->name('utility.get_subcategories');
	Route::post('/get-shipping-address', 'UtilityFunctionController@getShippingAddress')->name('utility.get_shipping_address');
	Route::post('/toggle-default-address', 'UtilityFunctionController@toggleDefaultAddress')->name('utility.toggle_default_address');
	Route::post('/validate-step-1', 'UtilityFunctionController@validateFirstStep')->name('utility.validate_step_1');
	Route::post('/update-privacy-read', 'UtilityFunctionController@updatePrivacyRead')->name('utility.update_privacy_read');
});

// Home/Index
Route::get('/', 'PageController@index')->name('home');
Route::get('/home', 'PageController@redirectToIndex');

// Store
Route::get('/store', 'PageController@store')->name('store.index');
Route::get('/store/{store_name}', 'PageController@storeShow')->name('store.show');

// Product
Route::get('/store/{store_name}/products/{category_name?}/{subcategory_name?}', 'PageController@products')->name('products.index');
Route::get('/store/{store_name}/products/{category_name}/item/{product_id}', 'PageController@productsShow')->name('products.show');

// Services
Route::get('/services', 'PageController@services')->name('services.index');
Route::get('/services/obituaries', 'PageController@servicesObituary')->name('services.obituary.index');
Route::get('/services/obituaries/request', 'PageController@servicesObituaryRequest')->name('services.obituary.request');
Route::post('/services/obituaries/request/store', 'PageController@serviceObituaryStore')->name('services.obituary.request.store');
Route::get('/services/obituaries/{id}', 'PageController@servicesObituaryShow')->name('services.obituary.show');
Route::get('/services/livestream', 'PageController@servicesLivestream')->name('services.livestream');

// About Us
Route::get('/about-us', 'PageController@aboutUs')->name('about-us');

// Contact Us
Route::get('/contact-us', 'PageController@contactUs')->name('contact-us');

// Subscribers (NEWS LETTER RELATED)
Route::post('/subscribe', 'SubscriberController@store')->name('subscriber.store');

// Login
Route::get('/login', 'UserController@login')->name('login');
Route::post('/authenticate', 'UserController@authenticate')->name('authenticate');

// Register
Route::get('/register', 'UserController@register')->name('register');
Route::post('/register-account', 'UserController@store')->name('register-account');
Route::get('/welcome', 'UserController@welcome')->name('welcome');
Route::get('/asd', function() {return redirect()->route('welcome')->with('isFirst', true);});

// Recover Password
Route::get('/recovery', 'UserController@recoverPassword')->name('recoverPass');
Route::post('/recovery/submit', 'UserController@recoverPasswordSubmit')->name('recoverPass.submit');

// Change Password
Route::get('/recovery/change-password', 'UserController@recoveryChangePassword')->name('recoverPass.changePass');
Route::post('/recovery/update-password', 'UserController@recoveryUpdatePassword')->name('recoverPass.updatePass');

// Livestream Access
Route::get('/livestreams/{id}', 'PageController@showLivestream')->name('livestream.show');

///////////////////////////
// AUTHENTICATION NEEDED //
///////////////////////////
Route::group(['middleware' => ['auth']], function() {
	// Email Verification
	Route::get('/verification-page/{code?}', 'EmailVerificationController@verificationForm')->name('email-verification.form');
	Route::get('/verify/{code?}', 'EmailVerificationController@verifyCode')->name('email-verification.verify');
	Route::post('/resend-code', 'EmailVerificationController@resend')->name('email-verification.resend');

	// Account
	Route::get('/account', 'UserController@index')->name('account');
	Route::get('/account/edit', 'UserController@edit')->name('account.edit');
	Route::post('/account/edit/update', 'UserController@update')->name('account.update');
	Route::get('/logout', 'UserController@logout')->name('logout');

	// Account - Order Tracking
	Route::get('/account/orders', 'UserController@orderTrackingIndex')->name('account.orders');
	Route::get('/account/orders/{id}', 'UserController@orderTrackingShow')->name('account.orders.show');
	Route::get('/account/orders/cancel/{id}', 'SalesOrderController@cancel')->name('account.orders.cancel');
	Route::get('/account/orders/{id}/upload-proof', 'SalesOrderController@uploadProof')->name('account.orders.upload_proof');
	Route::get('/account/orders/{id}/received', 'SalesOrderController@orderReceived')->name('account.orders.received');
	Route::post('/upload-proof/{id}/orders', 'SalesOrderController@postUploadProof')->name('upload-proof.orders');

	// Account - Livestreams
	Route::get('/account/livestreams', 'UserController@livestreamTrackingIndex')->name('account.livestreams');
	Route::get('/account/livestreams/{id}', 'UserController@livestreamTrackingShow')->name('account.livestreams.show');
	Route::get('/account/livestreams/cancel/{id}', 'UserController@cancelLivestream')->name('account.livestreams.cancel');
	Route::get('/account/livestreams/{id}/upload-proof', 'UserController@uploadProof')->name('account.livestreams.upload_proof');
	Route::post('/upload-proof/{id}/livestream', 'UserController@postUploadProof')->name('upload-proof.livestreams');

	// Shipping Address
	Route::post('shipping-address/store', 'ShippingAddressController@store')->name('shipping-address.store');
	Route::post('shipping-address/update/{id}', 'ShippingAddressController@update')->name('shipping-address.update');
	Route::post('shipping-address/delete/{id}', 'ShippingAddressController@delete')->name('shipping-address.delete');

	// Cart
	Route::get('/cart', 'CartController@index')->name('cart.index');
	Route::get('/cart/add/{id}', 'CartController@addToCart')->name('cart.addToCart');
	Route::get('/cart/addToExisting/{id}', 'CartController@addToExisting')->name('cart.addToExisting');
	Route::get('/cart/subtractToExisting/{id}', 'CartController@subtractToExisting')->name('cart.subtractToExisting');
	Route::get('/cart/{id}/delete', 'CartController@delete')->name('cart.delete');
	Route::get('/cart/livestream/addToExisting/{id}', 'LivestreamManagementController@addToExisting')->name('cart.livestream.addDuration');
	Route::get('/cart/livestream/subtractToExisting/{id}', 'LivestreamManagementController@subtractToExisting')->name('cart.livestream.subtractDuration');
	Route::get('/cart/livestream/{id}/delete', 'LivestreamManagementController@delete')->name('cart.livestream.delete');

	// Billing
	Route::get('/check-out', 'CheckoutController@index')->name('billing');
	Route::post('/billing-payment', 'CheckoutController@payment')->name('billing-payment');
	Route::get('/billing-payment', 'CheckoutController@showPayment')->name('billing-payment-show');
	Route::post('/billing-review', 'CheckoutController@review')->name('billing-review');
	Route::get('/billing-review', 'CheckoutController@showReview')->name('billing-review-show');
	Route::post('/billing-order', 'CheckoutController@order')->name('billing-order');
	Route::get('/billing-livestream', 'CheckoutController@livestreamBilling')->name('billing.livestream');
	Route::post('/billing-livestream/finished', 'CheckoutController@livestreamBillingDone')->name('billing.livestream.finished');

	// Livestream access
	Route::get('/livestream/{id}/delete', 'UserController@deleteLivestream')->name('livestream.delete');
	Route::post('/livestream/create', 'UserController@addLivestreamToCart')->name('livestream.create');
	Route::post('/update-options/{id}', 'UtilityFunctionController@updateLivestreamOption')->name('livestream.options.update');
	Route::post('/token-extend', function() {return csrf_token();})->name('token-extend');

	// Mailing
	Route::post('/inquiry/send', 'PageController@sendInquiry')->name('mail.inquiry.send');

	////////////////////////////////
	////////// ADMIN SIDE //////////
	////////////////////////////////
	Route::group(['middleware' => ['admin'], 'prefix' => 'admin'], function() {
		// Dashboard
		Route::get('/', 'PageController@redirectToAdmin')->name('admin');
		Route::get('/dashboard', 'PageController@adminDashboard')->name('admin.dashboard');

		// Sales Order
		Route::get('/sales-order/{id}/print', 'SalesOrderController@prints')->name('admin.sales-order.print');
		Route::get('/sales-order/{id}/accept', 'SalesOrderController@acceptProof')->name('admin.sales-order.proof.accept');
		Route::get('/sales-order/{id}/reject', 'SalesOrderController@rejectProof')->name('admin.sales-order.proof.reject');
		Route::resource('sales-order', 'SalesOrderController');

		// Store
		Route::get('/store/{store_id}/show/{id}', 'StoreController@showSalesOrder')->name('admin.store.sales-order.show');
		Route::get('/store/{store_id}/show/{id}/print', 'StoreController@printSalesOrder')->name('admin.store.sales-order.print');
		Route::resource('store', 'StoreController');
		Route::post('/store/{id}', 'StoreController@update')->name('admin.store.update');
		Route::get('/store/{id}/delete', 'StoreController@delete')->name('admin.store.delete');

		// Store-Map
		Route::get('/store/{id}/map', 'StoreController@mapShow')->name('admin.store.map.show');
		Route::get('/store/{id}/map/edit', 'StoreController@mapEdit')->name('admin.store.map.edit');
		Route::post('/store/{id}/map/update', 'StoreController@mapUpdate')->name('admin.store.map.update');

		// User-Management
		Route::resource('user-management', 'UserManagementController');
		Route::get('/user-management/{id}/delete', 'UserManagementController@delete')->name('admin.user-management.delete');
		Route::post('/user-management/{id}/update', 'UserManagementController@update')->name('admin.user-management.update');

		// Services
		Route::get('/services', 'ServicesController@index')->name('admin.services.index');
	    Route::post('/services/store', 'ServicesController@store')->name('admin.services.store');
	    Route::get('/services/{service_name}', 'ServicesController@show')->name('admin.services.show');
	    Route::get('/services/{service_name}/edit', 'ServicesController@edit')->name('admin.services.edit');
	    Route::post('/services/{id}/update', 'ServicesController@update')->name('admin.services.update');
	    // Route::get('/services/{id}/delete', 'ServicesController@delete')->name('admin.services.delete');

		// Livestream Management
		Route::resource('livestream-management', 'LivestreamManagementController');
		Route::get('/livestream-management/{id}/delete', 'LivestreamManagementController@delete')->name('admin.livestream-management.delete');
		Route::get('/livestream/{id}/approve', 'LivestreamManagementController@approve')->name('livestream.approve');
		Route::get('/livestream/{id}/cancel', 'LivestreamManagementController@cancel')->name('livestream.cancel');
		Route::get('/livestream/{id}/streaming', 'LivestreamManagementController@streaming')->name('livestream.streaming');
		Route::get('/livestream/{id}/done', 'LivestreamManagementController@done')->name('livestream.done');
		Route::get('/livestrea/options/get/{id}', 'UtilityFunctionController@getLivestreamOptions')->name('livestream.options.get');
	});

	////////////////////////////////
	////////// OWNER SIDE //////////
	////////////////////////////////
	Route::group(['middleware' => ['owner'], 'prefix' => 'owner'], function() {
		// Index
		Route::get('/', 'PageController@redirectToOwner')->name('owner');
		Route::get('/dashboard', 'PageController@ownerDashboard')->name('owner.dashboard');

		// Sales Order
		Route::get('/sales-order/{id}/print', 'OwnerSalesOrderController@prints')->name('owner.sales-order.print');
		Route::get('/sales-order/{id}/accept', 'OwnerSalesOrderController@acceptProof')->name('owner.sales-order.proof.accept');
		Route::get('/sales-order/{id}/reject', 'OwnerSalesOrderController@rejectProof')->name('owner.sales-order.proof.reject');
		Route::post('/sales-order/{id}/tracking-number/add', 'OwnerSalesOrderController@addTrackingNumber')->name('owner.sales-order.tracking_number.add');
		Route::post('/sales-order/{id}/tracking-number/update', 'OwnerSalesOrderController@updateTrackingNumber')->name('owner.sales-order.tracking_number.update');
		Route::resource('sales-order', 'OwnerSalesOrderController');

		// Category
		Route::get('/products', 'OwnerProductCategoryController@index')->name('owner.category.index');
		Route::get('/products/create', 'OwnerProductCategoryController@create')->name('owner.category.create');
		Route::post('/products/category/{id}/update', 'OwnerProductCategoryController@update')->name('owner.category.update');
		Route::get('/products/category/{id}/delete', 'OwnerProductCategoryController@delete')->name('owner.category.delete');

		// Subcategory
		Route::get('/products/{category}/', 'OwnerProductSubCategoryController@index')->name('owner.subcategory.index');
		Route::get('/products/{category}/create', 'OwnerProductSubCategoryController@create')->name('owner.subcategory.create');
		Route::get('/products/subcategory/{id}/delete', 'OwnerProductSubCategoryController@delete')->name('owner.subcategory.delete');

		// Product
		Route::get('/products/{id}/delete', 'OwnerProductController@delete')->name('owner.product.delete');
		Route::get('/products/{category}/{subcategory}/', 'OwnerProductController@index')->name('owner.product.index');
		Route::get('/products/{category}/{subcategory}/create', 'OwnerProductController@create')->name('owner.product.create');
		Route::post('/products/store', 'OwnerProductController@store')->name('owner.product.store');
		Route::get('/products/{category}/{subcategory}/{product}', 'OwnerProductController@show')->name('owner.product.show');
		Route::get('/products/{category}/{subcategory}/{product}/edit', 'OwnerProductController@edit')->name('owner.product.edit');
		Route::post('/products/update', 'OwnerProductController@update')->name('owner.product.update');

		// Obituary
		Route::get('/obituary', 'OwnerObituaryController@index')->name('owner.obituary.index');
		Route::get('/obituary/create', 'OwnerObituaryController@create')->name('owner.obituary.create');
		Route::post('/obituary/store', 'OwnerObituaryController@store')->name('owner.obituary.store');
		Route::get('/obituary/{name}', 'OwnerObituaryController@show')->name('owner.obituary.show');
		Route::get('/obituary/{name}/edit', 'OwnerObituaryController@edit')->name('owner.obituary.edit');
		Route::post('/obituary/{id}/update', 'OwnerObituaryController@update')->name('owner.obituary.update');
		Route::get('/obituary/{name}/delete', 'OwnerObituaryController@delete')->name('owner.obituary.delete');
		Route::get('/obituary/{id}/status-update/{status}', 'OwnerObituaryController@updateStatus')->name('owner.obituary.update-status');

		// Contact
		Route::get('/contact', 'PageController@ownerContact')->name('owner.contact');
		Route::post('/contact/update', 'PageController@ownerContactUpdate')->name('owner.contact.update');
	});
});