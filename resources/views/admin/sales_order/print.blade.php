@extends('template.print-details')

@section('title', 'Print Sales Order Details')
@section('sidebar', 'admin')

@section('body')
<h3 class="font-weight-normal text-center">Order No. {{$livestream->id}}</h3>
<br>
<div class="row px-0">
	<div class="col-6 d-flex flex-d-col">
		<span class="font-weight-bold">Customer Details</span>
		<span class="pl-3">{{$livestream->ls_user->getName()}}</span>
		<span class="pl-3">(+63){{$livestream->ls_user->contact_number}}</span>
		<span class="pl-3">{{$livestream->ls_user->email}}</span>
	</div>

	<div class="col-6 d-flex flex-d-col">
		<span class="font-weight-bold">Billing Details</span>
		<span class="pl-3">{{$livestream->ls_user->getName()}}</span>
		<span class="pl-3">{{$livestream->ls_user->getAddress(true, true)}}</span>
		<span class="pl-3">{{$livestream->ls_user->city}}, {{$livestream->ls_user->region}}</span>
	</div>
</div>
<br>
<div class="row px-0">
	<div class="col-6 d-flex flex-d-col">
		<span class="font-weight-bold">Payment Method</span>
		<span class="pl-3">Bank Transfer</span>
	</div>
</div>
<br>
<h5><b>Description:</b></h5>
<p>{{ $livestream->description }}</p>
<div class="row px-0">
	<div class="col-12">
		<table class="table table-print">
			<thead class="text-center" style="border: inherit!important;">
				<tr>
					<td>Category</td>
					<td>Title</td>
					<td>Price</td>
					<td>Duration</td>
					<td>Total Amount</td>
				</tr>
			</thead>

			<tbody class="text-center">
				<tr>
					<td>{{ucwords($livestream->category == null ? 'None' : $livestream->category)}}</td>
					<td>{{$livestream->title}}</td>
					<td>₱{{number_format(100, 2)}}</td>
					<td>{{$livestream->duration}}</td>
					<td>₱{{number_format($livestream->price, 2)}}</td>
				</tr>

				<tr>
					<td colspan="3" rowspan="4">
						@if ($livestream->proof_of_payment != null)
						<p class="text-left">Proof of Payment:</p>
						<img src="/proof/livestreams/{{$livestream->proof_of_payment}}" class="border img-fluid">
						@endif
					</td>
					<td>Subtotal:</td>
					<td>₱{{number_format($livestream->price, 2)}}</td>
				</tr>

				<tr>
					<td>Tax:</td>
					<td>₱0.00</td>
				</tr>

				<tr>
					<td colspan="2">₱{{number_format($livestream->price, 2)}}</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>
@endsection