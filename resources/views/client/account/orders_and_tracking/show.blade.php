@extends('template.client.account')

@section('subtitle', 'My Account')

@section('page-css')
<link rel="stylesheet" href="/css/order_tracking.show.css" >
@endsection

@section('sub-body')
<div class="container mt-3 pt-2 bg-light">
	<div class="itemrow row">
		<div class="col-3"> 
			<h5><a href="{{ route('account.orders') }}" class="font-weight-bold text-decoration-none text-dark"><i class="bi bi-chevron-left text-dark"></i>Back</a></h5>
		</div>
		<div class="col-9 right-align-items px-4">
			<h5><b>ORDER ID. {{$salesOrder->id}}</b></h5>
		</div>
	</div>
</div>
<div class="row mt-5">
	@if ($salesOrder->status > 0)
	<div class="step-progress w-75 mx-auto mb-3" data-progress='{{$salesOrder->status-1}}'>
		<div class="component">
			<div class="bar">
				<hr class="bar-progress">
				<hr class="bar-remaining">
			</div>

			<div class="steps">
				<div class="step-icon">
					<img src="/images/UI/order_payment.png" alt="Payment">
				</div>
				<div class="step-icon">
					<img src="/images/UI/order_processing.png" alt="Processing">
				</div>
				<div class="step-icon">
					<img src="/images/UI/order_shipped.png" alt="Shipped">
				</div>
				<div class="step-icon">
					<img src="/images/UI/order_received.png" alt="Received">
				</div>
				<div class="step-icon">
					<img src="/images/UI/order_complete.png" alt="Complete">
				</div>
			</div>
		</div>

		<div class="labels">
			@foreach (\App\SalesOrder::getStatusList() as $s)
			<div class="step-label">
				{{$s}}
			</div>
			@endforeach
		</div>
	</div>
	@else
	<div class="w-75 mx-auto mb-3 d-flex flex-d-col align-items-center">
		<div class="step-icon">
			<img src="/images/UI/order_cancelled.png" alt="">
		</div>
		<div class="step-label">
			Cancelled
		</div>
	</div>
	@endif
</div>

<div class="container itemrow bg-light p-3">
	<div class="row p-3">
		<div class="col-md-6 mt-2" id="order_info">
			@if ($salesOrder->status == 1)
			<div class="row">
				<div class="col-12 d-flex flex-column">
					<h5 class="mx-auto">You haven't uploaded the proof yet.</h5>
					<a href="{{ route('account.orders.upload_proof', [$salesOrder->id]) }}" class="btn btn-dark mx-auto">Upload Proof</a>
				</div>
			</div>
			@elseif ($salesOrder->status == 2)
			<div class="d-flex flex-column h-100">
				<h5 class="m-auto">Waiting for confirmation from the store...</h5>
			</div>
			@elseif ($salesOrder->status == 3)
			<div class="d-flex flex-column h-100">
				<div class="m-auto">
					<h5 class="text-center">Proof of payment accepted!</h5>
					<p class="text-center">Shipping the order to {{$salesOrder->courier}}</p>
				</div>
			</div>
			@elseif ($salesOrder->status == 4)
			<div class="row">
				<div class="col">
					<b>Received by {{$salesOrder->getETA()}}</b>
					<p class="text-muted">Shipped with {{$salesOrder->courier}}</p>
				</div>
			</div>

			<div class="row">
				<div class="col-8">
					<p>
						<b>Tracking Number</b><br>
						<span class="text-muted">{{$salesOrder->tracking_number}}</span>
						<input type="hidden" id="tracking_number" value="{{$salesOrder->tracking_number}}">
					</p>
				</div>

				<div class="col-4 right-align-items">
					<button class="btn btn-dark" onclick="copyToClipboard(this);" data-copy-target="#tracking_number">Copy</button>
				</div>

			</div>
			<div class="row">
				<div class="col-8">
					<a href="{{ route('account.orders.received', [$salesOrder->id]) }}" class="btn btn-dark"><i class="fas fa-check-circle mr-2"></i>Package Received</a>
				</div>
			</div>
			@elseif ($salesOrder->status == 5)
			<div class="row mt-3">
				<p>
					<b>Delivery Address</b><br>
					<span class="text-muted">
						{{$salesOrder->shippingAddress->getName()}}<br>
						(+63) {{$salesOrder->shippingAddress->contact_number}} <br>
						{{$salesOrder->shippingAddress->getAddress()}}
					</span>
				</p>
			</div>
			@endif
		</div>
			
		<div class="col-md-6 mt-2">
			<p>
				Logs<br>
				@forelse ($salesOrder->logs->reverse() as $l)
				@if ($l->isAccepted())
				<i class="fas fa-check-circle text-success mx-2"></i>{{$l->updated_at == null ? \Carbon\Carbon::parse($l->updated_at)->format('M d, Y') : \Carbon\Carbon::parse($l->created_at)->format('M d, Y')}} - {{\Carbon\Carbon::parse($l->time)->format('H:i A')}} -  {{$l->getDescription()}}<br>
				@elseif ($l->isRejected())
				<i class="fas fa-times-circle text-danger mx-2"></i>{{$l->updated_at == null ? \Carbon\Carbon::parse($l->updated_at)->format('M d, Y') : \Carbon\Carbon::parse($l->created_at)->format('M d, Y')}} - {{\Carbon\Carbon::parse($l->time)->format('H:i A')}} -  {{$l->getDescription()}}<br>
				@else
				<i class="fas fa-info-circle text-dark mx-2"></i>{{$l->updated_at == null ? \Carbon\Carbon::parse($l->updated_at)->format('M d, Y') : \Carbon\Carbon::parse($l->created_at)->format('M d, Y')}} - {{\Carbon\Carbon::parse($l->time)->format('H:i A')}} -  {{$l->getDescription()}}<br>
				@endif
				@empty
				<i class="fas fa-info-circle fa-lg text-dark mx-2"></i>Nothing has happened yet...
				@endforelse
			</p>
		</div>
	</div>
</div>

@foreach ($salesOrder->items as $i)
<div class="row my-2 order_items text-white">
	<div class="col-md-2 col-12 mb-4">
		<img src="/uploads/products/{{$i->product->category->category_name}}/{{$i->product->image_name}}" alt="{{$i->product->product_name}}" class="img-fluid">
	</div>
	<div class="col-md-4 col-12 mt-3 mt-md-0">
		<h5>{{$i->product->product_name}}</h5>
	</div>
	<div class="col-md-6 col-12 right-align-items">
		<br><br>
		<small>x{{$i->quantity}}</small>
		<h4><b>&#8369;{{number_format($i->price, 2)}}</b></h4>
	</div>
</div>
@endforeach

<div class="row px-3 my-4">
	<div class="col-6">
		<p class="text-muted">
			Product Subtotal: <br>
			<!-- Promo Code Applied: <br> -->
			Shipping Fee:
		</p>
		<h5><b>Order Total:</b></h5>

		<div class="col-6 right-align-items">
			<p class="text-muted">
				&#8369;{{number_format($salesOrder->total, 2)}}<br>
				<!-- ₱0.00 <br> -->
				&#8369;0.00 <br>
			</p>
			<h5><b>&#8369;{{number_format($salesOrder->total, 2)}}</b></h5>
		</div>
	</div>
</div>
@endsection

@section('subscript')
<script>
$(document).ready(() => {stepProgressInit();});

// COPYING
function copyToClipboard(element) {
	let temp = $("<input>");
	$("body").append(temp);

	let textToCopy;
	if (typeof $(element).attr('data-copy-target') != 'undefined')
		textToCopy = $($(element).attr('data-copy-target')).val();
	else if (typeof $(element).attr('data-copy-text') != 'undefined')
		textToCopy = $(element).attr('data-copy-text');
	else
		textToCopy = $(element).val();

	temp.val(textToCopy).select();
	document.execCommand("copy");
	temp.remove();
	
	Swal.fire({
		title: `Text copied`,
		position: `bottom`,
		showConfirmButton: false,
		toast: true,
		timer: 3750,
		background: `#28a745`,
		customClass: {
			title: `text-white`,
			popup: `px-0`
		},
		width: 150
	});
}

// STEP PROGRESS
function stepProgressInit() {
	let obj = $('.step-progress');
	let bars = obj.find('.bar').children();
	let steps = obj.find('.steps').children();
	let labels = obj.find('.labels').children();
	let stepCount = steps.length-1;
	// In percentage
	let progress = (obj.attr('data-progress')*100)/stepCount;
	let remaining = 100-progress;

	// Set length and height of steps
	obj.find('.steps').css('width', obj.find('.bar').width());
	obj.find('.steps').css('height', obj.find('.bar').height());

	// Attaching a resize event listener to .steps
	$(window).resize((e) => {
		obj.find('.steps').css('width', obj.find('.bar').width());
		obj.find('.steps').css('height', obj.find('.bar').height());
	});

	// Placing the step milestone
	steps.css('width', (100/stepCount) + "%");
	$(steps[stepCount]).css('width', '');

	// Setting the color for step milestone
	for (let i = 0; i <= obj.attr('data-progress'); i++)
		$(steps[i]).addClass('active');

	// Setting the length of the bar
	$(bars[0]).css('width', progress + "%");
	$(bars[1]).css('width', remaining + "%");

	// Setting the labels
	labels.css('width', (100/stepCount) + "%");
};
</script>

<script type="text/javascript">
	var nav = document.querySelector('nav');

	window.addEventListener('scroll', function () {
		if (window.pageYOffset > 100) {
			nav.classList.add('bg-dark', 'shadow');
		} else {
			nav.classList.remove('bg-dark', 'shadow');
		}
	});
</script>

<script>
	document.getElementById("togglemenu").addEventListener("click", function() {
		document.getElementById("navbarmain").classList.add('bg-dark', 'shadow');
	});
</script>
@endsection