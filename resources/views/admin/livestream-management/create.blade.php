@extends('template.admin')

@section('title', 'Create Livestream')
@section('sidebar', 'admin')

@section('body')
<h1 class="font-weight-bold"><a class="text-dark" href="javascript:void(0);" onclick="confirmLeave('{{route('admin.livestream-management.index')}}');"><i class="fas fa-chevron-left mr-2"></i>Create Livestream</a></h1>

<hr class="hr-thick" style="border-color: #707070;">

<form action="{{ route('admin.livestream-management.store') }}" method="POST" class="form w-lg-50" enctype="multipart/form-data">
	{{csrf_field()}}

	<div class="row">
		<div class="form-group col-12 col-lg-6">
			<label class="form-label font-weight-bold">Title</label>
			<input type="text" class="form-control" name="title" value="{{old('title')}}">
			<span class="badge badge-danger w-100 validation-message">{{$errors->first('title')}}</span>
		</div>
	</div>

	<div class="row">
		<div class="form-group col-12 col-lg-6">
			<label class="form-label font-weight-bold">Description</label>
			<textarea name="description" class="form-control not-resizable">{{old('description')}}</textarea>
			<span class="badge badge-danger w-100 validation-message">{{$errors->first('description')}}</span>
		</div>
	</div>

	<div class="row">
		<div class="form-group col-12 col-lg-6">
			<label class="form-label my-0 h4 mb-2" for="category">Category</label>
			<select class="custom-select" name="category" id="category">
				<option class="text-dark" value="none">None</option>
				@foreach (App\Livestream::getCategoryList() as $c)
				<option class="text-dark" value="{{strtolower($c)}}" {{ old('category') == strtolower($c) ? 'selected' : '' }}>{{ $c }}</option>
				@endforeach
			</select>
			<span class="badge badge-danger w-100 validation-message">{{$errors->first('category')}}</span>
		</div>
	</div>

	<hr class="hr-thick-50 border-color-custom">
	<div class="row">
		<div class="form-group col-12 col-lg-6 d-flex flex-column flex-d-lg-row">
			<div class="d-flex justify-content-center align-items-center">
				<label class="form-label my-0 mx-3 h4" for="schedule_date">Date:</label>
			</div>
			<input type="date" class="form-control text-right" id="schedule_date" name="schedule_date" min="{{\Carbon\Carbon::now()->format('Y-m-d')}}" value="{{ old('schedule_date') != null ? old('schedule_date') : \Carbon\Carbon::now()->format('Y-m-d') }}">
			<span class="badge badge-danger w-100 validation-message">{{$errors->first('schedule_date')}}</span>
		</div>
	</div>

	<div class="row">
		<div class="form-group col-12 col-lg-6 d-flex flex-column flex-d-lg-row">
			<div class="d-flex justify-content-center align-items-center">
				<label class="form-label my-0 mx-3 h4" for="schedule_time">Time:</label>
			</div>
			<input type="time" class="form-control text-right" id="schedule_time" name="schedule_time" value="{{ old('schedule_time') != null ? old('schedule_time') : \Carbon\Carbon::now()->addHour()->timezone('Asia/Manila')->format('H:i') }}">
			<span class="badge badge-danger w-100 validation-message">{{$errors->first('schedule_time')}}</span>
		</div>
	</div>

	<div class="row">
		<div class="form-group col-12 col-lg-6 d-flex flex-column flex-d-lg-row">
			<div class="d-flex justify-content-center align-items-center">
				<label class="form-label my-0 mx-3 h4" for="duration">Duration:</label>
				<span class="badge badge-danger w-100 validation-message">{{$errors->first('duration')}}</span>
			</div>
			<div class="input-group">
				<input type="number" class="form-control text-right" id="duration" name="duration" min="1" value="{{ old('duration') != null ? old('duration') : '1' }}">
				<div class="input-group-append">
					<span class="input-group-text">hour(s)</span>
				</div>
			</div>
			<span class="badge badge-danger w-100 validation-message">{{$errors->first('schedule_time')}}</span>
		</div>
	</div>

	<div class="row">
		<div class="form-group col-12 col-lg-6 d-flex flex-column flex-d-lg-row">
			<div class="d-flex justify-content-center align-items-center">
				<label class="form-label my-0 mx-3 h4" for="price">Price:</label>
			</div>
			<div class="input-group">
				<div class="input-group-prepend">
					<span class="input-group-text">₱</span>
				</div>
				<input type="number" class="form-control text-right" id="price" name="price" min="100" value="{{ old('price') != null ? old('price') : '100' }}" readonly>
				<div class="input-group-append">
					<span class="input-group-text">.00</span>
				</div>
				<span class="badge badge-danger w-100 validation-message">{{$errors->first('schedule_time')}}</span>
			</div>
		</div>
	</div>


	<hr class="hr-thick-50 border-color-custom">
	<div class="row">
		<div class="form-group col-12 col-lg-6">
			<label class="form-label font-weight-bold">User</label>
			<select class="custom-select" name="user_id">
				@foreach($users as $x)
					<option value="{{$x->id}}">{{$x->getName()}} ({{$x->email}})</option>
				@endforeach
			</select>
			<span class="badge badge-danger w-100 validation-message">{{$errors->first('user_type')}}</span>
		</div>

		
	</div>

	<br>
	<div class="row">
		<div class="col-12">
			<button type="submit" class="btn btn-dark" data-action="submit">Save Changes</button>
		</div>
	</div>
</form>
@endsection

@section('script')
<script type="text/javascript">
	$(document).ready(function() {
		$('#duration').on('change', (e) => {
			$('#price').val(parseFloat($(e.target).val() * 100).toFixed(2));
		});
	});
</script>
@endsection