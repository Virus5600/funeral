<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Http\Requests;

use App\Store;
use App\User;

use DB;
use Hash;
use Log;
use Validator;

class UserManagementController extends Controller
{
	protected function index() {
		$user = User::query();
		$search = null;

		if (request()->has('s')) {
			$search = request()->get('s');
			$user = $user->leftJoin('stores', 'stores.id', '=', 'users.store_id');

			$user = $user->where('users.first_name', 'LIKE', '%'.$search.'%')
				->orWhere('users.middle_name', 'LIKE', '%'.$search.'%')
				->orWhere('users.last_name', 'LIKE', '%'.$search.'%')
				->orWhere('users.contact_number', 'LIKE', '%'.$search.'%')
				->orWhere('users.address', 'LIKE', '%'.$search.'%')
				->orWhere('users.region', 'LIKE', '%'.$search.'%')
				->orWhere('users.city', 'LIKE', '%'.$search.'%')
				->orWhere('users.zip_code', 'LIKE', '%'.$search.'%')
				->orWhere('users.email', 'LIKE', '%'.$search.'%')
				->orWhere('stores.name', 'LIKE', '%'.$search.'%')
				->select('users.*')
				->distinct();
		}

		return view('admin.user-management.index', [
			'search' => $search,
			'users' => $user->paginate(15)
		]);
	}

	protected function create() {
		return view('admin.user-management.create', [
			'stores' => Store::get()
		]);
	}

	protected function store(Request $req) {
		$validator = Validator::make($req->all(), [
			'first_name' => 'required|min:2',
			'last_name' => 'required|min:2',
			'email' => 'required|email',
			'contact_number' => 'required|numeric|min:10',
			'address' => 'required|min:5',
			'region' => 'required',
			'city' => 'required',
			'zip_code' => 'numeric',
			'user_type' => array('required', 'regex:/(^admin$|^client$|^owner$)/'),
			'store_owner' => 'required_if:user_type,owner'
		], [
			'first_name.required' => 'First Name is required.',
			'first_name.min' => 'Please provide a proper name.',
			'last_name.required' => 'Last Name is required.',
			'last_name.min' => 'Please provide a proper name.',
			'email.required' => 'E-mail is required.',
			'email.email' => 'E-mail provided is not a valid e-mail.',
			'contact_number.required' => 'Contact number is required.',
			'contact_number.numeric' => 'Contact number must only contains number.',
			'contact_number.min' => 'Contact number provided is invalid.',
			'address.required' => 'Address is required.',
			'address.min' => 'Please provide your proper address.',
			'region.required' => 'Region is required.',
			'city.required' => 'City/Province is required.',
			'zip_code.numeric' => 'Zip Codes contains numbers only.',
			'user_type.required' => 'User type is required.',
			'user_type.regex' => 'Please refrain from modifying the page.',
			'store_owner.required_if' => 'Please select a store that this user owns.'
		]);
		
		if ($validator->fails())
			return redirect()
				->back()
				->withErrors($validator)
				->withInput();

		try {
			DB::beginTransaction();

			$pass = Str::random(15);
			// 7oeyXCbL74Nh6wh
			$user = User::create([
				'first_name' => $req->first_name,
				'middle_name' => $req->middle_name,
				'last_name' => $req->last_name,
				'suffix' => $req->suffix,
				'email' => $req->email,
				'password' => Hash::make($pass),
				'contact_number' => $req->contact_number,
				'address' => $req->address,
				'region' => $req->region,
				'city' => $req->city,
				'zip_code' => $req->zip_code,
				'type' => $req->user_type,
			]);

			if ($req->user_type == 'owner') {
				$user->store_id = $req->store_owner;
				$user->save();
			}

			DB::commit();
		} catch (\Exception $e) {
			Log::error($e);
			DB::rollback();

			return redirect()
				->back()
				->with('flash_error', 'Something went wrong, please try again later.')
				->withInput();
		}

		return redirect()
			->route('admin.user-management.index')
			->with('flash_success', 'Successfully added "' . $req->first_name . ' ' . $req->last_name . '."')
			->with('has_icon', 'true')
			->with('message', '<p>Password: <b>' . $pass . '</b></p>')
			->with('position', 'center')
			->with('is_toast', 'false')
			->with('has_timer', 'false');
	}


	protected function show($id) {
		return view('admin.user-management.edit', [
			'user' => User::find($id),
			'stores' => Store::get(),
			'isShow' => true
		]);
	}

	protected function edit($id) {
		return view('admin.user-management.edit', [
			'user' => User::find($id),
			'stores' => Store::get(),
			'isShow' => false
		]);
	}

	protected function update(Request $req, $id) {
		$validator = Validator::make($req->all(), [
			'first_name' => 'required|min:2',
			'last_name' => 'required|min:2',
			'email' => 'required|email',
			'contact_number' => 'required|numeric|min:10',
			'address' => 'required|min:5',
			'region' => 'required',
			'city' => 'required',
			'zip_code' => 'numeric',
			'user_type' => array('required', 'regex:/(^admin$|^client$|^owner$)/'),
			'store_owner' => 'required_if:user_type,owner'
		], [
			'first_name.required' => 'First Name is required.',
			'first_name.min' => 'Please provide a proper name.',
			'last_name.required' => 'Last Name is required.',
			'last_name.min' => 'Please provide a proper name.',
			'email.required' => 'E-mail is required.',
			'email.email' => 'E-mail provided is not a valid e-mail.',
			'contact_number.required' => 'Contact number is required.',
			'contact_number.numeric' => 'Contact number must only contains number.',
			'contact_number.min' => 'Contact number provided is invalid.',
			'address.required' => 'Address is required.',
			'address.min' => 'Please provide your proper address.',
			'region.required' => 'Region is required.',
			'city.required' => 'City/Province is required.',
			'zip_code.numeric' => 'Zip Codes contains numbers only.',
			'user_type.required' => 'User type is required.',
			'user_type.regex' => 'Please refrain from modifying the page.',
			'store_owner.required_if' => 'Please select a store that this user owns.'
		]);

		if ($validator->fails())
			return redirect()
				->back()
				->withErrors($validator)
				->withInput();

		try {
			DB::beginTransaction();

			$user = User::find($id);
			$user->first_name = $req->first_name;
			$user->last_name = $req->last_name;
			$user->email = $req->email;
			$user->contact_number = $req->contact_number;
			$user->address = $req->address;
			$user->region = $req->region;
			$user->city = $req->city;
			$user->zip_code = $req->zip_code;
			$user->type = $req->user_type;
			if ($req->user_type == 'owner')
				$user->store_id = $req->user_type == 'owner' ? $req->store_owner : NULL;
			$user->save();

			DB::commit();
		} catch (\EWxception $e) {
			Log::error($e);
			DB::rollback();

			return redirect()
				->back()
				->with('flash_error', 'Something went wrong, please try again later.');
		}

		return redirect()
			->route('admin.user-management.index')
			->with('flash_success', 'Successfully updated "' . $user->first_name . ' ' . $user->last_name . '."');
	}

	protected function delete($id) {
		User::find($id)->delete();

		return redirect()
			->back()
			->with('flash_success', 'Successfully removed user.');
	}
}