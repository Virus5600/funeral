<?php

namespace App;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;

use Carbon\Carbon;

class CartItem extends Model
{
	protected $fillable = [
		'user_id',
		'product_id',
		'quantity'
	];

	protected function user() {
		return $this->belongsTo('App\User');
	}

	protected function product() {
		return $this->belongsTo('App\Product');
	}

	public function total() {
		return ($this->product->selling_price * $this->quantity);
	}

	public function cartTotal() {
		$total = 0.00;
		foreach (Auth::user()->cartItem as $c)
			$total += $c->total();

		return $total;
	}
}