<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Requests;

use App\ShippingAddress;

use Validator;
use Log;
use DB;

class ShippingAddressController extends Controller
{
	protected function store(Request $req) {

		$validator = Validator::make($req->all(), [
			'first_name' => 'required|min:2',
			'last_name' => 'required|min:2',
			'contact_number' => 'required|numeric|min:10',
			'address' => 'required|min:5',
			'barangay' => 'required|min:3',
			'city' => 'required',
			'zip_code' => 'numeric',
			'region' => 'required',
			'address_name' => 'required'
		], [
			'first_name.required' => 'First Name is required.',
			'first_name.min' => 'Please provide a proper name.',
			'last_name.required' => 'Last Name is required.',
			'last_name.min' => 'Please provide a proper name.',
			'contact_number.required' => 'Contact number is required.',
			'contact_number.numeric' => 'Contact number must only contains number.',
			'contact_number.min' => 'Contact number provided is invalid.',
			'address.required' => 'Address is required.',
			'address.min' => 'Please provide your proper address.',
			'barangay.required' => 'Barangay is required.',
			'barangay.min' => 'Please provide your proper barangay name.',
			'city.required' => 'City/Province is required.',
			'zip_code.numeric' => 'Zip Codes contains numbers only.',
			'region.required' => 'Region is required.',
			'address_name.required' => 'Address name is required.'
		]);

		if ($validator->fails())
			return array($validator->messages(), 'isSuccess' => false);

		try {
			DB::beginTransaction();

			$data = ShippingAddress::create([
				'user_id' => Auth::user()->id,
				'first_name' => $req->first_name,
				'last_name' => $req->last_name,
				'contact_number' => $req->contact_number,
				'address' => $req->address,
				'barangay' => $req->barangay,
				'city' => $req->city,
				'zip_code' => $req->zip_code,
				'region' => $req->region,
				'address_name' => $req->address_name
			]);

			DB::commit();
		} catch (\Exception $e) {
			Log::info($e);
			DB::rollback();
		}

		$regionAppend = '';
		$cityAppend = '';

		foreach (ShippingAddress::getRegions() as $r) {
			$regionAppend .= '<option value="' . $r . '" ' . ($data->region == $r ? 'selected' : '') . '>' . $r . '</option>';
		}

		foreach (ShippingAddress::getCities($data->region) as $c) {
			$cityAppend .= '<option value="' . $c . '" ' . ($data->city == $c ? 'selected' : '') . '>' . $c . '</option>';
		}

		$append = 
			'<div class="row mt-3">' .
				'<form action="shipping-address/update/' . $data->id . '" method="POST" enctype="multipart/form-data" class="container-fluid bg-light" data-id="' . $data->id .'">' .
					csrf_field() .
					'<div class="row bg-gray py-3">' .
						'<div class="col-6">' .
							'<h5>Billing/Shipping Address</h5>' .
						'</div>' .

						'<div class="col-6 form-group-buttons" id="formButtons' . $data->id . '">' .
							'<button type="button" data-type="delete" class="btn float-right text-light" data-action="delete" data-action-message="Deleting" data-url="shipping-address/delete/' . $data->id . '><i class="fas fa-trash mr-2"></i>Delete</a>' .
							'<button type="button" data-type="edit" class="btn float-right text-light"><i class="fas fa-pencil-alt mr-2"></i>Edit</button>' .
							'<button type="button" data-type="cancel" class="btn float-right text-light"><i class="fas fa-times mr-2"></i>Cancel</button>' .
							'<button type="button" data-type="submit" class="btn float-right text-light"><i class="fas fa-check mr-2"></i>Save</button>' .

							'<button type="button" data-type="bookmark" data-id="' . $data->id . '" class="btn float-right text-light"><i class="far fa-bookmark mr-2"></i><span>Make Default</span></button>' .
						'</div>' .
					'</div>' .

					'<div class="row">' .
						'<span class="badge badge-success w-100 validation-message br-0" id="success' . $data->id . '"></span>' .
					'</div>' .

					'<div class="row my-3 forminfo" id="form' . $data->id . '">' .

						'<div class="col col-lg-8 mx-5">' .
							'<div class="row my-2">' .
								'<div class="form-group col-lg-6">' .
									'<label class="form-label">Address Name</label>' .
									'<input type="text" class="form-control" value="' . $data->address_name . '" name="address_name" disabled>' .
									'<span class="badge badge-danger w-100 validation-message" id="address_name{{$s->id}}"></span>' .
								'</div>' .
							'</div>' .

							'<div class="row my-2">' .
								'<div class="form-group col-lg-6">' .
									'<label class="form-label">First Name</label>' .
									'<input type="text" class="form-control" value="' . $data->first_name . '" name=\'first_name\' disabled>' .
									'<span class="badge badge-danger w-100 validation-message" id="first_name' . $data->id . '"></span>' .
								'</div>' .

								'<div class="form-group col-lg-6">' . 
									'<label class="form-label">Last Name</label>' .
									'<input type="text" class="form-control" value="' . $data->last_name .  '" name=\'last_name\' disabled>' .
									'<span class="badge badge-danger w-100 validation-message" id="last_name' . $data->id . '"></span>' .
								'</div>' .
							'</div>' .

							'<div class="row my-2">' .
								'<div class="form-group col-lg-6">' .
									'<label class="form-label">Contact Number</label>' .
									'<input type="text" class="form-control" name="contact_number" name="contact_number" value="' . $data->contact_number . '" data-mask data-mask-format="+63999 999 9999" disabled>' .
									'<span class="badge badge-danger w-100 validation-message" id="contact_number' . $data->id . '"></span>' .
								'</div>' .
							'</div>' .

							'<div class="row my-2">' .
								'<div class="form-group">' .
									'<label class="form-label">Address</label>' .
									'<input type="text" class="form-control" placeholder="House Number, Street Name, Subdivision" name="address" value="' . $data->address . '" disabled>' .
									'<span class="badge badge-danger w-100 validation-message" id="address' . $data->id . '"></span>' .
								'</div>' .
							'</div>' .

							'<div class="row my-2">' .
								'<div class="form-group col-lg-6">' .
									'<label class="form-label">Barangay</label>' .
									'<input type="text" class="form-control" name="barangay" value="' . $data->barangay . '" disabled>' .
									'<span class="badge badge-danger w-100 validation-message" id="barangay' . $data->id . '"></span>' .
								'</div>' .

								'<div class="form-group col-lg-6">' .
									'<label class="form-label">Province/City</label>' .
									'<select class="form-select" name="city" id="formCity' . $data->id . '" disabled>' .
										$cityAppend .
									'</select>' .
									'<span class="badge badge-danger w-100 validation-message" id="city' . $data->id . '"></span>' .
								'</div>' .
							'</div>' .

							'<div class="row my-2">' .
								'<div class="col-lg-6">' .
									'<label class="form-label">Zip Code</label>' .
									'<input type="number" class="form-control" name="zip_code" value="' . $data->zip_code . '" disabled>' .
									'<span class="badge badge-danger w-100 validation-message" id="zip_code' . $data->id . '"></span>' .
								'</div>' .

								'<div class="form-group col-lg-6">' .
									'<label class="form-label">Region</label>' .
									'<select class="form-select" name="region" data-target="#formCity' . $data->id . '" disabled>' .
										$regionAppend .
									'</select>' .
									'<span class="badge badge-danger w-100 validation-message" id="region' . $data->id . '"></span>' .
								'</div>' .
							'</div>' .
						'</div>' .
					'</div>' .
				'</form>' .
			'</div>';

		return array(array('addSuccess' => 'Successfully updated shipping details.'), 'data' => $append, 'isSuccess' => true, 'address' => $data);
	}

	protected function update(Request $req, $id) {

		$validator = Validator::make($req->all(), [
			'contact_number' => 'required|numeric|min:10',
			'address' => 'required|min:5',
			'barangay' => 'required|min:3',
			'city' => 'required',
			'zip_code' => 'numeric',
			'region' => 'required',
			'address_name' => 'required',
		], [
			'contact_number.required' => 'Contact number is required.',
			'contact_number.numeric' => 'Contact number must only contains number.',
			'contact_number.min' => 'Contact number provided is invalid.',
			'address.required' => 'Address is required.',
			'address.min' => 'Please provide your proper address.',
			'barangay.required' => 'Barangay is required.',
			'barangay.min' => 'Please provide your proper barangay name.',
			'city.required' => 'City/Province is required.',
			'zip_code.numeric' => 'Zip Codes contains numbers only.',
			'region.required' => 'Region is required.',
			'address_name.required' => 'Address name is required.'
		]);

		if ($validator->fails())
			return array(array('validationError' => $validator->messages(), 'isSuccess' => false));

		try {
			DB::beginTransaction();

			$sa = ShippingAddress::find($id);
			$sa->first_name = $req->first_name;
			$sa->last_name = $req->last_name;
			$sa->contact_number = $req->contact_number;
			$sa->address = $req->address;
			$sa->barangay = $req->barangay;
			$sa->city = $req->city;
			$sa->zip_code = $req->zip_code;
			$sa->region = $req->region;
			$sa->address_name = $req->address_name;
			$sa->save();

			DB::commit();
		} catch (\Exception $e) {
			Log::info($e);
			DB::rollback();
			return array(array('error' => 'Something went wrong, please try again later.'), 'isSuccess' => false);
		}

		return array(array('success' => 'Successfully updated shipping details.'), 'isSuccess' => true);
	}

	protected function delete($id) {
		$add = ShippingAddress::find($id);

		if (!$add)
			return array(array('deleteSuccess' => 'Successfully deleted shipping details.'), 'isSuccess' => true);

		try {
			DB::beginTransaction();

			$add->delete();

			DB::commit();
		} catch (\Exception $e) {
			Log::info($e);
			DB::rollback();

			return array(array('deleteSuccess' => 'Something went wrong, please try again later.'), 'isSuccess' => false);
		}

		return array(array('deleteSuccess' => 'Successfully deleted shipping details'), 'isSuccess' => true);
	}
}