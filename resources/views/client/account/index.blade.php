@extends('template.client.account')

@section('subtitle', 'My Account')

@section('page-css')
<link rel="stylesheet" href="/css/account.index.css" >
@endsection

@section('sub-body')
<div class="row mt-5">
	<div class="container bg-light">
		<div class="row bg-gray py-3">
			<h5>Personal Details</h5>
		</div>

		<div class="row mt-3 forminfo">
			<table class="table">
				<tr>
					<th scope="col">Name:</th>
					<td>{{Auth::user()->first_name}} {{Auth::user()->last_name}}</td>
				</tr>

				<tr>
					<th scope="col">Email:</th>
					<td>
						@php
							$i = 0;
							$hitAt = false;
							$output = array();
							foreach (str_split(Auth::user()->email) as $c) {
								$hitAt ? true : $hitAt = $c == "@" ? true : false;
								$i < 3 ? array_push($output, $c) : ($hitAt ? array_push($output, $c) : array_push($output, "*"));
								$i++;
							}
						@endphp
						{{implode($output)}}
					</td>
				</tr>

				<tr>
					<th scope="col">Birthday:</th>
					<td>{{\Carbon\Carbon::parse(Auth::user()->birthday)->format('M d, Y')}}</td>
				</tr>

				<tr>
					<th scope="col">Gender: </th>
					<td>{{ucwords(Auth::user()->gender)}}</td>
				</tr>

				<tr>
					<th scope="col">Password:</th>
					<td>*******</td>
				</tr>

				<tr>
					<th scope="col">Contact Number:</th>
					<td>
						@php
							$i = 0;
							$output = array('+', '6', '3');
							foreach (str_split(Auth::user()->contact_number) as $c) {
								$hitAt ? true : $hitAt = $c == "@" ? true : false;
								$i < 3 ? array_push($output, $c) : array_push($output, "*");
								$i++;
							}
						@endphp
						{{implode($output)}}
					</td>
				</tr>
			</table>
		</div>
	</div>

	<div class="flex-row-reverse my-3">
		<div class="col-lg-1 d-grid" id="buttonrow" >
			<a href="{{ route('account.edit') }}" class="btn-lg btn-light text-decoration-none text-dark text-center">Edit</a>
		</div>
	</div>
</div>

<div class="row" id="shipping-addresses">
	@foreach(Auth::user()->shippingAddress as $s)
	<div class="row mt-3">
		<form action="{{ route('shipping-address.update', [$s->id]) }}" method="POST" enctype="multipart/form-data" class="container-fluid bg-light" data-id="{{$s->id}}">
			{{csrf_field()}}
			<div class="row bg-gray py-3">
				<div class="col-6">
					<h5>Billing/Shipping Address</h5>
				</div>

				<div class="col-6 form-group-buttons" id="formButtons{{$s->id}}">
					<button type="button" data-type="delete" class="btn float-right text-light" data-action="delete" data-action-message="Deleting" data-url="{{route('shipping-address.delete', [$s->id])}}"><i class="fas fa-trash mr-2"></i>Delete</a>
					<button type="button" data-type="edit" class="btn float-right text-light"><i class="fas fa-pencil-alt mr-2"></i>Edit</button>
					<button type="button" data-type="cancel" class="btn float-right text-light"><i class="fas fa-times mr-2"></i>Cancel</button>
					<button type="button" data-type="submit" class="btn float-right text-light"><i class="fas fa-check mr-2"></i>Save</button>

					@if ($s->is_default)
					<button type="button" data-type="bookmark" data-id="{{ $s->id }}" class="btn float-right text-light"><i class="fas fa-bookmark mr-2"></i><span>Remove Default</span></button>
					@else
					<button type="button" data-type="bookmark" data-id="{{ $s->id }}" class="btn float-right text-light"><i class="far fa-bookmark mr-2"></i><span>Make Default</span></button>
					@endif
				</div>
			</div>

			<div class="row">
				<span class="badge badge-success w-100 validation-message br-0" id="success{{$s->id}}"></span>
			</div>

			<div class="row my-3 forminfo" id="form{{$s->id}}">
				<div class="col col-lg-8 mx-5">
					<div class="row my-2">
						<div class="form-group col-lg-6">
							<label class="form-label">Address Name</label>
							<input type="text" class="form-control" value="{{ $s->address_name }}" name='address_name' disabled>
							<span class="badge badge-danger w-100 validation-message" id="address_name{{$s->id}}"></span>
						</div>
					</div>

					<div class="row my-2">
						<div class="form-group col-lg-6">
							<label class="form-label">First Name</label>
							<input type="text" class="form-control" value="{{ $s->first_name }}" name='first_name' disabled>
							<span class="badge badge-danger w-100 validation-message" id="first_name{{$s->id}}"></span>
						</div>

						<div class="form-group col-lg-6">
							<label class="form-label">Last Name</label>
							<input type="text" class="form-control" value="{{ $s->last_name }}" name='last_name' disabled>
							<span class="badge badge-danger w-100 validation-message" id="last_name{{$s->id}}"></span>
						</div>
					</div>

					<div class="row my-2">
						<div class="form-group col-lg-6">
							<label class="form-label">Contact Number</label>
							<input type="text" class="form-control" name="contact_number" name="contact_number" value="{{$s->contact_number}}" data-mask data-mask-format="+63999 999 9999" disabled>
							<span class="badge badge-danger w-100 validation-message" id="contact_number{{$s->id}}"></span>
						</div>
					</div>

					<div class="row my-2">
						<div class="form-group">
							<label class="form-label">Address</label>
							<input type="text" class="form-control" placeholder="House Number, Street Name, Subdivision" name="address" value="{{$s->address}}" disabled>
							<span class="badge badge-danger w-100 validation-message" id="address{{$s->id}}"></span>
						</div>
					</div>

					<div class="row my-2">
						<div class="form-group col-lg-6">
							<label class="form-label">Barangay</label>
							<input type="text" class="form-control" name="barangay" value="{{$s->barangay}}" disabled>
							<span class="badge badge-danger w-100 validation-message" id="barangay{{$s->id}}"></span>
						</div>

						<div class="form-group col-lg-6">
							<label class="form-label">Province/City</label>
							<select class="form-select" name="city" id="formCity{{$s->id}}" disabled>
								@foreach(Auth::user()->getCities($s->region) as $c)
								<option value="{{$c}}" {{$s->city == $c ? 'selected' : ''}}>{{$c}}</option>
								@endforeach
							</select>
							<span class="badge badge-danger w-100 validation-message" id="city{{$s->id}}"></span>
						</div>
					</div>

					<div class="row my-2">
						<div class="col-lg-6">
							<label class="form-label">Zip Code</label>
							<input type="number" class="form-control" name="zip_code" value="{{$s->zip_code}}" disabled>
							<span class="badge badge-danger w-100 validation-message" id="zip_code{{$s->id}}"></span>
						</div>

						<div class="form-group col-lg-6">
							<label class="form-label">Region</label>
							<select class="form-select" name="region" data-target="#formCity{{$s->id}}" disabled>
								@foreach(Auth::user()->getRegions() as $r)
								<option value="{{$r}}" {{$s->region == $r ? 'selected' : ''}}>{{$r}}</option>
								@endforeach
							</select>
							<span class="badge badge-danger w-100 validation-message" id="region{{$s->id}}"></span>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
	@endforeach
</div>

<div class="row" id="shipping-form">
	<form action="{{ route('shipping-address.store') }}" method="POST" enctype="multipart/form-data" class="row mt-3">
		{{csrf_field()}}
		<div class="container bg-light">
			<div class="row bg-gray py-3">
				<h5>Billing/Shipping Address</h5>
			</div>

			<div class="row">
				<span class="badge badge-success w-100 validation-message" id="addSuccess"></span>
			</div>

			<div class="row my-5 forminfo">
				<div class="col col-lg-8 mx-5">
					<div class="row my-2">
						<div class="form-group col-lg-6">
							<label class="form-label">Address Name</label>
							<input type="text" class="form-control" placeholder="Address Name" value="{{ old('address_name') }}" name='address_name'>
							<span class="badge badge-danger w-100 validation-message" id="address_name"></span>
						</div>
					</div>

					<div class="row my-2">
						<div class="form-group col-lg-6">
							<label class="form-label">First Name</label>
							<input type="text" class="form-control" value="{{ old('first_name') == null ? Auth::user()->first_name : old('first_name') }}" name='first_name'>
							<span class="badge badge-danger w-100 validation-message" id="first_name"></span>
						</div>

						<div class="form-group col-lg-6">
							<label class="form-label">Last Name</label>
							<input type="text" class="form-control" value="{{ old('last_name') == null ? Auth::user()->last_name : old('last_name') }}" name='last_name'>
							<span class="badge badge-danger w-100 validation-message" id="last_name"></span>
						</div>
					</div>

					<div class="row my-2">
						<div class="form-group col-lg-6">
							<label class="form-label">Contact Number</label>
							<input type="text" class="form-control" name="contact_number" value="{{old('contact_number')}}" data-mask data-mask-format="+63999 999 9999">
							<span class="badge badge-danger w-100 validation-message" id="contact_number"></span>
						</div>
					</div>

					<div class="row my-2">
						<div class="form-group">
							<label class="form-label">Address</label>
							<input type="text" class="form-control" placeholder="House Number, Street Name, Subdivision" name="address" value="{{old('address')}}">
							<span class="badge badge-danger w-100 validation-message" id="address"></span>
						</div>
					</div>

					<div class="row my-2">
						<div class="form-group col-lg-6">
							<label class="form-label">Barangay</label>
							<input type="text" class="form-control" name="barangay" value="{{old('barangay')}}">
							<span class="badge badge-danger w-100 validation-message" id="barangay"></span>
						</div>

						<div class="form-group col-lg-6">
							<label class="form-label">City/Province</label>
							<select class="form-select" name="city" id="formCity">
								@foreach(Auth::user()->getCities() as $c)
								<option value="{{$c}}" {{old('city') == $c ? 'selected' : ''}}>{{$c}}</option>
								@endforeach
							</select>
							<span class="badge badge-danger w-100 validation-message" id="city"></span>
						</div>
					</div>

					<div class="row my-2">
						<div class="col-lg-6">
							<label class="form-label">Zip Code</label>
							<input type="number" class="form-control" name="zip_code">
							<span class="badge badge-danger w-100 validation-message" id="zip_code"></span>
						</div>

						<div class="form-group col-lg-6">
							<label class="form-label">Region</label>
							<select class="form-select" name="region" data-target="#formCity">
								@foreach(Auth::user()->getRegions() as $r)
								<option value="{{$r}}" {{old('region') == $r ? 'selected' : ''}}>{{$r}}</option>
								@endforeach
							</select>
							<span class="badge badge-danger w-100 validation-message" id="region"></span>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="flex-row-reverse mt-3">
			<div class="col-lg-2 d-grid" id="buttonrow">
				<button type="button" class="btn-lg btn-light" data-action="submit" data-spinner-color='#000' data-text="Add Address">Add Address</button>
			</div>
		</div>
	</form>
</div>
@endsection

@section('script')
<script type="text/javascript">
	$(document).ready(() => {
		var shippingFormData = [];

		// Masking the contact_number field
		$("[data-mask]").inputmask('mask', {
			'mask' : "+63 999 999 9999",
			'removeMaskOnSubmit' : true,
			'autoUnmask':true
		});

		// Changing the city field of that respected form group
		$(document).on('change', '[name=region]', (e) => {
			let obj = $(e.currentTarget).attr('data-target');

			$.ajax({
				url: '{{route('utility.get_cities')}}',
				method: 'POST',
				data: {
					_token: '{{ csrf_token() }}',
					region: $(e.currentTarget).val()
				},
				success: function(data, status, xhr) {
					$(obj).html('');
					for (let i = 0; i < data.length; i++)
						$(obj).append(`<option value="` + data[i] + `">` + data[i] + `</option>`);
				}
			});
		});

		// Initial hiding of the buttons save and cancel for shipping address
		$('[data-type=submit]').hide();
		$('[data-type=cancel]').hide();
		$('[data-type=delete]').hide();

		// Changing the edit button to submit/cancel and vice versa
		$(document).on('click', '.form-group-buttons > *', (e) => {
			let fields = ['success', 'address_name', 'contact_number', 'address', 'barangay', 'city', 'zip_code', 'region'];

			let obj = $(e.currentTarget);
			let form = obj.parent().parent().parent();

			if (obj.attr('data-type') == 'edit') {
				shippingFormData[form.attr('data-id')] = form.html();
				
				obj.siblings().show();
				obj.hide();

				form.find('input, select').prop('disabled', false);
			}
			else if (obj.attr('data-type') == 'bookmark') {
				// FIX TO MAKE THE ADDRESS DEFAULT
				$.post('{{ route("utility.toggle_default_address") }}', {
					_token: '{{csrf_token()}}',
					id: obj.attr('data-id')
				}).done((data) => {
					if (data.status == 1) {
						$('[data-type=bookmark]').each((k, v) => {
							$($(v).children()[0]).attr('data-prefix', 'far');
							$($(v).children()[1]).text('Make Default');
						});

						if (data.data.is_default) {
							$(obj.children()[0]).attr('data-prefix', 'fas');
							$(obj.children()[1]).text('Remove Default');

							Swal.fire({
								title: 'Successfully made ' + data.data.address_name + ' as the default address.',
								position: 'bottom',
								toast: true,
								timer: 10000,
								background: '#28a745',
								showConfirmButton: false,
								customClass: {
									title: `text-white`,
									content: `text-white`,
									popup: `px-3`
								},
							});
						}
						else {
							Swal.fire({
								title: 'Successfully removed ' + data.data.address_name + ' as the default address.',
								position: 'bottom',
								toast: true,
								timer: 10000,
								background: '#28a745',
								showConfirmButton: false,
								customClass: {
									title: `text-white`,
									content: `text-white`,
									popup: `px-3`
								},
							});
						}

					}
					else if (data.status == 0) {
						Swal.fire({
							title: 'Address no longer exists.',
							position: 'bottom',
							toast: true,
							timer: 10000,
							background: '#17a2b8',
							showConfirmButton: false,
							customClass: {
								title: `text-white`,
								content: `text-white`,
								popup: `px-3`
							},
						});

						$('form[data-id=' + data.id + ']').remove();
					}
					else if (data.status == -1) {
						console.log("MESSAGE: " + data.message);
						console.log("ERROR MESSAGE: " + data.error_message);

						Swal.fire({
							title: 'Something went wrong, please try again later.',
							position: 'bottom',
							toast: true,
							timer: 10000,
							background: '#dc3545',
							showConfirmButton: false,
							customClass: {
								title: `text-white`,
								content: `text-white`,
								popup: `px-3`
							},
						});
					}
				});
			}
			else {
				obj.parent().find('[data-type=edit]').show();
				obj.parent().find('button:not("[data-type=edit]")').hide();

				form.find('input, select').prop('disabled', true);
			}

			// Submitting the form
			if (obj.attr('data-type') == 'submit') {
				$.post(form.attr('action'), {
					_token: form.find('[name=_token]').val(),
					first_name: form.find('[name=first_name]').val(),
					last_name: form.find('[name=last_name]').val(),
					contact_number: form.find('[name=contact_number]').val(),
					address: form.find('[name=address]').val(),
					barangay: form.find('[name=barangay]').val(),
					city: form.find('[name=city]').val(),
					zip_code: form.find('[name=zip_code]').val(),
					region: form.find('[name=region]').val(),
					address_name: form.find('[name=address_name]').val()
				}).done((data) => {
					if (!data['isSuccess']) {
						form.html(shippingFormData[form.attr('data-id')]);
						$("form input[data-mask]").inputmask('mask', {
							'mask': "+63 999 999 9999",
							'removeMaskOnSubmit' : true,
							'autoUnmask':true
						});

						Swal.fire({
							title: 'Please fill out the form properly.',
							position: 'bottom',
							toast: true,
							timer: 10000,
							background: '#dc3545',
							showConfirmButton: false,
							customClass: {
								title: `text-white`,
								content: `text-white`,
								popup: `px-3`
							},
						});

						$.each(data[0], (k, v) => {
							if (k == 'validationError') {
								$.each(v, (kk, vv) => {
									$('#' + kk + form.attr('data-id')).text(vv);
								});
								$('#success' + form.attr('data-id')).text('');
							}
							else
								$('#'+k).text(v);
						});
					}
					else {
						for (let i = 0; i < fields.length; i++) {
							form.find('#' + fields[i] + form.attr('data-id')).text('');
							if (form.find('[name=' + fields[i] + ']').is('select')) {
								$.each(form.find('[name=' + fields[i] + ']').children(), (k, v) => {
									$(v).removeAttr('selected');
									if ($(v).val() == form.find('[name=' + fields[i] + ']').val())
										$(v).attr('selected', true);
								});
							}
							else
								form.find('[name=' + fields[i] + ']').attr('value', form.find('[name=' + fields[i] + ']').val());
						}

						shippingFormData[form.attr('data-id')] = form.html();

						Swal.fire({
							title: data[0].success,
							position: 'bottom',
							toast: true,
							timer: 10000,
							background: '#28a745',
							showConfirmButton: false,
							customClass: {
								title: `text-white`,
								content: `text-white`,
								popup: `px-3`
							},
						});
					}
					$.each(data[0], (k, v) => {$('#'+k+form.attr('data-id')).text(v);});
				});
			}
			else if (obj.attr('data-type') == 'cancel') {
				form.html(shippingFormData[form.attr('data-id')]);

				$("form input[data-mask]").inputmask('mask', {
					'mask' : "+63 999 999 9999",
					'removeMaskOnSubmit' : true,
					'autoUnmask':true
				});
			}
			else if (obj.attr('data-type') == 'delete') {
				$.post(obj.attr('data-url'), {
					_token: form.find('[name=_token]').val()
				}).done((data) => {
					if (!data['isSuccess']) {
						// IF NOT SUCCESS
					}
					else {
						form.parent().remove();

						Swal.fire({
							title: data[0].deleteSuccess,
							position: 'bottom',
							toast: true,
							timer: 10000,
							background: '#28a745',
							showConfirmButton: false,
							customClass: {
								title: `text-white`,
								content: `text-white`,
								popup: `px-3`
							},
						});
					}
				});
			}
		});

		// Shipping form
		$('#shipping-form').on('click', '#buttonrow > button', (e) => {
			e.preventDefault();
			let fields = ['success', 'address_name', 'contact_number', 'address', 'barangay', 'city', 'zip_code', 'region'];

			let obj = $(e.currentTarget);
			let form = obj.parent().parent().parent();
			let submitText = form.find('[data-action=submit]').attr('data-text');

			form.find('[data-action=submit]').prop('disabled', true);
			form.find('[data-action=submit]').addClass('disabled');

			// Submitting the form
			$.post(form.attr('action'), {
				_token: form.find('[name=_token]').val(),
				first_name: form.find('[name=first_name]').val(),
				last_name: form.find('[name=last_name]').val(),
				contact_number: form.find('[name=contact_number]').val(),
				address: form.find('[name=address]').val(),
				barangay: form.find('[name=barangay]').val(),
				city: form.find('[name=city]').val(),
				zip_code: form.find('[name=zip_code]').val(),
				region: form.find('[name=region]').val(),
				address_name: form.find('[name=address_name]').val()
			}).done((data) => {
				form.find('[data-action=submit]').html(submitText);
				form.find('[data-action=submit]').prop('disabled', false);
				form.find('[data-action=submit]').removeClass('disabled');

				for (let i = 0; i < fields.length; i++) {
					$('#' + fields[i] + form.attr('data-id')).text('');
				}

				if (!data['isSuccess']) {
					form.html(shippingFormData[form.attr('data-id')]);

					$("form input[data-mask]").inputmask('mask', {
						'mask': "+63 999 999 9999",
						'removeMaskOnSubmit': true,
						'autoUnmask':true
					});

					form.find('#addSuccess').text('');

					Swal.fire({
						title: 'Please fill out the form properly',
						position: 'bottom',
						toast: true,
						timer: 10000,
						background: '#dc3545',
						showConfirmButton: false,
						customClass: {
							title: `text-white`,
							content: `text-white`,
							popup: `px-3`
						},
					});

					$.each(data[0], (k, v) => {
						$('#'+k).text(v);
					});
				}
				else {
					form.get(0).reset();

					$("#shipping-addresses").append(data['data']);
					
					$("form[data-id=" + data['address'].id + "]").find("#formButtons" + data['address'].id + " > [data-type=submit]").hide();
					$("form[data-id=" + data['address'].id + "]").find("#formButtons" + data['address'].id + " > [data-type=cancel]").hide();
					$("form[data-id=" + data['address'].id + "]").find("#formButtons" + data['address'].id + " > [data-type=delete]").hide();

					$.each(fields, (k, v) => {
						let validation = form.find('#' + v);
						$(validation).text('');
					});

					Swal.fire({
						title: 'Successfully added shipping/billing address.',
						position: 'bottom',
						toast: true,
						timer: 10000,
						background: '#28a745',
						showConfirmButton: false,
						customClass: {
							title: `text-white`,
							content: `text-white`,
							popup: `px-3`
						},
					});
				}
			});
		});
	});
</script>
@endsection