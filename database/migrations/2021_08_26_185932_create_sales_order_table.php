<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales_order', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('customer_id')->unsigned();
            $table->string('courier');
            $table->string('payment_method');
            $table->tinyInteger('pay_later')->default(0);
            $table->integer('shipping_addresses_id')->unsigned();
            $table->string('instructions',1000);
            $table->decimal('total', 18, 2);
            $table->tinyInteger('status')->default(1);
            $table->string('proof_of_payment')->nullable()->default(null);
            $table->integer('store_id')->unsigned();
            $table->string('tracking_number')->nullable()->default(null);
            $table->timestamps();
            
            $table->foreign('customer_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('shipping_addresses_id')->references('id')->on('shipping_addresses')->onDelete('cascade');
            $table->foreign('store_id')->references('id')->on('stores')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sales_order');
    }
}
