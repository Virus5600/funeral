<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLivestreamTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('livestream', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->default('Funeral Wake Livestream');
            $table->string('stream_id')->nullable()->default(null);
            $table->string('stream_key')->nullable()->default(null);
            $table->string('stream_link')->nullable()->default(null);
            $table->string('description',1000)->nullable();
            $table->date('schedule_date')->nullable()->default(null);
            $table->time('schedule_time')->nullable()->default(null);
            $table->integer('duration')->unsigned();
            $table->decimal('price', 18, 2);
            $table->integer('user_id')->unsigned();
            $table->enum('status', ['cancelled', 'waiting for proof', 'pending', 'awaiting schedule', 'streaming', 'finished', 'cart'])->default('cart');
            $table->string('proof_of_payment')->nullable()->default(null);
            $table->tinyInteger('active')->default(0);
            $table->string('options')->nullable()->default('standing_photo=0;video_slideshow=0;flower_stands=0');
            $table->string('category')->nullable()->default(null);
            $table->timestamps();
            
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('livestream');
    }
}
