<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Carbon\Carbon;

class EmailVerification extends Model
{
	protected $fillable = [
		'user_id',
		'verification_code',
		'expiry_time'
	];

	// Relationships
	protected function user() { return $this->belongsTo('App\User'); }

	// Custom Function
	public static function isCodeValid($code) {
		$verifier = EmailVerification::where('verification_code', '=', $code)->first();

		if ($verifier->count() == 0)
			return -1;

		return Carbon::now()->lessThan(Carbon::parse($verifier->expiry_time));
	}

	public function isValid() {
		return EmailVerification::isCodeValid($this->verification_code);
	}
}