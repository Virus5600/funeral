@extends('template.admin')

@section('title', 'Obituary')
@section('sidebar', 'owner')

@section('body')
<h1 class="font-weight-bold">@include('template.breadcrumbs', ['startIndex' => 2, 'confirmLeave' => !$isShow])</h1>

<hr class="hr-thick" style="border-color: #707070;">

<form class="form" action="{{route('owner.product.update')}}" method="POST" enctype="multipart/form-data">
	@if (!$isShow)
	{{csrf_field()}}
	<input type="hidden" name="id" value="{{$product->id}}">
	@endif

	<div class="row">
		<div class="col-8">
			<div class="row">
				<div class="form-group col-12">
					@if ($isShow)
					<h3>{{$product->product_name}}</h3>
					<br>
					@else
					<label class="form-label font-weight-bold">Product Name</label>
					<input type="text" class="form-control" name="product_name" value="{{$product->product_name}}">
					<span class="badge badge-danger w-100 validation-message">{{ $errors->first('product_name') }}</span>
					@endif
				</div>
			</div>

			<div class="row">
				<div class="form-group col-12 col-lg-4">
					<label class="form-label font-weight-bold">Inventory{{ $isShow ? ':' : '' }}</label>
					@if ($isShow)
					<p>{{$product->inventory}}</p>
					@else
					<input class="form-control" type="number" name="inventory" min="0" value="{{$product->inventory}}">
					<span class="badge badge-danger w-100 validation-message">{{ $errors->first('inventory') }}</span>
					@endif
				</div>

				<div class="form-group col-12 col-lg-4">
					<label class="form-label font-weight-bold">Selling Price{{ $isShow ? ':' : '' }}</label>
					@if ($isShow)
					<p>&#8369; {{$product->selling_price}}</p>
					@else
					<div class="input-group">
						<div class="input-group-prepend">
							<span class="input-group-text">&#8369;</span>
						</div>
						<input type="number" class="form-control" name="selling_price" value="{{$product->selling_price}}" min='0.00'>
						<span class="badge badge-danger w-100 validation-message">{{ $errors->first('selling_price') }}</span>
					</div>
					@endif
				</div>

				<div class="form-group col-12 col-lg-4">
					<label class="form-label font-weight-bold">Status{{ $isShow ? ':' : '' }}</label>
					@if ($isShow)
					<p>{{$product->status ? 'Active' : 'Inactive'}}</p>
					@else
					<label class="form-label font-weight-bold">Status</label>
					<select class="custom-select" name="status">
						<option value='active' {{$product->status ? 'selected' : ''}}>Active</option>
						<option value="inactive" {{$product->status ? '' : 'selected'}}>Inactive</option>
					</select>
					<span class="badge badge-danger w-100 validation-message">{{ $errors->first('status') }}</span>
					@endif
				</div>
			</div>
		</div>

		<div class="form-group col-12 col-lg-4 d-flex flex-d-row">
			<div class="mx-auto text-center" style="overflow: hidden; max-width: 37.5%;">
				@if ($product->image_name == null)
				<img src="/uploads/products/product.png" class="{{$isShow ? '' : 'cursor-pointer'}} img-fluid" id="image" alt="Image">
				@else
				<img src="/uploads/products/{{$category->category_name}}/{{$product->image_name}}" class="{{$isShow ? '' : 'cursor-pointer'}} img-fluid" id="image" alt="Image">
				@endif
			</div>

			<div class="mx-auto">
				@if (!$isShow)
				<h6 class="form-label font-weight-bold" style="word-break: break-word" id="image_label">UPLOAD IMAGE</h6>
				<input type="file" name='image' class="hidden" accept=".jpg,.jpeg,.png" id="imgInput">
				<small class="text-muted pb-0 mb-0"><b>FORMATS ALLOWED:</b> JPEG, JPG, PNG</small><br>
				<small class="text-muted pt-0 mt-0"><b>MAX SIZE:</b> 5MB</small>
				<span class="badge badge-danger w-100 validation-message">{{ $errors->first('image') }}</span>
				@endif
			</div>
		</div>
	</div>

	@if (!$isShow)
	<div class="row">
		<div class="form-group col-12 col-lg-6">
			<label class="form-label font-weight-bold">Category</label>
			<input type="text" class="form-control" name="category" value="{{$product->category->category_name}}"/>
			<span class="badge badge-danger w-100 validation-message">{{ $errors->first('category') }}</span>
		</div>

		<div class="form-group col-12 col-lg-6">
			<label class="form-label font-weight-bold">Subcategory</label>
			<input type="text" class="form-control" name="subcategory" value="{{$product->subcategory->subcategory_name}}" />
			<span class="badge badge-danger w-100 validation-message">{{ $errors->first('subcategory') }}</span>
		</div>
	</div>
	@endif

	<div class="row">
		<div class="form-group col-12">
			<label class="form-label font-weight-bold">Size{{ $isShow ? ':' : '' }}</label>
			@if ($isShow)
			<p>{{$product->size or 'N/A'}}</p>
			@else
			<input type="text" class="form-control" name="size" value="{{$product->size}}">
			<span class="badge badge-danger w-100 validation-message">{{ $errors->first('size') }}</span>
			@endif
		</div>
	</div>

	<div class="row">
		<div class="form-group col-12">
			<label class="form-label font-weight-bold">Description{{ $isShow ? ':' : '' }}</label>
			@if ($isShow)
			<p>{{$product->description or 'N/A'}}</p>
			@else
			<textarea class="form-control not-resizable" name="description" rows="5">{{$product->description}}</textarea>
			<span class="badge badge-danger w-100 validation-message">{{ $errors->first('description') }}</span>
			@endif
		</div>
	</div>

	@if (!$isShow)
	<div class="row">
		<div class="col-12">
			<button type="submit" class="btn btn-dark" data-action="update">Save Changes</button>
		</div>
	</div>
	@endif
</form>
@endsection

@section('script')
<script type="text/javascript">
	function openInput(obj) {
		$("[name=" + obj.attr("id") + "]").trigger("click");
	}

	function swapImgFile(obj) {
		if (obj.files && obj.files[0]) {
			let reader = new FileReader();

			reader.onload = function(e) {
				$("#image").attr("src", e.target.result);
				$("#image_label").html(obj.files[0].name);
			}

			reader.readAsDataURL(obj.files[0])
		}
		else {
			$("#image").attr("src", "/uploads/products/yamero.jpg");
			$("#image_label").html("UPLOAD IMAGE");
		}
	}

	$(document).ready(() => {
		// AUTOCOMPLETE
		@if (!$isShow)
		let availableCategories = [
			@foreach ($categories as $c)
			'{{$c->category_name}}',
			@endforeach
		];

		$('[name=category]').autocomplete({
			source: availableCategories,
			minLength: 0,
			delay: 0
		}).on('click focus', (e) => {
			$(e.currentTarget).autocomplete('search', $(e.currentTarget).val());
		});

		let availableSubcategories = [
			@foreach ($subcategories as $s)
			'{{$s->subcategory_name}}',
			@endforeach
		];

		$('[name=subcategory]').autocomplete({
			source: availableSubcategories,
			minLength: 0,
			delay: 0
		}).on('click focus', (e) => {
			$(e.currentTarget).autocomplete('search', $(e.currentTarget).val());
		});
		@endif

		// Triggering click on the image box
		$("#image").on("click", (e) => {
			openInput($(e.currentTarget));
		});
		// Applying changes when file input changes
		$("#imgInput").on('change', (e) => {
			swapImgFile(e.currentTarget);
		});

		// Change subcategory if category changed
		$('[name=category]').on('input autocompleteselect', (e, ui) => {
			let obj = $(e.currentTarget);

			$.post('{{ route('utility.get_subcategories') }}', {
				_token: '{{csrf_token()}}',
				category: e.type == 'autocompleteselect' ? ui.item['value'] : obj.val()
			}).done((data) => {
				availableSubcategories = [];
				let i = 0;

				$.each(data, (k, v) => {
					availableSubcategories[i++] = v['subcategory_name'];
				});

				$('[name=subcategory]').autocomplete({
					source: availableSubcategories,
					delay: 0
				});
			});
		});
	});
</script>
@endsection