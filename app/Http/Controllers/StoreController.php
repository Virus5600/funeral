<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;

use App\Store;
use App\SalesOrder;
use App\Location;

use Validator;
use Log;
use DB;

class StoreController extends Controller
{
	protected function index() {
		$stores = Store::query();
		$search = null;

		if (request()->has('s')) {
			$search = request()->get('s');

			$stores = $stores->where('name', 'LIKE', '%'.$search.'%');
		}

		return view('admin.store.index', [
			'search' => $search,
			'stores' => $stores->paginate(15)
		]);
	}

	protected function store(Request $req) {
		$validator = Validator::make($req->all(), [
			'store_name' => 'required|min:2|unique:stores,name'
		], [
			'store_name.required' => 'The name of the store is required.',
			'store_name.min' => 'Please provide the proper store name.',
			'store_name.unique' => 'Store is already added.'
		]);

		if ($validator->fails())
			return redirect()
				->back()
				->withErrors($validator)
				->withInput()
				->with('create_error', true);

		try {
			DB::beginTransaction();

			Store::create([
				'name' => $req->store_name,
			]);

			DB::commit();
		} catch (\Exception $e) {
			Log::error($e);
			DB::rollback();

			return redirect()
				->back()
				->withInput()
				->with('flash_error', 'Something went wrong, please try again later.');
		}

		return redirect()
			->back()
			->with('target_store', 'create_new')
			->with('flash_success', 'Successfully added ' . $req->store_name . ' to lists of stores.');
	}

	protected function update(Request $req, $id) {
		$validator = Validator::make($req->all(), [
			'store_name' => 'required|min:2|unique:stores,name'
		], [
			'store_name.required' => 'The name of the store is required.',
			'store_name.min' => 'Please provide the proper store name.',
			'store_name.unique' => 'Store is already added.'
		]);

		if ($validator->fails())
			return redirect()
				->back()
				->withErrors($validator)
				->with('update_error', true)
				->with('target_store', $id);

		$oldStoreName = '';
		try {
			DB::beginTransaction();

			$store = Store::find($id);
			$oldStoreName = $store->name;

			$store->name = $req->store_name;
			$store->save();

			DB::commit();
		} catch (\Exception $e) {
			Log::error($e);
			DB::rollback();

			return redirect()
				->back()
				->withInput()
				->with('flash_error', 'Something went wrong, please try again later.');
		}

		return redirect()
			->back()
			->with('flash_success', 'Successfully updated "' . $oldStoreName . '" to "' . $req->store_name . '."');
	}

	protected function show($id) {
		return view('admin.store.sales_order.index', [
			'so' => SalesOrder::where('store_id', '=', $id)->paginate(15),
			'store' => Store::find($id)
		]);
	}

	protected function delete($id) {
		$oldStoreName = '';
		try {
			DB::beginTransaction();

			$store = Store::find($id);
			$oldStoreName = $store->name;
			$store->delete();

			DB::commit();
		} catch (\Exception $e) {
			Log::error($e);
			DB::rollback();

			return redirect()
				->back()
				->with('flash_error', 'Couldn\'t delete "' . $oldStoreName . '." Please try again later.');
		}

		return redirect()
			->back()
			->with('flash_success', 'Successfully removed "' . $oldStoreName . '."');
	}

	///////////////////////////////////
	// SALES ORDER RELATED FUNCTIONS //
	///////////////////////////////////
	protected function showSalesOrder($store_id, $id)
	{
		$so = SalesOrder::find($id);
		$store = Store::find($store_id);

		return view('admin.store.sales_order.show', [
			'steps' => SalesOrder::getStatusList(),
			'store' => $store,
			'sales_order' => $so
		]);
	}

	protected function printSalesOrder($store_id, $id) {
		return view('admin.store.sales_order.print', [
			'sales_order' => SalesOrder::find($id)
		]);
	}

	///////////////////////////
	// MAP RELATED FUNCTIONS //
	///////////////////////////
	protected function mapShow($id) {
		$store = Store::find($id);
		$location = $store->location;

		if ($location == null) {
			$location = Location::hydrate([
				(object) [
					'store_id' => $store->id,
					'address' => 'N/A',
					'latitude' => 0.0,
					'longitude' => 0.0
				]
			])[0];
		}

		return view('admin.store.map.edit', [
			'store' => $store,
			'location' => $location,
			'isShow' => true
		]);
	}

	protected function mapEdit($id) {
		$store = Store::find($id);
		$location = $store->location;

		if ($location == null) {
			$location = Location::hydrate([
				(object) [
					'store_id' => $store->id,
					'address' => 'N/A',
					'latitude' => 0.0,
					'longitude' => 0.0
				]
			])[0];
		}

		return view('admin.store.map.edit', [
			'store' => $store,
			'location' => $location,
			'isShow' => false
		]);
	}

	protected function mapUpdate(Request $req, $id) {
		$validator = Validator::make($req->all(), [
			'latitude' => 'required|numeric',
			'longitude' => 'required|numeric',
			'address' => 'string'
		]);

		if ($validator->fails()) {
			return redirect()
				->back()
				->withErrors($validator)
				->with('hasErrors', true);
		}

		$store = Store::find($id);

		try {
			DB::beginTransaction();

			if ($store->location == null) {
				Location::create([
					'store_id' => $store->id,
					'latitude' => $req->latitude,
					'longitude' => $req->longitude,
					'address' => $req->address
				]);
			}
			else {
				$loc = $store->location;
				$loc->latitude = $req->latitude;
				$loc->longitude = $req->longitude;
				$loc->address = $req->address;
				$loc->save();
			}

			DB::commit();
		} catch (\Exception $e) {
			Log::error($e);
			DB::rollback();

			return redirect()
				->back()
				->with('flash_error', 'Something went wrong, please try again later.');
		}

		return redirect()
			->route('admin.store.map.show', [$store->id])
			->with('flash_success', 'Successfully updated store\'s location.');
	}
}