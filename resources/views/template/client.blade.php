<!DOCTYPE html>
<html lang="en">
	<head>
		{{-- META DATA --}}
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		{{-- SITE META --}}
		<meta name="author" content="Code Senpai, Project on Rush">
		<meta name="type" content="website">
		<meta name="title" content="{{ env('APP_NAME') }}">
		<meta name="description" content="{{ env('APP_DESC') }}">
		<meta name="image" content="/images/branding/soulace_black.jpg">
		<meta name="keywords" content="Soulace, Funeral, Parlor, Funeral Parlor">
		<meta name="application-name" content="{{ env('APP_NAME') }}">

		{{-- TWITTER META --}}
		<meta name="twitter:card" content="summary_large_image">
		<meta name="twitter:title" content="{{ env('APP_NAME') }}">
		<meta name="twitter:description" content="{{ env('APP_DESC') }}">
		<meta name="twitter:image" content="{{Request::url()}}/images/branding/soulace_black.jpg">

		{{-- OG META --}}
		<meta name="og:url" content="{{Request::url()}}">
		<meta name="og:type" content="website">
		<meta name="og:title" content="{{ env('APP_NAME') }}">
		<meta name="og:description" content="{{ env('APP_DESC') }}">
		<meta name="og:image" content="/images/branding/soulace_black.jpg">

		{{-- jQuery 3.6.0 --}}
		<!-- <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script> -->
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

		{{-- popper.js 1.16.0 --}}
		<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>

		{{-- Bootstrap 4.4 --}}
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

		{{-- Bootrstrap 5.0.2 --}}
		<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
		<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">

		{{-- Removes the code that shows up when script is disabled/not allowed/blocked --}}
		<script type="text/javascript" id="for-js-disabled-js">$('head').append('<style id="for-js-disabled">#js-disabled { display: none; }</style>');$(document).ready(function() {$('#js-disabled').remove();$('#for-js-disabled').remove();$('#for-js-disabled-js').remove();});</script>

		{{-- Sweet Alert 2 --}}
		<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>

		{{-- Input Mask 5.0.5 --}}
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/5.0.5/jquery.inputmask.min.js"></script>

		{{-- Fontawesome --}}
		<script src="https://kit.fontawesome.com/d4492f0e4d.js" crossorigin="anonymous"></script>

		{{-- Map CSS 2.3.1 --}}
		<link href='https://api.mapbox.com/mapbox-gl-js/v2.3.1/mapbox-gl.css' rel='stylesheet'>

		{{-- Custom CSS --}}
		@yield('page-css')
		<link rel="stylesheet" href="/css/style.css" >
		<link rel="stylesheet" href="/css/client.css" >
		@yield('css')

		{{-- Favicon --}}
		<link rel='icon' type='image/png' href='/images/branding/logo_gradient.png'>

		{{-- Title --}}
		<title>{{ env('APP_NAME') }} | @yield('title')</title>
	</head>

	<body>
		{{-- SHOWS THIS INSTEAD WHEN JAVASCRIPT IS DISABLED --}}
		<div style="position: absolute; height: 100vh; width: 100vw; background-color: #ccc;" id="js-disabled">
			<style type="text/css">
				/* Make the element disappear if JavaScript isn't allowed */
				.js-only {
					display: none!important;
				}
			</style>
			<div class="row h-100">
				<div class="col-12 col-md-4 offset-md-4 py-5 my-auto">
					<div class="card shadow my-auto">
						<h4 class="card-header card-title text-center">Javascript is Disabled</h4>

						<div class="card-body">
							<p class="card-text">This website required <b>JavaScript</b> to run. Please allow/enable JavaScript and refresh the page.</p>
							<p class="card-text">If the JavaScript is enabled or allowed, please check your firewall as they might be the one disabling JavaScript.</p>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- Navbar -->
		<nav class="navbar fixed-top navbar-expand-lg navbar-dark p-md-3" style="transition: 0.125s; font-size: 1rem; padding: 1rem;" id="navbarmain">
			<div class="container">
				<button class="navbar-toggler float-left" type="button" data-bs-toggle="collapse" data-bs-target="#navmenu" id="togglemenu">
					<span class="navbar-toggler-icon"></span>
				</button>

				<div class="brand_logo">
					<a href="#" class="navbar-brand">
						<img src="/images/branding/soulace_white.png" id="soulace">
					</a>
				</div>

				<div class="collapse navbar-collapse hr-width-150" id="navmenu">
					<ul class="nav navbar-nav mx-3">
						<li class="nav-item mx-3">
							<div class="d-flex flex-d-col">
								@if (\Request::is('/'))
								<hr class="p-0 m-0 w-75">
								<span class="nav-link text-decoration-none active" aria-current="page">HOME</span>
								@else
								<a href="{{ route('home') }}" class="nav-link">HOME</a>
								@endif
							</div>
						</li>

						<li class="nav-item mx-3">
							<div class="d-flex flex-d-col">
								@if (\Request::is('store'))
								<hr class="p-0 m-0 w-75">
								<span class="nav-link text-decoration-none active" aria-current="page">STORE</span>
								@elseif (\Request::is('store/*'))
								<hr class="p-0 m-0 w-75">
								<a href="{{ route('store.index') }}" class="nav-link active" aria-current="page">STORE</a>
								@else
								<a href="{{ route('store.index') }}" class="nav-link">STORE</a>
								@endif
							</div>
						</li>

						<li class="nav-item mx-3">
							<div class="d-flex flex-d-col">
								@if (\Request::is('services/*'))
								<hr class="p-0 m-0 w-75">
								<a href="#" role="button" id="serviceBtn" data-toggle="dropdown" aria-expanded="false" class="nav-link dropdown-toggle active">SERVICES</a>
								@else
								<a href="#" role="button" id="serviceBtn" data-toggle="dropdown" aria-expanded="false" class="nav-link dropdown-toggle">SERVICES</a>
								@endif

								{{-- DROPDOWN MENU FOR SERVICES --}}
								<div class="dropdown-menu megamenu px-5 py-4" aria-labelledby="serviceBtn">
									<div class="row">
										<div class="col-12 col-lg-6" id="services_nav">
											<div class="row d-flex justify-content-center">
												<a href="/services#funeral" class="nav_serv card col-4 px-0 mx-2">
													<img src="/images/banners/services/ui_service1.png" alt="" class="card-img img-fluid">
													<div class="card-img-overlay flex-column my-auto p-0">
														<div class="my-auto w-100">
															Funeral
														</div>
													</div>
												</a>

												<a href="/services#memorial" class="nav_serv card col-4 px-0 mx-2">
													<img src="/images/banners/services/ui_service2.png" alt="" class="card-img img-fluid">
													<div class="card-img-overlay flex-column my-auto p-0">
														<div class="my-auto w-100">
															Memorial
														</div>
													</div>
												</a>
											</div>

											<div class="row mt-2 d-flex justify-content-center">
												<a href="/services#burial" class="nav_serv card col-4 px-0 mx-2">
													<img src="/images/banners/services/ui_service4.png" alt="" class="card-img img-fluid">
													<div class="card-img-overlay flex-column my-auto p-0">
														<div class="my-auto w-100">
															Burial
														</div>
													</div>
												</a>
											</div>
										</div>

										<div class="col-12 col-lg-6 px-5 align-items-center d-grid justify-content-center">
											<div class="col-12">
												<div class="row mt-2 mt-lg-0 d-flex justify-content-center">
													<a href="{{ route('services.obituary.index') }}" class="nav_serv card col-5 mx-2 px-0" id="navseobituary">
														<img src="/images/banners/services/obituary.png" alt="" class="card-img img-fluid">
														<div class="card-img-overlay flex-column my-auto p-0">
															<div class="my-auto w-100">
																Obituary
															</div>
														</div>
													</a>

													<a href="{{ route('services.livestream') }}" class="nav_serv card col-5 mx-2 px-0">
														<img src="/images/banners/services/livestream.png" alt="" class="card-img img-fluid">
														<div class="card-img-overlay flex-column my-auto p-0">
															<div class="my-auto w-100">
																Livestream
															</div>
														</div>
													</a>
												</div>
											</div>
										</div>
									</div> 
								</div>
							</div>
						</li>

						<li class="nav-item mx-3">
							<div class="d-flex flex-d-col">
								@if (\Request::is('about-us'))
								<hr class="p-0 m-0 w-75">
								<span class="nav-link text-decoration-none active" aria-current="page">ABOUT</span>
								@elseif (\Request::is('about-us/*'))
								<hr class="p-0 m-0 w-75">
								<a href="{{ route('about-us') }}" class="nav-link active">ABOUT</a>
								@else
								<a href="{{ route('about-us') }}" class="nav-link">ABOUT</a>
								@endif
								{{-- ABOUT-US CONVERSION --}}
							</div>
						</li>

						<li class="nav-item mx-3">
							<div class="d-flex flex-d-col">
								@if (\Request::is('contact-us'))
								<hr class="p-0 m-0 w-75">
								<span class="nav-link text-decoration-none active" aria-current="page">CONTACT</span>
								@elseif (\Request::is('contact-us/*'))
								<hr class="p-0 m-0 w-75">
								<a href="{{ route('contact-us') }}" class="nav-link active">CONTACT</a>
								@else
								<a href="{{ route('contact-us') }}" class="nav-link">CONTACT</a>
								@endif
							</div>
						</li>
					</ul>

					<hr style="flex: 1 1 auto; border-width: 1.5px; margin-bottom: 0; margin-right: 12.5%;">
				</div>

				<div class="nav-icons d-flex flex-row-reverse">
					@if (Auth::check())
					<a href="{{route('cart.index')}}" title="Cart" data-toggle="tooltip" data-bs-placement="bottom"><i class="fas fa-shopping-cart fa-lg mx-3"></i></a>
					<div class="dropdown">
						<a href="#" role="button" id="profileBtn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="far fa-user-circle fa-lg"></i></a>

						{{-- DROPDOWN MENU FOR SERVICES --}}
						<div class="dropdown-menu dropdown-menu-right" aria-labelledby="profileBtn">
							<a href="{{ route('account') }}" class="dropdown-item">My Account</a>
							<a href="{{ route('account.orders') }}" class="dropdown-item">Orders and Tracking</a>
							<a href="{{ route('account.livestreams') }}"  class="dropdown-item">Livestream Requests</a>
							@if (Auth::check())
								@if (Auth::user()->type == 'owner')
								<div class="dropdown-divider"></div>
								<a href="{{ route('owner') }}" class="dropdown-item">Dashboard</a>
								@elseif (Auth::user()->type == 'admin')
								<div class="dropdown-divider"></div>
								<a href="{{ route('admin') }}" class="dropdown-item">Dashboard</a>
								@endif
							@endif
							<div class="dropdown-divider"></div>
							<a href="{{ route('logout') }}" class="dropdown-item text-danger">Log out</a>
						</div>
					</div>
					@else
					<a href="{{ route('login') }}" title="Login" data-toggle="tooltip" data-bs-placement="bottom"><i class="fas fa-sign-in-alt fa-lg mx-3"></i></a>
					<a href="{{ route('register') }}" title="Register" data-toggle="tooltip" data-bs-placement="bottom"><i class="fas fa-user-plus fa-lg"></i></a>
					@endif
				</div>
			</div>
		</nav>
			
		<!-- Main Content-->
		<div class="container-fluid p-0 m-0">
			@yield('body')

			<div class="modal fade child-text-gray child-br-0" id="privacy_statetment" tabindex="-1" aria-labelledby="pslabel" aria-hidden="true" data-bs-backdrop="static" data-bs-keyboard="false">
				<div class="modal-dialog modal-dialog-centered modal-lg">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title text-center w-100 text-dark" id="pslabel">WE HAVE A NEW PRIVACY STATEMENT</h5>
						</div>
						
						<div class="modal-body">
							<p class="text-dark">Soulace Group respects your privacy and will keep secure and confidential all personal and sensitive information that you may provide to Soulace Group and/or those that Soulace Group may collect from you ("Personal Data")</p>
							<p class="text-dark">Please read carefully the Soulace Group Privacy Statement to understand how we treat Personal Data.</p>
							<p class="text-dark">Click <a href="#" target="_blank" style="color: blue;">here</a> to read the Privacy Statement in full.</p>
						</div>

						<div class="modal-footer">
							<button type="button" class="btn btn-dark text-white mx-auto px-5" data-bs-dismiss="modal" style="border-radius: 0;" id="privacy_policy_accepted">I AGREE</button>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- Footer -->
		<footer class="pt-1">
			<div class="container">
				<div class="row">
					<div class=" col-lg-3 col-sm-12" id="logo">
						<img src="/images/branding/soulace_white.png" alt="" class="img-fluid mb-2">
					
						<h5 class="mt-2">Keeping the memories alive.</h5>
						<div class="mt-3 px-3">
							{{-- <i class="bi-telephone-fill"> 111-9999</i> <br> --}}
							<i class="bi-envelope-fill">soulace@gmail.com</i> 
						</div>
					</div>

					<form action="{{ route('subscriber.store') }}" method="POST" class="col-lg-6 align-items-center p-5" id="newsletter">
						{{ csrf_field() }}
						<h3>Subscribe to our Newsletter to get the latest updates from us!</h3>
						
						<p class="lead">Get weekly dose of news, updates, tips, and special offers</p>
						
						<div class="d-grid form-group">
							<div class="input-group">
								<div class="input-group-prepend">
									<span class="input-group-text border-right-0" style="background-color: #00000000;"><i class="fas fa-envelope fa-lg color-custom"></i></span>
								</div>
								<input type="email" name="email" class="form-control border-left-0 text-white" style="background-color: #00000000; outline: none; border-top-right-radius: .25rem; border-bottom-right-radius: .25rem;" placeholder="Enter Your Email Address">
								<span class="badge badge-danger w-100 validation-message">{{$errors->first('email')}}</span>
								<span class="badge badge-success w-100 validation-message">{{Session::get('subscribe_success')}}</span>
							</div>
							<button type="submit" class="btn btn-light btn-lg btn-block mt-2" data-action="subscribe" data-action-message="Subscribing">Subscribe</button>
						</div>
					</form>

					<div class=" col-lg-3 mt-3">
						<div class="col" id="footer-links">
							<h4 class="footer-title font-weight-bold mb-3">NAVIGATION</h4>
							
							<a href="{{ route('home') }}" class="footer-link">HOME</a> <br>
							<a href="{{ route('store.index') }}" class="footer-link">STORE</a> <br>
							<a href="{{ route('about-us') }}" class="footer-link">ABOUT US</a> <br>
							<a href="{{ route('contact-us') }}" class="footer-link">CONTACT US</a> <br>
						</div>
					</div>
				</div>
				<div class="row mt-3">
					<div class="reserved col-lg-6 text-center text-lg-left py-4">
						<small>Privacy Policy | Terms and Conditions</small>
					</div>
					
					<div class="reserved col-lg-6 text-center text-lg-right py-4">
						<small>©2021 Soulace All Rights Reserved.</small>
					</div>
				</div>
			</div>
		</footer>
		
		<script src='https://api.mapbox.com/mapbox-gl-js/v2.3.1/mapbox-gl.js'></script>
		<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
		<script src="/js/client.js" type="text/javascript"></script>
		
		@yield('script')
		<script type="text/javascript">
			@if (Session::has('flash_error'))
			Swal.fire({
				{!!Session::has('has_icon') ? "icon: `error`," : ""!!}
				title: `{{Session::get('flash_error')}}`,
				{!!Session::has('message') ? 'html: `' . Session::get('message') . '`,' : ''!!}
				position: {!!Session::has('position') ? '`' . Session::get('position') . '`' : '`top`'!!},
				showConfirmButton: false,
				toast: {!!Session::has('is_toast') ? Session::get('is_toast') : true!!},
				{!!Session::has('has_timer') ? (Session::get('has_timer') ? (Session::has('duration') ? ('timer: ' . Session::get('duration')) . ',' : `timer: 10000,`) : '') : `timer: 10000,`!!}
				background: `#dc3545`,
				customClass: {
					title: `text-white`,
					content: `text-white`,
					popup: `px-3`
				},
			});

				@if (env('APP_ENV') == 'local' || env('APP_ENV') == 'dev')
					if ('{{Session::get('flash_error')}}' == 'Something went wrong, please try again later') {
						console.log(`{!!Session::get('flash_error_actual')!!}`);
					}
				@endif
			@elseif (Session::has('flash_message'))
			Swal.fire({
				{!!Session::has('has_icon') ? "icon: `info`," : ""!!}
				title: `{{Session::get('flash_message')}}`,
				{!!Session::has('message') ? 'html: `' . Session::get('message') . '`,' : ''!!}
				position: {!!Session::has('position') ? '`' . Session::get('position') . '`' : '`top`'!!},
				showConfirmButton: false,
				toast: {!!Session::has('is_toast') ? Session::get('is_toast') : true!!},
				{!!Session::has('has_timer') ? (Session::get('has_timer') ? (Session::has('duration') ? ('timer: ' . Session::get('duration')) . ',' : `timer: 10000,`) : '') : `timer: 10000,`!!}
				background: `#17a2b8`,
				customClass: {
					title: `text-white`,
					content: `text-white`,
					popup: `px-3`
				},
			});
			@elseif (Session::has('flash_success'))
			Swal.fire({
				{!!Session::has('has_icon') ? "icon: `success`," : ""!!}
				title: `{{Session::get('flash_success')}}`,
				{!!Session::has('message') ? 'html: `' . Session::get('message') . '`,' : ''!!}
				position: {!!Session::has('position') ? '`' . Session::get('position') . '`' : '`top`'!!},
				showConfirmButton: false,
				toast: {!!Session::has('is_toast') ? Session::get('is_toast') : true!!},
				{!!Session::has('has_timer') ? (Session::get('has_timer') ? (Session::has('duration') ? ('timer: ' . Session::get('duration')) . ',' : `timer: 10000,`) : '') : `timer: 10000,`!!}
				background: `#28a745`,
				customClass: {
					title: `text-white`,
					content: `text-white`,
					popup: `px-3`
				},
			});
			@endif

			@if (Session::has('move_to'))
			$(document).ready(() => {
				window.location = '{{ Request::url() . Session::get("move_to") }}';
			});
			@endif

			$(document).ready(() => {
				
			@if (Auth::check() && !Auth::user()->is_privacy_read)
				$('#privacy_statetment').modal('show');

				$('#privacy_policy_accepted').on('click', function() {
					$.post('{{ route('utility.update_privacy_read') }}', {
						_token: '{{csrf_token()}}',
					}).done((data) => {
						if (!data.status) {
							console.log("ERROR MESSAGE:");
							console.log(data.error_message);

							Swal.fire({
								icon: `error`,
								title: data.message,
								position: `top`,
								showConfirmButton: false,
								toast: true,
								timer: 10000,
								background: `#dc3545`,
								customClass: {
									title: `text-white`,
									content: `text-white`,
									popup: `px-3`
								},
							});
						}
					});
				});
			@endif
			});
		</script>
	</body>
</html>