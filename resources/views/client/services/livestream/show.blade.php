@extends('template.client')

@section('title', 'Store')

@section('page-css')
<link rel="stylesheet" href="/css/services.livestream.css" >
<link rel="stylesheet" href="https://unpkg.com/@mux/videojs-kit@latest/dist/index.css">
<link href="https://unpkg.com/@videojs/themes@1/dist/forest/index.css" rel="stylesheet">
@endsection

@section('body')
{{-- MAIN CONTENT --}}
<section class="p-md-5" id="banner" style="height: auto;">
	<div class="container pt-md-5" >
		<div class="d-sm-flex justify-content-between">
			<div class ="col-md-8 col-sm-12 p-5" id="bannertext">
				<h1>Livestream.</h1>
				<p class="lead my-4">Send your sympathy virtually. Soulace lets you watch funeral wakes online whenever and wherever you are.</p>
			</div>
		</div>
	</div>

	<div class="container-fluid d-flex flex-d-col">
		@if ($livestream->status == strtolower(App\Livestream::getStatusList()[4]) || $livestream->status == strtolower(App\Livestream::getStatusList()[5]))
		<h3 class="text-center mb-2">{{$livestream->title}}</h3>
		<video id="stream" class="video-js vjs-theme-forest vjs-fluid text-center w-100" controls data-setup="{}">
			<source src="{{$livestream->stream_link}}" type="application/x-mpegURL"/>

			<p class="vjs-no-js">
				To view this video please enable JavaScript, and consider upgrading to a
				web browser that
				<a href="https://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
			</p>
		</video>
		@elseif ($livestream->status == strtolower(App\Livestream::getStatusList()[2]))
		<h3 class="border rounded w-50 mx-auto py-3 text-center">Awaiting approval from Soulace...</h3>
		@elseif ($livestream->status == strtolower(App\Livestream::getStatusList()[0]))
		<h3 class="border rounded w-50 mx-auto py-3 text-center">This livestream has been called off. Sorry for the inconvenience...</h3>
		@endif
	</div>

	@if ($livestream->status == strtolower(App\Livestream::getStatusList()[4]))
	@if (Auth::check())
	@if (Auth::user()->id == $livestream->ls_user->id)
	<div class="container-fluid mt-5 d-flex flex-d-row" id="form-option">
		<input type="hidden" name="_token" value="{{csrf_token()}}">

		<div class="form-check mx-auto">
			<input type="checkbox" name="standing_photo" class="form-check-input" id="standing_photo" {{$livestream->getOptionStatus('standing_photo') == 1 ? 'checked' : ''}}>
			<label class="form-check-label" for="standing_photo">Standing Photo</label>
		</div>

		<div class="form-check mx-auto">
			<input type="checkbox" name="video_slideshow" class="form-check-input" id="video_slideshow" {{$livestream->getOptionStatus('video_slideshow') == 1 ? 'checked' : ''}}>
			<label class="form-check-label" for="video_slideshow">Video Slideshow</label>
		</div>

		<div class="form-check mx-auto">
			<input type="checkbox" name="flower_stands" class="form-check-input" id="flower_stands" {{$livestream->getOptionStatus('flower_stands') == 1 ? 'checked' : ''}}>
			<label class="form-check-label" for="flower_stands">Flower Stands</label>
		</div>
	</div>
	@endif
	@endif

	<div class="container-fluid {{Auth::check() ? (Auth::user()->id == $livestream->ls_user->id ? 'mt-5' : '') : ''}}">
		<p>{{$livestream->description}}</p>
	</div>
	@endif
</section>
@endsection

@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/video.js/7.0.0/video.min.js" integrity="sha512-LiILFcpZ9QKSH41UkK59Zag/7enHzqjr5lO2M0wGqGn8W19A/x2rV3iufAHkEtrln+Bt+Zv1U6NlLIp+lEqeWQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/videojs-contrib-hls/5.15.0/videojs-contrib-hls.min.js" integrity="sha512-R1+Pgd+uyqnjx07LGUmp85iW8MSL1iLR2ICPysFAt8Y4gub8C42B+aNG2ddOfCWcDDn1JPWZO4eby4+291xP9g==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

<script>
	var player = videojs('stream');
	// player.play();

	function keepTokenAlive() {
		$.ajax({
			url: '{{route('token-extend')}}',
			method: 'POST',
			headers: {
				'X-CSRF-TOKEN': $('[name=_token]').val()
			}
		}).then((data) => {
			console.log(new Date() + ' ' + result + ' ' + $('[name=_token]').val());
		});
	}

	$(document).ready(() => {
		// Runs every 15 minutes...
		setInterval(keepTokenAlive, 1000 * 60 * 15);

		$('#form-option').on('change', '[type=checkbox]', (e) => {
			let obj = $(e.currentTarget);
			$.post('{{route('livestream.options.update', [$livestream->id])}}', {
				'_token': $('[name=_token]').val(),
				'key': obj.attr('id'),
				'value': $('#' + obj.attr('id')).prop('checked') ? '1' : '0',
			});
		});
	});
</script>
@endsection