@extends('template.print-details')

@section('title', 'Print Sales Order Details')
@section('sidebar', 'admin')

@section('body')
<h3 class="font-weight-normal text-center">Order No. {{$sales_order->id}}</h3>
<br>
<div class="row px-0">
	<div class="col-6 d-flex flex-d-col">
		<span class="font-weight-bold">Customer Details</span>
		<span class="pl-3">{{$sales_order->customer->getName()}}</span>
		<span class="pl-3">(+63){{$sales_order->customer->contact_number}}</span>
		<span class="pl-3">{{$sales_order->customer->email}}</span>
		<br>
		<span class="font-weight-bold">Shipping Details</span>
		<span class="pl-3">{{$sales_order->shippingAddress->getAddress(true, true)}}</span>
		<span class="pl-3">{{$sales_order->shippingAddress->city}}, {{$sales_order->shippingAddress->region}}</span>
	</div>

	<div class="col-6 d-flex flex-d-col">
		<span class="font-weight-bold">Billing Details</span>
		<span class="pl-3">{{$sales_order->customer->getName()}}</span>
		<span class="pl-3">{{$sales_order->shippingAddress->getAddress(true, true)}}</span>
		<span class="pl-3">{{$sales_order->shippingAddress->city}}, {{$sales_order->shippingAddress->region}}</span>
	</div>
</div>
<br>
<div class="row px-0">
	<div class="col-6 d-flex flex-d-col">
		<span class="font-weight-bold">Payment Method</span>
		<span class="pl-3">{{ucwords($sales_order->payment_method)}}</span>
	</div>

	<div class="col-6 d-flex flex-d-col">
		<span class="font-weight-bold">Shipping Method</span>
		<span class="pl-3">{{$sales_order->courier}}</span>
	</div>
</div>
<br>
<div class="row px-0">
	<div class="col-12">
		<table class="table table-print">
			<thead class="text-center" style="border: inherit!important;">
				<tr>
					<td>Image</td>
					<td>Name</td>
					<td>Price</td>
					<td>QTY</td>
					<td>Total Amount</td>
				</tr>
			</thead>

			<tbody class="text-center">
				@foreach ($sales_order->items as $i)
				<tr>
					<td>
						<img class="img-fluid border" src='/uploads/products/{{$i->product->category->category_name}}/{{$i->product->image_name}}' style="max-width: 25%;">
					</td>
					<td>{{$i->product->product_name}}</td>
					<td>₱{{number_format($i->product->selling_price, 2)}}</td>
					<td>{{$i->quantity}}</td>
					<td>₱{{number_format($i->price, 2)}}</td>
				</tr>
				@endforeach

				<tr>
					<td colspan="3" rowspan="4">
						@if ($sales_order->proof_of_payment != null)
						<p class="text-left">Proof of Payment:</p>
						<img src="/proof/{{$sales_order->proof_of_payment}}" class="border img-fluid">
						@endif
					</td>
					<td>Subtotal:</td>
					<td>₱{{number_format($sales_order->total, 2)}}</td>
				</tr>

				<tr>
					<td>Shipping:</td>
					<td>₱0.00</td>
				</tr>

				<tr>
					<td>Tax:</td>
					<td>₱0.00</td>
				</tr>

				<tr>
					<td colspan="2">₱{{number_format($sales_order->total, 2)}}</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>
@endsection