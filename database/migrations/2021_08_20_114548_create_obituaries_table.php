<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateObituariesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('obituaries', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('customer_id')->unsigned();
			$table->string('name');
			$table->date('birth_date');
			$table->date('death_date');
			$table->integer('store_id')->unsigned();
			$table->mediumText('description')->nullable();
			$table->date('available_from');
			$table->date('available_to')->nullable();
			$table->time('begin_time');
			$table->string('location')->nullable();
			$table->mediumText('address')->nullable();
			$table->string('image_name')->nullable();
			$table->tinyInteger('status')->unsigned()->default(0);

			$table->foreign('customer_id')->references('id')->on('users')->onDelete('cascade');
			$table->foreign('store_id')->references('id')->on('stores')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('obituaries');
	}
}