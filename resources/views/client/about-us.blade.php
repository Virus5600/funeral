@extends('template.client')

@section('title', 'About Us')

@section('page-css')
<link rel="stylesheet" href="/css/about-us.css" >
@endsection

@section('body')
{{-- ABOUT-US BANNER --}}
<section class="p-5 mb-5" id="banner">
	<div class="container pt-md-5 mt-5">
		<div class="row text-center">
			<h1><img src="/images/UI/line.svg" class="imgline mr-2">About Us</h1>
			<h5>We are more than you think.</h5>
		</div>

		<div class="row mt-5">
			<h1>Our Story</h1>
			<p class="lead">We are researchers of FAR EASTERN UNIVERSITY CAVITE. We are currently taking Bachelor of Science in Information Technology. This platform is specifically made for our capstone project in order to complete our curriculum. And with that, We want to broad the e-commerce marketing strategy without the funeral businesses being left and that leads us with the portal.</p>
		</div>

		<div class="row mt-5">
			<div class="col-12 col-lg-6">
				<h1>Our Mission</h1>
				<p class="lead">To provide a platform that would help people commemorate the memories of their loved ones through the web app. To develop a connection and trust families and community.</p>
			</div>

			<div class="col-12 col-lg-6">
				<h1>Our Vision</h1>
				<p class="lead">To be leading online services portal for funeral homes.</p>
			</div>
		</div>
	</div>
</section>	  
@endsection