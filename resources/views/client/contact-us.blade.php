@extends('template.client')

@section('title', 'Contact Us')

@section('page-css')
<link rel="stylesheet" href="/css/contact-us.css" >
@endsection

@section('body')
{{-- CONTACT US BANNER --}}
<section class="p-5" id="banner">
	<div class="container pt-md-5 mt-5" >
		<div class="row text-center">
			<h1><img src="/images/UI/line.svg" class="imgline"> Contact Us</h1>
			<h5>Got some questions? Don't hesitate to send it now.</h5>
		</div>

		<form class="row" action="{{ route('mail.inquiry.send') }}" method="POST" enctype="multipart/form-data" id="inquiry_form">
			{{ csrf_field() }}

			<div class="col col-lg-6 col-12 my-5" id="inputform">
				<div class="mb-3">
					<input type="text" class="form-control" placeholder="Your Name" name="name" value="{{ old('name') }}">
					<span class="badge badge-danger w-100 validation-message">{{$errors->first('name')}}</span>
				</div>

				<div class="mb-3">
				    <input type="email" class="form-control" placeholder="Email Address" name="email" value="{{ old('email') }}">
				    <span class="badge badge-danger w-100 validation-message">{{$errors->first('email')}}</span>
				</div>

				<div class="mb-3">
				    <textarea class="form-control" placeholder="Your Message Here" rows="10" name="message">{{ old('message') }}</textarea>
				    <span class="badge badge-danger w-100 validation-message">{{$errors->first('message')}}</span>
				</div>

				<div class="mb-3">
				    <button class="btn btn-lg btn-light">Submit</button>
				</div>
			</div>
            
			<div class="col col-lg-6 col-12 my-5 p-4">
				<div class="mx-md-5">
					<h5><i class="bi bi-house-fill mx-sm-3"></i>1107 Metro Manila, NCR, Philippines</h5>
					<h5 class="my-4"><i class="bi bi-envelope-open-fill mx-sm-3"></i> soulace@gmail.com</h5>
					<h5 class="my-4"><i class="bi bi-telephone-inbound-fill mx-sm-3"></i>(012) 123-4567</h5>
				</div>

				<div class="mx-md-5 my-5">
					<h4>Visit us at..</h4>
					<h5 class="my-4"><i class="bi bi-facebook mr-2"></i>facebook.com/soulace</h5>
					<h5 class="my-4"><i class="bi bi-twitter mr-2"></i>twitter.com/soulace</h5>
				</div>
			</div>
		</form>
	</div>
</section>
@endsection