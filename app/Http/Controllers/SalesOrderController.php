<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Requests;

use App\Livestream;
use App\LivestreamLog;
use App\SalesOrder;
use App\SalesOrderLog;

use DB;
use Exception;
use Log;
use Validator;

class SalesOrderController extends Controller
{
	// ADMIN SIDE //
	public function index() {
		$so = Livestream::where('status', '!=', 'cart');
		$search = null;

		if (request()->has('s')) {
			$search = request()->get('s');
			$so = $so->leftJoin('users', 'users.id', '=', 'sales_order.customer_id');

			$so = $so->where('livestream.title', '=', '%'.$search.'%')
				->orWhere('livestream.stream_id', 'LIKE', '%'.$search.'%')
				->orWhere('livestream.stream_key', 'LIKE', '%'.$search.'%')
				->orWhere('users.first_name', 'LIKE', '%'.$search.'%')
				->orWhere('users.middle_name', 'LIKE', '%'.$search.'%')
				->orWhere('users.last_name', 'LIKE', '%'.$search.'%')
				->select('livestream.*');
		}

		return view('admin.sales_order.index', [
			'search' => $search,
			'so' => $so->paginate(15)
		]);
	}

	public function prints($id) {
		return view('admin.sales_order.print', [
			'livestream' => Livestream::find($id)
		]);
	}

	public function show($id) {
		return view('admin.sales_order.show', [
			'livestream' => Livestream::find($id),
			'steps' => array_slice(Livestream::getStatusList(), 1)
		]);
	}

	protected function acceptProof($id) {
		$so = Livestream::find($id);

		if ($so->displayStatus(true) != 2) {
			return redirect()
				->back()
				->with('flash_error', 'Illegal action! Please refrain from doing similar actions in the future!');
		}

		try {
			DB::beginTransaction();

			$so->status = strtolower(Livestream::getStatusList()[3]);
			$so->save();

			LivestreamLog::create([
				'livestream_id' => $so->id,
				'description' => 'Submitted proof accepted. {image: ' . $so->proof_of_payment . '}',
				'time' => \Carbon\Carbon::now(),
				'type' => 2,
			]);

			DB::commit();
		} catch (Exception $e) {
			DB::rollback();
			Log::error($e);

			return redirect()
				->route('admin.sales-order.index')
				->with('flash_error', 'Something went wrong, please try again later.');
		}

		return redirect()
			->back()
			->with('flash_success', 'Proof accepted!');
	}

	protected function rejectProof($id) {
		$so = Livestream::find($id);

		if ($so->displayStatus(true) != 2) {
			return redirect()
				->back()
				->with('flash_error', 'Illegal action! Please refrain from doing similar actions in the future!');
		}

		try {
			DB::beginTransaction();

			LivestreamLog::create([
				'livestream_id' => $so->id,
				'description' => 'Submitted proof rejected. {image: ' . $so->proof_of_payment . '}',
				'time' => \Carbon\Carbon::now(),
				'type' => 2,
			]);

			$so->proof_of_payment = null;
			$so->status = Livestream::getStatusList()[1];
			$so->save();

			DB::commit();
		} catch (Exception $e) {
			DB::rollback();
			Log::error($e);

			return redirect()
				->route('admin.sales-order.index')
				->with('flash_error', 'Something went wrong, please try again later.');
		}

		return redirect()
			->back()
			->with('flash_success', 'Proof rejected!');
	}

	// CLIENT SIDE //
	protected function cancel($id) {
		$so = SalesOrder::find($id);

		try {
			DB::beginTransaction();

			$so->status = 0;
			$so->save();

			SalesOrderLog::create([
				'order_id' => $so->id,
				'description' => 'Cancelled Order.',
				'time' => \Carbon\Carbon::now(),
				'type' => 0
			]);

			DB::commit();
		} catch (\Exception $e) {
			Log::error($e);
			DB::rollback();
		}

		return redirect()
			->route('account.orders');
	}

	protected function uploadProof($id) {
		$so = SalesOrder::find($id);

		if ($so->status == 1)
			return view('client.account.orders_and_tracking.upload-proof', [
				'sales_order' => $so
			]);
		else
			return redirect()->back();
	}

	protected function postUploadProof(Request $req, $id) {
		$validator = Validator::make($req->all(), [
			'proof' => 'required|mimes:jpg,jpeg,png|max:5120'
		], [
			'proof.required' => 'The proof of payment is required.',
			'proof.mimes' => 'The accepted formats are JPG, JPEG, and PNG.',
			'proof.max' => 'The maximum allowed file size is 5MB.'
		]);

		if ($validator->fails()) {
			return redirect()
				->back()
				->withErrors($validator);
		}

		if ($req->proof) {
			$filenameWithExt = $req->proof->getClientOriginalName();
			$filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
			$extension = $req->proof->getClientOriginalExtension();
			$add_photo = 'user_'.Auth::user()->id.\Carbon\Carbon::now()->timestamp.'.'.$extension;
			$path = $req->proof->move(public_path().'/proof',$add_photo);
		}

		try {
			DB::beginTransaction();

			$so = SalesOrder::find($id);
			$so->proof_of_payment = $add_photo;
			$so->status = 2;
			$so->save();

			SalesOrderLog::create([
				'order_id' => $so->id,
				'description' => 'Submitted Proof of Payment',
				'time' => \Carbon\Carbon::now(),
				'type' => 1
			]);

			DB::commit();
		} catch (\Exception $e) {
			Log::error($e);
			DB::rollback();

			return redirect()
				->route('account.orders')
				->with('flash_error', 'Something went wrong, please try again later.');
		}

		return redirect()
			->route('account.orders')
			->with('flash_success', 'Proof of Payment uploaded successfully.');
	}

	protected function orderReceived($id) {
		$so = SalesOrder::find($id);
		
		try {
			DB::beginTransaction();

			$so->status = 5;
			$so->save();

			SalesOrderLog::create([
				'order_id' => $so->id,
				'description' => 'Successfully received order.',
				'time' => \Carbon\Carbon::now(),
				'type' => 4
			]);

			DB::commit();
		} catch (\Excepotion $e) {
			Log::error($e);
			DB::rollback();

			return redirect()
				->route('account.orders')
				->with('flash_error', 'Something went wrong, please try again later.');
		}

		return redirect()
				->route('account.orders')
				->with('flash_success', 'Order successfully received! Thank you for trusting Soulace and ' . $so->store->name . '.');
	}
}