@extends('template.admin')

@section('title', 'Dashboard')
@section('sidebar', 'owner')

@section('css')
<style type="text/css">
	div.table-container {
		overflow-x: auto;
	}

	div.table-container > table {
		white-space: nowrap;
	}
</style>
@endsection

@section('body')
<h1 class="h3 h1-lg font-weight-bold text-truncate">Dashboard Overview</h1>

<hr class="hr-thick" style="border-color: #707070;">

{{-- FIRST ROW --}}
<div class="row mx-0 mt-2 mb-5">
	{{-- INVOICE TOTAL --}}
	<div class="col-12 col-md-6 col-xl-3 my-2 my-xl-0">
		<div class="px-2 d-flex flex-d-col flex-d-md-row border rounded dark-shadow summary-card h-100 py-2 py-lg-0">
			<i class="fas fa-file-invoice fa-5x mx-auto mx-lg-2 my-auto"></i>
			<div class="d-flex flex-d-col text-right w-100">
				<h4 class="h6 h4-lg mx-auto mx-lg-0">Invoice Total</h4>
				<h1 class="mx-auto mx-lg-0">{{count(App\SalesOrder::where('status', '>', 0)->where('store_id', '=', Auth::user()->store_id)->get())}}</h1>
			</div>
		</div>
	</div>

	{{-- PAID TOTAL --}}
	<div class="col-12 col-md-6 col-xl-3 my-2 my-xl-0">
		<div class="px-2 d-flex flex-d-col flex-d-md-row border rounded dark-shadow summary-card h-100 py-2 py-lg-0">
			<i class="fas fa-receipt fa-5x mx-auto mx-lg-2 my-auto"></i>
			<div class="d-flex flex-d-col text-right w-100">
				<h4 class="h6 h4-lg mx-auto mx-lg-0">Paid Total</h4>
				<h1 class="mx-auto mx-lg-0">{{count(App\SalesOrder::where('status', '>', '1')->where('store_id', '=', Auth::user()->store_id)->get())}}</h1>
			</div>
		</div>
	</div>

	{{-- UNPAID TOTAL --}}
	<div class="col-12 col-md-6 col-xl-3 my-2 my-xl-0">
		<div class="px-2 d-flex flex-d-col flex-d-md-row border rounded dark-shadow summary-card h-100 py-2 py-lg-0">
			<i class="fas fa-money-check-alt fa-5x mx-auto mx-lg-2 my-auto"></i>
			<div class="d-flex flex-d-col text-right w-100">
				<h4 class="h6 h4-lg mx-auto mx-lg-0">Unpaid Total</h4>
				<h1 class="mx-auto mx-lg-0">{{count(App\SalesOrder::where('status', '=', '1')->where('store_id', '=', Auth::user()->store_id)->get())}}</h1>
			</div>
		</div>
	</div>

	{{-- INVOICE TOTAL --}}
	<div class="col-12 col-md-6 col-xl-3 my-2 my-xl-0">
		<div class="px-2 d-flex flex-d-col flex-d-md-row border rounded dark-shadow summary-card h-100 py-2 py-lg-0">
			<i class="fas fa-history fa-5x fa-flip-horizontal mx-auto mx-lg-2 my-auto"></i>
			<div class="d-flex flex-d-col text-right w-100">
				<h4 class="h6 h4-lg mx-auto mx-lg-0">Pending Order</h4>
				<h1 class="mx-auto mx-lg-0">{{count(App\SalesOrder::where('status', '=', '2')->where('store_id', '=', Auth::user()->store_id)->get())}}</h1>
			</div>
		</div>
	</div>
</div>

{{-- SECOND ROW --}}
<div class="row mx-0 my-5">
	{{-- MONTHLY EARNINGS --}}
	<div class="col-12 col-xl-4 d-flex flex-d-col my-3 my-xl-0">
		<div class="card border rounded dark-shadow h-100">
			<h5 class="card-header">Monthly Earnings</h5>
			<div class="card-body d-flex">
				<canvas id="monthlyEarnings" class="rounded m-auto" style="border: solid 1px #707070;"></canvas>
			</div>
		</div>
	</div>

	{{-- POPULAR PRODUCT --}}
	<div class="col-12 col-xl-4 d-flex flex-d-col my-3 my-xl-0">
		<div class="card border rounded dark-shadow h-100">
			<h5 class="card-header">Popular Products</h5>
			<div class="card-body table-container px-0">
				<table class="table table-striped overflow-x">
					<thead>
						<tr>
							<td>Product</td>
							<td>Store</td>
							<td>Quantity</td>
						</tr>
					</thead>

					<tbody>
						@php ($prodCount = 0)
						@forelse ($popular_products as $k => $v)
						<tr>
							<td>{{preg_replace('/_/', ' ', $k)}}</td>
							<td>{{$v['store_name']}}</td>
							<td class="text-center">{{$v['quantity']}}</td>
						</tr>
						@php
						$prodCount++;
						if ($prodCount == 4)
							break;
						@endphp
						@empty
						<tr>
							<td colspan="3" class="text-center">No Avaiable Data in Table</td>
						</tr>
						@endforelse
					</tbody>
				</table>
			</div>
		</div>
	</div>

	{{-- QUICK DETAILS --}}
	<div class="col-12 col-xl-4 my-3 my-xl-0">
		<div class="card border rounded dark-shadow h-100">
			<h5 class="card-header">Quick Details</h5>
			<div class="card-body d-flex flex-d-col">
				{{-- INVOICE TOTAL --}}
				<div class="row my-2 summary-card">
					<div class="col-2">
						<i class="fas fa-lg fa-file-invoice mx-2 my-auto"></i>
					</div>

					<div class="col">
						<h6>
							{{count(App\SalesOrder::where('status', '>', '0')->get())}}
							Invoices
						</h6>
					</div>

					<div class="col">
						<p class="text-muted">
							Total Sales
						</p>
					</div>
				</div>

				{{-- PAID TOTAL --}}
				<div class="row my-2 summary-card">
					<div class="col-2">
						<i class="fas fa-lg fa-receipt mx-2 my-auto"></i>
					</div>

					<div class="col">
						<h6>
							{{count(App\SalesOrder::where('status', '>', '1')->get())}}
							Paids
						</h6>
					</div>

					<div class="col">
						<p class="text-muted">
							Total Paid Sales
						</p>
					</div>
				</div>

				{{-- UNPAID TOTAL --}}
				<div class="row my-2 summary-card">
					<div class="col-2">
						<i class="fas fa-lg fa-money-check-alt mx-2 my-auto"></i>
					</div>

					<div class="col">
						<h6>
							{{count(App\SalesOrder::where('status', '=', '1')->get())}}
							Unpaids
						</h6>
					</div>

					<div class="col">
						<p class="text-muted">
							Awaiting Payment
						</p>
					</div>
				</div>

				{{-- INVOICE TOTAL --}}
				<div class="row my-2 summary-card">
					<div class="col-2">
						<i class="fas fa-lg fa-history fa-flip-horizontal mx-2 my-auto"></i>
					</div>

					<div class="col">
						<h6>
							{{count(App\SalesOrder::where('status', '=', '2')->get())}}
							Pending Orders
						</h6>
					</div>

					<div class="col">
						<p class="text-muted">
							Awaiting Process
						</p>
					</div>
				</div>

				{{-- OBITUARIES TOTAL --}}
				<div class="row my-2 summary-card">
					<div class="col-2">
						<i class="fas fa-lg fa-book-open mx-2 my-auto"></i>
					</div>

					<div class="col">
						<h6>
							{{count(App\Obituary::where('store_id', '=', Auth::user()->store_id)->get())}}
							Obituaries
						</h6>
					</div>

					<div class="col">
						<p class="text-muted">
							Total Amount of Obituaries
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

{{-- THIRD ROW --}}
<div class="row mx-0 my-5">
	{{-- ORDERS TO BE PROCESSED --}}
	<div class="col-12">
		<div class="card">
			<h5 class="card-header">Orders to be Processed</h5>
			<div class="card-body overflow-auto">
				<table class="table table-striped">
					<thead>
						<tr>
							<td>Order #</td>
							<td>Order Date</td>
							<td>Customer Name</td>
							<td>Total Amount</td>
							<td>Payment Method</td>
							<td>Shipping</td>
						</tr>
					</thead>

					<tbody>
						@forelse ($unprocessed_sales as $u)
						<tr>
							<td>{{$u->id}}</td>
							<td>{{$u->created_at}}</td>
							<td>{{$u->customer->getName()}}</td>
							<td>₱{{number_format($u->total, 2)}}</td>
							<td>{{ucwords($u->payment_method)}}</td>
							<td>{{$u->courier}}</td>
						</tr>
						@empty
						<tr>
							<td colspan="7" class="text-center">No Avaiable Data in Table</td>
						</tr>
						@endforelse
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@endsection

@section('script')
<script>
	$(document).ready(() => {
		let target = $('#monthlyEarnings');
		let labels = [
			@foreach ($months as $m)
			'{{$m}}',
			@endforeach
		];
		let dataset = [{
			data: [
				@for ($i = 0; $i < count($months); $i++)
				{{$monthly_earnings[$i]}},
				@endfor
			],
			borderColor: '#707070',
			backgroundColor: '#707070',
		}];

		new Chart(target, {
			type: 'bar',
			data: {
				labels: labels,
				datasets: dataset
			},
			options: {
				plugins: {
					legend: {
						display: false
					}
				},
				responsive: true,
				maintainAspectRatio: false
			}
		});

		window.addEventListener('beforeprint', () => {
			myChart.resize(600, 600);
		});

		window.addEventListener('afterprint', () => {
			myChart.resize();
		});
	});
</script>
@endsection