<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function (Blueprint $table) {
			$table->increments('id');
			$table->string('first_name');
			$table->string('middle_name')->nullable();
			$table->string('last_name');
			$table->string('suffix')->nullable();
			$table->string('contact_number')->nullable();
			$table->mediumText('address')->nullable();
			$table->enum('region', ['NCR', 'CAR', 'Region I', 'Region II', 'Region III', 'Region IV-A', 'Region IV-B', 'Region V', 'Region VI', 'Region VII', 'Region VIII', 'Region IX', 'Region X', 'Region XI', 'Region XII', 'Region XIII', 'BARMM'])->nullable();
			$table->string('city')->nullable();
			$table->integer('zip_code')->nullable();
			$table->date('birthday');
			$table->enum('gender', ['Male', 'Female', 'Others'])->default('Others');
			$table->string('email')->unique();
			$table->string('password');
			$table->enum('type', ['client', 'owner', 'admin'])->default('client');
			$table->integer('store_id')->unsigned()->nullable();
			$table->tinyInteger('is_privacy_read')->unsigned()->default(0);
			$table->tinyInteger('is_validated')->unsigned()->default(0);
			$table->rememberToken();
			$table->timestamps();

			$table->foreign('store_id')->references('id')->on('stores')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}
}