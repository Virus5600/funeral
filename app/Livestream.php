<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Exception;
use DB;
use Log;

class Livestream extends Model {
	
	protected $table ="livestream";

	protected $fillable = [
		'title',
		'stream_id',
		'stream_key',
		'stream_link',
		'description',
		'schedule_date',
		'schedule_time',
		'duration',
		'price',
		'user_id',
		'status',
		'proof_of_payment',
		'active',
		'options',
		'category'
	];

	protected function ls_user() {
		return $this->belongsTo('App\User','user_id');
	}

	protected function logs() {
		return $this->hasMany('App\LivestreamLog', 'livestream_id');
	}

	public function getOptionStatus($option='all') {
		$options = $this->options;

		$result = null;
		if ($option == 'all') {
			$split = preg_split('/;/i', $options);
			
			$result = array();
			foreach ($split as $s) {
				$result[preg_split('/=/i', $s)[0]] = preg_split('/=/i', $s)[1];
			}
		}
		else if ($option == 'standing_photo' || $option == 'video_slideshow' || $option == 'flower_stands') {
			$result = preg_replace('/.*(' . $option . '=\d).*/', '$1', $options);
			$result = preg_split('/=/', $result)[1];
		}

		return $result;
	}

	public function updateOptionStatus($value='1', $option='all') {

		try {
			DB::beginTransaction();

			$options_raw = $this->options;
			$options = array();

			foreach (preg_split('/;/i', $options_raw) as $s) {
				$split = preg_split('/=/i', $s);
				$options[$split[0]]	= $split[1];
			}

			if ($option == 'all') {
				foreach ($options as $k => $v) {
					$options[$k] = $value;
				}
			}
			else if ($option == 'standing_photo' || $option == 'video_slideshow' || $option == 'flower_stands') {
				$options[$option] = $value;
			}

			$result = '';
			foreach ($options as $k => $v) {
				$result .= $k.'='.$v.';';
			}

			$this->options = substr($result, 0, strlen($result)-1);
			$this->save();

			DB::commit();
		} catch (Exception $e) {
			DB::rollback();
			Log::error($e);

			return false;
		}

		return true;
	}

	public static function getCategoryList($inRegex=false) {
		$list = array(
			'Funeral',
			'Memorial',
			'Burial'
		);

		if (!$inRegex)
			return $list;
		else {
			$toRet = '/(none)|';
			foreach ($list as $l) {
				$toRet .= '('.strtolower($l).')|';
			}
			$toRet = substr($toRet, 0, strlen($toRet)-1);
			$toRet .= '/';
			return $toRet;
		}
	}

	public function isCart() {
		if ($this->status == 'cart')
			return true;
		return false;
	}

	public static function getStatusList() {
		return ['Cancelled', 'Waiting for Proof', 'Pending', 'Awaiting Schedule', 'Streaming', 'Finished', 'Cart'];
	}

	public function displayStatus($isNumber=false) {
		foreach (Livestream::getStatusList() as $k => $l)
			if (strtolower($l) == $this->status) {
				if ($isNumber)
					return $k;
				return $l;
			}
	}

	public function getLatestlog() {
		return $this->logs->sortByDesc('created_at')->first();
	}
}