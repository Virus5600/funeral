<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Requests;

use App\User;
use App\Category;
use App\Livestream;
use App\Subcategory;
use App\ShippingAddress;

use DB;
use Exception;
use Log;
use Validator;

class UtilityFunctionController extends Controller
{
	public function getRegions() {
		return User::getRegions();
	}

	public function getCities(Request $req) {
		$region = $req->region == null ? 'NCR' : $req->region;
		return User::getCities($region);
	}

	public function updatePrivacyRead(Request $req) {
		try {
			DB::beginTransaction();

			$user = Auth::user();
			$user->is_privacy_read = 1;
			$user->save();

			DB::commit();
		} catch (Exception $e) {
			DB::rollback();
			Log::error($e);

			return array(
				'status' => 0,
				'message' => 'Something went wrong, please try again later.',
				'error_message' => $e->getMessage());
		}

		return array(
			'status' => 1,
			'message' => 'Privacy Policy Read.',
			'data' => null
		);
	}

	public function getSubcategories(Request $req) {
		$category = $req->category == null ? Category::first()->subcategory : $req->category;

		if ($category == $req->category)
			return null;

		return $req->category == null ? Category::first()->subcategory : Category::where('category_name', '=', $category)->first()->subcategory;
	}

	public function getShippingAddress(Request $req) {
		return ShippingAddress::find($req->id);
	}

	public function validateFirstStep(Request $req) {
		$validator = Validator::make($req->all(), [
			'first_name' => 'required|min:2',
			'last_name' => 'required|min:2',
			'contact_number' => 'required|numeric|min:10',
			'address' => 'required|min:5',
			'barangay' => 'required|min:3',
			'city' => 'required',
			'zip_code' => 'numeric',
			'region' => 'required',
			'address_name' => 'required'
		], [
			'first_name.required' => 'First Name is required.',
			'first_name.min' => 'Please provide a proper name.',
			'last_name.required' => 'Last Name is required.',
			'last_name.min' => 'Please provide a proper name.',
			'contact_number.required' => 'Contact number is required.',
			'contact_number.numeric' => 'Contact number must only contains number.',
			'contact_number.min' => 'Contact number provided is invalid.',
			'address.required' => 'Address is required.',
			'address.min' => 'Please provide your proper address.',
			'barangay.required' => 'Barangay is required.',
			'barangay.min' => 'Please provide your proper barangay name.',
			'city.required' => 'City/Province is required.',
			'zip_code.numeric' => 'Zip Codes contains numbers only.',
			'region.required' => 'Region is required.',
			'address_name.required' => 'Address name is required.'
		]);

		if ($validator->fails())
			return array($validator->messages(), 'isSuccess' => false);
	}

	public function toggleDefaultAddress(Request $req) {
		$addrs = ShippingAddress::find($req->id);
		$addrss = Auth::user()->shippingAddress;

		if (!$addrs)
			return array(
				'status' => 0,
				'message' => 'Address doesn\'t exists',
				'id' => $req->id
			);

		try {
			DB::beginTransaction();

			foreach ($addrss as $a) {
				if ($a->id != $addrs->id) {
					$a->is_default = 0;
					$a->save();
				}
				else {
					$addrs->is_default = $a->is_default == 1 ? 0 : 1;
					$addrs->save();
				}

			}

			DB::commit();
		} catch (Exception $e) {
			Log::error($e);
			DB::rollback();

			return array(
				'status' => -1,
				'message' => 'Something went wrong, please try again later.',
				'error_message' => $e->getMessage());
		}

		return array(
			'status' => 1,
			'message' => 'Successfully updated default shipping address.',
			'data' => $addrs
		);
	}

	public function updateLivestreamOption(Request $req, $id) {
		$ls = Livestream::find($id);
		try {
			DB::beginTransaction();

			$ls->updateOptionStatus($req->value, $req->key);

			DB::commit();
		} catch (Exception $e) {
			Log::error($e);
			DB::rollback();

			return array(
				'status' => '-1',
				'message' => 'Something went wrong, please try again later.',
				'error_message' => $e->getMessage());
		}
	}

	public function getLivestreamOptions($id) {
		return Livestream::find($id)->getOptionStatus();
	}
}