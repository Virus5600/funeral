<?php

use Illuminate\Database\Seeder;

use App\Category;

class CategoriesTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Category::create([
			'category_name' => 'Casket'
		]);

		Category::create([
			'category_name' => 'Coffin'
		]);

		Category::create([
			'category_name' => 'Urn'
		]);

		Category::create([
			'category_name' => 'Flowers'
		]);
	}
}