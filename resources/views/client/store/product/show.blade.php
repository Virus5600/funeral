@extends('template.client')

@section('title', 'Store')

@section('page-css')
<link rel="stylesheet" href="/css/store.product.show.css" >
@endsection

@section('css')
<style type="text/css">
	.accordion-button:after {
		background-image: url("data:image/svg+xml,<svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 16 16' fill='%23ffffff'><path fill-rule='evenodd' d='M1.646 4.646a.5.5 0 0 1 .708 0L8 10.293l5.646-5.647a.5.5 0 0 1 .708.708l-6 6a.5.5 0 0 1-.708 0l-6-6a.5.5 0 0 1 0-.708z'/></svg>") !important;
	}

	.img-active {
		filter: brightness(0.5);
	}

	#gallery > div, .rand-prod {
		cursor: pointer;
		transition: 0.125s;
	}

	#gallery > div:hover, .rand-prod:hover {
		filter: brightness(0.5);
	}
</style>
@endsection

@section('body')
{{-- PRODUCT BANNER --}}
<section class="p-md-5 my-md-1 py-md-3 py-4" id="banner">
	<div class="container pt-md-5 my-5" >
		<div class="row mt-md-0 mt-5">
			<small>{{$store->name}} / {{$category->category_name}} / {{$product->product_name}}</small>
		</div>

		<div class="row d-sm-flex">
			<div class="col col-lg-6 col-12 mt-4">
				<img src="/uploads/products/{{$product->category->category_name}}/{{$product->image_name}}" alt="" class="img-fluid w-100" id="img-display">
				<div class="container-fluid">
					<div class="d-flex flex-row justify-content-center" style="overflow-x: auto;" id="gallery">
						@if ($product->image_name != null)
						<div class="col-2 pt-2 px-1 img-active">
							<img src="/uploads/products/{{$product->category->category_name}}/{{$product->image_name}}" alt="" class="img-fluid">
						</div>
						@endif

						@foreach ($product->productImage as $i)
						<div class="col-2 pt-2 px-1">
							<img src="/uploads/products/{{$i->product->category->category_name}}/{{$i->image_name}}" alt="" class="img-fluid">
						</div>
						@endforeach
					</div>
				</div>
			</div>

			<div class="col col-lg-6 col-12 mt-5">
				<div class="container">
					<div class="row">
						<h2><b>{{$product->product_name}}</b></h2>
						<h5><i class="bi bi-check-circle-fill mr-2"></i>Product on Stock</h5>
						<h4 class="mt-4">Regular Price: &#8369;{{number_format($product->selling_price, 2)}}</h4>
						<!-- <h3><b>Special Price: P99, 400.00</b></h3> -->
						<hr class="buttondiv mt-2">
					</div>

					<div class="row mt-2 mb-4">
						<div class="d-grid col-md-6 col-12">
							<a href="{{ route('cart.addToCart', [$product->id]) }}" class="btn btn-lg btn-outline-light">Add to Cart</a>
						</div>
						<div class="d-grid col-md-6 col-12 mt-md-0 mt-2">
							<a href="{{ route('billing', ['fromBuyNow' => 1, 'item' => $product->id, 'store' => $product->store->id]) }}" class="btn btn-lg btn-light">Buy Now</a>
						</div>
					</div>

					<div class="row my-3">
						<h4><b>Shipping to Philippines</b></h4>
						<h5><i class="bi bi-truck mx-2"></i>Delivery</h5>

						<p class="mx-sm-5">
							Estimated to be delivered on:<br>
							July 10, 2021 - July 13, 2021
						</p>

						<h5><i class="bi bi-box-seam mx-2"></i>COD Policy<i class="far fa-question-circle ml-2 fa-xs social-link-side" data-bs-toggle="modal" data-bs-target="#COD"></i></h5>
						<h5><i class="bi bi-arrow-up-left-square mx-2"></i>Return Policy<i class="far fa-question-circle ml-2 fa-xs social-link-side" data-bs-toggle="modal" data-bs-target="#POLICY"></i></h5>

						<!-- COD Policy -->
						<div class="modal fade" id="COD" tabindex="-1" aria-hidden="true">
							<div class="modal-dialog modal-dialog-centered modal-lg">
								<div class="modal-content">
									<div class="modal-header">
										<h3 class="modal-title text-dark">COD Policy</h3>
										<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
									</div>

									<div class="modal-body all-text-dark">
										<h5>
											The subtotal of the order shall not be more than 5000 PHP.<br><br>
											A COD CHARGE of 75 PHP to the shipping company as the delivery service fee.<br><br>
											We do not support opening the package for inspection.<br><br>
										</h5>
										
										<p class="mt-2">
											Cash on delivery is not supported in the following situations:
											<ul>
												<li>The selected city does not support COD service at present.</li>
												<li>Incorrect phone number format.</li>
												<li>There is a previous order that you have not signed for yet. Please finish signing for it before you place a new order.</li>
												<li>According to your historical order records, your COD service has been suspended.</li>
											</ul>
										</p>
									</div>

									<div class="modal-footer">
										<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>

						<!-- Return Policy -->
						<div class="modal fade" id="POLICY" tabindex="-1"aria-hidden="true">
							<div class="modal-dialog modal-dialog-centered modal-lg">
								<div class="modal-content">
									<div class="modal-header">
										<h3 class="modal-title text-dark">Return Policy</h3>
										<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
									</div>

									<div class="modal-body all-text-dark">
										<h5>Please Note:  Items in this category are not able to be returned or exchanged, please understand.</h5>
									</div>

									<div class="modal-footer">
										<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- Accordion -->
<section>
	<div class="container my-5">
		<div class="row justify-content-center">
			<div class="col col-md-6">
				<div class="accordion accordion-flush" style="background-color: #00000000; border: none;" id="accordionFlushExample">
					<div class="accordion-item" style="background-color: #00000000; border: none">
						<h2 class="accordion-header" id="flush-headingOne">
							<button class="accordion-button text-light collapsed" style="background-color: #00000000; border: none;" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne" aria-expanded="false" aria-controls="flush-collapseOne">
								Size
							</button>
						</h2>

						<div id="flush-collapseOne" class="accordion-collapse collapse" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
							<div class="accordion-body">
								{{$product->size or 'No size specified.'}}
							</div>
						</div>
					</div>

					<div class="accordion-item" style="background-color: #00000000; border: none">
						<h2 class="accordion-header" id="flush-headingTwo">
							<button class="accordion-button text-light collapsed" style="background-color: #00000000; border: none;" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseTwo" aria-expanded="false" aria-controls="flush-collapseTwo">
								Description
							</button>
						</h2>

						<div id="flush-collapseTwo" class="accordion-collapse collapse" aria-labelledby="flush-headingTwo" data-bs-parent="#accordionFlushExample">
							<div class="accordion-body">
								<p>
								   {{$product->description or 'No description specified.'}}
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- Also View -->
<section>
	<div class="container my-5">
		<div class="row">
			<h2><b>Customers also View</b></h2>

			<div class="mt-3">
				<div class="row mb-5 justify-content-center">
					@foreach ($randProd as $r)
					<div class="col-md-3 mt-2">
						<a href="{{ route('products.show', [preg_replace('/ /', '_', strtolower($r->store->name)), preg_replace('/ /', '_', strtolower($r->category->category_name)), $r->id]) }}" class="rand-prod">
							<img src="/uploads/products/{{$r->category->category_name}}/{{$r->image_name}}" alt="" class="img-fluid">
						</a>
					</div>
					@endforeach
				</div>
			</div>
		</div>
	</div>
</section>
@endsection

@section('script')
<script type="text/javascript">
	$(document).ready(() => {
		$(document).on('click', '#gallery > div', (e) => {
			let obj = $(e.currentTarget);
			let parent = obj.parent();

			parent.children().removeClass('img-active');
			obj.addClass('img-active');
			$('#img-display').attr('src', $(obj.find('img')[0]).attr('src'));
		});
	});
</script>
@endsection