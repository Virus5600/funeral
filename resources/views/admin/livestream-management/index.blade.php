@extends('template.admin')

@section('title', 'Livestream Management')
@section('sidebar', 'admin')

@section('body')
<h1 class="h3 h1-lg font-weight-bold text-truncate">Livestream Management</h1>

<hr class="hr-thick border-color-custom">

<div class="row mx-0 px-0">
	<form action="{{ route(Request::route()->getName()) }}" method="GET" class="col-12 col-md-6 col-xl-5 mx-0 px-0">
		<div class="input-group">
			<input type="text" class="form-control" placeholder="Search for..." name="s" value="{{$search}}">
			<div class="input-group-append">
				<button type="submit" data-action="none" class="btn btn-dark"><i class="fas fa-search"></i></button>
			</div>
		</div>
	</form>

	{{-- <div class="col-12 col-md-6 col-xl-2 text-center"> --}}
		{{-- <a href="{{ route('admin.livestream-management.create') }}" class="btn btn-success my-2 my-lg-0"><i class="fas fa-plus-circle mr-2"></i>Add Livestream</a> --}}
	{{-- </div> --}}
</div>

<div class="row mx-0 px-0">
	<div class="col-12 px-0">
		<div class="card border rounded dark-shadow h-100">
			<h3 class="card-header font-weight-bold text-custom">Requests</h3>
			<div class="card-body mx-0 pt-0 px-0 overflow-auto">
				{{-- TABLE --}}
				<table class="table table-striped">
					<thead>
						<tr class="font-weight-bold">
							<td>Account Name</td>
							<td>Email</td>
							<td>Contact Number</td>
							<td>Category</td>
							<td>Status</td>
							<td></td>
						</tr>
					</thead>

					<tbody id='table-content'>
						@forelse($livestreams as $x)
						<tr>
							<td>{{$x->ls_user->first_name}}</td>
							<td>{{$x->ls_user->email}}</td>
							<td>{{$x->ls_user->contact_number}}</td>
							<td>{{$x->category == null ? 'None' : ucfirst($x->category)}}</td>
							<td>
								@if ($x->status == 'cancelled')
								<i class="fas fa-circle mr-2 text-danger"></i>
								@elseif ($x->status == 'waiting for proof' || $x->status == 'awaiting schedule')
								<i class="fas fa-circle mr-2 text-info"></i>
								@elseif ($x->status == 'pending' || $x->status == 'streaming')
								<i class="fas fa-circle mr-2 text-warning"></i>
								@elseif ($x->status == 'finished')
								<i class="fas fa-circle mr-2 text-success"></i>
								@endif
								{{ $x->displayStatus() }}
							</td>
							<td>
								<button type="button" class="btn btn-primary dropdown-toggle py-0 float-right" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action</button>

								<div class="dropdown-menu dropdown-menu-right">
									<a class="dropdown-item" href="{{ route('admin.livestream-management.show', [$x->id]) }}"><i class="far fa-eye mr-2"></i>View Details</a>
									<a class="dropdown-item" href="{{ route('admin.livestream-management.edit', [$x->id]) }}"><i class="fas fa-pencil-alt mr-2"></i>Edit Details</a>
									@if ($x->status == 'pending')
									<a class="dropdown-item" href="{{route('livestream.approve', [$x->id])}}"><i class="fas fa-check mr-2"></i>Approve</a>
									<a class="dropdown-item" href="{{route('livestream.cancel', [$x->id])}}"><i class="fas fa-times mr-2"></i>Cancel</a>
									@elseif ($x->status == 'approved')
									<a class="dropdown-item" href="{{route('livestream.done', [$x->id])}}"><i class="fas fa-check mr-2"></i>Mark as Done</a>
									@endif
									<a class="dropdown-item" href="{{ route('admin.livestream-management.delete', [$x->id]) }}"><i class="fas fa-trash mr-2"></i>Delete</a>
								</div>
							</td>
						</tr>
						@empty
						<tr>
							<td colspan="6" class="text-center">No Avaiable Data in Table</td>
						</tr>
						@endforelse
					</tbody>
				</table>
				{{-- TABLE END --}}
			</div>
		</div>
	</div>
</div>
{{-- PAGINATION --}}
<div class="row mx-0 px-0">
	<div class="col-12 px-0">
		<span>Total Results {{($livestreams->currentPage()-1) * $livestreams->perPage() + 1}}-{{(($livestreams->currentPage()-1) * $livestreams->perPage()) + $livestreams->count()}} of {{$livestreams->total()}} <a href="{{$livestreams->previousPageUrl()}}" class="text-dark"><i class="fas fa-chevron-left mx-2 cursor-pointer"></i></a><a href="{{$livestreams->nextPageUrl()}}" class="text-dark"><i class="fas fa-chevron-right ml-2 cursor-pointer"></i></a></span>
	</div>
</div>
@endsection