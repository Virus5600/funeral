<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Requests;

use App\SalesOrder;
use App\SalesOrderLog;

use DB;
use Log;
use Exception;
use Validator;

class OwnerSalesOrderController extends Controller
{
	protected function index() {
		$so = SalesOrder::where('sales_order.store_id', '=', Auth::user()->store_id);
		$search = null;

		if (request()->has('s')) {
			$search = request()->get('s');
			$so = $so->leftJoin('users', 'users.id', '=', 'sales_order.customer_id');

			$so = $so->where('sales_order.id', '=', '%'.$search.'%')
				->orWhere('users.first_name', 'LIKE', '%'.$search.'%')
				->orWhere('users.middle_name', 'LIKE', '%'.$search.'%')
				->orWhere('users.last_name', 'LIKE', '%'.$search.'%')
				->select('sales_order.*');
		}

		return view('owner.sales_order.index', [
			'search' => $search,
			'sales_order' => $so->paginate(15)
		]);
	}

	protected function prints($id) {
		return view('owner.sales_order.print', [
			'sales_order' => SalesOrder::find($id)
		]);
	}

	protected function show($id) {
		$steps = array('Waiting for Payment', 'Processing', 'Shipped Out', 'To Receive', 'Completed');
		
		return view('owner.sales_order.show', [
			'sales_order' => SalesOrder::find($id),
			'steps' => $steps
		]);
	}

	protected function acceptProof($id) {
		$so = SalesOrder::find($id);

		if ($so->status != 2) {
			return redirect()
				->back()
				->with('flash_error', 'Illegal action! Please refrain from doing similar actions in the future!');
		}

		try {
			DB::beginTransaction();

			$so->status = 3;
			$so->save();

			SalesOrderLog::create([
				'order_id' => $so->id,
				'description' => 'Submitted proof accepted. {image: ' . $so->proof_of_payment . '}',
				'time' => \Carbon\Carbon::now(),
				'type' => 2,
			]);

			DB::commit();
		} catch (Exception $e) {
			DB::rollback();
			Log::error($e);

			return redirect()
				->route('owner.sales-order.index')
				->with('flash_error', 'Something went wrong, please try again later.');
		}

		return redirect()
			->back()
			->with('flash_success', 'Proof accepted!');
	}

	protected function rejectProof($id) {
		$so = SalesOrder::find($id);

		if ($so->status != 2) {
			return redirect()
				->back()
				->with('flash_error', 'Illegal action! Please refrain from doing similar actions in the future!');
		}

		try {
			DB::beginTransaction();

			SalesOrderLog::create([
				'order_id' => $so->id,
				'description' => 'Submitted proof rejected. {image: ' . $so->proof_of_payment . '}',
				'time' => \Carbon\Carbon::now(),
				'type' => 2,
			]);

			$so->proof_of_payment = null;
			$so->status = 1;
			$so->save();

			DB::commit();
		} catch (Exception $e) {
			DB::rollback();
			Log::error($e);

			return redirect()
				->route('owner.sales-order.index')
				->with('flash_error', 'Something went wrong, please try again later.');
		}

		return redirect()
			->back()
			->with('flash_success', 'Proof rejected!');
	}

	protected function addTrackingNumber(Request $req, $id) {
		$so = SalesOrder::find($id);

		$validator = Validator::make($req->all(), [
			'tracking_number' => 'required'
		], [
			'tracking_number.required' => 'Tracking number is required...'
		]);

		if ($validator->fails())
			return redirect()
				->back()
				->withErrors($validator)
				->withInput();

		try {
			DB::beginTransaction();

			$so->tracking_number = $req->tracking_number;
			$so->status = 4;
			$so->save();

			SalesOrderLog::create([
				'order_id' => $so->id,
				'description' => 'Products shipped out to courier.',
				'time' => \Carbon\Carbon::now(),
				'type' => 3,
			]);

			DB::commit();
		} catch (Exception $e) {
			Log::error($e);
			DB::rollback();

			return redirect()
				->route('owner.sales-order.index')
				->with('flash_error', 'Something went wrong, please try again later.');
		}

		return redirect()
			->back()
			->with('flash_success', 'Successfully added tracking number.');
	}

	protected function updateTrackingNumber(Request $req, $id) {
		$so = SalesOrder::find($id);

		$validator = Validator::make($req->all(), [
			'tracking_number' => 'required'
		], [
			'tracking_number.required' => 'Tracking number is required...'
		]);

		if ($validator->fails())
			return redirect()
				->back()
				->withErrors($validator)
				->withInput();

		try {
			DB::beginTransaction();

			$so->tracking_number = $req->tracking_number;
			$so->save();

			DB::commit();
		} catch (Exception $e) {
			Log::error($e);
			DB::rollback();

			return redirect()
				->route('owner.sales-order.index')
				->with('flash_error', 'Something went wrong, please try again later.');
		}

		return redirect()
			->back()
			->with('flash_success', 'Successfully updated tracking number.');
	}
}