@extends('template.client')

@section('title', 'Services')

@section('page-css')
<link rel="stylesheet" href="/css/services.index.css" >
@endsection

@section('body')
{{-- SERVICES BANNER --}}
<section class="p-md-5 d-flex flex-row" id="banner">
	@include('template.social-share')
	
	<div class="container pt-md-5" >
		<div class="d-sm-flex justify-content-between">
			<div class =" col-md-7 col-sm-12 p-5" id = "bannertext">
				<h1>We adhere to your needs.</h1>
				<p class="lead my-4">
					Soulace offers the best funeral services
					in the country.
				</p>
			</div>
		</div>
	</div>
</section>

{{-- SERVICES ACCORDION --}}
<section class="py-5">
	<div class="container">
		<h1 class="text-center">
		<img src="/images/UI/line.svg" class="imgline mr-2">Our Services
		</h1>
	</div>

	<div class="container pt-5">
		<div class="accordion" id="services">
			<div class="accordion-item mb-5">
				<div class="accordion-header"  type="button" data-bs-toggle="collapse" data-bs-target="#funeralAcc" aria-expanded="true" aria-controls="funeral">
					<div id="funeral" class="acc_pic">
						<div class="textserv">
							<span class="title_head"><h1>Personalised Funeral Services</h1></span>
							<img src="/images/UI/floral.png" alt="" class="floral">
						</div>
					</div>
				</div>

				<div id="funeralAcc" class="accordion-collapse collapse" aria-labelledby="funeral" data-bs-parent="#services">
					<div class="accordion-body">
						{{$services[0]->description}}
						<div class="d-flex flex-row w-100 justify-content-center mt-5">
							<a href="{{ route('services.livestream', ['c'=>'funeral']) }}" class="btn btn-light">Request Livestream for this</a>
						</div>
					</div>
				</div>
			</div>

			<div class="accordion-item mb-5">
				<div class="accordion-header"  type="button" data-bs-toggle="collapse" data-bs-target="#burialAcc" aria-expanded="false" aria-controls="burialAcc">
					<div id="burial" class="acc_pic">
						<div class="textserv">
							<span class="title_head"><h1>Burial Services </h1> </span>
							<img src="/images/UI/floral.png" alt="" class="floral">
						</div>
					</div>
				</div>

				<div id="burialAcc" class="accordion-collapse collapse" aria-labelledby="burial" data-bs-parent="#services">
					<div class="accordion-body">
						{{$services[1]->description}}
						<div class="d-flex flex-row w-100 justify-content-center mt-5">
							<a href="{{ route('services.livestream', ['c'=>'burial']) }}" class="btn btn-light">Request Livestream for this</a>
						</div>
					</div>
				</div>
			</div>

			<div class="accordion-item mb-5">
				<div class="accordion-header"  type="button" data-bs-toggle="collapse" data-bs-target="#memorialAcc" aria-expanded="false" aria-controls="memorialAcc">
					<div id="memorial" class="acc_pic">
						<div class="textserv">
							<span class="title_head"><h1>Memorial Services </h1> </span>
							<img src="/images/UI/floral.png" alt="" class="floral">
						</div>
					</div>
				</div>

				<div id="memorialAcc" class="accordion-collapse collapse" aria-labelledby="memorial" data-bs-parent="#services">
					<div class="accordion-body">
						{{$services[2]->description}}
						<div class="d-flex flex-row w-100 justify-content-center mt-5">
							<a href="{{ route('services.livestream', ['c'=>'memorial']) }}" class="btn btn-light">Request Livestream for this</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection

@section('script')
<script type="text/javascript">
	$(document).ready(() => {
		let target = window.location.href.replace(/.+(#.+)/gi, "$1");
		$(target).trigger('click');

		$(window).bind('popstate', (e) => {
			let target = window.location.href.replace(/.+(#.+)/gi, "$1");
			$(target).trigger('click');
		});
	});
</script>
@endsection