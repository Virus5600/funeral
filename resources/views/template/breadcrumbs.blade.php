<?php 
	# SET link VARIABLE
	$link = "";

	# SET startIndex IF NOTHING WAS PASSED
	if (!isset($startIndex))
		$startIndex = 1;

	# SETS THE INITIAL VALUE FOR link
	for ($i = 1; $i < $startIndex; $i++)
		$link .= "/" . Request::segment($i);

	# SETS THE BREADCRUMBS DISPLAY
	for ($i = $startIndex; $i <= count(Request::segments()); $i++) {
		if ($i < count(Request::segments()) && $i > 0) {
			$link .= "/" . Request::segment($i);
			if (isset($confirmLeave) && $confirmLeave)
				echo ' <a class="text-dark" href="javascript:void(0);" onclick="confirmLeave(\'' . $link . '\');">' . ucwords(str_replace('_', ' ', Request::segment($i))) . '</a> <i class="fas fa-chevron-right"></i> ';
			else
				echo ' <a class="text-dark" href="' . $link . '">' . ucwords(str_replace('_', ' ', Request::segment($i))) . '</a> <i class="fas fa-chevron-right"></i> ';

		}
		else {
			echo ucwords(str_replace('_', ' ', Request::segment($i)));
		}
	}
?>