<div style="margin: 0; padding: 0; width: 100%; height: 100%; display: table; font-family: 'Arial'; color: #000; background-color: rgb(230, 230, 230);">
	<div style="margin: 2rem; background-color: white; padding: 1rem;">
		<table style="width: 100%;">
			<tr>
				<td style="width: 100%; text-align: center;">
					<img src="{{ asset('images/branding/logo_gradient.png') }}" width="75px">
					<h5>Password Recovery</h5>
				</td>
			</tr>
		</table>

		<hr>

		<p style="text-align: center;">Hello <span style="font-weight: bold;">{{ $user->getName() }},</span></p>
		<p style="text-align: center;">A request was sent to recover your password. Please click the link below to continue. If you did not do this, please ignore this email.</p>

		<p style="text-align: center; margin-top: 0;"><small>Click here to change your password:</small></p>

		<p style="text-align: center;">
			<a href="{{ route('recoverPass.changePass') }}?token={{$token}}" style="padding: .75rem 1rem; font-size: 1.25rem; line-height: 1.5; border-radius: .3rem; color: white; background-image: linear-gradient(45deg, #FF8000, #FFE600); background-color: #FF8000; border-color: #dae0e5; text-decoration: none; font-weight: 600;">Change Password</a>
		</p>
		<br>

		<p style="text-align: center;"><small>Soulace</small></p>
	</div>
</div>