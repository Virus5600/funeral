@extends('template.client')

@section('title', 'Store')

@section('page-css')
<link rel="stylesheet" href="/css/services.livestream.css" >
@endsection

@section('body')
@php
	$category = null;
	if (Request::has('c'))
		$category = Request::get('c');
@endphp

{{-- MAIN CONTENT --}}
<section class="p-md-5" id="banner">
	<div class="container pt-md-5" >
		<div class="d-sm-flex justify-content-between">
			<div class ="col-xl-11 col-md-8 col-sm-12 p-5" id="bannertext">
				<h1> Livestream.</h1>
				<p class="lead my-4">Send your sympathy virtually. Soulace lets you watch funeral wakes online whenever and wherever you are.</p>
			</div>
		</div>

		@if (Auth::check())
		<div class="container align-items-center text-center">
			<h1><u>Schedule Your Livestream</u></h1>
		</div>
		@endif

		<div class="container-fluid">
			<form action="{{route('livestream.create')}}" method="POST" enctype="multipart/form-data">
				@if (Auth::check())
				{{csrf_field()}}
				<div class="w-50 mx-auto my-5 row">
					<div class="form-group col-12 my-3">
						<label class="form-label my-0 mx-3 h4 mb-2" style="margin-left: 0!important;" for="title">Title:</label>
						<input type="text" class="form-control" id="title" name="title" {{Auth::check() ? '' : 'disabled'}} value="{{old('title')}}">
						<span class="badge badge-danger w-100 validation-message">{{$errors->first('title')}}</span>
					</div>

					<div class="form-group col-12 my-3">
						<label class="form-label my-0 mx-3 h4 mb-2" style="margin-left: 0!important;" for="description">Description:</label>
						<textarea class="form-control not-resizable" rows="5" name="description" id="description" {{Auth::check() ? '' : 'disabled'}}>{{old('description')}}</textarea>
						<span class="badge badge-danger w-100 validation-message">{{$errors->first('description')}}</span>
					</div>

					<div class="form-group col-12">
						<div class="row">
							<div class="col-12 col-lg-6 mx-auto">
								<div class="form-group">
									<label class="form-label my-0 h4 mb-2" for="category">Category</label>
									<select class="custom-select" name="category" id="category">
										<option class="text-dark" value="none">None</option>
										@foreach (App\Livestream::getCategoryList() as $c)
										<option class="text-dark" value="{{strtolower($c)}}" {{ $category == strtolower($c) ? 'selected' : '' }}>{{ $c }}</option>
										@endforeach
									</select>
									<span class="badge badge-danger w-100 validation-message">{{$errors->first('category')}}</span>
								</div>
							</div>
						</div>
					</div>

					<div class="form-group col-6 d-flex flex-row my-3">
						<div class="d-flex justify-content-center align-items-center">
							<label class="form-label my-0 mx-3 h4" for="date">Date:</label>
						</div>
						<div class="d-flex flex-column">
							<input type="date" class="form-control" id="date" name="date" {{Auth::check() ? '' : 'disabled'}} value="{{old('date')}}" min="{{\Carbon\Carbon::now()->format('Y-m-d')}}">
							<span class="badge badge-danger w-100 validation-message">{{$errors->first('date')}}</span>
						</div>
					</div>

					<div class="form-group col-6 d-flex flex-row my-3">
						<div class="d-flex justify-content-center align-items-center">
							<label class="form-label my-0 mx-3 h4" for="time">Time:</label>
						</div>
						<div class="d-flex flex-column">
							<input type="time" class="form-control" id="time" name="time" {{Auth::check() ? '' : 'disabled'}} value="{{old('time')}}">
							<span class="badge badge-danger w-100 validation-message">{{$errors->first('time')}}</span>
						</div>
					</div>

					<div class="form-group col-6">
						<div class="d-flex align-items-center">
							<label class="form-label my-0 h4" for="duration">Duration:</label>
							<span class="badge badge-danger w-100 validation-message">{{$errors->first('duration')}}</span>
						</div>
						
						<div class="input-group">
							<input type="number" class="form-control text-right" id="duration" name="duration" min="1" value="{{ old('duration') != null ? old('duration') : '1' }}">
							<div class="input-group-append">
								<span class="input-group-text">hour(s)</span>
							</div>
						</div>
						<span class="badge badge-danger w-100 validation-message">{{$errors->first('schedule_time')}}</span>
					</div>

					<div class="form-group col-6">
						<div class="d-flex align-items-center">
							<label class="form-label my-0 h4" for="price">Price:</label>
						</div>

						<div class="input-group">
							<div class="input-group-prepend">
								<span class="input-group-text">₱</span>
							</div>
							<input type="number" class="form-control text-right" id="price" name="price" min="100" value="{{ old('price') != null ? old('price') : '100' }}" readonly>
							<div class="input-group-append">
								<span class="input-group-text">.00</span>
							</div>
							<span class="badge badge-danger w-100 validation-message">{{$errors->first('schedule_time')}}</span>
						</div>
					</div>
				</div>
				@endif

				<div class="row">
					<div class="col-12 col-lg-6 mx-auto d-flex flex-column justify-content-center align-items-center">
						<p class="h4 w-100 mx-auto text-center">
							@if (Auth::check())
							The link will be sent to the email address you provided during your registration.
							@else
							Please <a class="text-decoration-none" href="{{route('register')}}">register</a> to access this feature.
							@endif
						</p>
						<br>
						
						@if (Auth::check())
						<p>{{Auth::user()->email}}</p>
						@endif
						<br>

						@if (Auth::check())
						<input class="btn btn-light" type="submit" data-action="submit" value="Add To Cart"/>
						@endif
					</div>
				</div>
			</form>
		</div>
	</div>
</section>

<section class="py-5">
	
</section>
@endsection

@section('script')
<script type="text/javascript">
	$(document).ready(function() {
		$('#duration').on('change', (e) => {
			$('#price').val(parseFloat($(e.target).val() * 100).toFixed(2));
		});
	});
</script>
@endsection