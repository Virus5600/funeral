<?php

use Illuminate\Database\Seeder;

use App\ProductImage;

class ProductImagesTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		ProductImage::insert([
			'product_id' => 1,
			'image_name' => 'item-2.png'
		]);

		ProductImage::insert([
			'product_id' => 1,
			'image_name' => 'item-3.png'
		]);

		ProductImage::insert([
			'product_id' => 1,
			'image_name' => 'item-4.png'
		]);

		ProductImage::insert([
			'product_id' => 1,
			'image_name' => 'item-5.png'
		]);
	}
}