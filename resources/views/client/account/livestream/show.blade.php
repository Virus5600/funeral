@extends('template.client.account')

@section('subtitle', 'My Account')

@section('page-css')
<link rel="stylesheet" href="/css/order_tracking.show.css" >
@endsection

@section('sub-body')
<div class="container mt-3 pt-2 bg-light">
	<div class="itemrow row">
		<div class="col-3"> 
			<h5><a href="{{ route('account.livestreams') }}" class="font-weight-bold text-decoration-none text-dark"><i class="bi bi-chevron-left text-dark"></i>Back</a></h5>
		</div>
		<div class="col-9 right-align-items px-4">
			<h5><b>ORDER ID. {{$livestream->stream_id}}</b></h5>
		</div>
	</div>
</div>
<div class="row mt-5">
	@if ($livestream->displayStatus(true) > 0)
	<div class="step-progress w-75 mx-auto mb-3" data-progress='{{((int)$livestream->displayStatus(true))-1}}'>
		<div class="component">
			<div class="bar">
				<hr class="bar-progress">
				<hr class="bar-remaining">
			</div>

			<div class="steps">
				<div class="step-icon">
					<img src="/images/UI/order_payment.png" alt="Payment">
				</div>
				<div class="step-icon">
					<img src="/images/UI/order_processing.png" alt="Processing">
				</div>
				<div class="step-icon">
					<img src="/images/UI/awaiting_schedule.png" alt="Awaiting Schedule">
				</div>
				<div class="step-icon">
					<img src="/images/UI/streaming.png" alt="Streaming">
				</div>
				<div class="step-icon">
					<img src="/images/UI/recorded.png" alt="Complete">
				</div>
			</div>
		</div>

		<div class="labels">
			@php ($i = 0)
			@foreach (\App\Livestream::getStatusList() as $s)
			
			@php ($i++)
			@if ($i == 1 || $i == 7)
				@php
				continue;
				@endphp
			@endif

			<div class="step-label">
				{{$s}}
			</div>
			@endforeach
		</div>
	</div>
	@else
	<div class="w-75 mx-auto mb-3 d-flex flex-d-col align-items-center">
		<div class="step-icon">
			<img src="/images/UI/order_cancelled.png" alt="">
		</div>
		<div class="step-label">
			Cancelled
		</div>
	</div>
	@endif
</div>

<div class="container itemrow bg-light p-3">
	<div class="row p-3">
		<div class="col-md-6 mt-2" id="order_info">
			@if ($livestream->displayStatus(true) == 1)
			<div class="row">
				<div class="col-12 d-flex flex-column">
					<h5 class="mx-auto">You haven't uploaded the proof yet.</h5>
					<a href="{{ route('account.livestreams.upload_proof', [$livestream->id]) }}" class="btn btn-dark mx-auto">Upload Proof</a>
				</div>
			</div>
			@elseif ($livestream->displayStatus(true) == 2)
			<div class="d-flex flex-column h-100">
				<h5 class="m-auto">Waiting for confirmation from the store...</h5>
			</div>
			@elseif ($livestream->displayStatus(true) == 3)
			<div class="d-flex flex-column h-100">
				<div class="m-auto">
					<h5 class="text-center">Proof of payment accepted!</h5>
					<p class="text-center">Awaiting scheduled livestream on {{ Carbon\Carbon::parse($livestream->schedule_date)->format('M d, y') }}</p>
				</div>
			</div>
			@elseif ($livestream->displayStatus(true) == 4)
			<div class="row">
				<div class="col">
					<b>Livestream ongoing</b>
					<p class="text-muted">Go to <a class="text-primary" href="{{route('livestream.show', [$livestream->stream_id])}}" target="_balnk" title="{{$livestream->title or 'Livestream'}}">this page</a> to see current livestream.</p>
				</div>
			</div>

			<div class="row">
				<div class="col-8">
					<p>
						<b>Livestream Link</b><br>
						<span class="text-muted">{{route('livestream.show', [$livestream->stream_id])}}</span>
						<input type="hidden" id="livestream_url" value="{{route('livestream.show', [$livestream->stream_id])}}">
					</p>
				</div>

				<div class="col-4 right-align-items">
					<button class="btn btn-dark" onclick="copyToClipboard(this);" data-copy-target="#livestream_url">Copy</button>
				</div>

			</div>
			@elseif ($livestream->displayStatus(true) == 5)
			<div class="row mt-3">
				<h3>Livestream Concluded</h3>
				<p>
					<span class="text-muted">
						<b class="text-dark">Replay:</b><br>
						<a class="text-primary" href="{{route('livestream.show', [$livestream->stream_id])}}" target="_balnk" title="{{$livestream->title or 'Livestream'}}">{{route('livestream.show', [$livestream->stream_id])}}</a><br>
						<br>
						<b class="text-dark">Add-ons:</b><br>
						<ul>
							<li class="text-dark">
								<u class="text-dark">Standing Photo:</u>
								@if ($livestream->getOptionStatus('standing_photo') == 1)
								<i class="fas fa-circle mr-2 text-success"></i>Yes
								@else
								<i class="fas fa-circle mr-2 text-danger"></i>No
								@endif
							</li>

							<li class="text-dark">
								<u class="text-dark">Video Slideshow:</u>
								@if ($livestream->getOptionStatus('video_slideshow') == 1)
								<i class="fas fa-circle mr-2 text-success"></i>Yes
								@else
								<i class="fas fa-circle mr-2 text-danger"></i>No
								@endif
							</li>

							<li class="text-dark">
								<u class="text-dark">Flower Stands:</u>
								@if ($livestream->getOptionStatus('flower_stands') == 1)
								<i class="fas fa-circle mr-2 text-success"></i>Yes
								@else
								<i class="fas fa-circle mr-2 text-danger"></i>No
								@endif
							</li>
						</ul>
					</span>
				</p>
			</div>
			@endif
		</div>
			
		<div class="col-md-6 mt-2">
			<p>
				Logs<br>
				@forelse ($livestream->logs->reverse() as $l)
				@if ($l->isAccepted())
				<i class="fas fa-check-circle text-success mx-2"></i>{{$l->updated_at == null ? \Carbon\Carbon::parse($l->updated_at)->format('M d, Y') : \Carbon\Carbon::parse($l->created_at)->format('M d, Y')}} - {{\Carbon\Carbon::parse($l->time)->format('H:i A')}} -  {{$l->getDescription()}}<br>
				@elseif ($l->isRejected())
				<i class="fas fa-times-circle text-danger mx-2"></i>{{$l->updated_at == null ? \Carbon\Carbon::parse($l->updated_at)->format('M d, Y') : \Carbon\Carbon::parse($l->created_at)->format('M d, Y')}} - {{\Carbon\Carbon::parse($l->time)->format('H:i A')}} -  {{$l->getDescription()}}<br>
				@else
				<i class="fas fa-info-circle text-dark mx-2"></i>{{$l->updated_at == null ? \Carbon\Carbon::parse($l->updated_at)->format('M d, Y') : \Carbon\Carbon::parse($l->created_at)->format('M d, Y')}} - {{\Carbon\Carbon::parse($l->time)->format('H:i A')}} -  {{$l->getDescription()}}<br>
				@endif
				@empty
				<i class="fas fa-info-circle fa-lg text-dark mx-2"></i>Nothing has happened yet...
				@endforelse
			</p>
		</div>
	</div>
</div>

<div class="row px-3 my-4">
	<div class="col-6">
		<div class="row">
			<div class="col-6 right-align-items">
				<h5><b>Order Total:</b></h5>
			</div>
			
			<div class="col-6 right-align-items">
				<h5><b>&#8369;{{number_format($livestream->price, 2)}}</b></h5>
			</div>
		</div>
	</div>
</div>
@endsection

@section('subscript')
<script>
$(document).ready(() => {stepProgressInit();});

// COPYING
function copyToClipboard(element) {
	let temp = $("<input>");
	$("body").append(temp);

	let textToCopy;
	if (typeof $(element).attr('data-copy-target') != 'undefined')
		textToCopy = $($(element).attr('data-copy-target')).val();
	else if (typeof $(element).attr('data-copy-text') != 'undefined')
		textToCopy = $(element).attr('data-copy-text');
	else
		textToCopy = $(element).val();

	temp.val(textToCopy).select();
	document.execCommand("copy");
	temp.remove();
	
	Swal.fire({
		title: `Text copied`,
		position: `bottom`,
		showConfirmButton: false,
		toast: true,
		timer: 3750,
		background: `#28a745`,
		customClass: {
			title: `text-white`,
			popup: `px-0`
		},
		width: 150
	});
}

// STEP PROGRESS
function stepProgressInit() {
	let obj = $('.step-progress');
	let bars = obj.find('.bar').children();
	let steps = obj.find('.steps').children();
	let labels = obj.find('.labels').children();
	let stepCount = steps.length-1;
	// In percentage
	let progress = (obj.attr('data-progress')*100)/stepCount;
	let remaining = 100-progress;

	// Set length and height of steps
	obj.find('.steps').css('width', obj.find('.bar').width());
	obj.find('.steps').css('height', obj.find('.bar').height());

	// Attaching a resize event listener to .steps
	$(window).resize((e) => {
		obj.find('.steps').css('width', obj.find('.bar').width());
		obj.find('.steps').css('height', obj.find('.bar').height());
	});

	// Placing the step milestone
	steps.css('width', (100/stepCount) + "%");
	$(steps[stepCount]).css('width', '');

	// Setting the color for step milestone
	for (let i = 0; i <= obj.attr('data-progress'); i++)
		$(steps[i]).addClass('active');

	// Setting the length of the bar
	$(bars[0]).css('width', progress + "%");
	$(bars[1]).css('width', remaining + "%");

	// Setting the labels
	labels.css('width', (100/stepCount) + "%");
};
</script>

<script type="text/javascript">
	var nav = document.querySelector('nav');

	window.addEventListener('scroll', function () {
		if (window.pageYOffset > 100) {
			nav.classList.add('bg-dark', 'shadow');
		} else {
			nav.classList.remove('bg-dark', 'shadow');
		}
	});
</script>

<script>
	document.getElementById("togglemenu").addEventListener("click", function() {
		document.getElementById("navbarmain").classList.add('bg-dark', 'shadow');
	});
</script>
@endsection