@extends('template.admin')

@section('title', 'Obituary')
@section('sidebar', 'owner')

@section('body')
<h1 class="font-weight-bold">Obituary</h1>

<hr class="hr-thick" style="border-color: #707070;">

<div class="row mx-0 px-0">
	<form action="{{ route(Request::route()->getName()) }}" method="GET" class="col-12 col-lg-5 mx-0 px-0">
		<div class="input-group">
			<input type="text" class="form-control" placeholder="Search for..." name="s" value="{{$search}}">
			<div class="input-group-append">
		    	<button type="submit" data-action="none" class="btn btn-dark"><i class="fas fa-search"></i></button>
			</div>
		</div>
	</form>

	<div class="col-12 col-lg-2 mt-3 mt-lg-0">
		<a href="{{ route('owner.obituary.create') }}" class="btn btn-success w-100"><i class="fas fa-plus-circle mr-2"></i>Add Obituary</a>
	</div>
</div>

<div class="row mx-0 px-0">
	<div class="col-12 px-0">
		<div class="card border rounded dark-shadow h-100 overflow-x-auto">
			<h3 class="card-header font-weight-bold text-custom">Obituary List</h3>
			<div class="card-body mx-0 pt-0 px-0">
				{{-- TABLE --}}
				<table class="table table-striped">
					<thead>
						<tr class="font-weight-bold">
							<td>Name of the Deceased</td>
							<td>Date of Birth</td>
							<td>Date of Death</td>
							<td>Status</td>
							<td></td>
						</tr>
					</thead>

					<tbody id='table-content'>
						@forelse ($obituary as $o)
						<tr>
							<td>{{$o->name}}</td>
							<td>{{\Carbon\Carbon::parse($o->birth_date)->format('M d, Y')}}</td>
							<td>{{\Carbon\Carbon::parse($o->death_date)->format('M d, Y')}}</td>
							<td>
								@if ($o->available_to == null)
									<i class="fas fa-circle text-success mr-2"></i>Active
								@else
									@if ($o->status == 0)
									<i class="fas fa-circle text-danger mr-2"></i>Cancelled
									@elseif ($o->status == 1)
									<i class="fas fa-circle text-warning mr-2"></i>Pending
									@elseif ($o->status == 2)
										@if ($o->available_to > \Carbon\Carbon::now())
										<i class="fas fa-circle text-success mr-2"></i>Active
										@else
										<i class="fas fa-circle text-danger mr-2"></i>Inactive
										@endif
									@elseif ($o->status == 3)
									<i class="fas fa-circle text-danger mr-2"></i>Rejected
									@endif
								@endif
							</td>
							<td>
								<button type="button" class="btn btn-primary dropdown-toggle float-right py-0" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action</button>

								<div class="dropdown-menu dropdown-menu-right">
									<a class="dropdown-item" href="{{ route('owner.obituary.show', [preg_replace('/ /', '_', strtolower($o->name))]) }}"><i class="far fa-eye mr-2"></i>View Details</a>
									<a class="dropdown-item" href="{{ route('owner.obituary.edit', [preg_replace('/ /', '_', strtolower($o->name))]) }}"><i class="fas fa-pencil-alt mr-2"></i>Edit Details</a>
									@if ($o->status == 1)
									<a class="dropdown-item" href="{{route('owner.obituary.update-status', [$o->id, 2])}}"><i class="fas fa-check mr-2"></i>Approve</a>
									<a class="dropdown-item" href="{{route('owner.obituary.update-status', [$o->id, 3])}}"><i class="fas fa-times mr-2"></i>Reject</a>
									@endif
									<a class="dropdown-item" href="{{ route('owner.obituary.delete', [$o->id]) }}"><i class="fas fa-trash mr-2"></i>Delete</a>
								</div>
							</td>
						</tr>
						@empty
						<tr>
							<td colspan="3">No Avaiable Data in Table</td>
						</tr>
						@endforelse
					</tbody>
				</table>
				{{-- TABLE END --}}
			</div>
		</div>
	</div>
</div>
{{-- PAGINATION --}}
<div class="row mx-0 px-0">
	<div class="col-12 px-0 text-lg-left text-center">
		<span>Total Results {{($obituary->currentPage()-1) * $obituary->perPage() + 1}}-{{(($obituary->currentPage()-1) * $obituary->perPage()) + $obituary->count()}} of {{$obituary->total()}} <a href="{{$obituary->previousPageUrl()}}" class="text-dark"><i class="fas fa-chevron-left mx-2 cursor-pointer"></i></a><a href="{{$obituary->nextPageUrl()}}" class="text-dark"><i class="fas fa-chevron-right ml-2 cursor-pointer"></i></a></span>
	</div>
</div>
@endsection