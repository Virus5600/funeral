<div style="margin: 0; padding: 0; width: 100%; height: 100%; display: table; font-family: 'Arial'; color: #000; background-color: rgb(230, 230, 230);">
	<div style="margin: 2rem; background-color: white; padding: 1rem;">
		<table style="width: 100%;">
			<tr>
				<td style="width: 75%;">
					<h2 style="color: rgb(55, 81, 130); margin: 0 0 0.5rem 0;">Soulace</h2>
					<h5 style="margin: 0.5rem 0 0 0;">Inquiry</h5>
				</td>

				<td style="width: 25%; text-align: right;"></td>
			</tr>
		</table>
		<hr>

		<p>
			{{ $content }}
		</p>

		<p style="font-weight: bold; margin-bottom: 0;">{{ $name }}</p>
		<p style="margin-top: 0; font-size: 0.75rem;">{{ $email }}</p>
	</div>
</div>