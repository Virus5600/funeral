@if (\Request::is('owner/dashboard'))
<div class="sidebar-item active text-truncate">
	<i class="fas fa-border-all mr-2"></i>Dashboard
</div>
@else
<a class="sidebar-item text-truncate" href="{{ route('owner.dashboard') }}">
	<i class="fas fa-border-all mr-2"></i>Dashboard
</a>
@endif

@if (\Request::is('owner/sales-order'))
<div class="sidebar-item active">
	<i class="fas fa-percent mr-2"></i>Sales Order
</div>
@elseif (\Request::is('owner/sales-order/*'))
<a class="sidebar-item active" href="{{ route('owner.sales-order.index') }}">
	<i class="fas fa-percent mr-2"></i>Sales Order
</a>
@else
<a class="sidebar-item d-block" href="{{ route('owner.sales-order.index') }}">
	<i class="fas fa-percent mr-2"></i>Sales Order
</a>
@endif

@if (\Request::is('owner/products'))
<div class="sidebar-item active">
	<i class="fas fa-store mr-2"></i>Products
</div>
@elseif (\Request::is('owner/products/*'))
<a class="sidebar-item active" href="{{ route('owner.category.index') }}">
	<i class="fas fa-store mr-2"></i>Products
</a>
@else
<a class="sidebar-item" href="{{ route('owner.category.index') }}">
	<i class="fas fa-store mr-2"></i>Products
</a>
@endif

@if (\Request::is('owner/obituary'))
<div class="sidebar-item active">
	<i class="fas fa-monument mr-2"></i>Obituary
</div>
@elseif (\Request::is('owner/obituary/*'))
<a class="sidebar-item active" href="{{ route('owner.obituary.index') }}">
	<i class="fas fa-monument mr-2"></i>Obituary
</a>
@else
<a class="sidebar-item" href="{{ route('owner.obituary.index') }}">
	<i class="fas fa-monument mr-2"></i>Obituary
</a>
@endif

@if (\Request::is('owner/contact'))
<div class="sidebar-item active">
	<i class="far fa-address-book mr-2"></i>Contact
</div>
@else
<a class="sidebar-item text-truncate" href="{{ route('owner.contact') }}">
	<i class="far fa-address-book mr-2"></i>Contact
</a>
@endif