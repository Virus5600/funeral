SET foreign_key_checks = 0;
DROP TABLE `cart_items`, `categories`, `email_verifications`, `livestream`, `livestream_logs`, `locations`, `migrations`, `obituaries`, `password_resets`, `products`, `product_images`, `sales_order`, `sales_order_items`, `sales_order_logs`, `services`, `shipping_addresses`, `stores`, `subcategories`, `subscribers`, `users`, `var`;
SET foreign_key_checks = 1;