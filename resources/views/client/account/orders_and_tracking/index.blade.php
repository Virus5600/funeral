@extends('template.client.account')

@section('subtitle', 'My Account')

@section('page-css')
<link rel="stylesheet" href="/css/order_tracking.index.css">
@endsection

@section('sub-body')
<!-- Order Info -->
@forelse (Auth::user()->salesOrder as $s)
<div class="itemrow row mt-5 p-3 bg-light">
	<a href="{{ route('account.orders.show', [$s->id]) }}" class="container text-decoration-none">
		@foreach ($s->items as $i)
		<div class="row mt-2">
			<div class="col-md-2 col-12">
				<img src="/uploads/products/{{$i->product->category->category_name}}/{{$i->product->image_name}}" alt="{{$i->product->product_name}}" class="img-fluid">
			</div>

			<div class="col-md-4 col-12 mt-3 mt-md-0">
				<h5>{{$i->product->product_name}}</h5>
			</div>

			<div class="col-md-6 col-12 right-align-items">
				<!-- <b class="mt-2">To Receive</b><br><br> -->
				<small>x{{$i->quantity}}</small>
				<h4><b>&#8369;{{number_format($i->price, 2)}}</b></h4>
			</div>
		</div>

		<hr class="dotted">
		@endforeach
		<div class="row">
			<div class="col-6">
				<small>{{count($s->items)}} Item{{count($s->items) > 1 ? 's' : ''}}</small>
			</div>
			<div class="col-6 right-align-items" >
				<h5>Order Total: <b>&#8369;{{number_format($s->total, 2)}}</b></h5> 
			</div>
		</div>

		<hr class="solid">
		<div class="row">
			<div class="col-10 col-9">
				<p class="text-left"><span class="badge {{$s->getStatus($s->status) == 'Cancelled' ? 'badge-danger' : ($s->getStatus($s->status) == 'Completed' ? 'badge-success' : 'badge-info')}} text-white">{{$s->getStatus($s->status)}}</span></p>
				<p><i class="fas fa-info-circle text-dark mr-2"></i>{{$s->getLatestlog() == null ? 'Waiting for proof of payment.' : $s->getLatestLog()->getDescription()}}</p>
			</div>
			<div class="col-2 right-align-items">
			   <i class="bi bi-chevron-right text-dark"></i>
			</div>
		</div>

		<hr class="solid">
		@if ($s->status == 1)
		<div class="row" >
			<div class="col-md-3 col-12 ml-auto text-right">
				<a href="{{ route('account.orders.upload_proof', [$s->id]) }}" class="btn btn-dark">Upload Proof</a>
				<a href="{{ route('account.orders.cancel', [$s->id]) }}" class="btn btn-dark">Cancel</a>
			</div>
		</div>
		@elseif ($s->status == 2)
		<div class="row">
			<div class="col-md-3 col-12 ml-auto text-right">
				<button class="btn btn-dark" disabled>Processing your proof of payment</button>
			</div>
		</div> 
		@elseif ($s->status == 3)
		<div class="row">
			<div class="col-md-9 col-sm-12">
				<p>Receive order(s) and make payment by {{Carbon\Carbon::parse($s->getPaymentDeadline())->format('M d, Y')}}</p>
			</div>
			<div class="col-md-3 col-12 d-grid text-right">
				<button class="btn btn-secondary">Order Received</button>
			</div>
		</div>
		@endif
	</a>
</div>
@empty
<div class="itemrow row mt-5 p-3">
	<div class="container-fluid text-center border rounded p-3">
		No orders yet, nothing to show...
	</div>
</div>
@endforelse
<!-- End of Order Info -->
@endsection

@section('subscript')
<script type="text/javascript">
	var nav = document.querySelector('nav');

	window.addEventListener('scroll', function () {
		if (window.pageYOffset > 100) {
			nav.classList.add('bg-dark', 'shadow');
		} else {
			nav.classList.remove('bg-dark', 'shadow');
		}
	});
</script>

<script>
	document.getElementById("togglemenu").addEventListener("click", function() {
		document.getElementById("navbarmain").classList.add('bg-dark', 'shadow');
	});
</script>
@endsection