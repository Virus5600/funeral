<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\Obituary;

class ObituariesTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Obituary::create([
			'customer_id' => 2,
			'name' => 'Shannan Quinn',
			'birth_date' => Carbon::parse('November 14, 1963'),
			'death_date' => Carbon::parse('June 28, 2021'),
			'store_id' => 1,
			'description' => 'Shannan, passed away peacefully due to diabetes and congestive heart failure. She was born March 2, 2000 in Ooltewah, Tennessee to Carl and Annie Lynn. Donna loved to love on people. She sewed dolls and little quilted sayings constantly. Her hands were rarely still. Raggedy Annes and Andies were her favorites. She also shared her love with food such as fresh cut fries for her grandsons, lasagna and real fried chicken. Her life was a living example of her favorite Bible verse, Ephesians 4:32 — “And be kind and compassionate to one another, forgiving one another, just as God also forgave you in Christ.',
			'available_from' => Carbon::now(),
			'available_to' => null,
			'begin_time' => Carbon::now(),
			'location' => 'ST. AGUSTIN\'S CHAPEL',
			'address' => '1631 Denman Ave. Coshocton, Ohio 43812',
			'image_name' => 'shannan_quinn.png',
			'status' => 2
		]);

		Obituary::create([
			'customer_id' => 2,
			'name' => 'Tatiana Watson',
			'birth_date' => Carbon::parse('November 14, 1963'),
			'death_date' => Carbon::parse('June 28, 2021'),
			'store_id' => 1,
			'description' => 'Tatiana, passed away peacefully due to diabetes and congestive heart failure. She was born March 2, 2000 in Ooltewah, Tennessee to Carl and Annie Lynn. Donna loved to love on people. She sewed dolls and little quilted sayings constantly. Her hands were rarely still. Raggedy Annes and Andies were her favorites. She also shared her love with food such as fresh cut fries for her grandsons, lasagna and real fried chicken. Her life was a living example of her favorite Bible verse, Ephesians 4:32 — “And be kind and compassionate to one another, forgiving one another, just as God also forgave you in Christ.',
			'available_from' => Carbon::now(),
			'available_to' => null,
			'begin_time' => Carbon::now(),
			'location' => 'ST. AGUSTIN\'S CHAPEL',
			'address' => '1631 Denman Ave. Coshocton, Ohio 43812',
			'image_name' => 'tatiana_watson.png',
			'status' => 2
		]);
	}
}