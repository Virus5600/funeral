@extends('template.admin')

@section('title', 'Create Livestream')
@section('sidebar', 'admin')

@section('body')
<h1 class="font-weight-bold"><a class="text-dark" href="javascript:void(0);" onclick="confirmLeave('{{route('admin.livestream-management.index')}}');"><i class="fas fa-chevron-left mr-2"></i>Edit Livestream</a></h1>

<hr class="hr-thick" style="border-color: #707070;">

<form action="{{ route('admin.livestream-management.update',[$livestream->id]) }}" method="POST" class="form container-fluid" enctype="multipart/form-data">
	{{csrf_field()}}
	<input type="hidden" name="_method" value="PUT">
	<div class="row">
		<div class="col d-flex flex-d-col">
			<div class="form-group">
				<label class="form-label">Title</label>
				<input type="text" name="title" value="{{$livestream->title}}" class="form-control"/>
				<span class="badge badge-danger w-100 validation-message">{{$errors->first('title')}}</span>
			</div>

			<div class="form-group">
				<label class="form-label">Description</label>
				<textarea class="form-control not-resizable" rows="5" name="description">{{$livestream->description}}</textarea>
				<span class="badge badge-danger w-100 validation-message">{{$errors->first('description')}}</span>
			</div>

			<div class="form-group">
				<label class="form-label">Scheduled Date</label>
				<input type="date" name="schedule_date" value="{{$livestream->schedule_date}}" min="{{\Carbon\Carbon::now()->format('Y-m-d')}}" class="form-control" />
				<span class="badge badge-danger w-100 validation-message">{{$errors->first('schedule_date')}}</span>
			</div>

			<div class="form-group">
				<label class="form-label">Scheduled Time</label>
				<input type="time" name="schedule_time" value="{{$livestream->schedule_time}}" class="form-control"/>
				<span class="badge badge-danger w-100 validation-message">{{$errors->first('schedule_time')}}</span>
			</div>

			<div class="form-group">
				<label class="form-label">Duration</label>
				<div class="input-group">
					<input type="number" id="duration" name="duration" value="{{$livestream->duration}}" class="form-control" readonly/>
					<div class="input-group-append">
						<span class="input-group-text">hour(s)</span>
					</div>
				</div>
				<span class="badge badge-danger w-100 validation-message">{{$errors->first('duration')}}</span>
			</div>

			<div class="form-group">
				<label class="form-label">Price</label>
				<div class="input-group">
					<div class="input-group-prepend">
						<span class="input-group-text">₱</span>
					</div>
					<input type="number" id="price" name="price" value="{{$livestream->price}}" class="form-control" readonly/>
				</div>
				<span class="badge badge-danger w-100 validation-message">{{$errors->first('price')}}</span>
			</div>

			<div class="form-group">
				<label class="form-label">Status</label>
				@if ($livestream->status == 'pending' || $livestream->status == 'cancelled')
				<select class="custom-select" name="status">
					<option value="pending" {{ $livestream->status == 'pending' ? 'selected' : ''}}>Pending</option>
					<option value="approved">Approved</option>
					<option value="cancelled" {{ $livestream->status == 'cancelled' ? 'selected' : ''}}>Cancelled</option>
				</select>
				@elseif ($livestream->status == 'approved')
				<select class="custom-select" name="status">
					<option value="pending">Pending</option>
					<option value="approved" selected>Approved</option>
					<option value="cancelled">Cancelled</option>
					<option value="done">Finished</option>
				</select>
				@else
				<select class="custom-select" disabled>
					<option value="{{$livestream->status}}" selected>{{ucwords($livestream->status)}}</option>
				</select>
				<input type="hidden" name="status" value="{{$livestream->status}}">
				@endif
				<span class="badge badge-danger w-100 validation-message">{{$errors->first('status')}}</span>
			</div>

			<div class="form-group">
				<label class="form-label font-weight-bold">User</label>
				<select class="custom-select" name="user_id">
					@foreach($users as $x)
						<option value="{{$x->id}}" @if($x->id==$livestream->user_id) selected @endif>{{ $x->getName()}} ({{$x->email}})</option>
					@endforeach
				</select>
				<span class="badge badge-danger w-100 validation-message">{{$errors->first('user_type')}}</span>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col">
			<button type="submit" data-action="update" class="btn btn-dark float-right">Submit</button>
		</div>
	</div>
</form>

<div class="row mb-5 pb-5">
	<div class="col-12 col-md-6">
		<hr class="hr-thick" style="border-color: #707070;">
		<h3>Stream Details</h3>

		<div class="d-flex flex-d-col mt-3">
			<p>
				<b>Stream ID:</b><br>
				{{$livestream->stream_id}}
			</p>

			<p>
				<b>Stream Key:</b><br>
				{{$livestream->stream_key}}
			</p>

			<p>
				<b>Stream Link:</b><br>
				<a class="text-decoration-none" href="{{route('livestream.show', [$livestream->stream_id])}}" target="_balnk" title="{{$livestream->title or 'Livestream'}}">{{route('livestream.show', [$livestream->stream_id])}}</a>
			</p>
		</div>
	</div>

	<div class="col-12 col-md-6">
		<hr class="hr-thick" style="border-color: #707070;">
		<h3>Add-ons</h3>

		<div class="d-flex flex-d-col mt-3">
			<p>
				<b>Standing Photo:</b><br>
				<span id="standing_photo">
					@if ($livestream->getOptionStatus('standing_photo') == 1)
					<i class="fas fa-circle mr-2 text-success"></i>Yes
					@else
					<i class="fas fa-circle mr-2 text-danger"></i>No
					@endif
				</span>
			</p>

			<p>
				<b>Video Slideshow:</b><br>
				<span id="video_slideshow">
					@if ($livestream->getOptionStatus('video_slideshow') == 1)
					<i class="fas fa-circle mr-2 text-success"></i>Yes
					@else
					<i class="fas fa-circle mr-2 text-danger"></i>No
					@endif
				</span>
			</p>

			<p>
				<b>Flower Stands:</b><br>
				<span id="flower_stands">
					@if ($livestream->getOptionStatus('flower_stands') == 1)
					<i class="fas fa-circle mr-2 text-success"></i>Yes
					@else
					<i class="fas fa-circle mr-2 text-danger"></i>No
					@endif
				</span>
			</p>
		</div>
	</div>
</div>
@endsection

@section('script')
<script type="text/javascript">
	$(document).ready(function() {
		$('#duration').on('change', (e) => {
			$('#price').val(parseFloat($(e.target).val() * 100).toFixed(2));
		});
	});
</script>
@endsection