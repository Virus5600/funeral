@extends('template.admin')

@section('title', 'Map')
@section('sidebar', 'admin')

@section('body')
@if ($isShow)
<h1 class="font-weight-bold"><a class="text-dark" href="{{route('admin.store.index')}}"><i class="fas fa-chevron-left mr-2"></i>Map</a></h1>
@else
<h1 class="font-weight-bold"><a class="text-dark" href="javascript:void(0);" onclick="confirmLeave('{{route('admin.store.index')}}');"><i class="fas fa-chevron-left mr-2"></i>Store</a></h1>
@endif

<hr class="hr-thick" style="border-color: #707070;">

<form class="form" action="{{ route('admin.store.map.update', [$store->id]) }}" method="POST" enctype="multipart/form-data">
	{{ csrf_field() }}

	<div class="row">
		<div class="col-12">
			<div id="map" class="w-100" style="height: 50vh;"></div>
		</div>
	</div>
	<br>

	@if ($isShow)
	<div class="d-flex flex-column justify-content-center align-items-center mb-3">
		<button type="button" class="btn btn-dark" id="return_to_marker">Return to Map Marker</button>
	</div>
	@endif

	<div class="row">
		<div class="col-12 col-md-6">
			<div class="form-group">
				<label class="form-label">Latitude:</label>
				@if (!$isShow)
				<input type="number" class="form-control" name="latitude" value="{{ Session::get('hasErrors') ? old('longitude') : $location->latitude }}" step='0.0000000000000001' min='-90' max='90'>
				<span class="badge badge-danger w-100 validation-message">{{$errors->first('latitude')}}</span>
				@else
				<input type="number" class="form-control" value="{{$location->latitude or 0.0}}" disabled>
				@endif
			</div>
		</div>

		<div class="col-12 col-md-6">
			<div class="form-group">
				<label class="form-label">Longitude:</label>
				@if (!$isShow)
				<input type="number" class="form-control" name="longitude" value="{{ Session::get('hasErrors') ? old('longitude') : $location->longitude }}" step='0.0000000000000001' min='-180' max='180'>
				<span class="badge badge-danger w-100 validation-message">{{$errors->first('longitude')}}</span>
				@else
				<input type="number" class="form-control" value="{{$location->longitude or 0.0}}" disabled>
				@endif
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-12">
			<div class="form-group">
				<label class="form-label">Address:</label>
				@if (!$isShow)
				<textarea rows="5" class="form-control not-resizable" name="address">{{ Session::get('hasErrors') ? old('address') : $location->address }}</textarea>
				<span class="badge badge-danger w-100 validation-message">{{$errors->first('address')}}</span>
				@else
				<textarea rows="5" class="form-control not-resizable" disabled>{{$location->address or 'N/A'}}</textarea>
				@endif

				@if (!$isShow)
				<div class="d-flex flex-column justify-content-center align-items-center my-3">
					<button type="button" class="btn btn-secondary" id="get_address">Get Address</button>
				</div>
				@endif
			</div>
		</div>
	</div>

	@if (!$isShow)
	<div class="row">
		<div class="col-12">
			<button type="submit" class="btn btn-dark" data-action="update">Save Changes</button>
		</div>
	</div>
	@endif
</form>
@endsection

@section('script')
<script src="https://maps.googleapis.com/maps/api/js?key={{env('MAP_API_KEY')}}&callback=initMap&libraries=&v=weekly" async></script>
<script type="text/javascript">
	function initMap() {
		// Map drawing
		const location = {
			lat: {{$location->latitude or 0.0}},
			lng: {{$location->longitude or 0.0}}
		};

		const map = new google.maps.Map($('#map').get()[0], {
			zoom: 17,
			center: location,
		});

		const marker = new google.maps.Marker({
			position: {
				lat: location.lat,
				lng: location.lng
			},
			map: map,
			animation: google.maps.Animation.DROP,
			title: `{!!$store->name!!}`
		});

		const geocoder = new google.maps.Geocoder();

		@if (!$isShow)
		map.addListener('center_changed', function() {
			marker.setPosition(new google.maps.LatLng(map.getCenter().lat(), map.getCenter().lng()));
			$('[name=latitude]').val(map.getCenter().lat());
			$('[name=longitude]').val(map.getCenter().lng());
		});

		$('[name=latitude], [name=longitude]').on('change', (e) => {
			map.panTo(new google.maps.LatLng($('[name=latitude]').val(), $('[name=longitude]').val()));
		});

		$('#get_address').on('click', (e) => {
			geocoder.geocode({
				location: {
					lat: map.getCenter().lat(),
					lng: map.getCenter().lng()
				}
			}).then((response) => {
				console.log(response)
				if (response.results[0]) {
					$('[name=address]').val(response.results[0].formatted_address);
				}
			});
		});
		@else
		const content = `<h3>{!! $store->name !!}</h3><p>{!! $location->address !!}</p>`;
		const infoWin = new google.maps.InfoWindow({
			content: content
		});

		marker.addListener('click', (e) => {
			infoWin.open({
				anchor: marker,
				map,
			});
		});


		$('#return_to_marker').on('click', () => {
			map.panTo(new google.maps.LatLng(location.lat, location.lng));
		});
		@endif
	}
</script>
@endsection