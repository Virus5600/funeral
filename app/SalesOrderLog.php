<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalesOrderLog extends Model
{
	protected $fillable = [
		'order_id',
		'description',
		'time',
		'type',
	];

	protected function salesOrder() {
		return $this->belongsTo('App\SalesOrder', 'order_id');
	}

	public function getProofImage() {
		return preg_replace('/(Submitted proof \w+\. )\{image: (.+)\}/i', '$2', $this->description);
	}

	public function getDescription() {
		return preg_replace('/(Submitted proof \w+\. )\{image: (.+)\}/i', '$1', $this->description);
	}

	public function isAccepted() {
		return preg_replace('/(Submitted proof )(\w+)\. \{image: (.+)\}/i', '$2', $this->description) == 'accepted';
	}

	public function isRejected() {
		return preg_replace('/(Submitted proof )(\w+)\. \{image: (.+)\}/i', '$2', $this->description) == 'rejected';
	}
}