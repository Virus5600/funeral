@extends('template.client')

@section('title', 'Store')

@section('page-css')
<link rel="stylesheet" href="/css/store.index.css" >
@endsection

@section('body')
{{-- STORE BANNER --}}
<section class="p-5 d-flex flex-row" id="banner">
	@include('template.social-share')

	<div class="container-fluid">
		<div class="container">
			<div class="row mt-5 pt-3" id="buttons">
				<div class="col-lg-3 mx-0 px-1">
					<a class="btn semi-transparent-button d-flex justify-content-center flex-column text-white" href="#">
						GET P1000 OFF<br>
						<small id="code">ON YOUR FIRST ORDER CODE:SOL1ST </small>
					</a>
				</div> 

				<div class="col-lg-3 mx-0 px-1">
					<a class="btn semi-transparent-button d-flex justify-content-center flex-column text-white" href="#">SHIPPING INFO</a>
				</div>

				<div class="col-lg-3 mx-0 px-1">
					<a class="btn semi-transparent-button d-flex justify-content-center flex-column text-white" href="#">CASH ON DELIVERY</a>
				</div>

				<div class="col-lg-3 mx-0 px-1">
					<a class="btn semi-transparent-button d-flex justify-content-center flex-column text-white" href="#">EASY BUY, FREE RETURN</a>
				</div>
			</div>
		</div>

		<div class="container pt-md-5" >
			<div class="d-sm-flex justify-content-between">
				<div class ="col-xl-8 col-md-8 col-sm-12 p-5" id="bannertext">
					<h1>Welcome to Soulace's Store.</h1>			   
					<p class="lead my-4">Enjoy discounts and special offers when you shop with us!</p>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- Items -->
<section class="py-5">
	<div class="container text-center">
		<div class="row my-5">
			<h1 class="mb-5">Shop on these Stores</h1>
			
			@php ($i = 0)
			@foreach ($stores as $s)
			<div class="col-lg-6 my-3 mx-auto">
				<div class="cardstore card img-fluid">
					<img class="card-img-top" src="/images/banners/store/{{'store-'.(($i%4)+1)}}.png" alt="Card image" >
					
					<a href="{{route('store.show', [strtolower(preg_replace('/ /i', '_', $s->name))])}}" class="card-img-overlay text-decoration-none">
						<h1>{{strtoupper($s->name)}}</h1>
					</a>
				</div>
			</div>
			@php ($i++)
			@endforeach
		</div>
	</div>
</section>
@endsection