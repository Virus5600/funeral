<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Carbon\Carbon;

class SalesOrder extends Model
{
	protected $table ="sales_order";

	protected $fillable = [
		'customer_id',
		'courier',
		'shipping_addresses_id',
		'payment_method',
		'pay_later',
		'instructions',
		'total',
		'status',
		'proof_of_payment',
		'store_id',
		'tracking_number'
	];
	/*
	Status:
	0 - Cancelled
	1 - Waiting for Proof
	2 - Pending
	3 - Confirmed
	4 - Shipped
	5 - Received
	*/

	private static $status = [
		'Cancelled',
		'Waiting for Payment',
		'Processing',
		'Shipped Out',
		'To Receive',
		'Completed'
	];

	protected function customer() {
		return $this->belongsTo('App\User','customer_id');
	}

	protected function items() {
		return $this->hasMany('App\SalesOrderItems', 'order_id');
	}

	protected function shippingAddress() {
		return $this->belongsTo('App\ShippingAddress', 'shipping_addresses_id');
	}

	protected function logs() {
		return $this->hasMany('App\SalesOrderLog', 'order_id');
	}

	protected function store() {
		return $this->belongsTo('App\Store', 'store_id');
	}

	public function getPaymentDeadline() {
		return Carbon::parse($this->created_at)->addDays(7);
	}

	public static function getStatus($index) {
		return SalesOrder::$status[$index];
	}

	public static function getStatusList($isCancelled = false) {
		if ($isCancelled)
			return SalesOrder::$status[0];
		else
			return array_slice(SalesOrder::$status, 1);
	}

	public function getETA() {
		foreach ($this->logs as $l) {
			if ($l->type == 3)
				return \Carbon\Carbon::parse($l->created_at)->addDays(7)->format('D, M d, Y');
		}

		return null;
	}

	public function getLatestlog() {
		return $this->logs->sortByDesc('created_at')->first();
	}
}