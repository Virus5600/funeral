@extends('template.client')

@section('title', 'Services - Obituary')

@section('page-css')
<link rel="stylesheet" href="/css/services.obituaries.show.css" >
@endsection

@section('body')
{{-- OBITUARIES BANNER --}}
<section class="p-5" id="banner">
	<div class="container my-5">
		<div class="row py-5 justify-content-center">
			<div class="col-lg-10 py-5 bg-dark text-center">
				<h4>Obituary</h4>
				<h1>{{$obituary->name}}</h1>
				<h3>{{\Carbon\Carbon::parse($obituary->birth_date)->format('F d, Y')}} – {{\Carbon\Carbon::parse($obituary->death_date)->format('F d, Y')}}</h3>
				<img src="/uploads/obituary/{{$obituary->image_name or 'default.png'}}" alt="" class="my-4 p-2 img-fluid w-25" onerror="imgError(this, '{{asset('/uploads/obituary/default.png')}}');">
				<h3 class="mb-4">In care of {{$obituary->store->name}}</h3>
				<p class="lead px-md-5">{!! $obituary->description !!}</p>
			</div>
		</div>
	</div>
</section>

{{-- INFO --}}
<section class="mb-5 py-5">
	<div class="container mb-5">
		<div class="row bg-light text-dark justify-content-center lightbg">
			<div class="col-lg-1 col-md-2 col-12 my-md-5 pt-4" id="obdate">
				<h1 class="font-weight-bold">{{\Carbon\Carbon::parse($obituary->available_from)->format('d')}}</h1>
				<h2>{{\Carbon\Carbon::parse($obituary->available_from)->format('M')}}</h2>
			</div>

			<div class="col-lg-4 col-md-5 col-10 my-5 px-5">
				<h2><i>Memorial Service</i></h2>
				<h5>{{\Carbon\Carbon::parse($obituary->begin_time)->format('h:m A')}}</h5>
				<h4 class="font-weight-bold">{{$obituary->location or 'Not mentioned'}}</h4>
				<p>{{$obituary->address or 'Not mentioned'}}</p>
			</div>
		</div>
	</div>
</section>
@endsection