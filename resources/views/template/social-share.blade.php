<div class="d-flex flex-column align-items-center justify-content-top justify-content-lg-center py-5" style="width: 25px;">
	<div class="mb-3" style="flex: 1 1 auto;">
		<div class="h-100 border" style="width: 0; background-color: #919191!important; border-color: #919191!important;"></div>
	</div>

	<div class="d-flex flex-column">
		<a href="#" class="mb-1"><i class="fab fa-facebook-square fa-2x social-link-side"></i></a>
		<a href="#" class="mt-1"><i class="fab fa-twitter-square fa-2x social-link-side"></i></a>
	</div>

	<div class="mt-3" style="flex: 1 1 auto;">
		<div class="h-100 border" style="width: 0; background-color: #919191!important; border-color: #919191!important;"></div>
	</div>
</div>