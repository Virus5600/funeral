<!DOCTYPE html>
<html lang="en">
	<head>
		{{-- META DATA --}}
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		{{-- SITE META --}}
		<meta name="author" content="Code Senpai, Project on Rush">
		<meta name="type" content="website">
		<meta name="title" content="{{ env('APP_NAME') }}">
		<meta name="description" content="{{ env('APP_DESC') }}">
		<meta name="image" content="/images/branding/soulace_black.jpg">
		<meta name="keywords" content="Soulace, Funeral, Parlor, Funeral Parlor">
		<meta name="application-name" content="{{ env('APP_NAME') }}">

		{{-- TWITTER META --}}
		<meta name="twitter:card" content="summary_large_image">
		<meta name="twitter:title" content="{{ env('APP_NAME') }}">
		<meta name="twitter:description" content="{{ env('APP_DESC') }}">
		<meta name="twitter:image" content="{{Request::url()}}/images/branding/soulace_black.jpg">

		{{-- OG META --}}
		<meta name="og:url" content="{{Request::url()}}">
		<meta name="og:type" content="website">
		<meta name="og:title" content="{{ env('APP_NAME') }}">
		<meta name="og:description" content="{{ env('APP_DESC') }}">
		<meta name="og:image" content="/images/branding/soulace_black.jpg">

		{{-- Favicon --}}
		<link rel='icon' type='image/png' href='/images/branding/logo_gradient.png'>

		{{-- jQuery 3.6.0 --}}
		<!-- <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script> -->
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

		{{-- popper.js 1.16.0 --}}
		<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>

		{{-- Bootstrap 4.4 --}}
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

		{{-- Bootrstrap 5.0.2 --}}
		<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
		<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">

		{{-- Sweet Alert 2 --}}
		<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>

		{{-- Fontawesome --}}
		<script src="https://kit.fontawesome.com/d4492f0e4d.js" crossorigin="anonymous"></script>

		{{-- Custom CSS --}}
		<link rel="stylesheet" href="/css/login.css">

		{{-- Title --}}
		<title>{{ env('APP_NAME') }} | Login</title>
	</head>

	<body>
		<div class="container mt-5">
			<form action="{{ route('authenticate') }}" method="POST" enctype="multipart/form-data" class="row">
				{{csrf_field()}}

				<div class="col col-lg-6 col-12 px-5 my-5" id="login_form">
					<div class="text-center">
						<img src="/images/branding/soulace_white.png" class="img-fluid w-50" alt="">
					</div>

					<h1 class="mt-5">Log in Here.</h1>
					<h4>Enter your valid Credentials to log in.</h4>

					<div>
						<div class="mb-3 input-group bg-transparent-all">
							<div class="input-group-prepend">
								<span class="input-group-text border-right-0 bw-thick"><i class="fas fa-user"></i></span>
							</div>
							<input type="email" name="email" class="form-control bw-thick" id="exampleFormControlInput1" placeholder="Enter Email" value="{{ old('email') }}">
						</div>

						<div class="mb-3 input-group bg-transparent-all">
							<div class="input-group-prepend">
								<span class="input-group-text border-right-0 bw-thick"><i class="fas fa-lock"></i></span>
							</div>
							<input type="password" name="password" class="form-control bw-thick" id="exampleFormControlInput2" placeholder="Enter Password">
						</div>

						<div class="d-flex flex-row">
							<div class="form-check">
								<input class="form-check-input bg-transparent-all border-white" type="checkbox" name="remember_me" id="flexCheckDefault" {{ old('remember_me') ? 'checked' : ''}}>
								<label class="form-check-label" for="flexCheckDefault">Remember Me</label>
							</div>
							<span style='font-size: 1rem;' class="text-center ml-auto badge badge-danger">{{Session::get('flash_error')}}</span>
							<a href="{{ route('recoverPass') }}" class="ml-auto text-white">Forgot Password?</a>
						</div>

						<div class=" my-4 d-flex justify-content-center">
							<div class="d-grid col-6">
								<button class="btn btn-light btn-lg br-25">Sign In</button>
							</div>
						</div>

						<div class="my-5 text-center">Not registered yet? <a href="{{ route('register') }}" class="font-weight-bold text-white">Create an Account</a></div>
					</div>
				</div>

				<div class="col col-lg-6 col-12 my-5 d-flex align-items-center">
					<div class="container">
						<div class="row">
							<h1 id="tagline">Keeping the Memories<br><span id="alive">Alive.</span></h1>
						</div>
					</div>
				</div>
			</form>
		</div>

		<footer>
			<div class="container">
				<div class="row mt-3" id="footer_policy">
					<div class="reserved col-lg-6 float-left pt-4 pb-lg-4 pb-0 text-center text-lg-left">
						<small >Privacy Policy | Terms and Conditions</small>
					</div>

					<div class="reserved col-lg-6 pb-4 pt-lg-4 pt-0 text-center text-lg-right">
						<small>©2021 Soulace All Rights Reserved.</small>
					</div>
				</div>
			</div>
		</footer>

		<script type="text/javascript">
			@if (Session::has('flash_error'))
			Swal.fire({
				{!!Session::has('has_icon') ? "icon: `error`," : ""!!}
				title: `{{Session::get('flash_error')}}`,
				{!!Session::has('message') ? 'html: `' . Session::get('message') . '`,' : ''!!}
				position: {!!Session::has('position') ? '`' . Session::get('position') . '`' : '`top`'!!},
				showConfirmButton: false,
				toast: {!!Session::has('is_toast') ? Session::get('is_toast') : true!!},
				{!!Session::has('has_timer') ? (Session::get('has_timer') ? (Session::has('duration') ? ('timer: ' . Session::get('duration')) . ',' : `timer: 10000,`) : '') : `timer: 10000,`!!}
				background: `#dc3545`,
				customClass: {
					title: `text-white`,
					content: `text-white`,
					popup: `px-3`
				},
			});

				@if (env('APP_ENV') == 'local' || env('APP_ENV') == 'dev')
					if ('{{Session::get('flash_error')}}' == 'Something went wrong, please try again later') {
						console.log(`{!!Session::get('flash_error_actual')!!}`);
					}
				@endif
			@elseif (Session::has('flash_message'))
			Swal.fire({
				{!!Session::has('has_icon') ? "icon: `info`," : ""!!}
				title: `{{Session::get('flash_message')}}`,
				{!!Session::has('message') ? 'html: `' . Session::get('message') . '`,' : ''!!}
				position: {!!Session::has('position') ? '`' . Session::get('position') . '`' : '`top`'!!},
				showConfirmButton: false,
				toast: {!!Session::has('is_toast') ? Session::get('is_toast') : true!!},
				{!!Session::has('has_timer') ? (Session::get('has_timer') ? (Session::has('duration') ? ('timer: ' . Session::get('duration')) . ',' : `timer: 10000,`) : '') : `timer: 10000,`!!}
				background: `#17a2b8`,
				customClass: {
					title: `text-white`,
					content: `text-white`,
					popup: `px-3`
				},
			});
			@elseif (Session::has('flash_success'))
			Swal.fire({
				{!!Session::has('has_icon') ? "icon: `success`," : ""!!}
				title: `{{Session::get('flash_success')}}`,
				{!!Session::has('message') ? 'html: `' . Session::get('message') . '`,' : ''!!}
				position: {!!Session::has('position') ? '`' . Session::get('position') . '`' : '`top`'!!},
				showConfirmButton: false,
				toast: {!!Session::has('is_toast') ? Session::get('is_toast') : true!!},
				{!!Session::has('has_timer') ? (Session::get('has_timer') ? (Session::has('duration') ? ('timer: ' . Session::get('duration')) . ',' : `timer: 10000,`) : '') : `timer: 10000,`!!}
				background: `#28a745`,
				customClass: {
					title: `text-white`,
					content: `text-white`,
					popup: `px-3`
				},
			});
			@endif
		</script>
	</body>
</html>