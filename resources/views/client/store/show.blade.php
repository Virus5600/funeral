@extends('template.client')

@section('title', 'Store')

@section('page-css')
<link rel="stylesheet" href="/css/store.show.css" >
@endsection

@section('body')
{{-- STORE BANNER --}}
<section class="p-5" id="banner">
	<div class="container pt-md-5 mt-5">
		<div class="d-sm-flex justify-content-center">
			<h1 class="font-weight-bold">{{$store->name}}</h1>
		</div>

		<div class="container">
			<div class="row mt-3 pt-3" id="buttons">
				<div class="col mx-0 px-1 mt-2">
					<a class="btn headbutton " href="{{route('products.index', [strtolower(preg_replace('/ /i', '_', $store->name))])}}">PRODUCTS</a>
				</div> 

				@foreach ($store->categories() as $c)
				<div class="col mx-0 px-1 mt-2">
					<a class="btn headbutton " href="{{route('products.index', [strtolower(preg_replace('/ /i', '_', $store->name)), preg_replace('/ /', '_', $c->category_name)])}}">{{strtoupper($c->category_name)}}</a>
				</div> 
				@endforeach
			</div>
		</div>
	</div>
</section>

{{-- Store Front Image --}}
<section>
	<div class="container p-5 mb-5" id="storefront"></div>
</section>

<!-- Products -->
<section>
	<div class="container">
		<h2>All Products</h2>
		<hr id="itemhr">

		<div class="row mt-2">
			<div class="mx-md-2 mt-3 mb-5">
				<div class="container my-3">
					<div class="row">
						{{-- PRODUCT ITEMS --}}
						@forelse ($products as $p)
							@if ($p->inventory == 0)
							@php
							continue;
							@endphp
							@else
							<div class="col-lg-4 col-md-6 col-12 px-2 mt-3">
								<div class="card card-body text-center my-2 p-0 bg-light rounded h-100">
									<div style="position: relative;">
										<img src="/uploads/products/{{$p->category->category_name}}/{{$p->image_name}}" alt="{{$p->product_name}}" class="card-img-top img-fluid">
									
										<div class="card-img-overlay h-100 justify-content-center align-items-center">
											<a href="{{ route('cart.addToCart', [$p->id]) }}" class="btn btn-outline-light">Add to Cart</a>
										</div>
									</div>

									<a href="{{ route('products.show', [preg_replace('/ /', '_', strtolower($store->name)), preg_replace('/ /', '_', strtolower($p->category->category_name)), $p->id]) }}" class="card-text mt-4 h-100 d-flex flex-d-col justify-content-end text-decoration-none">
										<h4>{{$p->product_name}}</h4>
										<h5 class="my-2">Regular Price: &#8369;{{number_format($p->selling_price, 2)}}</h5>
										<!-- <h5 class="my-2"><span class="gold">Special Price: </span> P24,600.00</h5> -->
									</a>
								</div>
							</div>
							@endif
						@empty
							<span class="mx-auto">Sorry! There's no products from this store.</span>
						@endforelse
					</div>

					<div class="row mt-5">
						<div class="col">
							<div class="float-right d-flex flex-d-row bg-dark">
								@if ($products->previousPageUrl() == null)
								<span class="border border-end-0 mr-0 p-2" style="border-top-left-radius: .25rem; border-bottom-left-radius: .25rem; border-right: solid #fff;"><i class="text-muted fas fa-chevron-left mx-2"></i></span>
								@else
								<a href="{{$products->previousPageUrl()}}" class="text-white border border-end-0 mr-0 p-2 hover-fx-pagination" style="border-top-left-radius: .25rem; border-bottom-left-radius: .25rem; border-right: solid #fff;"><i class="fas fa-chevron-left mx-2 cursor-pointer"></i></a>
								@endif
								
								<span class="text-white border border-start-0 border-end-0 mx-0 p-2">Total Results {{($products->currentPage()-1) * $products->perPage() + 1}}-{{(($products->currentPage()-1) * $products->perPage()) + $products->count()}} of {{$products->total()}}</span>
								
								@if ($products->nextPageUrl() == null)
								<span class="border border-start-0 ml-0 p-2" style="border-top-right-radius: .25rem; border-bottom-right-radius: .25rem;"><i class="text-muted fas fa-chevron-right mx-2"></i></span>
								@else
								<a href="{{$products->nextPageUrl()}}" class="text-white border border-start-0 ml-0 p-2 hover-fx-pagination" style="border-top-right-radius: .25rem; border-bottom-right-radius: .25rem;"><i class="fas fa-chevron-right mx-2 cursor-pointer"></i></a>
								@endif
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Contact Us -->
	@if ($store->location != null || $store->email != null || $store->contact_number != null || $store->facebook != null || $store->twitter != null)
	<div class="row text-center">
		<h1><img src="/images/UI/line.svg" class="imgline">Contact Us</h1>
		<h5>Got some questions? Don't hesitate to send it now.</h5>
	</div>

	<div class="row">
		<div class="col col-lg-5 col-12 my-5 p-4 mx-auto text-center">
			<div class="mx-md-5">
				@if ($store->location != null)
				<h5><i class="bi bi-house-fill mx-sm-3"></i>{{$store->location->address}}</h5>
				@endif

				@if ($store->email != null)
				<h5 class="my-4"><i class="bi bi-envelope-open-fill mx-sm-3"></i>{{ $store->email }}</h5>
				@endif

				@if ($store->contact_number != null)
				<h5 class="my-4"><i class="bi bi-telephone-inbound-fill mx-sm-3"></i>{{ $store->contact_number }}</h5>
				@endif
			</div>

			<div class="mx-md-5 my-5">
				@if ($store->facebook != null || $store->twitter != null)
				<h4>Visit us at..</h4>
				@endif

				@if ($store->facebook != null)
				<h5 class="my-4"><i class="bi bi-facebook mr-2"></i>{{ $store->facebook }}</h5>
				@endif

				@if ($store->twitter != null)
				<h5 class="my-4"><i class="bi bi-twitter mr-2"></i>{{ $store->twitter }}</h5>
				@endif
			</div>
		</div>
	</div>
	@endif
</section>
@endsection