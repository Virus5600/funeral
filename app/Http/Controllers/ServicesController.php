<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Requests;

use App\Service;

use DB;
use Log;
use Validator;

class ServicesController extends Controller
{
	protected function index() {
		return view('admin.services.index', [
			'services' => Service::get()
		]);
	}

	protected function store(Request $req) {
		$validator = Validator::make($req->all(), [
			'description' => 'min:3'
		],[
			'description.min' => 'Description is too short.'
		]);

		if ($validator->fails()) {
			return redirect()
				->back()
				->withErrors($validator)
				->withInput()
				->with('create_error', true);
		}

		try {
			DB::beginTransaction();

			Service::create([
				'service_name' => $req->service_name,
				'description' => $req->description,
				'store_id' => Auth::user()->store_id
			]);

			DB::commit();
		} catch (\Exception $e) {
			Log::info($e);
			DB::rollback();

			return redirect()
				->back()
				->withInput()
				->with('flash_error', 'Something went wrong, please try again later.');
		}

		return redirect()
			->back()
			->with('flash_success', 'Successfully added "' . $req->service_name . '".');
	}

	protected function show($service_name) {
		return view('admin.services.edit', [
			'service' => Service::where('service_name', '=', preg_replace('/_/', ' ', $service_name))->first(),
			'isShow' => true
		]);
	}

	protected function edit($service_name) {
		return view('admin.services.edit', [
			'service' => Service::where('service_name', '=', preg_replace('/_/', ' ', $service_name))->first(),
			'isShow' => false
		]);
	}

	protected function update(Request $req, $id) {
		$validator = Validator::make($req->all(), [
			'service_name' => 'required|min:3',
			'description' => 'min:3'
		],[
			'service_name.required' => 'The name of the service is required.',
			'service_name.min' => 'Service name is too short.',
			'description.min' => 'Description is too short.'
		]);

		if ($validator->fails()) {
			return redirect()
				->back()
				->withErrors($validator)
				->withInput();
		}

		try {
			DB::beginTransaction();

			$service = Service::find($id);
			$service->service_name = $req->service_name;
			$service->description = $req->description;
			$service->save();

			DB::commit();
		} catch (\Exception $e) {
			Log::info($e);
			DB::rollback();

			return redirect()
				->back()
				->withInput()
				->with('flash_error', 'Something went wrong, please try again later.');
		}

		return redirect()
			->route('admin.services.index')
			->with('flash_success', 'Successfully updated "' . $req->service_name . '".');
	}

	// protected function delete($id) {
	// 	Service::find($id)->delete();

	// 	return redirect()
	// 		->route('admin.services.index')
	// 		->with('flash_success', 'Successfully removed service entry.');
	// }
}