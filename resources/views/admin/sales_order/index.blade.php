@extends('template.admin')

@section('title', 'Sales Order')
@section('sidebar', 'admin')

@section('body')
<h1 class="h3 h1-lg font-weight-bold text-truncate">Sales Order</h1>

<hr class="hr-thick" style="border-color: #707070;">

<div class="row mx-0 px-0">
	<form action="{{ route(Request::route()->getName()) }}" method="GET" class="col-12 col-lg-5 mx-0 px-0">
		<div class="input-group">
			<input type="text" class="form-control" placeholder="Search for..." name="s" value="{{$search}}">
			<div class="input-group-append">
		    	<button type="submit" data-action="none" class="btn btn-dark"><i class="fas fa-search"></i></button>
			</div>
		</div>
	</form>
</div>

<div class="row mx-0 px-0">
	<div class="col-12 px-0">
		<div class="card border rounded dark-shadow h-100">
			<h3 class="card-header font-weight-bold text-custom">Order List</h3>
			<div class="card-body mx-0 pt-0 px-0 overflow-auto">
				{{-- TABLE --}}
				<table class="table table-striped">
					<thead>
						<tr class="font-weight-bold">
							<td>Stream ID</td>
							<td>Order Date</td>
							<td>Customer Name</td>
							<td>Status</td>
							<td>Duration</td>
							<td>Total</td>
							<td></td>
						</tr>
					</thead>

					<tbody id='table-content'>
						
						@forelse($so as $x)
						<tr>
							<td>{{$x->stream_id}}</td>
							<td>{{\Carbon\Carbon::parse($x->created_at)->format('M d, Y')}}</td>
							<td>{{$x->ls_user->getName() }}</td>		
							<td>
								@if ($x->status == 'cancelled')
								<i class="fas fa-circle mr-2 text-danger"></i>
								@elseif ($x->status == 'waiting for proof' || $x->status == 'awaiting schedule')
								<i class="fas fa-circle mr-2 text-info"></i>
								@elseif ($x->status == 'pending' || $x->status == 'streaming')
								<i class="fas fa-circle mr-2 text-warning"></i>
								@elseif ($x->status == 'finished')
								<i class="fas fa-circle mr-2 text-success"></i>
								@endif
								{{ $x->displayStatus() }}
							</td>
							<td>{{$x->duration > 1 ? $x->duration . ' hours' : $x->duration . ' hour'}}</td>
							<td>&#8369;{{number_format($x->price, 2)}}</td>
							<td>
								<button type="button" class="btn btn-primary dropdown-toggle py-0" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action</button>

								<div class="dropdown-menu dropdown-menu-right">
									<a class="dropdown-item" href="{{ route('admin.sales-order.show', [$x->id]) }}"><i class="far fa-eye mr-2"></i>View Details</a>
									<a class="dropdown-item" href="{{ route('admin.sales-order.print', [$x->id]) }}" target="_blank"><i class="fas fa-print mr-2"></i>Print</a>
									@if ($x->status != 'cancelled')
									<a class="dropdown-item" href="{{route('livestream.cancel', [$x->id])}}"><i class="fas fa-times mr-2"></i>Cancel</a>
									@endif
								</div>
							</td>
						</tr>
						@empty
						<tr>
							<td colspan="8" class="text-center">No Avaiable Data in Table</td>
						</tr>
						@endforelse
						
					</tbody>
				</table>
				{{-- TABLE END --}}
			</div>
		</div>
	</div>
</div>
{{-- PAGINATION --}}
<div class="row mx-0 px-0">
	<div class="col-12 px-0">
		<span>Total Results {{($so->currentPage()-1) * $so->perPage() + 1}}-{{(($so->currentPage()-1) * $so->perPage()) + $so->count()}} of {{$so->total()}} <a href="{{$so->previousPageUrl()}}" class="text-dark"><i class="fas fa-chevron-left mx-2 cursor-pointer"></i></a><a href="{{$so->nextPageUrl()}}" class="text-dark"><i class="fas fa-chevron-right ml-2 cursor-pointer"></i></a></span>
	</div>
</div>
@endsection