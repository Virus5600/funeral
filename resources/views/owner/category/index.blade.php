@extends('template.admin')

@section('title', 'Products')
@section('sidebar', 'owner')

@section('body')
<h1 class="font-weight-bold">Product</h1>

<hr class="hr-thick" style="border-color: #707070;">

<div class="row mx-0 px-0">
	<form action="{{ route(Request::route()->getName()) }}" method="GET" class="col-12 col-lg-5 mx-0 px-0">
		<div class="input-group">
			<input type="text" class="form-control" placeholder="Search for..." name="s" value="{{$search}}">
			<div class="input-group-append"> 
		    	<button type="submit" data-action="none" class="btn btn-dark"><i class="fas fa-search"></i></button>
			</div>
		</div>
	</form>

	<div class="col-12 col-lg-2 mt-3 my-lg-0">
		<a class="btn btn-success w-100" href="{{ route('owner.category.create') }}"><i class="fas fa-plus-circle mr-2"></i>Add Category</a>
	</div>
</div>

<div class="row mx-0 px-0">
	<div class="col-12 px-0 overflow-x-auto">
		<div class="card border rounded dark-shadow h-100">
			<h3 class="card-header font-weight-bold text-custom">Product Category List</h3>
			<div class="card-body mx-0 pt-0 px-0">
				{{-- TABLE --}}
				<table class="table table-striped">
					<thead>
						<tr class="font-weight-bold">
							<td>Catergory Name</td>
							<td></td>
						</tr>
					</thead>

					<tbody id='table-content'>
						@forelse ($categories as $c)
						<tr>
							<td>{{$c->category_name}}</td>
							<td>
								<button type="button" class="btn btn-primary dropdown-toggle float-right py-0" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action</button>

								<div class="dropdown-menu dropdown-menu-right">
									<a class="dropdown-item" href="{{ route('owner.subcategory.index', [preg_replace('/ /', '_', strtolower($c->category_name))]) }}"><i class="far fa-eye mr-2"></i>View Subcategories</a>
									<button id="editTrigger{{$c->id}}" class="dropdown-item" data-toggle="modal" data-target="#editCategory{{$c->id}}"><i class="fas fa-pencil-alt mr-2"></i>Edit Category</button>
									<a class="dropdown-item" href="{{ route('owner.category.delete', [$c->id]) }}"><i class="fas fa-trash mr-2"></i>Delete</a>
								</div>

								<div class="modal fade" id="editCategory{{$c->id}}" tabindex="-1" role="dialog" aria-hidden="true">
									<div class="modal-dialog modal-dialog-centered" role="document">
										{{-- FORM --}}
										<form action="{{route('owner.category.update', [$c->id])}}" method="POST" class="modal-content px-3" enctype="multipart/form-data">
											{{csrf_field()}}
											<div class="modal-header border-bottom-0 mb-0 pb-0 px-0">
												<h5 class="modal-title">Add Category</h5>
												<button type="button" class="close" data-dismiss="modal" aria-label="Close">
													<span aria-hidden="true">&times;</span>
												</button>
											</div>

											<hr class="hr-thick w-100">

											<div class="modal-body border-bottom-0 py-0">
												<div class="form-group">
													<label class="form-label">Category Name</label>
													<input type='text' class='form-control' name="category_name" value="{{ $c->category_name }}">
													<span class="badge badge-danger w-100 validation-message" data-form-target="editCategory{{$c->id}}">{{$errors->first('category_name')}}</span>
												</div>
											</div>

											<div class="modal-footer border-top-0">
												<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
												<button type="submit" class="btn btn-dark" data-action="submit">Save changes</button>
											</div>
										</form>
										{{-- FORM END --}}
									</div>
								</div>
							</td>
						</tr>
						@empty
						<tr>
							<td colspan="3">No Avaiable Data in Table</td>
						</tr>
						@endforelse
					</tbody>
				</table>
				{{-- TABLE END --}}
			</div>
		</div>
	</div>
</div>

{{-- PAGINATION --}}
<div class="row mx-0 px-0">
	<div class="col-12 px-0 text-lg-left text-center">
		<span>Total Results {{($categories->currentPage()-1) * $categories->perPage() + 1}}-{{(($categories->currentPage()-1) * $categories->perPage()) + $categories->count()}} of {{$categories->total()}} <a href="{{$categories->previousPageUrl()}}" class="text-dark"><i class="fas fa-chevron-left mx-2 cursor-pointer"></i></a><a href="{{$categories->nextPageUrl()}}" class="text-dark"><i class="fas fa-chevron-right ml-2 cursor-pointer"></i></a></span>
	</div>
</div>
@endsection

@section('script')
<script type="text/javascript">
	$(document).ready(() => {

		// 

		@if (Session::get('create_error'))
		$("#addTrigger").trigger('click');
		@endif

		@if (Session::get('update_error'))
		$('[data-target="#{{Session::get('form-target')}}"]').trigger('click');
		@endif

		@if (Session::get('form-target'))
		$('.validation-message[data-form-target!={{Session::get('form-target')}}]').text('');
		@endif
	});
</script>
@endsection