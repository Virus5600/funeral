@extends('template.register')

@section('title', 'Change Password')

@section('body')
<div class="container mb-5 d-flex justify-content-center">
    <h1>Change <span class="highlighted">Password</span></h1>
</div>

<!-- Form -->
<form action="{{ route('recoverPass.updatePass') }}" method="POST" class="container-fluid" enctype="multipart/form-data">
	{{ csrf_field() }}
	<input type="hidden" name="token" value="{{ request()->token }}">

	<div class="row">
		<div class="col-12 mx-auto">
			<div class="row">
				<div class="col-lg-9 col-12 p-md-5 p-4 rounded mx-auto w-50" id="register_form">
					<p>In order to protect your account, make sure your password:</p>
					<ul>
						<li>Is longer than 7 characters.</li>
						<li>Does not match or significantly contain your username.</li>
						<li>Is not a member of the list of common passwords.</li>
					</ul>

					<div class="form-group">
						<label class="form-label font-weight-bold" for="password">Enter New Password</label>
						<input type="password" name="password" class="form-control" id="password">
						<span class="badge badge-danger w-100 validation-message">{{$errors->first('password')}}</span>
					</div>

					<div class="form-group">
						<label class="form-label font-weight-bold" for="repeat_password">Re-enter New Password</label>
						<input type="password" name="repeat_password" class="form-control" id="repeat_password">
						<span class="badge badge-danger w-100 validation-message">{{$errors->first('repeat_password')}}</span>
					</div>
				</div>
			</div>

			<div class="row mt-2">
				<div class="col-lg-9 col-12 mx-auto">
					<div class="form-group text-center">
						<input type="submit" class="btn-lg btn-light mb-2" data-action="update" value="Change Password"/>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>
@endsection