<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;

use App\CartItem;
use App\Product;
use App\SalesOrder;
use App\SalesOrderItems;
use App\SalesOrderLog;
use App\ShippingAddress;

use Auth;
use DB;
use Log;
use Session;
use Validator;

class CheckoutController extends Controller 
{
	protected function index() {
		return view('client.billing');
	}

	protected function payment(Request $request) {
		$validator = Validator::make($request->all(), [
			'pre_made_address' => 'required_without:first_name,last_name,contact_number,address,barangay,city,zip_code,region|required_unless:pre_made_address,other_address',
			'first_name' => 'required_if:pre_made_address,other_address|min:2',
			'last_name' => 'required_if:pre_made_address,other_address|min:2',
			'contact_number' => 'required_if:pre_made_address,other_address|numeric|min:10',
			'address' => 'required_if:pre_made_address,other_address|min:5',
			'barangay' => 'required_if:pre_made_address,other_address|min:3',
			'city' => 'required_if:pre_made_address,other_address',
			'zip_code' => 'numeric',
			'region' => 'required_if:pre_made_address,other_address',
		], [
			'pre_made_address.required_without' => 'Please select a pre-made address or fill up the form below.',
			'pre_made_address.required_unless' => 'Please select a pre-made address or fill up the form below.',
			'first_name.required_if' => 'First Name is required.',
			'first_name.min' => 'Please provide a proper name.',
			'last_name.required_if' => 'Last Name is required.',
			'last_name.min' => 'Please provide a proper name.',
			'contact_number.required_if' => 'Contact number is required.',
			'contact_number.numeric' => 'Contact number must only contains number.',
			'contact_number.min' => 'Contact number provided is invalid.',
			'address.required_if' => 'Address is required.',
			'address.min' => 'Please provide your proper address.',
			'barangay.required_if' => 'Barangay is required.',
			'barangay.min' => 'Please provide your proper barangay name.',
			'city.required_if' => 'City/Province is required.',
			'zip_code.numeric' => 'Zip Codes contains numbers only.',
			'region.required_if' => 'Region is required.',
		]);

		if ($validator->fails()) {
			return redirect()
				->back()
				->withInput()
				->withErrors($validator);
		}

		if ($request->pre_made_address == 'other_address') {
			try {
				DB::beginTransaction();

				$address = ShippingAddress::create([
					'first_name' => $request->first_name,
					'last_name' => $request->last_name,
					'user_id' => Auth::user()->id,
					'contact_number' => $request->contact_number,
					'address' => $request->address,
					'barangay' => $request->barangay,
					'city' => $request->city,
					'zip_code' => $request->zip_code,
					'region' => $request->region,
					'address_name' => 'Address',
					'is_default' => $request->is_default or 0
				]);

				DB::commit();
			} catch (\Exception $e) {
				Log::error($e);
				DB::rollback();

				return redirect()
					->back()
					->withInput()
					->withErrors($validator)
					->with('flash_error', 'Something went wrong, please try again later.');
			}
		}
		else {
			$address = ShippingAddress::find($request->pre_made_address);
		}

		if ($request->is_default) {
			foreach (Auth::user()->shippingAddress as $s) {
				try {
					DB::beginTransaction();
					
					if ($s->id != $address->id) {
						$s->is_default = 0;
					}
					else {
						$s->is_default = 1;
					}
					$s->save();
					
					DB::commit();
				} catch (\Exception $e) {
					DB::rollback();
					Log::error($e);
				}
			}
		}

		return redirect()
			->route('billing-payment-show', [
				'fromBuyNow' => $request->has('fromBuyNow') ? $request->get('fromBuyNow') : 0,
				'item' => $request->has('item') ? $request->get('item') : -1,
				'store' => $request->has('store') ? $request->get('store') : -1
			])
			->with('pre_made_address', $address->id)
			->with('instructions', $request->instructions)
			->with('is_valid', true);
	}

	protected function showPayment(Request $req) {
		if (Session::has('is_valid') && Session::get('is_valid'))
			return view('client.payment', [
					'fromBuyNow' => $req->has('fromBuyNow') ? $req->get('fromBuyNow') : 0,
					'item' => $req->has('item') ? $req->get('item') : -1,
					'store' => $req->has('store') ? $req->get('store') : -1
				])
				->with('pre_made_address', Session::get('pre_made_address'))
				->with('instructions', Session::get('instructions'))
				->with('is_valid', true)
				->with('flash_message', 'Please refrain from refreshing the website while on this page. You may lose your progress if you do.');
		
		return redirect()->route('home');
	}

	protected function review(Request $request) {
		$validator = Validator::make($request->all(), [
			'file' => 'file|required_if:pay_later,none|max:5120|mimes:jpeg,jpg,png',
			'pay_later' => 'required_if:file,null',
			'courier' => 'in:LBC,JT EXPRESS,LALAMOVE'
		], [
			'file.file' => 'Please upload a valid file.',
			'file.required_if' => 'Proof of payment is required if you will not pay later.',
			'file.max' => 'Maximum file size must be below 5MB.',
			'file.mimes' => 'Accepted image formats are JPG, JPEG, and PNG.',
			'pay_later.required_if' => 'Please check this checkbox if you still haven\'t paid yet.',
			'courier.in' => 'Please select one of the couriers listed in our list.'
		]);

		if ($validator->fails()) {
			return redirect()
				->back()
				->withInput()
				->withErrors($validator)
				->with('pre_made_address', $request->get('pre_made_address'))
				->with('instructions', $request->get('instructions'))
				->with('courier', $request->get('courier'))
				->with('pay_later', $request->get('pay_later'))
				->with('payment_method', $request->get('payment_method'))
				->with('filename', $request->get('add_photo'))
				->with('is_valid', true);
		}

		$add_photo="";

		if($request->file) {
			$filenameWithExt = $request->file->getClientOriginalName();
			$filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
			$extension = $request->file->getClientOriginalExtension();
			$add_photo = 'user_'.Auth::user()->id.\Carbon\Carbon::now()->timestamp.'.'.$extension;
			$path = $request->file->move(public_path().'/proof',$add_photo);
		}
		
		return redirect()
			->route('billing-review-show', [
				'fromBuyNow' => $request->has('fromBuyNow') ? $request->get('fromBuyNow') : 0,
				'item' => $request->has('item') ? $request->get('item') : -1,
				'store' => $request->has('store') ? $request->get('store') : -1
			])
			->with('pre_made_address', $request->pre_made_address)
			->with('instructions', $request->instructions)
			->with('courier', $request->courier)
			->with('pay_later', $request->pay_later)
			->with('payment_method', $request->payment_method)
			->with('filename', $add_photo)
			->with('is_valid', true);
	}

	protected function showReview(Request $req) {
		if (Session::has('is_valid') && Session::get('is_valid'))
			return view('client.review', [
				'fromBuyNow' => $req->has('fromBuyNow') ? $req->get('fromBuyNow') : 0,
				'item' => $req->has('item') ? $req->get('item') : -1,
				'store' => $req->has('store') ? $req->get('store') : -1
			])
			->with('pre_made_address', Session::get('pre_made_address'))
			->with('instructions', Session::get('instructions'))
			->with('courier', Session::get('courier'))
			->with('pay_later', Session::get('pay_later'))
			->with('payment_method', Session::get('payment_method'))
			->with('filename', Session::get('add_photo'))
			->with('is_valid', true)
			->with('flash_message', 'Please refrain from refreshing the website while on this page. You may lose your progress if you do.');
		
		return redirect()->route('home');
	}

	public function order(Request $request) {
		
		DB::beginTransaction();
		try {
			$so = SalesOrder::create([
				'shipping_addresses_id'=>$request->pre_made_address,
				'instructions'=>$request->instructions,
				'courier'=>$request->courier,
				'customer_id'=>Auth::user()->id,
				'pay_later'=>($request->pay_later=='') ? 0 : 1,
				'payment_method'=>$request->payment_method,
				'proof_of_payment'=>$request->filename,
				'status'=>$request->filename!=null?2:1,
				'store_id'=>($request->has('fromBuyNow') && $request->get('fromBuyNow')) ? $request->store : Auth::user()->getStoreOfCartItems()
			]);

			if ($request->filename)
				SalesOrderLog::create([
					'order_id' => $so->id,
					'description' => 'Submitted Proof of Payment',
					'time' => \Carbon\Carbon::now(),
					'type' => 1
				]);

			$total = 0;
			if ($request->has('fromBuyNow') && $request->get('fromBuyNow')) {
				$p = Product::find($request->item);

				SalesOrderItems::create([
					'customer_id' => Auth::user()->id,
					'order_id' => $so->id,
					'product_id' => $p->id,
					'quantity' => 1,
					'price' => $p->selling_price,
				]);
				$total = $p->selling_price;
			}
			else {
				foreach(Auth::user()->cartItem as $c) {
					SalesOrderItems::create([
						'customer_id'=>Auth::user()->id,
						'order_id'=>$so->id,
						'product_id'=>$c->product->id,
						'quantity'=>$c->quantity,
						'price'=>$c->product->selling_price,
					]);
					$total = $total + ($c->quantity * $c->product->selling_price);
				}

				CartItem::where('user_id', Auth::user()->id)->delete();
			}

			$so->total = $total;
			$so->save();

		   DB::commit();
		   return redirect('/')
		   	->with('flash_success', 'Sucessfully placed order.');
		}
		catch(\Exception $e)
		{
			DB::rollback();
			Log::alert($e);
			abort(500);
		}
		catch(\Throwable $e)
		{
			DB::rollback();
			Log::alert($e);
			abort(500);
		}
	}

	protected function livestreamBilling() {
		return view('client.livestream-billing.index');
	}

	protected function livestreamBillingDone(Request $req) {
		foreach (Auth::user()->livestreamCart() as $l) {
			$ls = UserController::storeLivestream($l);

			if ($ls['status'] == 0)
				return redirect()
					->back()
					->withInput()
					->with('flash_error', 'Something went wrong, please try again later.');
		}

		if (count(Auth::user()->cartItem) > 0)
			return redirect()
				->route('billing')
				->with('flash_success', 'Successfully checked out livestream services.');

		return redirect()
				->route('home')
				->with('flash_success', 'Successfully checked out livestream services.');
	}
}