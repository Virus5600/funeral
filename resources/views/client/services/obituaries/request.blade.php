@extends('template.client')

@section('title', 'Services - Obituary')

@section('page-css')
<link rel="stylesheet" href="/css/services.obituaries.show.css" >
@endsection

@section('body')
{{-- OBITUARIES BANNER --}}
<section class="p-md-5" id="banner">
	<div class="container pt-md-5" >
		<div class="d-sm-flex justify-content-between">
			<div class ="col-xl-11 col-md-8 col-sm-12 p-5" id="bannertext">
				<h1>Obituaries celebrate and honor unique lives.</h1>
			</div>
		</div>
	</div>
</section>

<!-- Obituary -->
<section class="py-5" id="obituary">
	<div class="container">
		<h3 class="pb-0">Remember your loved one.</h3>
		<h5 class="pt-0">Submit an obituary now.</h5>

		<div class="row">
			<form class="form" action="{{ route('services.obituary.request.store') }}" method="POST" enctype="multipart/form-data">
				{{ csrf_field() }}
				<div class="row">
					<div class="col-12 col-lg-8">
						<div class="row">
							<div class="form-group col-12">
								<label class="form-label font-weight-bold">Name</label>
								<input type="text" class="form-control" name="name" value="{{old('name')}}">
								<span class="badge badge-danger w-100 validation-message">{{ $errors->first('name') }}</span>
							</div>
						</div>

						<div class="row">
							<div class="form-group col-12 col-lg-6">
								<label class="form-label font-weight-bold">Date of Birth</label>
								<input type="date" class="form-control" name="birth_date" value="{{old('birth_date')}}">
								<span class="badge badge-danger w-100 validation-message">{{ $errors->first('birth_date') }}</span>
							</div>

							<div class="form-group col-12 col-lg-6">
								<label class="form-label font-weight-bold">Date of Death</label>
								<input type="date" class="form-control" name="death_date" value="{{old('death_date')}}">
								<span class="badge badge-danger w-100 validation-message">{{ $errors->first('death_date') }}</span>
							</div>
						</div>
					</div>

					<div class="form-group col-12 col-lg-4">
						<div class="d-flex flex-row">
							<div class="mx-auto text-center" style="overflow: hidden; max-width: 37.5%;">
								<img src="{{ asset('uploads/obituary/default.png') }}" class="cursor-pointer img-fluid" id="image" alt="Image">
							</div>

							<div class="mx-auto">
								<h6 class="form-label font-weight-bold" style="word-break: break-word" id="image_label">UPLOAD IMAGE</h6>
								<input type="file" name='image' class="hidden" accept=".jpg,.jpeg,.png" id="imgInput">
								<small class="text-muted pb-0 mb-0"><b>FORMATS ALLOWED:</b> JPEG, JPG, PNG</small><br>
								<small class="text-muted pt-0 mt-0"><b>MAX SIZE:</b> 5MB</small>
							</div>
						</div>
						<span class="badge badge-danger w-100 validation-message">{{ $errors->first('image') }}</span>
					</div>
				</div>

				<div class="row">
					<div class="form-group col-12">
						<label class="form-label font-weight-bold">In care of</label>
						<select class="custom-select" name="store">
							<option class="text-dark">-- Select a Store Care taker --</option>
							@foreach ($store as $s)
							<option class="text-dark" value="{{ $s->id }}" {{ old('store') == $s->id ? 'selected' : '' }}>{{ $s->name }}</option>
							@endforeach
						</select>
						<span class="badge badge-danger w-100 validation-message">{{ $errors->first('store') }}</span>
					</div>
				</div>

				<div class="row">
					<div class="form-group col-12">
						<label class="form-label font-weight-bold">Description</label>
						<textarea class="form-control not-resizable" name="description" rows="5">{{old('description')}}</textarea>
						<span class="badge badge-danger w-100 validation-message">{{ $errors->first('description') }}</span>
					</div>
				</div>

				<label class="form-label font-weight-bold">Date of Posted Obituary</label>
				<div class="row">
					<div class="form-group col-12 col-lg-6 d-flex flex-d-col">
						<div class="d-flex flex-d-row w-100 align-items-center">
							<div class="input-group">
								<div class="input-group-prepend">
									<label class="form-label input-group-text font-weight-bold pr-3 my-0">From:</label>
								</div>
								<input type="date" class="form-control" name="obituary_duration_start" value="{{old('obituary_duration_start')}}">
							</div>
						</div>
						<span class="badge badge-danger w-100 validation-message">{{ $errors->first('obituary_duration_start') }}</span>
					</div>

					<div class="form-group col-12 col-lg-6 d-flex flex-d-col">
						<div class="d-flex flex-d-row w-100 align-items-center">
							<div class="input-group">
								<div class="input-group-prepend">
									<label class="form-label input-group-text font-weight-bold pr-3 my-0">To:</label>
								</div>
								<input type="date" class="form-control" name="obituary_duration_end" value="{{old('obituary_duration_end')}}">
							</div>
						</div>
						<span class="badge badge-danger w-100 validation-message">{{ $errors->first('obituary_duration_end') }}</span>
					</div>
				</div>

				<hr class="hr-thick w-100 border-white">

				<h3>Memorial Service Details</h3>

				<div class="row">
					<div class="form-group col-12 col-lg-6">
						<label class="form-label font-weight-bold">Start Time</label>
						<input type="time" class="form-control" name="begin_time" value="{{old('begin_time')}}">
						<span class="badge badge-danger w-100 validation-message">{{ $errors->first('begin_time') }}</span>
					</div>
				</div>

				<div class="row">
					<div class="form-group col-12 col-lg-6">
						<label class="form-label font-weight-bold">Location</label>
						<input type="text" class="form-control" name="location" value="{{old('location')}}">
					</div>

					<div class="form-group col-12 col-lg-6">
						<label class="form-label font-weight-bold">Address</label>
						<input type="text" class="form-control" name="address" value="{{old('address')}}">
					</div>
				</div>

				<div class="row">
					<div class="col-12">
						<button type="submit" class="btn btn-light mr-2" data-action="submit">Request</button>
						<a href="{{ route('services.obituary.index') }}" class="btn btn-secondary ml-2">Go Back</a>
					</div>
				</div>
			</form>
		</div>
	</div>
</section>
@endsection

@section('script')
<script type="text/javascript">
	function openInput(obj) {
		$("[name=" + obj.attr("id") + "]").trigger("click");
	}

	function swapImgFile(obj) {
		if (obj.files && obj.files[0]) {
			let reader = new FileReader();

			reader.onload = function(e) {
				$("#image").attr("src", e.target.result);
				$("#image_label").html(obj.files[0].name);
			}

			reader.readAsDataURL(obj.files[0])
		}
		else {
			$("#image").attr("src", "/uploads/obituary/default.png");
			$("#image_label").html("UPLOAD IMAGE");
		}
	}

	$(document).ready(() => {
		// Triggering click on the image box
		$("#image").on("click", (e) => {
			openInput($(e.currentTarget));
		});
		// Applying changes when file input changes
		$("#imgInput").on('change', (e) => {
			swapImgFile(e.currentTarget);
		});
	});
</script>
@endsection