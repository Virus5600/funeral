<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Obituary extends Model
{
	protected $fillable = [
		'customer_id',
		'name',
		'birth_date',
		'death_date',
		'store_id',
		'description',
		'available_from',
		'available_to',
		'begin_time',
		'location',
		'address',
		'image_name',
		'status'
	];

	public $timestamps = false;

	protected function user() { return $this->belongsTo('App\User'); }
	protected function store() { return $this->belongsTo('App\Store'); }
}