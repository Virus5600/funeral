<?php

use Illuminate\Database\Seeder;

use App\ShippingAddress;

class ShippingAddressTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		ShippingAddress::create([
			'user_id' => 3,
			'first_name' => 'Client',
			'last_name' => 'クライエント',
			'contact_number' => '1234567890',
			'address' => 'Paseo Montereal, El Monteverde Subdivision',
			'barangay' => 'San Juan',
			'city' => 'Rizal',
			'region' => 'Region IV-A',
			'address_name' => 'Address 1'
		]);
	}
}