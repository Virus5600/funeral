@extends('template.admin')

@section('title', 'Dashboard')
@section('sidebar', 'owner')

@section('body')
<h1 class="h3 h1-lg font-weight-bold text-truncate">Contact</h1>

<hr class="hr-thick" style="border-color: #707070;">

<form action="{{ route('owner.contact.update') }}" method="POST" enctype="multipart/form-data">
	{{ csrf_field() }}

	<h4 class="h4 text-truncate">Primary Contact Details</h4>
	<div class="row">
		<div class="col-12 col-lg-5">
			<div class="form-group">
				<label class="form-label" for="email">Email Address:</label>
				<input type="email" name="email" id="email" class="form-control" value="{{ $store->email }}"/>
				<span class="badge badge-danger w-100 validation-message">{{$errors->first('email')}}</span>
			</div>

			<div class="form-group">
				<label class="form-label" for="contact_number">Phone Number:</label>
				<input type="text" name="contact_number" id="contact_number" class="form-control" data-mask data-mask-format="+63 999 999 9999" value="{{ $store->contact_number }}"/>
				<span class="badge badge-danger w-100 validation-message">{{$errors->first('contact_number')}}</span>
			</div>
		</div>
	</div>

	<h4 class="h4 text-truncate">Social Media Accounts</h4>
	<div class="row">
		<div class="col-12 col-lg-5">
			<div class="form-group">
				<label class="form-label" for="facebook">Facebook:</label>
				<input type="text" name="facebook" id="facebook" class="form-control" value="{{ $store->facebook }}"/>
				<span class="badge badge-danger w-100 validation-message">{{$errors->first('facebook')}}</span>
			</div>

			<div class="form-group">
				<label class="form-label" for="twitter">Twitter:</label>
				<input type="text" name="twitter" id="twitter" class="form-control" value="{{ $store->twitter }}"/>
				<span class="badge badge-danger w-100 validation-message">{{$errors->first('twitter')}}</span>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-12">
			<button type="submit" class="btn btn-dark" data-action="submit">Save Changes</button>
		</div>
	</div>
</form>
@endsection