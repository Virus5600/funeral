@extends('template.admin')

@section('title', 'User Management')
@section('sidebar', 'admin')

@section('body')
<h1 class="h3 h1-lg font-weight-bold text-truncate">User Management</h1>

<hr class="hr-thick border-color-custom">

<div class="row mx-0 px-0">
	<form action="{{ route(Request::route()->getName()) }}" method="GET" class="col-12 col-md-6 col-xl-5 mx-0 px-0">
		<div class="input-group">
			<input type="text" class="form-control" placeholder="Search for..." name="s" value="{{$search}}">
			<div class="input-group-append">
				<button type="submit" data-action="none" class="btn btn-dark"><i class="fas fa-search"></i></button>
			</div>
		</div>
	</form>

	<div class="col-12 col-md-6 col-xl-2 text-center">
		<a href="{{route('admin.user-management.create')}}" class="btn btn-success my-2 my-lg-0"><i class="fas fa-plus-circle mr-2"></i>Add User</a>
	</div>
</div>

<div class="row mx-0 px-0">
	<div class="col-12 px-0">
		<div class="card border rounded dark-shadow h-100">
			<h3 class="card-header font-weight-bold text-custom">Users</h3>
			<div class="card-body mx-0 pt-0 px-0 overflow-auto">
				{{-- TABLE --}}
				<table class="table table-striped">
					<thead>
						<tr class="font-weight-bold">
							<td>Name</td>
							<td>Email</td>
							<td>Contact Number</td>
							<td></td>
						</tr>
					</thead>

					<tbody id='table-content'>
						@forelse ($users as $u)
						<tr>
							<td>{{ $u->first_name . ' ' . $u->last_name}}</td>
							<td>{{ $u->email }}</td>
							<td>{!!$u->contact_number == null ? '<span class="badge badge-danger">No contact provided.</span>' : '+63' . $u->contact_number!!}</td>
							<td>
								<button type="button" class="btn btn-primary dropdown-toggle py-0 float-right" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action</button>

								<div class="dropdown-menu dropdown-menu-right">
									<a class="dropdown-item" href="{{ route('admin.user-management.show', [$u->id]) }}"><i class="far fa-eye mr-2"></i>View Details</a>
									<a class="dropdown-item" href="{{ route('admin.user-management.edit', [$u->id]) }}"><i class="fas fa-pencil-alt mr-2"></i>Edit Details</a>
									<a class="dropdown-item" href="{{ route('admin.user-management.delete', [$u->id]) }}"><i class="fas fa-trash mr-2"></i>Delete</a>
								</div>
							</td>
						</tr>
						@empty
						<tr>
							<td colspan="3">No Avaiable Data in Table</td>
						</tr>
						@endforelse
					</tbody>
				</table>
				{{-- TABLE END --}}
			</div>
		</div>
	</div>
</div>
{{-- PAGINATION --}}
<div class="row mx-0 px-0">
	<div class="col-12 px-0">
		<span>Total Results {{($users->currentPage()-1) * $users->perPage() + 1}}-{{(($users->currentPage()-1) * $users->perPage()) + $users->count()}} of {{$users->total()}} <a href="{{$users->previousPageUrl()}}" class="text-dark"><i class="fas fa-chevron-left mx-2 cursor-pointer"></i></a><a href="{{$users->nextPageUrl()}}" class="text-dark"><i class="fas fa-chevron-right ml-2 cursor-pointer"></i></a></span>
	</div>
</div>
@endsection