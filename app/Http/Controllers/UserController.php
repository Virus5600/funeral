<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Requests;
use Carbon\Carbon;

use App\EmailVerification;
use App\Livestream;
use App\LivestreamLog;
use App\PasswordReset;
use App\SalesOrder;
use App\User;

use DB;
use Exception;
use Hash;
use Log;
use Mail;
use Session;
use Validator;

class UserController extends Controller
{
	protected function login() {
		if (!Auth::check())
			return view('login');
		else
			return redirect()->back();
	}

	protected function register() {
		if (!Auth::check())
			return view('register');
		else
			return redirect()->back();
	}

	protected function recoverPassword() {
		if (!Auth::check())
			return view('recover_pass');
		else
			return redirect()->back();
	}

	protected function recoverPasswordSubmit(Request $req) {
		if (!Auth::check()) {
			$validator = Validator::make($req->all(), [
				'email' => 'required|email|min:5|exists:users,email'
			], [
				'email.required' => 'Please provide the email address of the lost account',
				'email.email' => 'Please provide a proper email address',
				'email.min' => 'Please provide a proper email address',
				'email.exists' => 'Email does not exists in our records'
			]);

			if ($validator->fails())
				return redirect()
					->back()
					->withErrors($validator)
					->withInput();

			try {
				DB::beginTransaction();
				
				$user = User::where('email', '=', $req->email)->first();
				$token = Str::random(60);

				if (PasswordReset::where('email', '=', $req->email)->first() != null) {
					$token = PasswordReset::where('email', '=', $req->email)->first()->token;
				}
				else {
					PasswordReset::insert([
						'email' => $req->email,
						'token' => $token,
						'created_at' => Carbon::now()
					]);
				}

				Mail::send(
					'template.email.recovery',
					['token' => $token, 'user' => $user],
					function ($m) use ($user) {
						$m->from('no-reply@soulace.com');
						$m->to($user->email, $user->getName())
							->subject('Recover Password');
					}
				);

				DB::commit();
			} catch (Exception $e) {
				DB::rollback();
				Log::error($e);

				return redirect()
					->back()
					->with('flash_error', 'Something went wrong, please try again later')
					->with('flash_error_actual', $e);
			}
		}
		else
			return redirect()
				->route('home')
				->with('flash_info', 'You are already logged in');

		return redirect()
			->route('home')
			->with('flash_success', 'A reset link has been sent to your email address');
	}

	protected function recoveryChangePassword() {
		if (!request()->has('token'))
			return redirect()->route('home');

		if (PasswordReset::where('token', '=', request()->token)->first() == null)
			return redirect()
				->route('home')
				->with('flash_message', 'Invalid token');

		if (!Auth::check())
			return view('change_pass', [
				'token' => request()->token
			]);
		else
			return redirect()->route('home');
	}

	protected function recoveryUpdatePassword(Request $req) {
		$validator = Validator::make($req->all(), [
			'password' => array('required','min:8','regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[\d\x])(?=.*[!@#$%^&*()-=_+\\|;:\'",.\/?]).*$/'),
			'repeat_password' => 'required|same:password'
		], [
			'password.required' => 'Password is required.',
			'password.min' => 'Password should be minimum of 8 characters, with at least one capitalized letter, a number and a special character.',
			'password.regex' => 'Password should be minimum of 8 characters, with at least one capitalized letter, a number and a special character.',
			'repeat_password.required' => 'Please repeat your password.',
			'repeat_password.same' => 'Repeated password should be the same as the password.'
		]);

		if ($validator->fails())
			return redirect()
				->back()
				->withInput()
				->withErrors($validator);

		try {
			DB::beginTransaction();

			$pr = PasswordReset::where('token', '=', $req->token)->first();
			$user = User::where('email', '=', $pr->email)->first();

			PasswordReset::where('token', '=', $req->token)->delete();
			$user->password = Hash::make($req->password);
			$user->save();

			DB::commit();
		} catch (Exception $e) {
			DB::rollback();
			Log::error($e);

			return redirect()
				->back()
				->withInput()
				->with('flash_error', 'Something went wrong, please try again later')
				->with('flash_error_actual', $e);
		}

		return redirect()
			->route('login')
			->with('flash_success', 'Successfully updated your password');
	}

	protected function authenticate() {
		$credentials = [
			'email' => Input::get('email'),
			'password' => Input::get('password')
		];

		if (Auth::attempt($credentials, (Input::get('remember_me') != null ? true : false))) {
			if (Auth::user()->is_validated) {
				return redirect()
					->intended('/');
			}
			else {
				return redirect()
					->route('email-verification.form');
			}
		}
		else {
			auth()->logout();
			return redirect()
				->back()
				->with('flash_error', 'Wrong email/password!')
				->withInput(Input::all());
		}
	}

	protected function logout() {
		if (Auth::check()) {
			auth()->logout();
			return redirect('/')->with('flash_success', 'Logged out!');
		}
		return redirect('/')->with('flash_error', 'Something went wrong, please try again.');
	}

	protected function welcome() {
		if (Session::has('isFirst') && Session::get('isFirst') && Auth::check())
			return view('client.welcome');

		return redirect()
			->route('home');
	}

	protected function index() {
		return view('client.account.index');
	}

	protected function store(Request $req) {
		$validator = Validator::make($req->all(), [
			'first_name' => 'required|min:2',
			'last_name' => 'required|min:2',
			'contact_number' => 'required|numeric|min:10',
			'birthday' => 'required|date',
			'gender' => array('regex:/(^NULL$|^Male$|^Female$|^Others$)/'),
			'email' => 'required|email|unique:users,email',
			// Matches if the password contains atleast 8 characters, has lowercase & uppercase letters, has number, and has special characters (!@#$%^&*()-=_+\|;':",./?').
			'password' => array('required','min:8','regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[\d\x])(?=.*[!@#$%^&*()-=_+\\|;:\'",.\/?]).*$/'),
			'repeat_password' => 'required|same:password',
			'tos' => 'required',
		], [
			'first_name.required' => 'First Name is required.',
			'first_name.min' => 'Please provide a proper name.',
			'last_name.required' => 'Last Name is required.',
			'last_name.min' => 'Please provide a proper name.',
			'contact_number.required' => 'Contact number is required',
			'contact_number.numeric' => 'Contact number must only contains number.',
			'contact_number.min' => 'Contact number provided is invalid.',
			'birthday.required' => 'Birth date is required.',
			'birthday.date' => 'Birth date provided must be a date.',
			'gender.regex' => 'Please refrain from modifying the page.',
			'email.required' => 'E-mail is required.',
			'email.email' => 'E-mail provided is not a valid e-mail.',
			'password.required' => 'Password is required.',
			'password.min' => 'Password should be minimum of 8 characters, with at least one capitalized letter, a number and a special character.',
			'password.regex' => 'Password should be minimum of 8 characters, with at least one capitalized letter, a number and a special character.',
			'repeat_password.required' => 'Please repeat your password.',
			'repeat_password.same' => 'Repeated password should be the same as the password.',
			'tos.required' => 'You must accept our <a href="" class="text-white">Terms and Services</a>, and <a href="" class="text-white">Privacy Policy</a> if you wish to create an account.'
		]);

		if ($validator->fails()) {
			return redirect()
				->back()
				->withErrors($validator)
				->withInput();
		}

		try {
			DB::beginTransaction();
			
			$user = User::create([
				'first_name' => $req->first_name,
				'last_name' => $req->last_name,
				'contact_number' => $req->contact_number,
				'birthday' => $req->birthday,
				'gender' => $req->gender,
				'email' => $req->email,
				'password' => Hash::make($req->password),
				'subscriber' => $req->subscribe ? 1 : 0,
			]);

			// Create the verification code
			$ev = EmailVerification::create([
				'user_id' => $user->id,
				'verification_code' => Str::random(15),
				'expiry_time' => Carbon::now()->addMinutes(5)
			]);

			Mail::send(
				'template.email.verification',
				['code' => $ev->verification_code, 'user' => $user],
				function ($m) use ($user) {
					$m->from('no-reply@soulace.com');
					$m->to($user->email, $user->getName())
						->subject('Verify Your Email');
				}
			);

			DB::commit();
			
			Auth::attempt(['email' => $req->email, 'password' => $req->password]);
		} catch (\Exception $e) {
			Log::error($e);
			DB::rollback();
			dd($e);

			return redirect()
				->back()
				->withInput()
				->with('flash_error', 'Something went wrong, please try again later.');
		}

		return redirect()
			->route('email-verification.form');
	}

	protected function edit() {
		return view('client.account.edit');
	}

	protected function update(Request $req) {
		$validator = Validator::make($req->all(), [
			'first_name' => 'required|min:2',
			'last_name' => 'required|min:2',
			'contact_number' => 'required|numeric|min:10',
			'birthday' => 'required|date',
			'gender' => array('regex:/(^NULL$|^Male$|^Female$|^Others$)/'),
		], [
			'first_name.required' => 'First Name is required.',
			'first_name.min' => 'Please provide a proper name.',
			'last_name.required' => 'Last Name is required.',
			'last_name.min' => 'Please provide a proper name.',
			'contact_number.required' => 'Contact number is required',
			'contact_number.numeric' => 'Contact number must only contains number.',
			'contact_number.min' => 'Contact number provided is invalid.',
			'birthday.required' => 'Birth date is required.',
			'birthday.date' => 'Birth date provided must be a date.',
			'gender.regex' => 'Please refrain from modifying the page.',
		]);

		if ($validator->fails())
			return redirect()
				->back()
				->withErrors($validator);

		try {
			DB::beginTransaction();

			$user = User::find(Auth::user()->id);
			$user->first_name = $req->first_name;
			$user->last_name = $req->last_name;
			$user->contact_number = $req->contact_number;
			$user->birthday = $req->birthday;
			$user->gender = $req->gender;
			$user->save();

			DB::commit();
			// FIX VALIDATION ERROR
		} catch (\Exception $e) {
			Log::error($e);
			DB::rollback();

			return redirect()
				->back()
				->withInput()
				->with('flash_error', 'Something went wrong, please try again later.');
		}

		return redirect()
			->back()
			->with('flash_success', 'Successfully updated profile details.');
	}

	protected function orderTrackingIndex() {
		return view('client.account.orders_and_tracking.index');
	}

	protected function orderTrackingShow($id) {
		return view('client.account.orders_and_tracking.show', [
			'salesOrder' => SalesOrder::find($id)
		]);
	}

	protected function livestreamTrackingIndex() {
		return view('client.account.livestream.index');
	}

	protected function livestreamTrackingShow($id) {
		return view('client.account.livestream.show', [
			'livestream' => Livestream::find($id)
		]);
	}

	protected function uploadProof($id) {
		$l = Livestream::find($id);

		if ($l->displayStatus(true) == 1)
			return view('client.account.livestream.upload-proof', [
				'livestream' => $l
			]);
		else
			return redirect()->back();
	}

	protected function postUploadProof(Request $req, $id) {
		$validator = Validator::make($req->all(), [
			'proof' => 'required|mimes:jpg,jpeg,png|max:5120'
		], [
			'proof.required' => 'The proof of payment is required.',
			'proof.mimes' => 'The accepted formats are JPG, JPEG, and PNG.',
			'proof.max' => 'The maximum allowed file size is 5MB.'
		]);

		if ($validator->fails()) {
			return redirect()
				->back()
				->withErrors($validator);
		}

		if ($req->proof) {
			$filenameWithExt = $req->proof->getClientOriginalName();
			$filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
			$extension = $req->proof->getClientOriginalExtension();
			$add_photo = 'user_'.Auth::user()->id.\Carbon\Carbon::now()->timestamp.'.'.$extension;
			$path = $req->proof->move(public_path().'/proof/livestreams',$add_photo);
		}

		try {
			DB::beginTransaction();

			$l = Livestream::find($id);
			$l->proof_of_payment = $add_photo;
			$l->status = Livestream::getStatusList()[2];
			$l->save();

			LivestreamLog::create([
				'livestream_id' => $l->id,
				'description' => 'Submitted Proof of Payment',
				'time' => \Carbon\Carbon::now(),
				'type' => 1,
			]);

			DB::commit();
		} catch (\Exception $e) {
			Log::error($e);
			DB::rollback();

			return redirect()
				->route('account.livestreams')
				->with('flash_error', 'Something went wrong, please try again later.');
		}

		return redirect()
			->route('account.livestreams')
			->with('flash_success', 'Proof of Payment uploaded successfully.');
	}

	protected function addLivestreamToCart(Request $req) {
		$validator = Validator::make($req->all(), [
			'title' => 'max:255',
			'description' => 'max:1000',
			'category' => array('regex:'.Livestream::getCategoryList(true)),
			'date' => 'required|date_format:Y-m-d',
			'time' => 'required|date_format:H:i',
			'duration' => 'required|numeric',
			'price' => 'required|numeric'
		], [
			'date.date_format' => 'Please use this format: yyyy-mm-dd',
			'time.date_format' => 'Please follow this format: hh:mm'
		]);

		if ($validator->fails()) {
			return redirect()
				->back()
				->withErrors($validator)
				->withInput();
		}

		try {
			DB::beginTransaction();

			$ls = Livestream::create([
				'title' => $req->title,
				'description' => $req->description,
				'schedule_date' => $req->date,
				'schedule_time' => $req->time,
				'duration' => $req->duration,
				'price' => $req->price,
				'user_id' => Auth::user()->id,
				'category' => $req->category
			]);

			DB::commit();
		} catch (\Exception $e) {
			Log::error($e);
			DB::rollback();

			return redirect()
				->back()
				->withInput()
				->with('flash_error', 'Something went wrong, please try again later.');
		}

		return redirect()
			->back()
			->with('flash_success', 'Successfully added livestream services to cart.');
	}

	public static function storeLivestream(Livestream $ls) {
		try {
			DB::beginTransaction();

			$mux = new MuxController();
			$result = $mux->createNewLivestream();
			$resultGet = $mux->getLiveStream($result['data']['id']);

			$ls->stream_id = $result['data']['id'];
			$ls->stream_key = $result['data']['stream_key'];
			$ls->stream_link = 'https://stream.mux.com/'.$resultGet['data']['playback_ids'][0]['id'].'.m3u8';
			$ls->status = strtolower(Livestream::getStatusList()[1]);
			$ls->save();

			DB::commit();
		} catch (Exception $e) {
			Log::error($e);
			DB::rollback();

			return [
				'status' => 0,
				'message' => 'Something went wrong, please try again later.'
			];
		}
		return [
			'status' => 1
		];
	}

	protected function deleteLivestream($id) {
		$livestream = Livestream::find($id);

		if ($livestream == null) {
			return redirect()
			->back()
			->with('flash_info', 'Livestream already removed from cart. Please refresh page if the item still shows up.');
		}

		try {
			DB::beginTransaction();
			$livestream->delete();
			DB::commit();
		} catch (Exception $e) {
			DB::rollback();
			Log::error($e);

			return redirect()
				->back()
				->with('flash_error', 'Something went wrong, please try again later.');
		}

		return redirect()
			->back()
			->with('flash_success', 'Successfully removed livestream from cart');
	}

	protected function cancelLivestream($id) {
		$livestream = Livestream::find($id);

		try {
			DB::beginTransaction();

			$livestream->status = 'cancelled';
			$livestream->save();
			
			DB::commit();
		} catch (Exception $e) {
			Log::error($e);
			DB::rollback();

			return redirect()
				->back()
				->with('flash_error', 'Something went wrong, please try again later.');
		}

		return redirect()
			->back()
			->with('flash_success', 'Successfully cancelled livestream service');
	}
}