<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Requests;

use App\Category;
use App\Subcategory;
use App\Product;

use Validator;
use Log;
use DB;

class OwnerProductController extends Controller
{
	protected function index($category_name, $subcategory_name) {
		$products = Product::where('subcategory_id', '=', Subcategory::where('subcategory_name', '=', preg_replace('/_/', ' ', $subcategory_name))->first()->id);
		$search = null;

		if (request()->has('s')) {
			$search = request()->get('s');
			$products = $products->where('description', 'LIKE', '%'.$search.'%')
				->orWhere('product_name', 'LIKE', '%'.$search.'%')
				->distinct();
		}

		return view('owner.category.subcategory.product.index', [
			'search' => $search,
			'category_name' => $category_name,
			'subcategory_name' => $subcategory_name,
			'products' => $products->paginate(15)
		]);
	}

	protected function create($category_name, $subcategory_name) {
		return view('owner.category.subcategory.product.create', [
			'isFrom' => 'product',
			'categories' => Category::where('category_name', '=', preg_replace('/_/', ' ', $category_name))->first(),
			'subcategories' => Subcategory::where('subcategory_name', '=', preg_replace('/_/', ' ', $subcategory_name))->first()
		]);
	}

	protected function store(Request $req) {
		$req->status = $req->status ? 1 : 0;

		$validator = Validator::make($req->all(), [
			'product_name' => 'required|min:3',
			'selling_price' => 'required|numeric|min:0',
			'image' => 'file|mimes:jpeg,jpg,png|max:5120',
			'category' => 'required',
			'subcategory' => 'required'
		], [
			'product_name.required' => 'Product name is required.',
			'product_name.min' => 'Product name is too short.',
			'selling_price.required' => 'Product price is required.',
			'selling_price.numeric' => 'Product price must only composed of numbers.',
			'selling_price.min' => 'Minimum price should be ₱0.00.',
			'image.file' => 'Image must be a valid file.',
			'image.mimes' => 'Image formats allowed are only JPEG, JPG, and PNG.',
			'image.max' => 'Maximum image file size should be 5MB.',
			'category.required' => 'Product category is required.',
			'subcategory.required' => 'Product subcategory is required.'
		]);

		if ($validator->fails()) {
			return redirect()
				->back()
				->withErrors($validator)
				->withInput();
		}

		$added = '';
		$categoryAdded = false;
		$subcategoryAdded = false;
		try {
			DB::beginTransaction();

			// Category Handling
			if (Category::where('category_name', '=', $req->category)->first() == null) {
				$category = Category::create(['category_name' => $req->category]);
				
				$categoryAdded = true;
				$added  .= 'category';
			}
			else
				$category = Category::where('category_name', '=', $req->category)->first();
			// Subcategory Handling
			if (Subcategory::where('subcategory_name', '=', $req->subcategory)->first() == null) {
				$subcategory = Subcategory::create([
					'subcategory_name' => $req->subcategory,
					'category_id' => $category->id
				]);

				$subcategoryAdded = true;
				$added .= 'subcategory';
				if ($categoryAdded)
					$added .= ', subcategory';
			}
			else {
				$subcategory = Subcategory::where('subcategory_name', '=', $req->subcategory)->first();
			}

			// File Handling
			$file = null;
			if ($req->image != null) {
				$ext = $req->image->getClientOriginalExtension();
				$file = 'product_'.uniqid().'.'.$ext;
				$destinationPath = 'uploads/products/'.$category->category_name;
				$req->image->move($destinationPath, $file);
			}
			Product::create([
				'subcategory_id' => $subcategory->id,
				'store_id' => Auth::user()->store_id,
				'product_name' => $req->product_name,
				'selling_price' => $req->selling_price,
				'status' => $req->status == 1 ? 1 : 0,
				'inventory' => $req->inventory,
				'size' => $req->size,
				'description' => $req->description,
				'image_name' => $file
			]);

			if ($categoryAdded || $subcategoryAdded)
				$added .= ' and product';
			else
				$added .= 'product';

			DB::commit();
		} catch (\Exception $e) {
			Log::error($e);
			DB::rollback();

			return redirect()
				->back()
				->withInput()
				->with('flash_error', 'Something went wrong, please try again later.');
		}

		return redirect()
			->route('owner.category.index')
			->with('flash_success', 'Successfully added ' . $added . '.');
	}

	protected function show($category_name, $subcategory_name, $product_name) {
		$product = Product::where('product_name', '=', preg_replace('/_/', ' ', $product_name))
			->where('store_id', '=', Auth::user()->store_id)
			->first();

		$category = Category::where('category_name', '=', preg_replace('/_/', ' ', $category_name))->first();
		$subcategory = Subcategory::where('subcategory_name', '=', preg_replace('/_/', ' ', $subcategory_name))->first();

		return view('owner.category.subcategory.product.show', [
            'isShow' => true,
			'category' => $category,
			'subcategory' => $subcategory,
			'product' => $product
		]);
	}

    protected function edit($category_name, $subcategory_name, $product_name) {
    	$product = Product::where('product_name', '=', preg_replace('/_/', ' ', $product_name))
			->where('store_id', '=', Auth::user()->store_id)
			->first();

		$category = Category::where('category_name', '=', preg_replace('/_/', ' ', $category_name))->first();
		$subcategory = Subcategory::where('subcategory_name', '=', preg_replace('/_/', ' ', $subcategory_name))->first();

        return view('owner.category.subcategory.product.show', [
            'isShow' => false,
            'category' => $category,
            'subcategory' => $subcategory,
            'product' => $product,
            'categories' => Category::get(),
            'subcategories' => $product->category->subcategory
        ]);
    }

    protected function update(Request $req) {
    	$req->status = $req->status ? 1 : 0;

		$validator = Validator::make($req->all(), [
			'product_name' => 'required|min:3',
			'selling_price' => 'required|numeric|min:0',
			'image' => 'file|mimes:jpeg,jpg,png|max:5120',
			'category' => 'required',
			'subcategory' => 'required'
		], [
			'product_name.required' => 'Product name is required.',
			'product_name.min' => 'Product name is too short.',
			'selling_price.required' => 'Product price is required.',
			'selling_price.numeric' => 'Product price must only composed of numbers.',
			'selling_price.min' => 'Minimum price should be ₱0.00.',
			'image.file' => 'Image must be a valid file.',
			'image.mimes' => 'Image formats allowed are only JPEG, JPG, and PNG.',
			'image.max' => 'Maximum image file size should be 5MB.',
			'category.required' => 'Product category is required.',
			'subcategory.required' => 'Product subcategory is required.'
		]);

		if ($validator->fails()) {
			return redirect()
				->back()
				->withErrors($validator)
				->withInput();
		}

		$added = '';
		$categoryAdded = false;
		$subcategoryAdded = false;
		try {
			DB::beginTransaction();

			// Category Handling
			if (Category::where('category_name', '=', $req->category)->first() == null) {
				$category = Category::create(['category_name' => $req->category]);
				
				$categoryAdded = true;
				$added  .= 'category';
			}
			else {
				$category = Category::where('category_name', '=', $req->category)->first();
			}
			
			// Subcategory Handling
			if (Subcategory::where('subcategory_name', '=', $req->subcategory)->first() == null) {
				$subcategory = Subcategory::create([
					'subcategory_name' => $req->subcategory,
					'category_id' => $category->id
				]);

				$subcategoryAdded = true;
				$added .= 'subcategory';
				if ($categoryAdded)
					$added .= ', subcategory';
			}
			else {
				$subcategory = Subcategory::where('subcategory_name', '=', $req->subcategory)->first();
			}

			$p = Product::find($req->id);
			// File Handling
			$file = $p->image_name;
			if ($req->image != null) {
				$ext = $req->image->getClientOriginalExtension();
				$file = 'product_'.uniqid().'.'.$ext;
				$destinationPath = 'uploads/products/'.$category->category_name;
				$req->image->move($destinationPath, $file);
			}

			$p->product_name = $req->product_name;
			$p->inventory = $req->inventory;
			$p->selling_price = $req->selling_price;
			$p->status = $req->status;
			$p->subcategory_id = $subcategory->id;
			$p->size = $req->size;
			$p->description = $req->description;
			$p->image_name = $file;
			$p->save();

			if ($categoryAdded || $subcategoryAdded)
				$added .= ' and product';
			else
				$added .= 'product';

			DB::commit();
		} catch (\Exception $e) {
			Log::error($e);
			DB::rollback();

			return redirect()
				->back()
				->withInput()
				->with('flash_error', 'Something went wrong, please try again later.');
		}

		return redirect()
			->route('owner.category.index')
			->with('flash_success', 'Successfully updated ' . $added . '.');
    }

	protected function delete($id) {
		try {
			DB::beginTransaction();

			$p = Product::find($id);

			if ($p == null)
				return redirect()
				->back()
				->with('flash_error', 'Product not available anymore.');
			else
				$p->delete();

			DB::commit();
		} catch (\Exception $e) {
			DB::rollback();
			Log::error($e);

			return redirect()
				->back()
				->with('flash_error', 'Something went wrong, please try again later.');
		}

		return redirect()
			->back()
			->with('flash_success', 'Successfully removed product from list.');
	}
}