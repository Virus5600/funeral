@extends('template.admin')

@section('title', 'Sales Order')
@section('sidebar', 'owner')

@section('body')
<h1 class="font-weight-bold"><a class="text-dark" href="{{route('owner.sales-order.index')}}"><i class="fas fa-chevron-left mr-2"></i>Order Details {{$sales_order->id}}</a></h1>

<hr class="hr-thick" style="border-color: #707070;">

<div class="row mx-0 px-0">
	<div class="col-12 mx-0 px-0">
		<div class="card border rounded dark-shadow px-0 overflow-x-auto">
			<div class="card-body px-0">
				@if (\App\SalesOrder::getStatus($sales_order->status) != 'Cancelled')
				{{-- STEP BAR START --}}
				<div class="step-progress w-75 mx-auto mb-3" data-progress='{{$sales_order->status-1}}'>
					<div class="component">
						{{-- BAR --}}
						<div class="bar">
							<hr class="bar-progress">
							<hr class="bar-remaining">
						</div>

						{{-- STEPS --}}
						<div class="steps">
							@foreach ($steps as $s)
							<div class="step-icon">
								<i class="fas fa-circle fa-2x h-100"></i>
							</div>
							@endforeach
						</div>
					</div>

					<div class="labels">
						@foreach ($steps as $s)
						<div class="step-label">
							{{$s}}
						</div>
						@endforeach
					</div>
				</div>
				{{-- STEP BAR END --}}
				@else
				<div class="w-75 mx-auto mb-3 d-flex flex-d-col align-items-center">
					<div class="step-icon cancelled">
						<i class="fas fa-circle fa-2x h-100"></i>
					</div>
					<div class="step-label">
						Cancelled
					</div>
				</div>
				@endif

				{{-- TABLE START --}}
				<table class="table mt-5">
					<thead class="text-white text-center" style="background-color: rgba(112, 112, 112, 0.75);">
						<tr>
							<td>Image</td>
							<td>Name</td>
							<td>Price</td>
							<td>QTY</td>
							<td>Total Amount</td>
						</tr>
					</thead>

					<tbody class="text-center">
						@foreach ($sales_order->items as $i)
						<tr>
							<td>
								<img class="img-fluid border" src='/uploads/products/{{$i->product->category->category_name}}/{{$i->product->image_name}}' alt="{{$i->product->product_name}}" style="max-width: 25%;">
							</td>
							<td>{{$i->product->product_name}}</td>
							<td>₱{{number_format($i->product->selling_price, 2)}}</td>
							<td>{{$i->quantity}}</td>
							<td>₱{{number_format($i->price, 2)}}</td>
						</tr>
						@endforeach

						<tr>
							<td colspan="3" rowspan="4"></td>
							<td>Subtotal:</td>
							<td>₱{{number_format($sales_order->total, 2)}}</td>
						</tr>

						<tr>
							<td>Shipping:</td>
							<td>₱0.00</td>
						</tr>

						<tr>
							<td>Tax:</td>
							<td>₱0.00</td>
						</tr>

						<tr>
							<td colspan="2" class="font-weight-bold">₱{{number_format($sales_order->total, 2)}}</td>
						</tr>
					</tbody>
				</table>
				{{-- TABLE END --}}
			</div>
		</div>
	</div>
</div>

<div class="row-spacing-3">
	<div class="row mx-0 px-0">
		<div class="col-12 col-lg-6">
			<div class="card h-100">
				<div class="card-body d-flex flex-d-col">
					<span class="font-weight-bold">Customer Details</span>
					<span>{{$sales_order->customer->getName()}}</span>
					<span>(+63){{$sales_order->customer->contact_number}}</span>
					<span>{{$sales_order->customer->email}}</span>
					<br>
					<span class="font-weight-bold">Shipping Details</span>
					<span>{{$sales_order->shippingAddress->getAddress(true, true)}}</span>
					<span>{{$sales_order->shippingAddress->city}}, {{$sales_order->shippingAddress->region}}</span>
				</div>
			</div>
		</div>

		<div class="col-12 col-lg-6">
			<div class="card h-100">
				<div class="card-body d-flex flex-d-col">
					<span class="font-weight-bold">Billing Details</span>
					<span>{{$sales_order->customer->getName()}}</span>
					<span>{{$sales_order->shippingAddress->getAddress(true, true)}}</span>
					<span>{{$sales_order->shippingAddress->city}}, {{$sales_order->shippingAddress->region}}</span>
				</div>
			</div>
		</div>
	</div>

	<div class="row mx-0 px-0">
		<div class="col-12 col-lg-6">
			<div class="card h-100">
				<div class="card-body d-flex flex-d-col">
					<span class="font-weight-bold">Payment Method</span>
					<span>{{ucwords($sales_order->payment_method)}}</span>
				</div>
			</div>
		</div>

		<div class="col-12 col-lg-6">
			<div class="card h-100">
				<div class="card-body d-flex flex-d-col">
					<span class="font-weight-bold">Shipping Method</span>
					<span>{{$sales_order->courier}}</span>
				</div>
			</div>
		</div>
	</div>
</div>

<hr class="hr-thick">
<h5>Proof of Payment</h5>
<div class="row">
	@foreach ($sales_order->logs->reverse() as $l)
		@if ($l->type == 1 || $l->type == 2)
		<div class="col-12">
			<div class="card">
				<div class="card-body">
					<div class="row">
						<div class="col-9">
							<p class="mb-2"><i class="fas fa-info-circle mr-2"></i><b>{{\Carbon\Carbon::parse($l->create_at)->format('M d, Y')}}:</b> {{$l->getDescription()}}</p>
						</div>
						<div class="col-3 text-right">
							@if ($sales_order->status == 2 && $l->description == 'Submitted Proof of Payment' && $l->id == \App\SalesOrderLog::where('order_id', '=', $sales_order->id)->orderBy('id', 'DESC')->first()->id)
								<a href="{{ route('owner.sales-order.proof.accept', [$sales_order->id]) }}" class="btn btn-success" title="Accept"><i class="fas fa-check-circle fa-lg"></i></a>
								<a href="{{ route('owner.sales-order.proof.reject', [$sales_order->id]) }}" class="btn btn-danger" title="Reject"><i class="fas fa-times-circle fa-lg"></i></a>
							@else
							<span class="badge {{ $l->description == 'Submitted Proof of Payment' ? 'badge-info' : ($l->isRejected() ? 'badge-danger' : ($l->isAccepted() ? 'badge-success' : 'badge-info'))}}">
								@if ($l->description == 'Submitted Proof of Payment')
								Submitted
								@elseif ($l->isRejected())
								Rejected
								@else
								Accepted
								@endif
							</span>
							@endif
						</div>
					</div>

					<div class="row">
						<div class="col-12 col-md-3">
							@if ($l->isRejected() || $l->isAccepted())
							<button class="btn btn-dark" data-toggle="modal" data-target="#proof_modal{{$l->id}}">Show Proof</button>

							<div class="modal fade" id="proof_modal{{$l->id}}" aria-hidden="true">
								<div class="modal-dialog modal-dialog-centered modal-lg">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>

										<div class="modal-body d-flex flex-d-row">
											<img src="/proof/{{$l->getProofImage()}}" alt="{{ $l->getProofImage() }}" class="img-fluid w-100 mx-auto">
										</div>
									</div>
								</div>
							</div>
							@elseif ($sales_order->logs[count($sales_order->logs)-1]->id == $l->id)
							<img src="/proof/{{$sales_order->proof_of_payment}}" alt="{{ $sales_order->proof_of_payment }} thumbnail" class="img-fluid img-thumbnail cursor-pointer" data-toggle="modal" data-target="#proof_modal">

							<div class="modal fade" id="proof_modal" aria-hidden="true">
								<div class="modal-dialog modal-dialog-centered modal-lg">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>

										<div class="modal-body d-flex flex-d-row">
											<img src="/proof/{{$sales_order->proof_of_payment}}" alt="{{ $sales_order->proof_of_payment }}" class="img-fluid w-100 mx-auto">
										</div>
									</div>
								</div>
							</div>
							@endif
						</div>
					</div>
				</div>
			</div>
		</div>
		@endif
	@endforeach
</div>

@if ($sales_order->status == 3)
<hr class="hr-thick">
<h5>Shipment</h5>
<div class="row">
	<div class="col">
		<div class="card">
			<div class="card-body">
				<h5>Shipping to {{$sales_order->courier}}...</h5>
				<form class="form" action="{{route('owner.sales-order.tracking_number.add', [$sales_order->id])}}" method="POST" enctype="multipart/form-data">
					{{csrf_field()}}
					<div class="form-group">
						<label class="form-label" for="tracking_number">Tracking Number:</label>
						<input type="text" name="tracking_number" id="tracking_number" class="form-control">
						<span class="badge badge-danger w-100 validation-message">{{$errors->first('tracking_number')}}</span>
					</div>

					<input type="submit" data-action="submit" class="btn btn-success">
				</form>
			</div>
		</div>
	</div>
</div>
@endif

@if ($sales_order->status == 4)
<hr class="hr-thick">
<h5>Shipment</h5>
<div class="row">
	<div class="col">
		<div class="card">
			<div class="card-body">
				<h5>{{$sales_order->courier}} Shipping Details</h5>
				<form class="form" action="{{route('owner.sales-order.tracking_number.update', [$sales_order->id])}}" method="POST" enctype="multipart/form-data" id="tracking_number_form">
					{{csrf_field()}}
					<div class="form-group">
						<label class="form-label" for="tracking_number">Tracking Number:</label>
						<input type="text" name="tracking_number" id="tracking_number" value="{{$sales_order->tracking_number}}" class="form-control" disabled>
						<span class="badge badge-danger w-100 validation-message">{{$errors->first('tracking_number')}}</span>
					</div>

					<div class="button-group">
						<button type="button" data-type="edit" class="btn btn-dark"><i class="fas fa-pencil-alt mr-2"></i>Edit Tracking Number</button>
						<button type="submit" data-type="update" data-action="update" class="btn btn-success hidden"><i class="fas fa-save mr-2"></i>Update</button>
						<button type="button" data-type="cancel" class="btn btn-dark hidden"><i class="fas fa-ban mr-2"></i>Cancel</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endif
@endsection

@section('script')
<script type="text/javascript">
	let formerVal = '';

	$(document).ready(() => {
		$('#tracking_number_form .button-group > button').on('click', (e) => {
			let obj = $(e.currentTarget);
			let parent = obj.parent();
			let type = obj.attr('data-type');

			if (type == 'edit') {
				formerVal = $('#tracking_number').val();
				parent.find('button').not('[data-type=edit]').removeClass('hidden');
				obj.addClass('hidden');
				$('#tracking_number').prop('disabled', false);
			}
			else if (type == 'update') {
				parent.find('button').prop('disabled', true);
				parent.parent().submit();
			}
			else if (type == 'cancel') {
				parent.find('button').not('[data-type=edit]').addClass('hidden');
				parent.find('[data-type=edit]').removeClass('hidden');
				$('#tracking_number').val(formerVal);
				$('#tracking_number').prop('disabled', true);
			}
		});
	});
</script>
@endsection