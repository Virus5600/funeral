<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use DB;

class ShippingAddress extends Model
{
	use SoftDeletes;
	
	protected $fillable = [
		'first_name',
		'last_name',
		'user_id',
		'contact_number',
		'address',
		'barangay',
		'city',
		'zip_code',
		'region',
		'address_name',
		'is_default'
	];

	protected function user() {
		return $this->belongsTo('App\User');
	}

	public function getName() {
		return $this->first_name . ' ' . $this->last_name;
	}

	public function getAddress($omitCity=false, $omitRegion=false) {
		$address = $this->address;
		if ($this->barangay != null)
			$address .= ', ' . $this->barangay;
		if ($this->city != null && !$omitCity)
			$address .= ', ' . $this->city;
		if ($this->region != null && !$omitRegion)
			$address .= ', ' . $this->region; 
		if ($this->zip_code != null)
			$address .= ', ' . $this->zip_code;

		return $address;
	}

	public static function getRegions() {
		$type = DB::select(DB::raw('SHOW COLUMNS FROM users WHERE Field = "region"'))[0]->Type;
		preg_match('/^enum\((.*)\)$/', $type, $matches);
		$values = array();
		foreach(explode(',', $matches[1]) as $value){
			$values[] = trim($value, "'");
		}
		return $values;
	}

	public static function getCities($region='NCR') {
		$cities = array(
			'NCR' => array('Caloocan', 'Las Piñas', 'Makati', 'Malabon', 'Mandaluyong', 'Manila', 'Marikina', 'Muntinlupa', 'Navotas', 'Parañaque', 'Pasay', 'Pasig', 'Pateros', 'Quezon City', 'San Juan', 'Taguig', 'Valenzuela'),
			'CAR' => array('Abra', 'Apayao', 'Baguio', 'Benguet', 'Ifugao', 'Kalinga', 'Mountain Province'),
			'Region I' => array('Dagupan', 'Ilocos Norte', 'Ilocos Sur', 'La Union', 'Pangasinan'),
			'Region II' => array('Batanes', 'Cagayan', 'Isabela', 'Nueva Vizcaya', 'Quirino', 'Santiago'),
			'Region III' => array('Angeles City', 'Aurora', 'Bataan', 'Bulacan', 'Nueva Ecija', 'Olongapo', 'Pampanga', 'Tarlac', 'Zambales'),
			'Region IV-A' => array('Batangas', 'Cavite', 'Laguna', 'Lucena', 'Quezon', 'Rizal'),
			'Region IV-B' => array('Marinduque', 'Occidental Mindoro', 'Oriental Mindoro', 'Palawan', 'Puerto Princesa', 'Romblon'),
			'Region V' => array('Albay', 'Camarines Norte', 'Camarines Sur', 'Catanduanes', 'Masbate', 'Naga', 'Sorsogon'),
			'Region VI' => array('Albay', 'Camarines Norte', 'Camarines Sur', 'Catanduanes', 'Masbate', 'Naga', 'Sorsogon'),
			'Region VII' => array('Bohol', 'Cebu', 'Cebu City', 'Lapu-Lapu', 'Mandaue', 'Negros Oriental', 'Siquijor'),
			'Region VIII' => array('Biliran', 'Eastern Samar', 'Leyte', 'Northern Samar', 'Ormoc', 'Samar', 'Southern Leyte', 'Tacloban'),
			'Region IX' => array('Isabela City', 'Zamboanga City', 'Zamboanga del Norte', 'Zamboanga del Sur', 'Zamboanga Sibugay'),
			'Region X' => array('Bukidnon', 'Cagayan de Oro', 'Camiguin', 'Iligan', 'Lanao del Norte', 'Misamis Occidental', 'Misamis Oriental'),
			'Region XI' => array('Davao City', 'Davao de Oro', 'Davao del Norte', 'Davao del Sur', 'Davao Occidental', 'Davao Oriental'),
			'Region XII' => array('Cotabato', 'General Santos', 'Sarangani', 'South Cotabato', 'Sultan Kudarat'),
			'Region XIII' => array('Agusan del Norte', 'Agusan del Sur', 'Butuan', 'Dinagat Islands', 'Surigao del Norte', 'Surigao del Sur'),
			'BARMM' => array('Basilan', 'Cotabato City', 'Lanao del Sur', 'Maguindanao', 'Sulu', 'Tawi-Tawi', '63 barangays in Cotabato')
		);

		return $cities[$region];
	}
}