/**
 * Warns the user that they're leaving without saving their changes.
 * @param urlTo String value. The page they're attempting to open.
 */
function confirmLeave(urlTo) {
	Swal.fire({
		icon: 'warning',
		html: '<h4>Are you sure?</h4><p>You have unsave changes.</p>',
		showDenyButton: true,
		confirmButtonText: 'Yes',
		denyButtonText: 'No'
	}).then((result) => {
		if (result.isConfirmed) {
			window.location.href = urlTo;
		}
	});
}

/**
 * Reinstate a new image when the intended image cannot be loaded or found. Maximum of 10 tries or depending how long the array provided is.
 * @param image HTML Element. The image element.
 * @param newSrc String value. The path towards the new image that will be used
 * @param isArray Boolean value, "false" by default. Identify if the value provided to "newSrc" is an array or not.
 */
function imgError(image, newSrc, isArray=false) {
	if (typeof $(image).attr('data-loop') == 'undefined')
		$(image).attr('data-loop', 0);

	if (isArray)
		if ($(image).attr('data-loop') == newSrc.length)
			image.onerror = null;
	else
		if ($(image).attr('data-loop') == '10')
			image.onerror = null;

	if (isArray)
		image.src = newSrc[parseInt($(image).attr('data-loop'))];
	else
		image.src = newSrc;
	
	$(image).attr('data-loop', parseInt($(image).attr('data-loop'))+1);

	return true;
}

$(document).ready(() => {
	// Change submit to either "Updating" or "Submitting" after click
	$('[type=submit], [data-action]').click(function(e) {
		let action = $(e.currentTarget).attr('data-action');
		let spinnerColor = typeof $(e.currentTarget).attr('data-spinner-color') == 'undefined' ? true : $(e.currentTarget).attr('data-spinner-color');

		if ($(e.currentTarget).attr('data-clicked') == 'true') {
			e.preventDefault()
		}
		else {
			if (action == 'submit')
				$(e.currentTarget).html(`<div class="spinner-border spinner-border-sm ` + (spinnerColor == true ? 'text-light' : '') + `" role="status" ` + (spinnerColor == true ? '' : 'style="color: ' + spinnerColor + ';"') + `><span class="sr-only"></span></div> Submitting...`);
			else if (action == 'update')
				$(e.currentTarget).html(`<div class="spinner-border spinner-border-sm ` + (spinnerColor == true ? 'text-light' : '') + `" role="status" ` + (spinnerColor == true ? '' : 'style="color: ' + spinnerColor + ';"') + `><span class="sr-only"></span></div> Updating...`);
			else if (action == 'nothing')
				return false;
			else
				$(e.currentTarget).html(`<div class="spinner-border spinner-border-sm ` + (spinnerColor == true ? 'text-light' : '') + `" role="status" ` + (spinnerColor == true ? '' : 'style="color: ' + spinnerColor + ';"') + `><span class="sr-only"></span></div> ` + $(e.currentTarget).attr('data-action-message') + `...`);
		}

		$(e.currentTarget).addClass(`disabled cursor-default`);
	});

	// Activates Tooltip
	$('[data-toggle=tooltip]').tooltip();

	// Activate masking
	$(document).on('ready load click focus', "[data-mask]", (e) => {
		let obj = $(e.currentTarget);
		if (!obj.attr('data-masked')) {
			obj.inputmask('mask', {
				'mask' : obj.attr('data-mask-format'),
				'removeMaskOnSubmit' : true,
				'autoUnmask':true
			});

			obj.attr('data-masked', 'true');
		}
	});

	// Toggle changes navbar
	$("#togglemenu").on('click', (e) => {
		$("#navbarmain").addClass('bg-dark shadow');
	});	  

	// Scroll changes navbar
	$(window).on('scroll load resize', (e) => {
		if ($(window).width() > 992) {
			if (e.currentTarget.pageYOffset > 100)
				$('#navbarmain').addClass('bg-dark shadow');
			else
				$('#navbarmain').removeClass('bg-dark shadow');
		}
		else
			if (!$('#navbarmain').hasClass('bg-dark shadow'))
				$('#navbarmain').addClass('bg-dark shadow');
	});

	// Opens Services nav-item when hovered
	$("#serviceBtn").parent().on('mouseover', (e) => {
		$(e.currentTarget).children('a').first().dropdown('show');
	}).on('mouseleave', (e) => {
		$(e.currentTarget).children('a').first().dropdown('hide');
	});
});