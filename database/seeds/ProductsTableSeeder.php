<?php

use Illuminate\Database\Seeder;

use App\Product;

class ProductsTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		// Casket
		Product::create([
			'subcategory_id' => 1,
			'store_id'  => 1,
			'product_name'  => 'Baby Bear Infant Casket - Pink',
			'selling_price'  => 49550.00,
			'inventory' => rand(0,1000),
			'status'  => 1,
			'description'  => null,
			'size'  => null,
			'image_name'  => 'casket_1.jpg'
		]);

		Product::create([
			'subcategory_id' => 1,
			'store_id'  => 1,
			'product_name'  => 'Baby Bear Infant Casket - Blue',
			'selling_price'  => 49550.00,
			'inventory' => rand(0,1000),
			'status'  => 1,
			'description'  => null,
			'size'  => null,
			'image_name'  => 'casket_2.jpg'
		]);

		Product::create([
			'subcategory_id' => 1,
			'store_id'  => 1,
			'product_name'  => 'Arisius Infant Casket',
			'selling_price'  => 149160.00,
			'inventory' => rand(0,1000),
			'status'  => 1,
			'description'  => null,
			'size'  => null,
			'image_name'  => 'casket_3.png'
		]);

		Product::create([
			'subcategory_id' => 1,
			'store_id'  => 1,
			'product_name'  => 'Auber Child Casket',
			'selling_price'  => 149160.00,
			'inventory' => rand(0,1000),
			'status'  => 1,
			'description'  => null,
			'size'  => null,
			'image_name'  => 'casket_4.jpg'
		]);

		Product::create([
			'subcategory_id' => 1,
			'store_id'  => 1,
			'product_name'  => 'Grecian Urn - Half Lid MDF Casket - White',
			'selling_price'  => 190070.00,
			'inventory' => rand(0,1000),
			'status'  => 1,
			'description'  => null,
			'size'  => null,
			'image_name'  => 'casket_5.jpg'
		]);

		Product::create([
			'subcategory_id' => 1,
			'store_id'  => 1,
			'product_name'  => 'Grecian Urn - Half Lid MDF Casket - Rosewood',
			'selling_price'  => 190070.00,
			'inventory' => rand(0,1000),
			'status'  => 1,
			'description'  => null,
			'size'  => null,
			'image_name'  => 'casket_6.jpg'
		]);

		// Coffin
		Product::create([
			'subcategory_id' => 7,
			'store_id'  => 1,
			'product_name'  => 'Serenity Flat Lid Coffin - Brown',
			'selling_price'  => 49550.00,
			'inventory' => rand(0,1000),
			'status'  => 1,
			'description'  => null,
			'size'  => null,
			'image_name'  => 'coffin_1.jpg'
		]);

		Product::create([
			'subcategory_id' => 7,
			'store_id'  => 1,
			'product_name'  => 'Serenity Flat Lid Coffin - White',
			'selling_price'  => 49550.00,
			'inventory' => rand(0,1000),
			'status'  => 1,
			'description'  => null,
			'size'  => null,
			'image_name'  => 'coffin_2.jpg'
		]);

		Product::create([
			'subcategory_id' => 7,
			'store_id'  => 1,
			'product_name'  => 'The Light Wicker',
			'selling_price'  => 74450.00,
			'inventory' => rand(0,1000),
			'status'  => 1,
			'description'  => null,
			'size'  => null,
			'image_name'  => 'coffin_3.png'
		]);

		Product::create([
			'subcategory_id' => 7,
			'store_id'  => 1,
			'product_name'  => 'The Standard Wicker',
			'selling_price'  => 74450.00,
			'inventory' => rand(0,1000),
			'status'  => 1,
			'description'  => null,
			'size'  => null,
			'image_name'  => 'coffin_4.jpg'
		]);

		Product::create([
			'subcategory_id' => 7,
			'store_id'  => 1,
			'product_name'  => 'Freedom Raised Lid Coffin - White',
			'selling_price'  => 99400.00,
			'inventory' => rand(0,1000),
			'status'  => 1,
			'description'  => null,
			'size'  => null,
			'image_name'  => 'coffin_5.jpg'
		]);

		Product::create([
			'subcategory_id' => 7,
			'store_id'  => 1,
			'product_name'  => 'Freedom Raised Lid Coffin - Brown',
			'selling_price'  => 99400.00,
			'inventory' => rand(0,1000),
			'status'  => 1,
			'description'  => null,
			'size'  => null,
			'image_name'  => 'coffin_6.jpg'
		]);

		// Urn
		Product::create([
			'subcategory_id' => 10,
			'store_id'  => 1,
			'product_name'  => 'Blooming Fields Ceramic Urn',
			'selling_price'  => 24600.00,
			'inventory' => rand(0,1000),
			'status'  => 1,
			'description'  => null,
			'size'  => null,
			'image_name'  => 'urn_1.jpg'
		]);

		Product::create([
			'subcategory_id' => 10,
			'store_id'  => 1,
			'product_name'  => 'Majestic White Ceramic Urn',
			'selling_price'  => 24600.00,
			'inventory' => rand(0,1000),
			'status'  => 1,
			'description'  => null,
			'size'  => null,
			'image_name'  => 'urn_2.jpg'
		]);

		Product::create([
			'subcategory_id' => 10,
			'store_id'  => 1,
			'product_name'  => 'Lavander Love Ceramic Urn',
			'selling_price'  => 74450.00,
			'inventory' => rand(0,1000),
			'status'  => 1,
			'description'  => null,
			'size'  => null,
			'image_name'  => 'urn_3.jpg'
		]);

		Product::create([
			'subcategory_id' => 10,
			'store_id'  => 1,
			'product_name'  => 'Maybelle MDF Urns',
			'selling_price'  => 149160.00,
			'inventory' => rand(0,1000),
			'status'  => 1,
			'description'  => null,
			'size'  => null,
			'image_name'  => 'urn_4.jpg'
		]);

		Product::create([
			'subcategory_id' => 10,
			'store_id'  => 1,
			'product_name'  => 'Glouchester MDF Urn',
			'selling_price'  => 190070.00,
			'inventory' => rand(0,1000),
			'status'  => 1,
			'description'  => null,
			'size'  => null,
			'image_name'  => 'urn_5.jpg'
		]);

		Product::create([
			'subcategory_id' => 10,
			'store_id'  => 1,
			'product_name'  => 'Adalberto Wood Urn',
			'selling_price'  => 190070.00,
			'inventory' => rand(0,1000),
			'status'  => 1,
			'description'  => null,
			'size'  => null,
			'image_name'  => 'urn_6.jpg'
		]);

		// Flowers
		Product::create([
			'subcategory_id' => 14,
			'store_id'  => 1,
			'product_name'  => 'Lavander And White Funeral Standing Spray',
			'selling_price'  => 24600.00,
			'inventory' => rand(0,1000),
			'status'  => 1,
			'description'  => null,
			'size'  => null,
			'image_name'  => 'flower_1.jpg'
		]);

		Product::create([
			'subcategory_id' => 14,
			'store_id'  => 1,
			'product_name'  => 'White Floor Basket Arrangement',
			'selling_price'  => 24600.00,
			'inventory' => rand(0,1000),
			'status'  => 1,
			'description'  => null,
			'size'  => null,
			'image_name'  => 'flower_2.jpg'
		]);

		Product::create([
			'subcategory_id' => 14,
			'store_id'  => 1,
			'product_name'  => 'Blue And White Funeral Standing Basket',
			'selling_price'  => 74450.00,
			'inventory' => rand(0,1000),
			'status'  => 1,
			'description'  => null,
			'size'  => null,
			'image_name'  => 'flower_3.jpg'
		]);

		Product::create([
			'subcategory_id' => 14,
			'store_id'  => 1,
			'product_name'  => 'White Funeral Standing Basket',
			'selling_price'  => 149160.00,
			'inventory' => rand(0,1000),
			'status'  => 1,
			'description'  => null,
			'size'  => null,
			'image_name'  => 'flower_4.jpg'
		]);

		Product::create([
			'subcategory_id' => 14,
			'store_id'  => 1,
			'product_name'  => 'Classic All White Arrangement',
			'selling_price'  => 190070.00,
			'inventory' => rand(0,1000),
			'status'  => 1,
			'description'  => null,
			'size'  => null,
			'image_name'  => 'flower_5.jpg'
		]);

		Product::create([
			'subcategory_id' => 14,
			'store_id'  => 1,
			'product_name'  => 'Floral Embrace',
			'selling_price'  => 190070.00,
			'inventory' => rand(0,1000),
			'status'  => 1,
			'description'  => null,
			'size'  => null,
			'image_name'  => 'flower_6.jpg'
		]);
	}
}