<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
	protected $fillable = [
		'store_id',
		'address',
		'latitude',
		'longitude'
	];

	public $timestamps = false;

	protected function store() {
		return $this->belongsTo('App\Store');
	}
}