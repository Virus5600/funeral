@extends('template.client')

@section('title', 'Home')

@section('page-css')
<link rel="stylesheet" href="/css/home.css" >
@endsection

@section('css')
<style type="text/css">
	.child-text-gray * {
		color: #707070;
	}

	.child-br-0 * {
		border-radius: 0;
	}
</style>
@endsection

@section('body')
{{-- HOME BANNER --}}
<section class="p-5 d-flex flex-row" id="banner">
	@include('template.social-share')

	<div class="container pt-md-5 mt-5" >
		<div class="d-sm-flex justify-content-between">
			<div class="col-xl-11 col-md-8 col-sm-12 p-sm-5" id="bannertext">
				<h1> Funerals<br> and other Services </h1>
				<p class="lead my-4">
					We make everything easy for your needs.
				</p>
			</div>
		</div>
	</div>
</section>

{{-- SERVICES --}}
<section class="p-5" id="services">
	<div class="container">
		<h1 class="text-center pb-5">
			<img src="/images/UI/line.svg" class="imgline mr-2">We Offer the Best Services
		</h1>

		<div class="row">
			<div class="col-lg-3 col-md-6 mt-3 mt-md-5 mx-auto ms-md-auto me-md-0">
				<div class="services card img-fluid" >
					<img class="card-img-top" src="images/banners/home/services/service2.png" alt="Card image">
					<div class="card-img-overlay">
						<h4 class="card-title mt-5">Personalised Funeral Services</h4>
						<a href="/services#funeral" class="btn btn-outline-light">Learn More</a>
					</div>
				</div>
			</div>
			
			<div class="col-lg-3 col-md-6 mt-3 mx-auto mx-lg-0">
				<div class="services card img-fluid">
					<img class="card-img-top" src="images/banners/home/services/service3.png" alt="Card image">
					<div class="card-img-overlay">
						<h4 class="card-title mt-5 ">Memorial Services</h4>
						<a href="/services#memorial" class="btn btn-outline-light">Learn More</a>
					</div>
				</div>
			</div>

			<div class="col-lg-3 col-md-6 mt-3 mt-md-5 mx-auto ms-md-0 me-md-auto">
				<div class="services card img-fluid">
					<img class="card-img-top" src="images/banners/home/services/service4.png" alt="Card image">
					<div class="card-img-overlay">
						<h4 class="card-title mt-5">Burial Services</h4>
						<a href="/services#burial" class="btn btn-outline-light">Learn More</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
	
{{-- GOING ONLINE --}}
<section class="p-5" id="onlineserv">
	<div class="container align-items-center justify-content-between mt-5">
		<div class="row align-items-center" id="goingonline">
			<div class="col-md-4">
				<h1>
					<img src="/images/UI/line.svg" class="imgline mr-2">We are going online!
				</h1>
				<p class="lead">
					Soulace now offers online funeral services.
				</p>
			</div>
			<div class="col-md-8">
				<img src="/images/banners/home/onlineimg.svg" class="img-fluid" alt=""/>
			</div>
		</div>
	</div>
</section>

{{-- FEEDBACKS --}}
<section class="py-5 text-center justify-content-between" id="opinion">
	<h1 class="my-5">
		<img src="/images/UI/line.svg" class="imgline mr-2">What People Say About Us
	</h1>
	
	<div class="container">
		<div id="carouselContent" class="carousel slide" data-ride="carousel">
			<div class="carousel-inner" role="listbox">
				<div class="carousel-item active p-4">
					<div class="col-md-7" id="item">
						<h2>
							"Soulace services are good and
							efficient. They provide the best care
							they can possibly offer for their clients. "
						</h2>
						
						<h3 class="mt-5">-John</h3>
					</div>
				</div>   
			</div>
			
			<a class="carousel-control-prev" href="#carouselContent" role="button" data-slide="prev">
				<span class="carousel-control-prev-icon" aria-hidden="true"></span>
			</a>

			<a class="carousel-control-next" href="#carouselContent" role="button" data-slide="next">
				<span class="carousel-control-next-icon" aria-hidden="true"></span>
			</a>
		</div>
	</div>
</section>

{{-- OBITUARY --}}
<section class="py-5" id="obituary">
	<div class="container">
		<div class="row align-items-center">
			<h1 class="mt-5">
				<img src="/images/UI/line.svg" class="imgline mr-2">Obituary
			</h1>
			<h5>Let's pray for their departed souls.</h5>
		</div>

		<div class="container py-5">
			@forelse($obituaries as $o)
			<a href="{{ route('services.obituary.show', [$o->id]) }}" class="obituarycard card mb-3 text-decoration-none hover-enlarge">
				<div class="row">
					<div class="fillimg col-md-3">
						<img src="uploads/obituary/{{$o->image_name or 'default.png'}}" onerror="imgError(this, '{{asset('/uploads/obituary/default.png')}}');" class="card-img" alt="...">
					</div>

					<div class="col-md-7 mt-1">
						<div class="card-body">
							<h1 class="obname card-title">{{$o->name}}</h1>
							<h2 class="card-text"><small class="text-muted">{{\Carbon\Carbon::parse($o->birth_date)->format('F d, Y')}} – {{\Carbon\Carbon::parse($o->death_date)->format('F d, Y')}}</small></h2>
						</div>
					</div>

					<div class="col-md-2">
						<img src="images/UI/bookmark.svg" class="mark card-img" alt="...">
					</div>
				</div>
			</a>
			@empty
			<h1>No Obituaries</h1>
			@endforelse
		</div>
	</div>
</section>

{{-- LET'S TALK --}}
<section class="py-5">
	<div class="container align-items-center py-5">
		{{-- FORM --}}
		<form class="row" action="{{ route('mail.inquiry.send') }}" method="POST" enctype="multipart/form-data" id="inquiry_form">
			{{ csrf_field() }}

			<div class="col-lg-7" id="inputtalk">
				<div class="my-2">
					<h1>Lets Talk</h1>

					<h4>
						Have an inquiry or some feedback for us?<br>
						Fill out the form below to contact us.
					</h4>
				</div>

				<div class="form-group row mt-3">
					<div class="col-12">
						<input type="text" class="form-control" placeholder="Full Name" name="name">
						<span class="badge badge-danger w-100 validation-message">{{$errors->first('name')}}</span>
					</div>
				</div>

				<div class="form-group mt-3" id="emailform">
					<input type="email" class="form-control" placeholder="Email Address" name="email">
					<span class="badge badge-danger w-100 validation-message">{{$errors->first('email')}}</span>
				</div>
			</div>
			
			<div class="col-lg-5 mt-1">
				<div class="form-group" id="messagearea">
					<textarea class="form-control mt-2 not-resizable" id="message" rows="8" placeholder="Enter your Message Here" name="message"></textarea>
					<span class="badge badge-danger w-100 validation-message">{{$errors->first('message')}}</span>
				</div>
				
				<div class="text-center text-lg-right">
					<button class="btn btn-lg btn-light mt-2 px-3 py-1" type="submit" data-action="submit">Submit</button>
				</div>
			</div>
		</form>
		{{-- FORM END --}}
	</div>
</section>

<section class="py-5 text-center">
	<div class="col-lg-12">
		<h1 class="pb-5">
			<img src="/images/UI/line.svg" class="imgline mr-2">Branches Near You
		</h1>

		<div id="map"></div>
	</div>
</section>
@endsection

@section('script')
<script src="https://maps.googleapis.com/maps/api/js?key={{env('MAP_API_KEY')}}&callback=initMap&libraries=&v=weekly" async></script>
<script type="text/javascript">
	function initMap() {
		// Map drawing
		const locations = [
			@forelse ($locations as $l)
			{
				lat: {{$l->latitude}},
				lng: {{$l->longitude}}
			},
			@empty
			{
				lat: 0,
				lng: 0
			}
			@endforelse
		];

		const map = new google.maps.Map($('#map').get()[0], {
			zoom: 17,
			center: locations[0],
		});

		const infoWin = [
			@forelse($locations as $l)
			new google.maps.InfoWindow({content: `<div class="text-left"><h3 class="text-dark">{!! $l->store->name !!}</h3><p class="text-dark font-weight-normal">{!!$l->address!!}</p></div>`}),
			@empty
			new google.maps.InfoWindow({content: `<h3>---</h3><p>------------------------------</p>`}),
			@endforelse
		];

		const markers = [
			@forelse ($locations as $l)
			new google.maps.Marker({
				position: {
					lat: {{$l->latitude}},
					lng: {{$l->longitude}}
				},
				map: map,
				title: `{!!$l->store->name!!}`,
				animation: google.maps.Animation.DROP
			}),
			@empty
			new google.maps.Marker({
				position: locations[0],
				map: map,
				animation: google.maps.Animation.DROP
			})
			@endforelse
		];

		for (let i = 0; i < markers.length; i++) {
			markers[i].addListener('click', (e) => {
				console.log(infoWin[i])
				infoWin[i].open({
					anchor: markers[i],
					map,
				});
			});
		}
	}
</script>
@endsection