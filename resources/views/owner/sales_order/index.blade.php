@extends('template.admin')

@section('title', 'Sales Order')
@section('sidebar', 'owner')

@section('body')
<h1 class="font-weight-bold">Sales Order</h1>

<hr class="hr-thick" style="border-color: #707070;">

<div class="row mx-0 px-0">
	<form action="{{ route(Request::route()->getName()) }}" method="GET" class="col-12 col-lg-5 mx-0 px-0">
		<div class="input-group">
			<input type="text" class="form-control" placeholder="Search for..." name="s" value="{{$search}}">
			<div class="input-group-append">
		    	<button type="submit" data-action="none" class="btn btn-dark"><i class="fas fa-search"></i></button>
			</div>
		</div>
	</form>
</div>

<div class="row mx-0 px-0">
	<div class="col-12 px-0">
		<div class="card border rounded dark-shadow h-100">
			<h3 class="card-header font-weight-bold text-custom">Order List</h3>
			<div class="card-body mx-0 pt-0 px-0 overflow-x-auto">
				{{-- TABLE --}}
				<table class="table table-striped">
					<thead>
						<tr class="font-weight-bold">
							<td>Order #</td>
							<td>Order Date</td>
							<td>Customer Name</td>
							<td>Type of Payment</td>
							<td>Status</td>
							<td>Total</td>
							<td></td>
						</tr>
					</thead>

					<tbody id='table-content'>
						@forelse ($sales_order as $o)
						<tr>
							<td>{{$o->id}}</td>
							<td>{{\Carbon\Carbon::parse($o->created_at)->format('M d, Y')}}</td>
							<td>{{$o->customer->getName()}}</td>
							<td>{{ucwords($o->payment_method or 'Not Specified')}}</td>
							<td>
								@if ($o->status == 0)
								<i class="fas fa-circle mr-2 text-danger"></i>
								@elseif ($o->status == 1 || $o->status == 4)
								<i class="fas fa-circle mr-2 text-info"></i>
								@elseif ($o->status == 2 || $o->status == 3)
								<i class="fas fa-circle mr-2 text-warning"></i>
								@elseif ($o->status == 5)
								<i class="fas fa-circle mr-2 text-success"></i>
								@endif
								{{\App\SalesOrder::getStatus($o->status)}}
							</td>
							<td>&#8369;{{number_format($o->total, 2)}}</td>
							<td>
								<button type="button" class="btn btn-primary dropdown-toggle py-0" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action</button>

								<div class="dropdown-menu dropdown-menu-right">
									<a class="dropdown-item" href="{{ route('owner.sales-order.show', [$o->id]) }}"><i class="far fa-eye mr-2"></i>View Details</a>
									<a class="dropdown-item" href="{{ route('owner.sales-order.print', [$o->id]) }}" target="_blank"><i class="fas fa-print mr-2"></i>Print</a>
								</div>
							</td>
						</tr>
						@empty
						<tr>
							<td colspan="7">No Avaiable Data in Table</td>
						</tr>
						@endforelse
					</tbody>
				</table>
				{{-- TABLE END --}}
			</div>
		</div>
	</div>
</div>
{{-- PAGINATION --}}
<div class="row mx-0 px-0">
	<div class="col-12 px-0 text-lg-left text-center">
		<span>Total Results {{($sales_order->currentPage()-1) * $sales_order->perPage() + 1}}-{{(($sales_order->currentPage()-1) * $sales_order->perPage()) + $sales_order->count()}} of {{$sales_order->total()}} <a href="{{$sales_order->previousPageUrl()}}" class="text-dark"><i class="fas fa-chevron-left mx-2 cursor-pointer"></i></a><a href="{{$sales_order->nextPageUrl()}}" class="text-dark"><i class="fas fa-chevron-right ml-2 cursor-pointer"></i></a></span>
	</div>
</div>
@endsection