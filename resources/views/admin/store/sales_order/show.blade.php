@extends('template.admin')

@section('title', 'Sales Order')
@section('sidebar', 'admin')

@section('css')
<style type="text/css">
	@media (min-width: 768px) {
		.so-img {
			max-width: 25%;
		}
	}
</style>
@endsection

@section('body')
<h1 class="font-weight-bold"><a class="text-dark" href="{{route('admin.store.show', [$store->id])}}"><i class="fas fa-chevron-left mr-2"></i>Order Details {{$sales_order->id}}</a></h1>

<hr class="hr-thick" style="border-color: #707070;">

<div class="row mx-0 px-0">
	<div class="col-12 mx-0 px-0">
		<div class="card border rounded dark-shadow px-0 overflow-auto">
			<div class="card-body px-0">
				{{-- STEP BAR START --}}
				<div class="step-progress w-75 mx-auto mb-3" data-progress='{{$sales_order->status-1}}'>
					<div class="component">
						{{-- BAR --}}
						<div class="bar">
							<hr class="bar-progress">
							<hr class="bar-remaining">
						</div>

						{{-- STEPS --}}
						<div class="steps">
							@foreach ($steps as $s)
							<div class="step-icon">
								<i class="fas fa-circle fa-2x h-100"></i>
							</div>
							@endforeach
						</div>
					</div>

					<div class="labels">
						@foreach ($steps as $s)
						<div class="step-label">
							{{$s}}
						</div>
						@endforeach
					</div>
				</div>
				{{-- STEP BAR END --}}

				{{-- TABLE START --}}
				<table class="table mt-5">
					<thead class="text-white text-center" style="background-color: rgba(112, 112, 112, 0.75);">
						<tr>
							<td>Image</td>
							<td>Name</td>
							<td>Price</td>
							<td>QTY</td>
							<td>Total Amount</td>
						</tr>
					</thead>

					<tbody class="text-center">
						@foreach ($sales_order->items as $i)
						<tr>
							<td class="text-left">
								<img class="img-fluid border so-img" src='/uploads/products/{{$i->product->category->category_name}}/{{$i->product->image_name}}' alt="{{$i->product->product_name}}">
							</td>
							<td>{{$i->product->product_name}}</td>
							<td>₱{{number_format($i->product->selling_price, 2)}}</td>
							<td>{{$i->quantity}}</td>
							<td>₱{{number_format($i->price, 2)}}</td>
						</tr>
						@endforeach

						<tr>
							<td colspan="3" rowspan="4"></td>
							<td class="text-right">Subtotal:</td>
							<td class="text-left">₱{{number_format($sales_order->total, 2)}}</td>
						</tr>

						<tr>
							<td class="text-right">Shipping:</td>
							<td class="text-left">₱0.00</td>
						</tr>

						<tr>
							<td class="text-right">Tax:</td>
							<td class="text-left">₱0.00</td>
						</tr>

						<tr>
							<td colspan="2" class="font-weight-bold">₱{{number_format($sales_order->total, 2)}}</td>
						</tr>
					</tbody>
				</table>
				{{-- TABLE END --}}
			</div>
		</div>
	</div>
</div>

<div class="row-spacing-3">
	<div class="row mx-0 px-0">
		<div class="col-6">
			<div class="card h-100">
				<div class="card-body d-flex flex-d-col">
					<span class="font-weight-bold">Customer Details</span>
					<span>{{$sales_order->customer->getName()}}</span>
					<span>(+63){{$sales_order->customer->contact_number}}</span>
					<span>{{$sales_order->customer->email}}</span>
					<br>
					<span class="font-weight-bold">Shipping Details</span>
					<span>{{$sales_order->shippingAddress->getAddress(true, true)}}</span>
					<span>{{$sales_order->shippingAddress->city}}, {{$sales_order->shippingAddress->region}}</span>
				</div>
			</div>
		</div>

		<div class="col-6">
			<div class="card h-100">
				<div class="card-body d-flex flex-d-col">
					<span class="font-weight-bold">Billing Details</span>
					<span>{{$sales_order->customer->getName()}}</span>
					<span>{{$sales_order->shippingAddress->getAddress(true, true)}}</span>
					<span>{{$sales_order->shippingAddress->city}}, {{$sales_order->shippingAddress->region}}</span>
				</div>
			</div>
		</div>
	</div>

	<div class="row mx-0 px-0">
		<div class="col-6">
			<div class="card h-100">
				<div class="card-body d-flex flex-d-col">
					<span class="font-weight-bold">Payment Method</span>
					<span>{{ucwords($sales_order->payment_method)}}</span>
				</div>
			</div>
		</div>

		<div class="col-6">
			<div class="card h-100">
				<div class="card-body d-flex flex-d-col">
					<span class="font-weight-bold">Shipping Method</span>
					<span>{{$sales_order->courier}}</span>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection