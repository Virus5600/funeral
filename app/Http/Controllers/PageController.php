<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use \Carbon\Carbon;

use App\Category;
use App\Livestream;
use App\Obituary;
use App\Product;
use App\SalesOrder;
use App\SalesOrderItems;
use App\Service;
use App\Store;
use App\Subcategory;
use App\Location;

use Auth;
use Exception;
use DB;
use Log;
use Mail;
use Validator;

class PageController extends Controller {
	/////////////////
	// CLIENT SIDE //
	/////////////////
	// Home/Landing/Index Page
	protected function redirectToIndex() {
		return redirect()->route('home');
	}

	protected function index() {
		return view('client.index', [
			'obituaries' => Obituary::orderBy('id', 'DESC')->where('status', '=', '2')->get()->take(5),
			'locations' => Location::get()
		]);
	}

	// Store
	protected function store() {

		$stores = Store::get();

		return view('client.store.index', [
			'stores' => $stores
		]);
	}

	protected function storeShow($store_name) {
		$store = Store::where('name', '=', preg_replace('/_/', ' ', $store_name))->first();
		
		return view('client.store.show', [
			'store' => $store,
			'products' => Product::where('store_id', '=', $store->id)->paginate(9)
		]);
	}

	// Product
	protected function products($store_name, $category_name='all', $subcategory_name='all') {
		$store = Store::where('name', '=', preg_replace('/_/', ' ', $store_name))->first();
		$products = Product::where('store_id', '=', $store->id);

		if ($category_name != 'all') {
			$subcategories = Category::where('category_name', '=', preg_replace('/_/', ' ', $category_name))
				->first()->subcategory;

			$subcategory_ids = array();
			foreach ($subcategories as $s) {
				array_push($subcategory_ids, $s->id);
			}

			$products = $products->whereIn('subcategory_id', $subcategory_ids);
		}

		if ($subcategory_name != 'all') {
			$subcategory = Subcategory::where('subcategory_name', '=', preg_replace('/_/', ' ', $subcategory_name))
				->first();
			$products->where('subcategory_id', '=', $subcategory->id);
		}

		$products = $products->where('status', '=', 1)->paginate(6);

		return view('client.store.product.index', [
			'store' => $store,
			'products' => $products,
			'obituary' => $store->obituary->sortByDesc('id'),
			'category_name' => $category_name
		]);
	}

	protected function productsShow($store_name, $category_name, $product_id) {
		$store = Store::where('name', '=', preg_replace('/_/', ' ', $store_name))->first();
		$category = Category::where('category_name', '=', preg_replace('/_/', ' ', $category_name))->first();
		$product = Product::find($product_id);

		if ($category == null)
			$category = $product->category;

		return view('client.store.product.show', [
			'store' => $store,
			'category' => $category,
			'product' => $product,
			'randProd' => Product::where('id', '<>', $product_id)->where('store_id', '=', $store->id)->where('status', '=', 1)->get()->random(4)->shuffle()
		]);
	}

	// Services
	protected function services() {
		return view('client.services.index', [
			'services' => Service::get()
		]);
	}

	protected function servicesObituary() {
		return view('client.services.obituaries.index', [
			'obituaries' => Obituary::orderBy('id', 'DESC')->where('status', '=', '2')->paginate(5)
		]);
	}

	protected function servicesObituaryRequest() {
		if (Auth::check())
			return view('client.services.obituaries.request', [
				'store' => Store::get()
			]);
		return redirect()
			->back();
	}

	protected function serviceObituaryStore(Request $req) {
		$validator = Validator::make($req->all(), [
			'name' => 'required|min:3',
			'birth_date' => 'required|date|before:death_date',
			'death_date' => 'required|date|after:birth_date',
			'store' => 'required|exists:stores,id',
			'image' => 'file|mimes:jpeg,jpg,png|max:5120',
			'description' => 'min:5',
			'obituary_duration_start' => 'required|date|before:obituary_duration_end',
			'obituary_duration_end' => 'required|date|after:obituary_duration_start',
			'begin_time' => 'required',
		], [
			'name.required' => 'Full name is required.',
			'name.min' => 'Please provide the full name of the deceased.',
			'birth_date.required' => 'Date of birth is required.',
			'birth_date.date' => 'Please provide a proper date value.',
			'birth_date.before' => 'Date of birth must be before the date of death.',
			'death_date.required' => 'Date of death is required.',
			'death_date.date' => 'Please provide a proper date value.',
			'death_date.after' => 'Date of death should be after the date of birth.',
			'store.required' => 'Please provide the store that will handle the obituary request.',
			'store.exists' => 'Please select from the list of available partnered funeral service providers above.',
			'image.file' => 'Image must be a valid file.',
			'image.mimes' => 'Image formats allowed are only JPEG, JPG, and PNG.',
			'image.max' => 'Maximum image file size should be 5MB.',
			'description.min' => 'Please provide a proper description or leave it blank entirely.',
			'obituary_duration_start.required' => 'The start date of when this obituary will be displayed is required.',
			'obituary_duration_start.date' => 'Please provide a proper date value.',
			'obituary_duration_start.before' => 'Date of birth must be before the date of death.',
			'obituary_duration_end.required' => 'The end date of when this obituary will be displayed is required.',
			'obituary_duration_end.date' => 'Please provide a proper date value.',
			'obituary_duration_end.after' => 'Date of death should be after the date of birth.',
			'begin_time.required' => 'This field is required.',
		]);

		if ($validator->fails()) {
			return redirect()
				->back()
				->withErrors($validator)
				->withInput();
		}

		try {
			DB::beginTransaction();

			// Generate file name
			$fileName = null;
			if ($req->hasFile('image')) {
				$file = $req->file('image');
				$fileName = 'obituaries-'.uniqid().'.'.$file->getClientOriginalExtension();
				// Move file
				$filepath = 'uploads/obituary/';
				$file->move($filepath, $fileName);
			}

			Obituary::create([
				'customer_id' => Auth::user()->id,
				'name' => $req->name,
				'birth_date' => $req->birth_date,
				'death_date' => $req->death_date,
				'store_id' => $req->store,
				'description' => $req->description,
				'available_from' => $req->obituary_duration_start,
				'available_to' => $req->obituary_duration_end,
				'begin_time' => $req->begin_time,
				'location' => $req->location,
				'address' => $req->address,
				'image_name' => $fileName,
				'status' => 1
			]);

			DB::commit();
		} catch (\Exception $e) {
			Log::error($e);
			DB::rollback();

			return redirect()
				->back()
				->with('flash_error', 'Something went wrong, please try again later.')
				->withInput();
		}

		return redirect()
			->back()
			->with('flash_success', 'Successfully requested obituary for '.$req->name.'.');
	}

	protected function servicesObituaryShow($id) {
		return view('client.services.obituaries.show', [
			'obituary' => Obituary::find($id)
		]);
	}

	protected function servicesLivestream() {

		$product_id = DB::table('var')
            ->where('name','=','product_id')
            ->first();

		return view('client.services.livestream.index',[
			'product_id'=>$product_id,
			]);
	}

	protected function showLivestream($id) {
		$ls = Livestream::where('stream_id', '=', $id)->first();

		if ($ls == null) {
			return redirect()
				->route('home')
				->with('flash_message', 'The page you are trying to access is not available.');
		}

		return view('client.services.livestream.show', [
			'livestream' => $ls
		]);
	}

	// About Us
	protected function aboutUs() {
		return view('client.about-us');
	}

	// Contact Us
	protected function contactUs() {
		return view('client.contact-us');
	}

	// MAILER
	protected function sendInquiry(Request $req) {
		$validator = Validator::make($req->all(), [
			'name' => 'required|min:2|max:255',
			'email' => 'required|email|min:2|max:255',
			'message' => 'required|min:2|max:2500'
		]);

		if ($validator->fails()) {
			return redirect()
				->back()
				->withInput()
				->withErrors($validator)
				->with('move_to', '#inquiry_form');
		}
		try {
			Mail::send(
				'template.email.inquiry',
				['name' => $req->name, 'email' => $req->email, 'content' => $req->message],
				function ($m) use ($req) {
					$m->from($req->email);
					$m->to(env('MAIL_RECEIVER'), env('MAIL_RECEIVER'))
						->subject('Contact Inquiry');
				}
			);
		} catch (Exception $e) {
			Log::error($e);
			return redirect()
				->back()
				->withInput()
				->with('move_to', '#inquiry_form')
				->with('flash_error', 'Something went wrong, please try again later')
				->with('flash_error_actual', $e);
		}

		return redirect()
			->back()
			->with('move_to', '#inquiry_form')
			->with('flash_success', 'Successfully sent an inquiry');
	}

	////////////////
	// OWNER SIDE //
	////////////////
	protected function redirectToOwner() {
		return redirect()->route('owner.dashboard');
	}

	// Dashboard
	protected function ownerDashboard() {
		$months = array();
		$monthly_earnings = array();

		for ($i = 1; $i <= Carbon::now()->format('m'); $i++) {
			array_push($months, Carbon::parse(Carbon::now()->format('Y') . '-' . $i . '-' . Carbon::now()->format('d'))->format('M'));
			array_push(
				$monthly_earnings,
				SalesOrder::where('created_at', '>=', Carbon::parse(Carbon::now()->format('Y') . '-' . $i . '-01'))
					->where('created_at', '<=', Carbon::parse(Carbon::now()->format('Y') . '-' . $i)->endOfMonth())
					->where('store_id', '=', Auth::user()->store_id)
					->where('status', '>=', 3)
					->get()
					->sum('total')
			);
		}

		$popular_products = array();

		foreach (SalesOrderItems::get() as $i) {
			if ($i->product->store_id != Auth::user()->store_id)
				continue;

			$product_name = preg_replace('/ /', '_', $i->product->product_name);

			if (array_key_exists($product_name, $popular_products))
				$popular_products[$product_name]['quantity'] += $i->quantity;
			else {
				$popular_products[$product_name] = array(
					'quantity' => $i->quantity,
					'store_name' => $i->product->store->name
				);
			}
		}

		$unprocessed_sales = SalesOrder::where('status', '=', 2)
			->where('store_id', '=', Auth::user()->store_id)
			->get();

		return view('owner.dashboard', [
			'sales_order' => SalesOrder::where('status', '>', 0)->where('store_id', '=', Auth::user()->store_id)->get(),
			'months' => $months,
			'monthly_earnings' => $monthly_earnings,
			'popular_products' => $popular_products,
			'unprocessed_sales' => $unprocessed_sales
		]);
	}

	// Contact
	protected function ownerContact() {
		return view('owner.contact', [
			'store' => Store::find(Auth::user()->store_id)
		]);
	}

	protected function ownerContactUpdate(Request $req) {
		$validator = Validator::make($req->all(), [
			'address' => 'min:5',
			'email' => 'email',
			'contact_number' => 'numeric|min:10',
			// 'facebook' => array('regex:/(https?:\/\/)?([\w\.]*)facebook\.com\/([a-zA-Z0-9_]*)$/'),
			// 'twitter' => array('regex:/(https?:\/\/)?([\w\.]*)twitter\.com\/([a-zA-Z0-9_]*)$/')
		]);

		if ($validator->fails()) {
			return redirect()
				->back()
				->withErrors($validator)
				->withInput();
		}

		try {
			DB::beginTransaction();

			$store = Store::find(Auth::user()->store_id);

			if ($req->email != '')
				$store->email = $req->email;
			if ($req->contact_number != '')
				$store->contact_number = $req->contact_number;
			if ($req->facebook != '')
				$store->facebook = $req->facebook;
			if ($req->twitter != '')
				$store->twitter = $req->twitter;

			$store->save();

			DB::commit();
		} catch (Exception $e) {
			DB::rollback();
			Log::error($e);

			return redirect()
				->back()
				->withInput()
				->with('flash_error', 'Something went wrong, please try again later');
		}

		return redirect()
			->back()
			->with('flash_success', 'Successfully updated details');
	}

	////////////////
	// ADMIN SIDE //
	////////////////
	protected function redirectToAdmin() {
		return redirect()->route('admin.dashboard');
	}

	// Dashboard
	protected function adminDashboard() {
		$months = array();
		$monthly_earnings = array();

		for ($i = 1; $i <= Carbon::now()->format('m'); $i++) {
			array_push($months, Carbon::parse(Carbon::now()->format('Y') . '-' . $i . '-' . Carbon::now()->format('d'))->format('M'));

			array_push(
				$monthly_earnings,
				SalesOrder::where('created_at', '>=', Carbon::parse(Carbon::now()->format('Y') . '-' . $i . '-01'))
					->where('created_at', '<=', Carbon::parse(Carbon::now()->format('Y') . '-' . $i)->endOfMonth())
					->where('status', '>=', '3')
					->get()
					->sum('price')
			);
		}

		$unprocessed_sales = Livestream::where('status', '=', 2)
			->get();

		return view('admin.dashboard', [
			'months' => $months,
			'monthly_earnings' => $monthly_earnings,
			'unprocessed_sales' => $unprocessed_sales
		]);
	}
}