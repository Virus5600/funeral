<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Carbon\Carbon;

use App\Variable;
use App\User;
use App\Livestream;

use DB;
use Log;
use Validator;

class LivestreamManagementController extends Controller {
	
	protected function index() {
		$livestreams = Livestream::query();
		foreach (Livestream::getStatusList() as $k => $v) {
			if (($k > 0 && $k < 3) || $k == 6)
				continue;
			$livestreams = $livestreams->orWhere('status', '=', strtolower($v));
		}
		$search = null;

		if (request()->has('s')) {
			$search = request()->get('s');
			$livestreams = $livestreams->leftJoin('users', 'users.id', '=', 'livestream.user_id');

			$livestreams = $livestreams->where('livestream.title', 'LIKE', '%'.$search.'%')
				->orWhere('livestream.description', 'LIKE', '%'.$search.'%')
				->orWhere('livestream.options', 'LIKE', '%'.$search.'%')
				->orWhere('users.first_name', 'LIKE', '%'.$search.'%')
				->orWhere('users.middle_name', 'LIKE', '%'.$search.'%')
				->orWhere('users.last_name', 'LIKE', '%'.$search.'%')
				->orWhere('users.contact_number', 'LIKE', '%'.$search.'%')
				->orWhere('users.address', 'LIKE', '%'.$search.'%')
				->orWhere('users.region', 'LIKE', '%'.$search.'%')
				->orWhere('users.city', 'LIKE', '%'.$search.'%')
				->orWhere('users.zip_code', 'LIKE', '%'.$search.'%')
				->orWhere('users.email', 'LIKE', '%'.$search.'%')
				->select('livestream.*')
				->distinct();
		}

		return view('admin.livestream-management.index', [
			'search' => $search,
			'livestreams' => $livestreams->paginate(15),
		]);
	}

	protected function show($id) {
		$livestream = Livestream::find($id);

		return view('admin.livestream-management.show', [
			'livestream' => $livestream,
		]);
	}

	protected function edit($id) {
		$users = User::get();
		$livestream = Livestream::find($id);
		return view('admin.livestream-management.edit', [
				'livestream'=> $livestream,
				'users' => $users,
		]);
	}

	protected function create() {
		$users = User::get();
		return view('admin.livestream-management.create',[
			'users' => $users,
		]);
	}

	protected function store(Request $request) {
		$validator = Validator::make($request->all(), [
			'title' => 'max:255',
			'description' => 'max:1000',
			'category' => array('regex:'.Livestream::getCategoryList(true)),
			'schedule_date' => 'required|date_format:Y-m-d',
			'schedule_time' => 'required|date_format:H:i',
			'duration' => 'required|numeric',
			'price' => 'required|numeric'
		], [
			'title.max' => 'Title must not exceed 255 characters',
			'description.max' => 'Description must not exceed 1000 characters',
			'schedule_date.required' => 'Date of when the stream will take place is required',
			'schedule_date.date_format' => 'Please use this format: yyyy-mm-dd',
			'schedule_time.required' => 'Time of when the stream will take place is required',
			'schedule_time.date_format' => 'Please follow this format: hh:mm',
			'duration.required' => 'Please provide how long the stream will last',
			'duration.numeric' => 'Duration should be numeric'
		]);

		if ($validator->fails()) {
			return redirect()
				->back()
				->withErrors($validator)
				->withInput();
		}

		try {
			DB::beginTransaction();

			$mux = new MuxController();
			$result = $mux->createNewLivestream();
			$resultGet = $mux->getLiveStream($result['data']['id']);

			if ($result == null)
				throw new Exception('Unable to create new live stream.');

			Livestream::create([
				'title' => $request->title,
				'stream_id' => $result['data']['id'],
				'stream_key' => $result['data']['stream_key'],
				'stream_link' => 'https://stream.mux.com/'.$resultGet['data']['playback_ids'][0]['id'].'.m3u8',
				'description' => $request->description,
				'schedule_date' => $request->schedule_date,
				'schedule_time' => $request->schedule_time,
				'duration' => $request->duration,
				'price' => $request->price,
				'user_id' => $request->user_id,
				'status' => Livestream::getStatusList()[1]
			]);

			DB::commit();
		} catch (\Exception $e) {
			Log::error($e);
			DB::rollback();

			return redirect()
				->back()
				->with('flash_error', 'Something went wrong, please try again later.')
				->withInput();
		}

		return redirect()
			->route('admin.livestream-management.index')
			->with('flash_success', 'Successfully added new Livestream entry.');
	}

	protected function update(Request $request,$id) {

		$validator = Validator::make($request->all(), [
			'title' => 'max:255',
			'description' => 'max:1000',
			'schedule_date' => 'required|date_format:Y-m-d',
			'schedule_time' => 'required|date_format:H:i|after:'.Carbon::now()->timezone('Asia/Manila'),
			'duration' => 'required|numeric',
			'price' => 'required|numeric'
		], [
			'title.max' => 'Title must not exceed 255 characters',
			'description.max' => 'Description must not exceed 1000 characters',
			'schedule_date.required' => 'Date of when the stream will take place is required',
			'schedule_date.date_format' => 'Please use this format: yyyy-mm-dd',
			'schedule_time.required' => 'Time of when the stream will take place is required',
			'schedule_time.date_format' => 'Please follow this format: hh:mm (input was ' . $request->schedule_time . ')',
			'schedule_time.after' => 'Please set the new time past the current time.',
			'duration.required' => 'Please provide how long the stream will last',
			'duration.numeric' => 'Duration should be numeric'
		]);

		if ($validator->fails()) {
			return redirect()
				->back()
				->withErrors($validator)
				->withInput();
		}

		try {
			DB::beginTransaction();

			$livestream = Livestream::find($id);

			$livestream->title = $request->title;
			$livestream->description = $request->description;
			$livestream->schedule_date = $request->schedule_date;
			$livestream->schedule_time = $request->schedule_time;
			$livestream->user_id = $request->user_id;

			switch ($request->status) {
				case 'approved':
				$this->approve($id);
				break;

				case 'cancelled':
				$this->cancel($id);
				break;

				case 'done':
				$this->done($id);
				break;

				default:
				$livestream->status = $request->status;
			}

			$livestream->save();

			DB::commit();
		} catch (\Exception $e) {
			Log::error($e);
			DB::rollback();

			return redirect()
				->back()
				->with('flash_error', 'Something went wrong, please try again later.')
				->withInput();
		}

		return redirect()
			->route('admin.livestream-management.index')
			->with('flash_success', 'Successfully updated Livestream entry.');
	}

	protected function addToExisting($id) {
		try {
			DB::beginTransaction();

			$item = Livestream::find($id);
			$item->duration += 1;
			$item->price = $item->duration * 100;
			$item->save();

			DB::commit();
		} catch (Exception $e) {
			Log::error($e);
			DB::rollback();

			return redirect()
				->back()
				->with('flash_error', 'Something went wrong, please try again later.');
		}

		return redirect()
			->back()
			->with('flash_success', 'Successfully increased duration of livestream.');
	}

	protected function subtractToExisting($id) {
		$delete = false;
		try {
			DB::beginTransaction();

			$item = Livestream::find($id);

			if (($item->duration - 1) == 0) {
				$item->delete();
				$delete = true;
			}
			else {
				$item->duration -= 1;
				$item->save();
			}

			DB::commit();
		} catch (Exception $e) {
			Log::error($e);
			DB::rollback();

			return redirect()
				->back()
				->with('flash_error', 'Something went wrong, please try again later.');
		}

		if ($delete)
			return redirect()
				->back()
				->with('flash_message', 'Removed livestream from cart.');

		return redirect()
			->back()
			->with('flash_success', 'Successfully decreased duration of livestream.');
	}

	protected function delete($id) {
		$ls = Livestream::find($id);

		try {
			DB::beginTransaction();

			$success = true;
			if ($ls->status == strtolower(Livestream::getStatusList()[1])) {
				$mux = new MuxController();
				$success = $mux->deleteLiveStream($ls->stream_id);
			}

			if ($success)
				$ls->delete();
			else {
				return redirect()
					->route('admin.livestream-management.show', [$ls->id])
					->with('flash_error', 'Livestream ongoing. Please stop the livestream before deleting the entry.');
			}

			DB::commit();
		} catch (\Exception $e) {
			Log::error($e);
			DB::rollback();

			return redirect()
				->route('admin.livestream-management.index')
				->with('flash_error', 'Something went wrong, please try again later.');
		}

		return redirect()
			->route('admin.livestream-management.index')
			->with('flash_success', 'Successfully deleted Livestream entry.');
	}

	protected function approve($id) {
		$ls = Livestream::find($id);

		if ($ls == null) {
			return redirect()
				->route('admin.livestream-management.index')
				->with('flash_error', 'Livestream no longer exists...');
		}

		try {
			DB::beginTransaction();

			$ls->status = 'approved';
			$ls->save();

			DB::commit();
		} catch (\Exception $e) {
			DB::rollback();
			Log::error($e);

			return redirect()
				->route('admin.livestream-management.index')
				->with('flash_error', 'Something went wrong, please try again later.');
		}

		return redirect()
			->back()
			->with('flash_success', 'Successfully updated livestream\'s status.');
	}

	protected function cancel($id) {
		$ls = Livestream::find($id);

		if ($ls == null) {
			return redirect()
				->route('admin.livestream-management.index')
				->with('flash_error', 'Livestream no longer exists...');
		}

		try {
			DB::beginTransaction();

			$ls->status = 'cancelled';
			$ls->save();

			DB::commit();
		} catch (\Exception $e) {
			DB::rollback();
			Log::error($e);

			return redirect()
				->route('admin.livestream-management.index')
				->with('flash_error', 'Something went wrong, please try again later.');
		}

		return redirect()
			->back()
			->with('flash_success', 'Successfully updated livestream\'s status.');
	}

	protected function streaming($id) {
		$ls = Livestream::find($id);

		if ($ls == null) {
			return redirect()
				->route('admin.livestream-management.index')
				->with('flash_error', 'Livestream no longer exists...');
		}

		try {
			DB::beginTransaction();

			$ls->status = Livestream::getStatusList()[4];
			$ls->save();

			DB::commit();
		} catch (\Exception $e) {
			DB::rollback();
			Log::error($e);

			return redirect()
				->route('admin.livestream-management.index')
				->with('flash_error', 'Something went wrong, please try again later.');
		}

		return redirect()
			->back()
			->with('flash_success', 'Successfully updated livestream\'s status.');
	}

	protected function done($id) {
		$ls = Livestream::find($id);

		if ($ls == null) {
			return redirect()
				->route('admin.livestream-management.index')
				->with('flash_error', 'Livestream no longer exists...');
		}

		try {
			DB::beginTransaction();

			$mux = new MuxController();
			$asset = $mux->getLastVideoPlayed($ls->stream_id);
			if ($asset == null) {
				return redirect()
					->back()
					->with('flash_info', 'Stream hasn\'t started yet. Cannot be marked as done.');
			}
			else if ($asset == 'streaming') {
				return redirect()
					->back()
					->with('flash_info', 'Stream ongoing. Please stop the livestream before setting status to "done."');
			}

			$ls->status = Livestream::getStatusList()[5];
			$ls->stream_link = 'https://stream.mux.com/'.$asset['data']['playback_ids'][0]['id'].'.m3u8';
			$ls->save();

			$mux = new MuxController();
			$mux->liveStreamComplete($ls->stream_id);

			DB::commit();
		} catch (\Exception $e) {
			DB::rollback();
			Log::error($e);

			return redirect()
				->route('admin.livestream-management.index')
				->with('flash_error', 'Something went wrong, please try again later')
				->with('flash_error_actual', $e);
		}

		return redirect()
			->back()
			->with('flash_success', 'Successfully updated livestream\'s status.');
	}
}