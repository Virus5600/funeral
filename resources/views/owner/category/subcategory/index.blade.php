@extends('template.admin')

@section('title', 'Products')
@section('sidebar', 'owner')

@section('body')
<h1 class="font-weight-bold">@include('template.breadcrumbs', ['startIndex' => 2])</h1>

<hr class="hr-thick" style="border-color: #707070;">

<div class="row mx-0 px-0">
	<form action="{{ route(Request::route()->getName(), [preg_replace('/ /', ' ', strtolower($category->category_name))]) }}" method="GET" class="col-12 col-lg-5 mx-0 px-0">
		<div class="input-group">
			<input type="text" class="form-control" placeholder="Search for..." name="s" value="{{$search}}">
			<div class="input-group-append">
		    	<button type="submit" data-action="none" class="btn btn-dark"><i class="fas fa-search"></i></button>
			</div>
		</div>
	</form>

	<div class="col-12 col-lg-2 mt-3 my-lg-0">
		<a class="btn btn-success w-100" href="{{route('owner.subcategory.create', [preg_replace('/ /', ' ', strtolower($category->category_name))])}}"><i class="fas fa-plus-circle mr-2"></i>Add Subcategory</a>
	</div>
</div>

<div class="row mx-0 px-0">
	<div class="col-12 px-0 overflow-x-auto">
		<div class="card border rounded dark-shadow h-100">
			<h3 class="card-header font-weight-bold text-custom">Product Subcategory List</h3>
			<div class="card-body mx-0 pt-0 px-0">
				{{-- TABLE --}}
				<table class="table table-striped">
					<thead>
						<tr class="font-weight-bold">
							<td>Subcatergory Name</td>
							<td></td>
						</tr>
					</thead>

					<tbody id='table-content'>
						@forelse ($subcategories as $s)
						<tr>
							<td>{{$s->subcategory_name}}</td>
							<td>
								<button type="button" class="btn btn-primary dropdown-toggle float-right py-0" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action</button>

								<div class="dropdown-menu dropdown-menu-right">
									<a class="dropdown-item" href="{{ route('owner.product.index', [preg_replace('/ /', '_', strtolower($category->category_name)), preg_replace('/ /', '_', strtolower($s->subcategory_name))]) }}"><i class="far fa-eye mr-2"></i>View Products</a>
									<a class="dropdown-item" href="{{ route('owner.subcategory.delete', [$s->id]) }}"><i class="fas fa-trash mr-2"></i>Delete</a>
								</div>
							</td>
						</tr>
						@empty
						<tr>
							<td colspan="3">No Avaiable Data in Table</td>
						</tr>
						@endforelse
					</tbody>
				</table>
				{{-- TABLE END --}}
			</div>
		</div>
	</div>
</div>

{{-- PAGINATION --}}
<div class="row mx-0 px-0">
	<div class="col-12 px-0 text-lg-left text-center">
		<span>Total Results {{($subcategories->currentPage()-1) * $subcategories->perPage() + 1}}-{{(($subcategories->currentPage()-1) * $subcategories->perPage()) + $subcategories->count()}} of {{$subcategories->total()}} <a href="{{$subcategories->previousPageUrl()}}" class="text-dark"><i class="fas fa-chevron-left mx-2 cursor-pointer"></i></a><a href="{{$subcategories->nextPageUrl()}}" class="text-dark"><i class="fas fa-chevron-right ml-2 cursor-pointer"></i></a></span>
	</div>
</div>
@endsection