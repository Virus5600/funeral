@extends('template.admin')

@section('title', 'Sales Order')
@section('sidebar', 'admin')

@section('body')
<h1 class="h3 h1-lg font-weight-bold text-truncate">Store</h1>

<hr class="hr-thick border-color-custom">

<div class="row mx-0 px-0">
	<form action="{{ route(Request::route()->getName()) }}" method="GET" class="col-12 col-md-6 col-xl-5 mx-0 px-0">
		<div class="input-group">
			<input type="text" class="form-control" placeholder="Search for..." name="s" value="{{$search}}">
			<div class="input-group-append">
				<button type="button" data-action="none" class="btn btn-dark"><i class="fas fa-search"></i></button>
			</div>
		</div>
	</form>

	<div class="col-12 col-md-6 col-xl-2 text-center">
		<button type="button" id="addStoreTrigger" class="btn btn-success my-2 my-lg-0" data-toggle="modal" data-target="#addStore"><i class="fas fa-plus-circle mr-2"></i>Add Store</button>

		<div class="modal fade" id="addStore" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered" role="document">
				{{-- FORM --}}
				<form action="{{ route('admin.store.store') }}" method="POST" class="modal-content px-3">
					{{ csrf_field() }}
					<div class="modal-header border-bottom-0 mb-0 pb-0 px-0">
						<h5 class="modal-title">Add Store</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>

					<hr class="hr-thick w-100">

					<div class="modal-body border-bottom-0 py-0">
						<div class="form-group">
							<label class="form-label">Store Name</label>
							<input type='text' class='form-control' name="store_name" value="{{ old('store_name') }}">
							<span class="badge badge-danger w-100 validation-message" data-store-target="create_new">{{$errors->first('store_name')}}</span>
						</div>
					</div>

					<div class="modal-footer border-top-0">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
						<button type="submit" class="btn btn-dark" data-action="submit">Save changes</button>
					</div>
				</form>
				{{-- FORM END --}}
			</div>
		</div>
	</div>
</div>

<div class="row mx-0 px-0">
	<div class="col-12 px-0">
		<div class="card border rounded dark-shadow h-100">
			<h3 class="card-header font-weight-bold text-custom">Store List</h3>
			<div class="card-body mx-0 pt-0 px-0 overflow-auto">
				{{-- TABLE --}}
				<table class="table table-striped">
					<thead>
						<tr class="font-weight-bold">
							<td>Store Name</td>
							<td></td>
						</tr>
					</thead>

					<tbody id='table-content'>
						@forelse ($stores as $s)
						<tr>
							<td>{{$s->name}}</td>
							<td>
								<button type="button" class="btn btn-primary dropdown-toggle py-0 float-right" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action</button>

								<div class="dropdown-menu dropdown-menu-right">
									<a class="dropdown-item" href="javascript:void(0);" data-toggle="modal" data-target="#editModal{{$s->id}}"><i class="fas fa-pencil-alt mr-2"></i>Edit Store Name</a>
									<a class="dropdown-item" href="{{ route('admin.store.show', [$s->id]) }}"><i class="fas fa-percent mr-2"></i>View Sales Order</a>
									<a class="dropdown-item" href="{{ route('admin.store.map.show', [$s->id]) }}"><i class="fas fa-map-marker-alt mr-2"></i>View Store Location</a>
									<a class="dropdown-item" href="{{ route('admin.store.map.edit', [$s->id]) }}"><i class="fas fa-map-marker mr-2"></i>Edit Store Location</a>
									<a class="dropdown-item" href="{{ route('admin.store.delete', [$s->id]) }}"><i class="fas fa-trash mr-2"></i>Delete</a>
								</div>

								<div class="modal fade" id="editModal{{$s->id}}" tabindex="-1" role="dialog" aria-hidden="true">
									<div class="modal-dialog modal-dialog-centered" role="document">
										{{-- FORM --}}
										<form action="{{ route('admin.store.update', [$s->id]) }}" method="POST" class="modal-content px-3">
											{{ csrf_field() }}
											<div class="modal-header border-bottom-0 mb-0 pb-0 px-0">
												<h5 class="modal-title">Edit Store</h5>
												<button type="button" class="close" data-dismiss="modal" aria-label="Close">
													<span aria-hidden="true">&times;</span>
												</button>
											</div>

											<hr class="hr-thick w-100">

											<div class="modal-body border-bottom-0 py-0">
												<div class="form-group">
													<label class="form-label">Store Name</label>
													<input type='text' class='form-control' name="store_name" value="{{$s->name}}">
													<span class="badge badge-danger w-100 validation-message" data-target-store="{{$s->id}}">{{$errors->first('store_name')}}</span>
												</div>
											</div>

											<div class="modal-footer border-top-0">
												<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
												<button type="submit" class="btn btn-dark" data-action="update">Save changes</button>
											</div>
										</form>
										{{-- FORM END --}}
									</div>
								</div>
							</td>
						</tr>
						@empty
						<tr>
							<td colspan="3">No Avaiable Data in Table</td>
						</tr>
						@endforelse
					</tbody>
				</table>
				{{-- TABLE END --}}
			</div>
		</div>
	</div>
</div>
{{-- PAGINATION --}}
<div class="row mx-0 px-0">
	<div class="col-12 px-0">
		<span>Total Results {{($stores->currentPage()-1) * $stores->perPage() + 1}}-{{(($stores->currentPage()-1) * $stores->perPage()) + $stores->count()}} of {{$stores->total()}} <a href="{{$stores->previousPageUrl()}}" class="text-dark"><i class="fas fa-chevron-left mx-2 cursor-pointer"></i></a><a href="{{$stores->nextPageUrl()}}" class="text-dark"><i class="fas fa-chevron-right ml-2 cursor-pointer"></i></a></span>
	</div>
</div>
@endsection

@section('script')
<script type="text/javascript">
	$(document).ready(() => {
		@if (Session::get('create_error'))
		$("#addStoreTrigger").trigger('click');
		@endif

		@if (Session::get('update_error'))
		$('[data-target="#editModal{{Session::get('target_store')}}"]').trigger('click');
		@endif

		@if (Session::has('target_store'))
		$('.validation-message[data-target-store!={{Session::get('target_store')}}]').text('');
		@endif
	});
</script>
@endsection