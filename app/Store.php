<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
	protected $fillable = [
		'name',
		'email',
		'contact_number',
		'facebook',
		'twitter'
	];

	protected function owner() {
		return $this->hasMany('App\User', 'store_id');
	}

	protected function product() {
		return $this->hasMany('App\Product', 'store_id');
	}

	protected function obituary() {
		return $this->hasMany('App\Obituary', 'store_id');
	}

	protected function services() {
		return $this->hasMany('App\Service', 'store_id');
	}

	protected function salesOrder() {
		return $this->hasMany('App\SalesOrder', 'store_id');
	}

	protected function location() {
		return $this->hasOne('App\Location');
	}

	public function categories() {
		$products = $this->product;
		$categories = array();

		foreach ($products as $p) {
			if (!in_array($p->category, $categories)) {
				array_push($categories, $p->category);
			}
		}

		return $categories;
	}
}