<?php

namespace App\Http\Controllers;

use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Requests;
use Carbon\Carbon;

use App\EmailVerification;

use Auth;
use DB;
use Exception;
use Log;
use Mail;
use Validator;

class EmailVerificationController extends Controller
{
	protected function verificationForm($code=null) {
		if (Auth::check() && Auth::user()->is_validated)
			return redirect()
				->route('home');

		return view('verify', [
			'code' => $code
		]);
	}

	protected function verifyCode(Request $req) {
		$validator = Validator::make($req->all(), [
			'code' => 'required|exists:email_verifications,verification_code'
		], [
			'code.required' => 'Please enter the verification code',
			'code.exists' => 'Invalid verification code: code doesn\'t exists'
		]);

		try {
			DB::beginTransaction();

			$verifier = EmailVerification::where('verification_code', '=', $req->code)->first();
			if ($verifier == null) {
				return redirect()
					->back()
					->with('flash_error', 'Invalid verification code: code doesn\'t exists');
			}

			if ($verifier->isValid()) {
				$verifier->delete();
			}
			else {
				$verifier->verification_code = Str::random(15);
				$verifier->expiry_time = Carbon::now()->addMinutes(5);
				$verifier->save();

				Mail::send(
					'template.email.verification',
					['code' => $verifier->verification_code, 'user' => $verifier->user],
					function ($m) use ($verifier) {
						$m->from('no-reply@soulace.com');
						$m->to($verifier->user->email, $verifier->user->getName())
							->subject('Verify Your Email');
					}
				);

				DB::commit();
				return redirect()
					->back()
					->with('flash_error', 'Verification code already expired')
					->with('flash_message', 'A new code was sent to your email.');
			}

			// If verification is successful.
			$verifier->user->is_validated = 1;
			$verifier->user->save();
			$verifier->delete();
			DB::commit();
		} catch (Exception $e) {
			DB::rollback();
			Log::error($e);

			return redirect()
				->back()
				->with('flash_error', 'Something went wrong, please try again later.');
		}

		return redirect()
			->route('welcome')
			->with('isFirst', true)
			->with('flash_success', 'Verification accepted!');
	}

	protected function resend(Request $req) {
		$user = Auth::user();
		$verifier = EmailVerification::where('user_id', '=', $user->id)->first();

		try {
			if ($verifier == null) {
				$verifier = EmailVerification::create([
					'user_id' => $user->id,
					'verification_code' => Str::random(15),
					'expiry_time' => Carbon::now()->addMinutes(5)
				]);
			}
			else {
				$verifier->verification_code = Str::random(15);
				$verifier->expiry_time = Carbon::now()->addMinutes(5);
				$verifier->save();
			}

			Mail::send(
				'template.email.verification',
				['code' => $verifier->verification_code, 'user' => $user],
				function ($m) use ($user) {
					$m->from('no-reply@soulace.com');
					$m->to($user->email, $user->getName())
						->subject('Verify Your Email');
				}
			);

		} catch (Exception $e) {
			Log::error($e);
			DB::rollback();

			return response()->json([
				'success' => false,
				'result' => $e->getMessage()
			], 200, array(), JSON_PRETTY_PRINT);
		}

		return response()->json([
			'success' => true,
			'result' => $verifier
		], 200, array(), JSON_PRETTY_PRINT);
	}
}