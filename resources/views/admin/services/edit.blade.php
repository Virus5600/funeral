@extends('template.admin')

@section('title', 'Obituary')
@section('sidebar', 'admin')

@section('body')
{{-- @if ($isShow) --}}
<!-- <h1 class="font-weight-bold"><a class="text-dark" href="{{route('admin.services.index')}}"><i class="fas fa-chevron-left mr-2"></i>Services</a></h1> -->
{{-- @else --}}
<!-- <h1 class="font-weight-bold"><a class="text-dark" href="javascript:void(0);" onclick="confirmLeave('{{route('admin.services.index')}}');"><i class="fas fa-chevron-left mr-2"></i>Services</a></h1> -->
{{-- @endif --}}
<h1 class="font-weight-bold">@include('template.breadcrumbs', ['startIndex' => 2, 'confirmLeave' => !$isShow])</h1>

<hr class="hr-thick" style="border-color: #707070;">

<form class="form" action="{{ route('admin.services.update', [$service->id]) }}" method="POST" enctype="multipart/form-data">
	@if (!$isShow) {{csrf_field()}} @endif

	<div class="row">
		<div class="col-12">
			@if ($isShow)
			<h2 class="font-weight-bold" style="text-decoration: underline;">{{ucwords(preg_replace('/\_/i', ' ', $service->service_name))}}</h2>
			@else
			<label class="form-label font-weight-bold">Service Name:</label>
			<input class="form-control" name="service_name" value="{{$service->service_name}}" readonly>
			@endif
		</div>
	</div>
	<br>
	<div class="row">
		<div class="form-group col-12">
			<label class="form-label font-weight-bold">Description{{ $isShow ? ':' : '' }}</label>
			@if ($isShow)
			<p>{{$service->description}}</p>
			@else
			<textarea class="form-control not-resizable" name="description" rows="5">{{$service->description}}</textarea>
			<span class="badge badge-danger w-100 validation-message">{{$errors->first('description')}}</span>
			@endif
		</div>
	</div>

	@if (!$isShow)
	<div class="row">
		<div class="col-12">
			<button type="submit" class="btn btn-dark" data-action="update">Save Changes</button>
		</div>
	</div>
	@endif
</form>
@endsection