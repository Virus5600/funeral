<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('products', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('subcategory_id')->unsigned();
			$table->integer('store_id')->unsigned();
			$table->string('product_name');
			$table->decimal('selling_price', 18, 2);
			$table->integer('inventory')->unsigned()->default(0);
			$table->tinyInteger('status');
			$table->mediumText('description')->nullable();
			$table->string('size')->nullable();
			$table->string('image_name')->nullable();
			$table->decimal('discount', 18, 2)->nullable();
			$table->date('discount_start')->nullable();
			$table->date('discount_end')->nullable();
			$table->timestamps();

			$table->foreign('subcategory_id')->references('id')->on('subcategories')->onDelete('cascade');
			$table->foreign('store_id')->references('id')->on('stores')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('products');
	}
}