@extends('template.client')

@section('title', 'Home')

@section('page-css')
<link rel="stylesheet" href="/css/cart.css" >
@endsection

@section('css')
<style type="text/css">
	.cart-tr-border {
		border-bottom-style: dashed!important;
		border-bottom-width: 1.25px!important;
		border-bottom-color: #707070!important;
	}
</style>
@endsection

@section('body')
<!-- Main Content-->   
<div class="container py-5 my-5">
	@if (count(Auth::user()->cartItem) > 0 || count(Auth::user()->livestreamCart()) > 0)
	<div class="row my-5">
		<div class="col-lg-8 col-md-6">
			<h1>Cart</h1>
			
			<div class="container bg-light pt-3 pb-4 rounded">
				<h3><span class="item">Item Summary</span></h3>
				<div class="table d-grid "id="table">
					{{-- LIVESTREAMS --}}
					<h3><span class="item">Livestream Summary</span></h3>
					<table class="table-light text-center all-text-dark">
						<thead>
							<tr>
								<th>Category</th>
								<th>Price</th>
								<th>Duration</th>
								<th>Total</th>
							</tr>
						</thead>

						<tbody>
							@forelse (Auth::user()->livestreamCart() as $l)
							<tr class="cart-tr-border">
								<td>{{ ucwords($l->category == null ? 'None' : $l->category) }}</td>
								<td>&#8369;{{number_format(100, 2)}}</td>
								<td>
									<div class="d-flex flex-row justify-content-center">
										<a href="{{ route('cart.livestream.addDuration', [$l->id]) }}" class="mx-1 text-decoration-none">+</a>
										<span class="mx-1">{{$l->duration}}</span>
										<a href="{{ route('cart.livestream.subtractDuration', [$l->id]) }}" class="mx-1 text-decoration-none">-</a>
									</div>
								</td>
								<td style="vertical-align: middle; position: relative;">
									&#8369;{{number_format($l->price, 2)}}
									<a href="{{ route('cart.livestream.delete', [$l->id]) }}" style="position: absolute; bottom: 0; right: 0;" class="text-decoration-none"><i class="far fa-times-circle"></i></a>
								</td>
							</tr>
							@empty
							<tr>
								<td colspan="5" class="text-center">Nothing to show...</td>
							</tr>
							@endforelse
						</tbody>
					</table>

					{{-- PRODUCTS --}}
					<h3><span class="item">Item Summary</span></h3>
					<table class="table-light text-center all-text-dark">
						<thead>
							<tr>
								<th class="text-left">Item</th>
								<th>Price</th>
								<th>Qty</th>
								<th>Total</th>
							</tr>
						</thead>

						<tbody>
							@forelse (Auth::user()->cartItem as $c)
							<tr class="cart-tr-border">
								<td class="text-left d-flex flex-row align-items-center">
									<img src="uploads/products/{{$c->product->category->category_name}}/{{$c->product->image_name}}" alt="" class="w-25 img-fluid border">
									<span class="item_name ml-2" >{{$c->product->product_name}}</span>
								</td>
								<td style="vertical-align: middle;">&#8369;{{number_format($c->product->selling_price, 2)}}</td>
								<td style="vertical-align: middle;">
									<div class="d-flex flex-row">
										<a href="{{ route('cart.addToExisting', [$c->id]) }}" class="mx-1 text-decoration-none">+</a>
										<span class="mx-1">{{$c->quantity}}</span>
										<a href="{{ route('cart.subtractToExisting', [$c->id]) }}" class="mx-1 text-decoration-none">-</a>
									</div>
								</td>
								<td style="vertical-align: middle; position: relative;">
									&#8369;{{number_format($c->total(), 2)}}
									<a href="{{ route('cart.delete', [$c->id]) }}" style="position: absolute; bottom: 0; right: 0;" class="text-decoration-none"><i class="far fa-times-circle"></i></a>
								</td>
							</tr>
							@empty
							<tr>
								<td colspan="5" class="text-center">Nothing to show...</td>
							</tr>
							@endforelse
						</tbody>
					</table>
				</div>
			</div>
		</div>

		<div class="col-lg-4 col-md-6">
			<div class="container bg-light mt-3 py-3 d-grid rounded">
				<h4><span class="item">Order Summary</span></h4> 
				<h3><span class="item">&#8369;{{number_format(Auth::user()->cartTotal() + Auth::user()->livestreamTotal(), 2)}}</span></h3><br>
				{{Auth::user()->livestreamTotal()}}
				@if (count(Auth::user()->livestreamCart()) > 0)
				<a href="{{ route('billing.livestream') }}" class="btn btn-lg btn-dark">Go to Checkout</a>
				@else
				<a href="{{ route('billing') }}" class="btn btn-lg btn-dark">Go to Checkout</a>
				@endif
			</div>

			<div class="container mt-3">
				<h5>Accepted Payment Methods</h5>
				<img src="images/UI/Pay_COD.png" alt="" class="pay img-fluid">
				<img src="images/UI/Pay_Bank.png" alt="" class="pay img-fluid">
				<img src="images/UI/Pay_Paypal.png" alt="" class="pay img-fluid">
				<img src="images/UI/Pay_GCash.png" alt="" class="pay img-fluid">
			</div>
		</div>
	</div>
	@else
	<section class="py-5 my-5">
		<div class="container py-5">
			<div class="row align-items-center text-center my-5">
				<img src="images/UI/cart-x.svg" alt="" width="100px" height="100px">
				<h3 class="mt-5"> <b>YOUR CART IS EMPTY!</b></h3>
				<p>Start shopping to fill your cart with your desired items.</p>
				<div class="d-grid gap-2 col-6 mx-auto">
					<a href="{{ route('store.index') }}" class="btn-lg btn-light btn-outline-dark text-decoration-none">SHOP NOW</a>
				</div>
			</div>
		</div>
	</section>
	@endif
</div>
@endsection