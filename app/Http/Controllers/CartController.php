<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Requests;

use App\CartItem;
use App\Product;

use DB;
use Log;
use Exception;

class CartController extends Controller
{
	protected function index() {
		return view('client.cart.index');
	}

	protected function addToCart($id) {
		$product = Product::find($id);

		if ($product == null)
			return redirect()
				->back()
				->with('flash_error', 'Product is non-existent...');

		if ($product->status == 1) {
			try {
				DB::beginTransaction();

				if (CartItem::where('product_id', '=', $product->id)->first() != null) {
					$item = CartItem::where('user_id', '=', Auth::user()->id)
						->where('product_id', '=', $product->id)
						->first();

					$item->quantity = $item->quantity+1;
					$item->save();
				}
				else {
					CartItem::create([
						'user_id' => Auth::user()->id,
						'product_id' => $product->id,
						'quantity' => 1
					]);
				}

				DB::commit();
			} catch (Exception $e) {
				Log::error($e);
				DB::rollback();

				return redirect()
					->back()
					->with('flash_error', 'Something went wrong, please try again later.');
			}
		}

		return redirect()
			->back()
			->with('flash_success', 'Succesfully added item to cart.');
	}

	protected function addToExisting($id) {
		try {
			DB::beginTransaction();

			$item = CartItem::find($id);

			if (($item->quantity + 1) > Product::find($item->product_id)->inventory) {
				return redirect()
					->back()
					->with('flash_error', 'Cannot add more than the current available stock.');
			}

			$item->quantity += 1;
			$item->save();

			DB::commit();
		} catch (Exception $e) {
			Log::error($e);
			DB::rollback();

			return redirect()
				->back()
				->with('flash_error', 'Something went wrong, please try again later.');
		}

		return redirect()
			->back()
			->with('flash_success', 'Successfully increased amount of item to cart.');
	}

	protected function subtractToExisting($id) {
		$delete = false;
		try {
			DB::beginTransaction();

			$item = CartItem::find($id);

			if (($item->quantity - 1) == 0) {
				$item->delete();
				$delete = true;
			}
			else {
				$item->quantity -= 1;
				$item->save();
			}

			DB::commit();
		} catch (Exception $e) {
			Log::error($e);
			DB::rollback();

			return redirect()
				->back()
				->with('flash_error', 'Something went wrong, please try again later.');
		}

		if ($delete)
			return redirect()
				->back()
				->with('flash_message', 'Removed the item from cart.');

		return redirect()
			->back()
			->with('flash_success', 'Successfully decreased amount of item to cart.');
	}

	protected function delete($id) {
		$item = CartItem::find($id);
		$itemName = $item->product->product_name;

		try {
			DB::beginTransaction();
			
			$item->delete();

			DB::commit();
		} catch (Exception $e) {
			Log::error($e);
			DB::rollback();

			return redirect()
				->back()
				->with('flash_error', 'Something went wrong, please try again later.');
		}

		return redirect()
			->back()
			->with('flash_success', 'Successfully removed "' . $itemName . '"" from the cart.');
	}
}