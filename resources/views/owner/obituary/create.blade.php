@extends('template.admin')

@section('title', 'Obituary')
@section('sidebar', 'owner')

@section('body')
<!-- <h1 class="font-weight-bold"><a class="text-dark" href="javascript:void(0);" onclick="confirmLeave('{{route('owner.obituary.index')}}');"><i class="fas fa-chevron-left mr-2"></i>Obituary</a></h1> -->
<h1 class="font-weight-bold">@include('template.breadcrumbs', ['startIndex' => 2, 'confirmLeave' => true])</h1>

<hr class="hr-thick" style="border-color: #707070;">

<form class="form" action="{{ route('owner.obituary.store') }}" method="POST" enctype="multipart/form-data">
	{{ csrf_field() }}
	<div class="row">
		<div class="col-12 col-lg-8">
			<div class="row">
				<div class="form-group col-12">
					<label class="form-label font-weight-bold">Name</label>
					<input type="text" class="form-control" name="name" value="{{old('name')}}">
					<span class="badge badge-danger w-100 validation-message">{{ $errors->first('name') }}</span>
				</div>
			</div>

			<div class="row">
				<div class="form-group col-12 col-lg-6">
					<label class="form-label font-weight-bold">Date of Birth</label>
					<input type="date" class="form-control" name="birth_date" value="{{old('birth_date')}}">
					<span class="badge badge-danger w-100 validation-message">{{ $errors->first('birth_date') }}</span>
				</div>

				<div class="form-group col-12 col-lg-6">
					<label class="form-label font-weight-bold">Date of Death</label>
					<input type="date" class="form-control" name="death_date" value="{{old('death_date')}}">
					<span class="badge badge-danger w-100 validation-message">{{ $errors->first('death_date') }}</span>
				</div>
			</div>
		</div>

		<div class="form-group col-12 col-lg-4">
			<div class="d-flex flex-row">
				<div class="mx-auto text-center" style="overflow: hidden; max-width: 37.5%;">
					<img src="{{ asset('uploads/obituary/default.png') }}" class="cursor-pointer img-fluid" id="image" alt="Image">
				</div>

				<div class="mx-auto">
					<h6 class="form-label font-weight-bold" style="word-break: break-word" id="image_label">UPLOAD IMAGE</h6>
					<input type="file" name='image' class="hidden" accept=".jpg,.jpeg,.png" id="imgInput">
					<small class="text-muted pb-0 mb-0"><b>FORMATS ALLOWED:</b> JPEG, JPG, PNG</small><br>
					<small class="text-muted pt-0 mt-0"><b>MAX SIZE:</b> 5MB</small>
				</div>
			</div>
			<span class="badge badge-danger w-100 validation-message">{{ $errors->first('image') }}</span>
		</div>
	</div>

	<div class="row">
		<div class="form-group col-12">
			<label class="form-label font-weight-bold">In care of</label>
			<input type="text" class="form-control" name="store" value="{{Auth::user()->store->name}}" disabled>
			<span class="badge badge-danger w-100 validation-message">{{ $errors->first('store') }}</span>
		</div>
	</div>

	<div class="row">
		<div class="form-group col-12">
			<label class="form-label font-weight-bold">Description</label>
			<textarea class="form-control not-resizable" name="description" rows="5">{{old('description')}}</textarea>
			<span class="badge badge-danger w-100 validation-message">{{ $errors->first('description') }}</span>
		</div>
	</div>

	<label class="form-label font-weight-bold">Date of Posted Obituary</label>
	<div class="row">
		<div class="form-group col-12 col-lg-6 d-flex flex-d-col">
			<div class="d-flex flex-d-row w-100 align-items-center">
				<div class="input-group">
					<div class="input-group-prepend">
						<label class="form-label input-group-text font-weight-bold pr-3 my-0">From:</label>
					</div>
					<input type="date" class="form-control" name="obituary_duration_start" value="{{old('obituary_duration_start')}}">
				</div>
			</div>
			<span class="badge badge-danger w-100 validation-message">{{ $errors->first('obituary_duration_start') }}</span>
		</div>

		<div class="form-group col-12 col-lg-6 d-flex flex-d-col">
			<div class="d-flex flex-d-row w-100 align-items-center">
				<div class="input-group">
					<div class="input-group-prepend">
						<label class="form-label input-group-text font-weight-bold pr-3 my-0">To:</label>
					</div>
					<input type="date" class="form-control" name="obituary_duration_end" value="{{old('obituary_duration_end')}}">
				</div>
			</div>
			<span class="badge badge-danger w-100 validation-message">{{ $errors->first('obituary_duration_end') }}</span>
		</div>
	</div>

	<div class="row">
		<div class="form-group col-12 col-lg-6 d-flex flex-d-col">
			<div class="d-flex flex-d-row w-100 align-items-center">
				<div class="input-group">
					<div class="input-group-prepend">
						<label class="form-label input-group-text font-weight-bold pr-3 my-0">Requested By:</label>
					</div>
					<input type="email" class="form-control" name="customer_email" value="{{old('customer_email')}}">
				</div>
			</div>
			<span class="badge badge-danger w-100 validation-message">{{ $errors->first('customer_email') }}</span>
		</div>
	</div>

	<hr class="hr-thick" style="border-color: #707070;">

	<h3>Memorial Service Details</h3>

	<div class="row">
		<div class="form-group col-12 col-lg-6">
			<label class="form-label font-weight-bold">Start Time</label>
			<input type="time" class="form-control" name="begin_time" value="{{old('begin_time')}}">
			<span class="badge badge-danger w-100 validation-message">{{ $errors->first('begin_time') }}</span>
		</div>
	</div>

	<div class="row">
		<div class="form-group col-12 col-lg-6">
			<label class="form-label font-weight-bold">Location</label>
			<input type="text" class="form-control" name="location" value="{{old('location')}}">
		</div>

		<div class="form-group col-12 col-lg-6">
			<label class="form-label font-weight-bold">Address</label>
			<input type="text" class="form-control" name="address" value="{{old('address')}}">
		</div>
	</div>

	<div class="row">
		<div class="col-12">
			<button type="submit" class="btn btn-dark" data-action="submit">Save Changes</button>
		</div>
	</div>
</form>
@endsection

@section('script')
<script type="text/javascript">
	function openInput(obj) {
		$("[name=" + obj.attr("id") + "]").trigger("click");
	}

	function swapImgFile(obj) {
		if (obj.files && obj.files[0]) {
			let reader = new FileReader();

			reader.onload = function(e) {
				$("#image").attr("src", e.target.result);
				$("#image_label").html(obj.files[0].name);
			}

			reader.readAsDataURL(obj.files[0])
		}
		else {
			$("#image").attr("src", "/uploads/obituary/default.png");
			$("#image_label").html("UPLOAD IMAGE");
		}
	}

	$(document).ready(() => {
		// Triggering click on the image box
		$("#image").on("click", (e) => {
			openInput($(e.currentTarget));
		});
		// Applying changes when file input changes
		$("#imgInput").on('change', (e) => {
			swapImgFile(e.currentTarget);
		});
	});
</script>
@endsection