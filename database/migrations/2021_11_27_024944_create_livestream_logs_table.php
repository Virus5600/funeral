<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLivestreamLogsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('livestream_logs', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('livestream_id')->unsigned();
			$table->string('description');
			$table->time('time');
			$table->integer('type')->default(1);
			$table->timestamps();

			$table->foreign('livestream_id')->references('id')->on('livestream')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('livestream_logs');
	}
}