<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;

use MuxPhp\Configuration;
use MuxPhp\Api\LiveStreamsApi;
use MuxPhp\Api\AssetsApi;
use MuxPhp\Models\CreateAssetRequest;
use MuxPhp\Models\CreateLiveStreamRequest;

use Exception;
use Log;

class MuxController extends Controller {
	private $config;
	private $livestreamApi = null;
	private $assetApi = null;
	private $init = false;

	public function __construct() {
		$this->init();
	}

	private function init() {
		// Authenticate
		if (!$this->init) {
			$this->config = Configuration::getDefaultConfiguration()
				->setUsername(env('MUX_TOKEN'))
				->setPassword(env('MUX_SECRET'));

			$this->init = true;
		}

		// API Client Initialization
		$this->livestreamApi = new LiveStreamsApi(new \GuzzleHttp\Client(), $this->config);
		$this->assetApi = new AssetsApi(new \GuzzleHttp\Client(), $this->config);
	}

	////////////////////
	// LIVESTREAM API //
	////////////////////
	public function createNewLiveStream() {
		if ($this->livestreamApi == null)
			$this->init();

		$result = null;

		$request = new CreateLiveStreamRequest([
			'playback_policy' => 'public',
			'new_asset_settings' => new CreateAssetRequest(['playback_policy' => 'public'])
		]);

		try {
			$result = $this->livestreamApi->createLiveStream($request);
		} catch (Exception $e) {
			Log::info('Exception when calling LiveStreamsApi->createLiveStream: ' . $e->getMessage());
			Log::error($e);
		}

		return $result;
	}

	public function getLiveStream($id) {
		if ($this->livestreamApi == null)
			$this->init();

		$result = null;

		try {
			$result = $this->livestreamApi->getLiveStream($id);
		} catch (Exception $e) {
			Log::info('Exception when calling LiveStreamsApi->getLiveStream: ' . $e->getMessage());
			Log::error($e);
		}

		return $result;
	}

	public function deleteLiveStream($id) {
		if ($this->livestreamApi == null || $this->assetApi == null)
			$this->init();

		try {
			// Gets the lists of assets
			$assets = $this->assetApi->listAssets()['data'];
			// Iterate through every single one then if its livestream id matches the target stream id...
			foreach ($assets as $a) {
				if ($a['live_stream_id'] == $id) {
					// ...delete the asset.
					$this->assetApi->deleteAsset($a['id']);
				}
			}
			// Delete the livestream after clearing the assets of this livestream.
			$this->livestreamApi->deleteLiveStream($id);

			return true;
		} catch (Exception $e) {
			Log::info('Exception when calling LiveStreamsApi->deleteLiveStream: ' . $e->getMessage());
			Log::error($e);
			return false;
		}
	}

	public function liveStreamComplete($id) {
		if ($this->livestreamApi == null)
			$this->init();

		try {
			$result = $this->livestreamApi->signalLiveStreamComplete($id);
		} catch (Exception $e) {
			Log::info('Exception when calling LiveStreamsApi->signalLiveStreamComplete: ' . $e->getMessage());
			Log::error($e);
		}
	}

	////////////////
	// ASSETS API //
	////////////////
	public function getLastVideoPlayed($id) {
		if ($this->assetApi == null || $this->livestreamApi == null)
			$this->init();
		
		$result = null;

		try {
			// Gets the livestream details...
			$ls = $this->livestreamApi->getLiveStream($id);
			
			// If the livestream is active...
			if ($ls['data']['status'] == 'active' && $ls['data']['active_asset_id'] != null)
				return 'streaming';
			else if ($ls['data']['recent_asset_ids'] == null)
				return null;

			// If the active asset is null and the recent asset id is NOT null, get the latest recent asset used
			$assetId = $ls['data']['active_asset_id'] == null ? $ls['data']['recent_asset_ids'][count($ls['data']['recent_asset_ids'])-1] : $ls['data']['active_asset_id'];
			if ($assetId == null)
				return null;

			$result = $this->assetApi->getAsset($assetId);
		} catch (Exception $e) {
			Log::info('Exception when calling AssetsApi->getAsset: ' . $e->getMessage());
			Log::error($e);
			throw $e;
		}

		return $result;
	}
}