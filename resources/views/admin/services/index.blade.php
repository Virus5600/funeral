@extends('template.admin')

@section('title', 'Services')
@section('sidebar', 'admin')

@section('body')
<h1 class="h3 h1-lg font-weight-bold text-truncate">Services</h1>

<hr class="hr-thick" style="border-color: #707070;">

<div class="row mx-0 px-0">
	<div class="col-12 px-0">
		<div class="card border rounded dark-shadow h-100">
			<h3 class="card-header font-weight-bold text-custom">Service List</h3>
			<div class="card-body mx-0 pt-0 px-0 overflow-auto">
				{{-- TABLE --}}
				<table class="table table-striped">
					<thead>
						<tr class="font-weight-bold">
							<td>Service Name</td>
							<td></td>
						</tr>
					</thead>

					<tbody id='table-content'>
						@forelse ($services as $s)
						<tr>
							<td>{{$s->service_name}}</td>
							<td>
								<button type="button" class="btn btn-primary dropdown-toggle float-right py-0" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Action</button>

								<div class="dropdown-menu dropdown-menu-right">
									<a class="dropdown-item" href="{{ route('admin.services.show', [preg_replace('/ /', '_', strtolower($s->service_name))]) }}"><i class="far fa-eye mr-2"></i>View Details</a>
									<a class="dropdown-item" href="{{ route('admin.services.edit', [preg_replace('/ /', '_', strtolower($s->service_name))]) }}"><i class="fas fa-pencil-alt mr-2"></i>Edit Details</a>
									{{-- <a class="dropdown-item" href="{{ route('admin.services.delete', [$s->id]) }}"><i class="fas fa-trash mr-2"></i>Delete</a> --}}
								</div>
							</td>
						</tr>
						@empty
						<tr>
							<td colspan="3">No Avaiable Data in Table</td>
						</tr>
						@endforelse
					</tbody>
				</table>
				{{-- TABLE END --}}
			</div>
		</div>
	</div>
</div>
@endsection

@section('script')
<script type="text/javascript">
	$(document).ready(() => {
		@if (Session::get('create_error'))
		$("#addServiceTrigger").trigger('click');
		@endif
	});
</script>
@endsection