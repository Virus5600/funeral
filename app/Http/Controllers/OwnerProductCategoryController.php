<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Requests;

use App\Category;
use App\Product;
use App\StoreCategory;

use Validator;
use Log;
use DB;

class OwnerProductCategoryController extends Controller
{
	protected function index() {
		$prod = Product::where('store_id', '=', Auth::user()->store_id)->get();
		$p = array();
		foreach ($prod as $v)
			if (!in_array($v->subcategory->category->id, $p))
				array_push($p, $v->subcategory->category->id);

		$c = Category::whereIn('categories.id', $p);
		$search = null;

		if (request()->has('s')) {
			$search = request()->get('s');
			$c = $c->leftJoin('subcategories', 'subcategories.category_id', '=', 'categories.id')
				->leftJoin('products', 'products.subcategory_id', '=', 'subcategories.id');

			$c = $c->where('categories.category_name', '=', '%'.$search.'%')
				->orWhere('subcategories.subcategory_name', 'LIKE', '%'.$search.'%')
				->orWhere('products.product_name', 'LIKE', '%'.$search.'%')
				->select('categories.*')
				->distinct();
		}

		return view('owner.category.index', [
			'search' => $search,
			'categories' => $c->paginate(15)
		]);
	}

	protected function create() {
		return view('owner.category.subcategory.product.create', [
			'isFrom' => 'category',
			'categories' => Category::get(),
			'subcategories' => Category::first()->subcategory
		]);
	}

	protected function store(Request $req) {
		$validator = Validator::make($req->all(), [
			'category_name' => 'required|min:2|unique:categories,category_name'
		], [
			'category_name.required' => 'Category name is required.',
			'category_name.min' => 'Category name is too short.',
		]);

		if ($validator->fails())
			return redirect()
				->back()
				->withErrors($validator)
				->withInput()
				->with('create_error', true)
				->with('form-target', 'create_new');

		try {
			DB::beginTransaction();

			Category::create([
				'category_name' => $req->category_name
			]);

			DB::commit();
		} catch (\Exception $e) {
			Log::error($e);
			DB::rollback();

			return redirect()
				->back()
				->withInput()
				->with('create_error', true)
				->with('form-target', 'create_new')
				->with('flash_error', 'Something went wrong, please try again later.');
		}

		return redirect()
			->route('owner.category.index')
			->with('flash_success', 'Successfully added "' . $req->category_name . '".');
	}

	protected function update(Request $req, $id) {
		$validator = Validator::make($req->all(), [
			'category_name' => 'required|min:2|unique:categories,category_name'
		], [
			'category_name.required' => 'Category name is required.',
			'category_name.min' => 'Category name is too short.',
		]);

		if ($validator->fails())
			return redirect()
				->back()
				->withErrors($validator)
				->withInput()
				->with('update_error', true)
				->with('form-target', 'editCategory'.$id);

		try {
			DB::beginTransaction();

			$category = Category::find($id);
			$categoryName = $category->category_name;

			$category->category_name = $req->category_name;
			$category->save();

			DB::commit();
		} catch (\Exception $e) {
			Log::error($e);
			DB::rollback();

			return redirect()
				->back()
				->withInput()
				->with('flash_error', 'Something went wrong, please try again later.');
		}
		return redirect()
			->route('owner.category.index')
			->with('flash_success', 'Successfully updated "' . $categoryName . '" to "' . $req->category_name . '".');
	}

	protected function delete($id) {
		try {
			DB::beginTransaction();

			$category = Category::find($id);
			$categoryName = $category->category_name;

			$category->delete();

			DB::commit();
		} catch (\Exception $e) {
			Log::error($e);
			DB::rollback();

			return redirect()
				->back()
				->with('flash_error', 'Something went wrong, please try again later.');
		}

		return redirect()
			->route('owner.category.index')
			->with('flash_success', 'Successfully removed "' . $categoryName . '".');
	}
}