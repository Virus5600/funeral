@extends('template.client')

@section('title', 'Store')

@section('page-css')
<link rel="stylesheet" href="/css/store.product.index.css" >
@endsection

@section('body')
{{-- PRODUCT BANNER --}}
<section class="p-5" id="banner">
	<div class="container pt-md-5 mt-5">
		<div class="row d-sm-flex justify-content-between">
			<div class ="col-xl-11 col-md-8 col-sm-12 p-5" id="bannertext">
				<h1 class="category">{{strtoupper($category_name)}}</h1>
				<h1 class="category mx-5" id="cat-outline">{{strtoupper($category_name)}}</h1>
			</div>
		</div>
	</div>
</section>


<!-- Items -->
<section>
	<div class="container my-3">
		<div class="row">
			<small>{{$store->name}} / {{preg_replace('/_/', ' ', ucwords($category_name))}}</small>
			<div class="col col-lg-3 col-sm-12 mx-2 my-3">
				<div class="bg-dark rounded">
					<div class="list mx-2 mt-3 mb-5 p-4">
						<h3>Categories</h3>
						<a href="{{ route('products.index', [strtolower(preg_replace('/ /i', '_', $store->name))]) }}" class="text-decoration-none"><h5>All</h5></a>
						
						@foreach ($store->categories() as $c)
						<h5 data-bs-toggle="collapse" href="#{{preg_replace('/ /', '_', $c->category_name)}}" role="button" aria-expanded="false">{{$c->category_name}}</h5>
						<div class="collapse multi-collapse" id="{{preg_replace('/ /', '_', $c->category_name)}}">
							<ul>
								<li><a href="{{ route('products.index', [strtolower(preg_replace('/ /i', '_', $store->name)), strtolower(preg_replace('/ /i', '_', $c->category_name))]) }}" class="text-light text-decoration-none">All</a></li>
								@foreach ($c->subcategory as $s)
								<li><a href="{{ route('products.index', [strtolower(preg_replace('/ /i', '_', $store->name)), strtolower(preg_replace('/ /i', '_', $c->category_name)), strtolower(preg_replace('/ /i', '_', $s->subcategory_name))]) }}" class="text-light text-decoration-none">{{$s->subcategory_name}}</a></li>
								@endforeach
							</ul>  
						</div>
						@endforeach
					</div>
				</div>
			</div>

			<div class="col-lg-8 col-sm-12 mx-md-2 mt-3 mb-5">
				<h1>
					{{ucwords($category_name)}}
					<hr id="itemhr">
				</h1> 

				<div class="container my-3">
					<div class="row">
						{{-- PRODUCT ITEMS --}}
						@forelse ($products as $p)
						@if ($p->inventory == 0)
						@php
						continue;
						@endphp
						@else
						<div class="col-md-6 col-12 px-2 mt-3">
							<div class="card card-body text-center my-2 p-0 bg-light rounded h-100">
								<div style="position: relative;">
									<img src="/uploads/products/{{$p->category->category_name}}/{{$p->image_name}}" alt="HTML Logo" class="card-img-top img-fluid">
								
									<div class="card-img-overlay h-100 justify-content-center align-items-center">
										<a href="{{ route('cart.addToCart', [$p->id]) }}" class="btn btn-outline-light">Add to Cart</a>
									</div>
								</div>

								<a href="{{ route('products.show', [preg_replace('/ /', '_', strtolower($store->name)), $category_name, $p->id]) }}" class="card-text text-decoration-none mt-4 d-flex flex-d-col justify-content-end h-100">
									<h4>{{$p->product_name}}</h4>
									<h5 class="my-2">Regular Price: &#8369;{{number_format($p->selling_price, 2)}}</h5>
									<!-- <h5 class="my-2"><span class="gold">Special Price: </span> P24,600.00</h5> -->
								</a>
							</div>
						</div>
						@endif
						@empty
						<span class="mx-auto">Sorry! There's no products in here.</span>
						@endforelse
					</div>

					<div class="row mt-5">
						<div class="col">
							<div class="float-right d-flex flex-d-row bg-dark">
								@if ($products->previousPageUrl() == null)
								<span class="border border-end-0 mr-0 p-2" style="border-top-left-radius: .25rem; border-bottom-left-radius: .25rem; border-right: solid #fff;"><i class="text-muted fas fa-chevron-left mx-2"></i></span>
								@else
								<a href="{{$products->previousPageUrl()}}" class="text-white border border-end-0 mr-0 p-2 hover-fx-pagination" style="border-top-left-radius: .25rem; border-bottom-left-radius: .25rem; border-right: solid #fff;"><i class="fas fa-chevron-left mx-2 cursor-pointer"></i></a>
								@endif
								
								<span class="text-white border border-start-0 border-end-0 mx-0 p-2">Total Results {{($products->currentPage()-1) * $products->perPage() + 1}}-{{(($products->currentPage()-1) * $products->perPage()) + $products->count()}} of {{$products->total()}}</span>
								
								@if ($products->nextPageUrl() == null)
								<span class="border border-start-0 ml-0 p-2" style="border-top-right-radius: .25rem; border-bottom-right-radius: .25rem;"><i class="text-muted fas fa-chevron-right mx-2"></i></span>
								@else
								<a href="{{$products->nextPageUrl()}}" class="text-white border border-start-0 ml-0 p-2 hover-fx-pagination" style="border-top-right-radius: .25rem; border-bottom-right-radius: .25rem;"><i class="fas fa-chevron-right mx-2 cursor-pointer"></i></a>
								@endif
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="p-2">
	<div class="container">
		<div class="row my-5">
			<h2 class="font-weight-bold">Book Our Services</h2>
		</div>

		<div class="container" id="bookserv">
			<div class="row">
				<div class="serv col-md-6 card my-2" id="funeralserv">
					<div style="position: relative;">
						<img src="/images/banners/services/funeral.png" alt="" class="img-fluid">
						
						<div class="card-img-overlay justify-content-center align-items-center">
							<a href="{{ route('services.index') }}#funeral" class="btn btn-outline-light">Learn More</a>
						</div>
					</div>
				</div>

				<div class="serv col-md-6 card my-2" id="memoserv">
					<div style="position: relative;">
						<img src="/images/banners/services/memory.png" alt="" class="img-fluid">
						
						<div class="card-img-overlay justify-content-center align-items-center">
							<a href="{{ route('services.index') }}#memorial" class="btn btn-outline-light">Learn More</a>
						</div>
					</div>
				</div>

				<div class="serv col-md-6 card my-2" id="cremserv">
					<div style="position: relative;">
						<img src="/images/banners/services/cremation.png" alt="" class="img-fluid">
						
						<div class="card-img-overlay justify-content-center align-items-center">
							<a href="{{ route('services.index') }}#cremation" class="btn btn-outline-light">Learn More</a>
						</div>
					</div>
				</div>

				<div class="serv col-md-6 card my-2" id="burialserv"> 
					<div style="position: relative;">
						<img src="/images/banners/services/burial.png" alt="" class="img-fluid">
						
						<div class="card-img-overlay justify-content-center align-items-center">
							<a href="{{ route('services.index') }}#burial" class="btn btn-outline-light">Learn More</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- Obituary -->
<section class="py-5" id="obituary">
	<div class="container">
		<div class="row align-items-center">
			<h1 class="mt-5"><img src="/images/UI/line.svg" class="imgline mr-2">Obituary</h1>
			<h5>Let's pray for their departed souls.</h5>
		</div>

		<div class="container py-5">
			@forelse($obituary as $o)
			<div class="obituarycard card mb-3">
				<div class="row">
					<div class="fillimg col-md-3">
						<img src="/uploads/obituary/{{$o->image_name}}" class="card-img" alt="...">
					</div>

					<div class="col-md-7 mt-1">
						<div class="card-body">
							<h1 class="obname card-title">{{$o->name}}</h1>
							<h2 class="card-text"><small class="text-muted">{{\Carbon\Carbon::parse($o->birth_date)->format('F d, Y')}} – {{\Carbon\Carbon::parse($o->death_date)->format('F d, Y')}}</small></h2>
						</div>
					</div>

					<div class="col-md-2">
						<img src="/images/UI/bookmark.svg" class="mark card-img" alt="...">
					</div>
				</div>
			</div>
			@empty
			<h1>No Obituaries</h1>
			@endforelse
		</div>
	</div>
</section>
@endsection

@section('script')
<script>
	$(document).ready(() => {
		if ('{{$category_name}}' != 'all')
			$('[href="#{{$category_name}}"]').trigger('click');
	});
</script>
@endsection