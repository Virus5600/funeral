<?php

use Illuminate\Database\Seeder;

use App\Subcategory;

class SubcategoriesTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Subcategory::create([
			'subcategory_name' => 'Infant Casket',
			'category_id' => 1
		]);

		Subcategory::create([
			'subcategory_name' => 'Cardboard Casket',
			'category_id' => 1
		]);

		Subcategory::create([
			'subcategory_name' => 'Cremation Casket',
			'category_id' => 1
		]);

		Subcategory::create([
			'subcategory_name' => 'MDF Casket',
			'category_id' => 1
		]);

		Subcategory::create([
			'subcategory_name' => 'Metal Caskets',
			'category_id' => 1
		]);

		Subcategory::create([
			'subcategory_name' => 'Wood Casket',
			'category_id' => 1
		]);

		Subcategory::create([
			'subcategory_name' => 'MDF Coffins',
			'category_id' => 2
		]);

		Subcategory::create([
			'subcategory_name' => 'Wood Coffins',
			'category_id' => 2
		]);

		Subcategory::create([
			'subcategory_name' => 'Wicker Coffins',
			'category_id' => 2
		]);

		Subcategory::create([
			'subcategory_name' => 'Bamboo Urn',
			'category_id' => 3
		]);

		Subcategory::create([
			'subcategory_name' => 'MDF Urns',
			'category_id' => 3
		]);

		Subcategory::create([
			'subcategory_name' => 'Ceramic Urn',
			'category_id' => 3
		]);

		Subcategory::create([
			'subcategory_name' => 'Wood Urns',
			'category_id' => 3
		]);

		Subcategory::create([
			'subcategory_name' => 'Funeral Standing Sprays',
			'category_id' => 4
		]);

		Subcategory::create([
			'subcategory_name' => 'Funeral Flowers',
			'category_id' => 4
		]);

		Subcategory::create([
			'subcategory_name' => 'Funeral Floor Baskets',
			'category_id' => 4
		]);

		Subcategory::create([
			'subcategory_name' => 'Funeral Flower Baskets',
			'category_id' => 4
		]);

		Subcategory::create([
			'subcategory_name' => 'Funeral Plants',
			'category_id' => 4
		]);

		Subcategory::create([
			'subcategory_name' => 'Funeral Wreaths',
			'category_id' => 4
		]);

		Subcategory::create([
			'subcategory_name' => 'Creamation Wreaths',
			'category_id' => 4
		]);

		Subcategory::create([
			'subcategory_name' => 'Casket Sprays',
			'category_id' => 4
		]);
	}
}