<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalesOrderItems extends Model
{
	protected $table ="sales_order_items";
	protected $fillable = [
		'customer_id',
		'order_id',
		'product_id',
		'quantity',
		'price',
	];

	protected function salesOrder() {
		return $this->belongsTo('App\SalesOrderItems', 'order_id');
	}

	protected function product() {
		return $this->belongsTo('App\Product');
	}
}