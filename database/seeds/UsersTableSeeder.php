<?php

use Illuminate\Database\Seeder;

use App\User;

class UsersTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		// Admin
		$this->create(
			'admin',
			'',
			'',
			\Carbon\Carbon::parse('06-05-2000'),
			'Others',
			'admin@admin.com',
			'admin',
			'admin',
		);

		$this->create(
			'Store',
			'Owner',
			'',
			\Carbon\Carbon::parse('06-05-2000'),
			'Others',
			'store_owner@ec.com',
			'owner',
			'owner',
			1
		);

		$this->create(
			'Sample',
			'User',
			'',
			\Carbon\Carbon::parse('06-05-2000'),
			'Others',
			'sample@client.com',
			'client'
		);
	}

	private function create($firstName, $lastName, $contact, $birthday, $gender, $email, $password, $type="client", $store=null, $is_validated=true) {
		User::create([
			'first_name' => $firstName,
			'last_name' => $lastName,
			'contact_number' => $contact,
			'birthday' => $birthday,
			'gender' => $gender,
			'email' => $email,
			'password' => Hash::make($password),
			'type' => $type,
			'store_id' => $store,
			'is_validated' => $is_validated
		]);	
	}
}