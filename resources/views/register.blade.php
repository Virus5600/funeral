@extends('template.register')

@section('title', 'Register')

@section('body')
<div class="container mb-5 d-flex justify-content-center">
    <h1>Create <span class="highlighted">Account</span></h1>
</div>

{{-- FORM --}}
<form action="{{ route('register-account') }}" method="POST" class="container-fluid" enctype="multipart/form-data">
	{{ csrf_field() }}
	<div class="row">
		<div class="col-12 mx-auto">
			<div class="row">
				<div class="col-lg-9 col-12 p-md-5 p-4 rounded mx-auto" id="register_form">
					<div class="row mb-3">
						<div class="form-group mt-1 col-lg-6">
							<label class="form-label important">First Name</label>
							<input type="text" class="form-control" name="first_name" value="{{ old('first_name') }}">
							<span class="badge badge-danger w-100 validation-message">{{$errors->first('first_name')}}</span>
						</div>

						<div class="form-group mt-1 col-lg-6">
							<label class="form-label important">Last Name</label>
							<input type="text" class="form-control" name="last_name" value="{{ old('last_name') }}">
							<span class="badge badge-danger w-100 validation-message">{{$errors->first('last_name')}}</span>
						</div>
					</div>
					
					<div class="row mb-2">
						<div class="form-group mt-1 mb-1">
							<label class="form-label important">Email</label>
							<input class="form-control" type="email" name="email" value="{{ old('email') }}">
							<span class="badge badge-danger w-100 validation-message">{{$errors->first('email')}}</span>
						</div>
					</div>
					
					<div class="row mb-2">
						<div class="form-group mt-1 col-12 col-lg-4">
							<label class="form-label important">Contact Number</label>
							<input type="text" class="form-control" name="contact_number" value="{{ old('contact_number') }}" data-mask>
							<span class="badge badge-danger w-100 validation-message">{{$errors->first('contact_number')}}</span>
						</div>

						<div class="form-group mt-1 col-12 col-lg-4">
							<label class="form-label important">Birthday</label>
							<input type="date" class="form-control" name="birthday" value="{{ old('birthday') }}">
							<span class="badge badge-danger w-100 validation-message">{{$errors->first('birthday')}}</span>
						</div>

						<div class="form-group mt-1 col-12 col-lg-4">
							<label class="form-label">Gender</label>
							<select class="custom-select" name="gender">
								@if (old('gender') == 'NULL' || old('gender') == null)
								<option class="text-dark" value="NULL" {{ old('gender') == "NULL" || old('gender') == null ? 'selected' : '' }} disabled id="toRemove">-- Select Gender --</option>
								@endif
								<option class="text-dark" value="Male" {{ old('gender') == "Male" ? 'selected' : '' }}>Male</option>
								<option class="text-dark" value="Female" {{ old('gender') == "Female" ? 'selected' : '' }}>Female</option>
								<option class="text-dark" value="Others" {{ old('gender') == "Others" ? 'selected' : '' }}>Others</option>
							</select>
							<span class="badge badge-danger w-100 validation-message">{{$errors->first('gender')}}</span>
						</div>
					</div>
					
					<div class="row mt-4 mb-3">
						<div class="form-group mt-2 col-lg-6">
							<label class="form-label important">Password</label>
							<input type="password" class="form-control" name="password">
							<span class="badge badge-danger w-100 validation-message">{{$errors->first('password')}}</span>
						</div>
						<div class="form-group mt-2 col-lg-6">
							<label class="form-label important">Repeat Password</label>
							<input type="password" class="form-control" name="repeat_password">
							<span class="badge badge-danger w-100 validation-message">{{$errors->first('repeat_password')}}</span>
						</div>
					</div>
					
					<p>Note: Password should be minimum of 8 characters, with at least one capitalized letter, a number and a special character.</p>
				</div>
			</div>

			<div class="row mt-2">
				<div class="col-lg-9 col-12 mx-auto">
					<div class="form-group">
						<div class="form-check">
							<input class="form-check-input" type="checkbox" name="subscribe" id="subscribe" {{ old('subscribe') ? 'checked' : '' }}>
							<label class="form-check-label" for="subscribe">Yes, I want to Subscribe to Soulace's Newsletter.</label>
						</div>
						
						<div class="form-check mt-2 mb-3">
							<input class="form-check-input" type="checkbox" name="tos" id="tos" {{ old('tos') ? 'checked' : '' }}>
							<label class="form-check-label" for="tos">I agree to the <span class="highlighted">Terms and Conditions</span> and <span class="highlighted">Privacy Policy.</span></label>
							<span class="badge badge-danger w-100 validation-message">{!!$errors->first('tos')!!}</span>
						</div>
					</div>

					<div class="form-group">
						<input type="submit" class="btn-lg btn-light mb-2" data-action="submit" value="Create Account"/>
						<p>Already have an account? <a href="/login" class="font-weight-bold text-white">Log In</a></p>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>
@endsection

@section('script')
<script type="text/javascript">
	$(document).ready(() => {
		$("[data-mask]").inputmask('mask', {
			'mask' : "+63 999 999 9999",
			'removeMaskOnSubmit' : true,
			'autoUnmask':true
		});

		$('#toRemove').parent().on('change', (e) => {
			$(e.currentTarget).children('#toRemove').remove();
		});

		// Change submit to either "Updating" or "Submitting" after click
		$('[type=submit], [data-action]').click(function(e) {
			let action = $(e.currentTarget).attr('data-action');

			if ($(e.currentTarget).attr('data-clicked') == 'true') {
				e.preventDefault()
			}
			else {
				if (action == 'submit')
					$(e.currentTarget).html(`<div class="spinner-border spinner-border-sm text-light" role="status"><span class="sr-only"></span></div> Submitting...`);
				else if (action == 'update')
					$(e.currentTarget).html(`<div class="spinner-border spinner-border-sm text-light" role="status"><span class="sr-only"></span></div> Updating...`);
			}

			$(e.currentTarget).addClass(`disabled cursor-default`);
		});
	});
</script>
@endsection