<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LivestreamLog extends Model
{
	protected $fillable = [
		'livestream_id',
		'description',
		'time',
		'type',
	];

	protected function livestream() {
		return $this->belongsTo('App\Livestream', 'livestream_id');
	}

	public function getProofImage() {
		return preg_replace('/(Submitted proof \w+\. )\{image: (.+)\}/i', '$2', $this->description);
	}

	public function getDescription() {
		return preg_replace('/(Submitted proof \w+\. )\{image: (.+)\}/i', '$1', $this->description);
	}

	public function isAccepted() {
		return preg_replace('/(Submitted proof )(\w+)\. \{image: (.+)\}/i', '$2', $this->description) == 'accepted';
	}

	public function isRejected() {
		return preg_replace('/(Submitted proof )(\w+)\. \{image: (.+)\}/i', '$2', $this->description) == 'rejected';
	}
}